<?php
use Illuminate\Support\Facades\Mail;
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| FRONT -END PAGES
|--------------------------------------------------------------------------
*/

Route::get('contact','PagesController@contact');
Route::get('gallery','PagesController@gallery');
Route::get('services','PagesController@services');
Route::get('destinations','PagesController@destination');
Route::get('destinations/{id}/package','PagesController@single');
Route::get('destinations/{name}/package/book','PagesController@book');
Route::post('destinations/add/booking','PagesController@add_booking');
Route::resource('/','PagesController');


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::get('/login1994', function () {
    return view('login');
});
Route::resource('/authenticate', 'AuthenticateController');
Route::post('/authenticate', 'AuthenticateController@authenticate');
Route::get('/logout', 'LogoutController@index');

/**
|===================
|=ADMIN CONTROLLER=
|===================
**/
/*Route::resource('admin/user','UsersController');*/
Route::get('admin/dashboard','AdminController@index');
Route::resource('admin/users','AdminController');

Route::get('admin/packages/destroy/{id}', 'AdminPackagesController@destroy');
Route::resource('admin/packages', 'AdminPackagesController');

Route::resource('admin/packages', 'AdminPackagesController');

Route::get('admin/booking/destroy/{id}', 'AdminBookingsController@destroy');
Route::resource('admin/booking', 'AdminBookingsController');

Route::get('admin/event-calendar/json', 'AdminCalendarsController@calendar_json');
Route::get('admin/event-calendar/{id}/destroy', 'AdminCalendarsController@destroy');
Route::resource('admin/event-calendar', 'AdminCalendarsController');
/**
|===================
|=Gallery CONTROLLER=
|===================
*/
Route::get('admin/gallery/destroy/{id}','AdminGalleriesController@destroy');
Route::resource('admin/gallery','AdminGalleriesController');
/**
|===================
|=Gallery CONTROLLER=
|===================
*/
Route::get('admin/gallery/name/destroy/{id}','AdminGallerytermsController@destroy');

Route::post('request/email','PagesController@e_inquery');

/*Route::group(['middleware' => ['web']], function () {
    
    // Authentication routes...
    Route::get('auth/login', 'Auth\AuthController@getLogin');
    Route::post('auth/login', 'Auth\AuthController@postLogin');
    Route::get('auth/logout', 'Auth\AuthController@getLogout');
    Route::get('package','PagesController@package');
    /*
    |=============================
    |===========Dashboard=========
    |=============================	
    Route::get('dashboard','UsersController@index');
	Route::get('request/email','PagesController@e_inquery');
	

    Route::get('mail', function() {

    	Mail::send('front.test', ['name' => 'Novica'], function( $message )
    	{
    		$message->to('jiggerstraveltours@gmail.com')->from('peterjude.gargaritano@gmail.com')->subject('Welcome!aaaaa');
    	});

    });

    Route::controllers([
        'auth' => 'Auth\AuthController',
        'password' => 'Auth\PasswordController'
    ]);
});*/
