<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Input;
use App\User;
use Crypt;
use Redirect;

class AuthenticateController extends Controller
{
    public function __construct()
    {
        $this->params = [
            'error'                 => false, 
            'status_code'           => 200,
            'msg'                   => '', 
            'is_logged'             => false,
       ];
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $password = Input::get('password');
        $email = Input::get('email');

        $user = User::where('email', strtolower($email))->first();

        
        if( !$user || $user->password == null || $user->password == '' ) {

            $this->params['msg'] = 'Username and password not match.';
            $this->params['error'] = true;
            $this->params['forced_login'] = false;
            return Redirect::to('/login1994')->withErrors( $this->params['msg'] );

        }
        
        
        $decrypted_password = Crypt::decrypt( $user->password );

        if( $password !== $decrypted_password ) {

            $this->params['msg'] = 'Username and password not match.';
            $this->params['error'] = true;
            $this->params['forced_login'] = false;
            return Redirect::to('/login1994')->withErrors( $this->params['msg'] );

            //return response()->json( $this->params );
        }

        Auth::login($user);

        if ( !Auth::check() ) {

             return Redirect::to('/login1994');

        } else {
            $this->params['msg'] = 'Successfully logged in.';
            $this->params['is_logged'] = true;
            $this->params['error'] = false;
            $this->params['forced_login'] = false;
            $this->params['user'] = $user;
            
            if( $user->user_type == "admin" ) {
                return Redirect::to('/admin/dashboard');
            }
        }

        return Redirect::to('/login1994')->withErrors( $this->params );
    }
}