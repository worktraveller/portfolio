<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Http\Requests;
use Response;
use Redirect;
use Session;
use Input;
use Crypt;
use Image;
use App\Booking;

class AdminBookingsController extends Controller
{
    //
    public function __construct()
    {
        
        $this->params = [
                'error'         => false, 
                'msg'           => '',
                'form_errors'   => null,
                'results'       => [],
                'is_logged'     => true,
                'forced_login'  => false,
                'module_name'   =>'booking'
            ];

        if( !Auth::check() ) return Redirect::to('/login1994')->send();

        $this->folder = 'admin';
        $this->user = Auth::user();
    }

	public function index()
    {

        $this->params['bookings'] = Booking::orderBy('created_at','desc')->get();
        $this->params['count'] = Booking::where('status','1')->count();
        
		return View( $this->folder.'.booking.index', $this->params );
	}

    public function show( $id )
    {
        //change status to 0 = read
        $booking = Booking::findOrFail( $id );
        
        if( $booking->status == 1 ) {
            $booking->status = 0;
            $booking->save();
        }

        $this->params['booking'] = Booking::findOrFail( $id );
        $this->params['count'] = Booking::where('status','1')->count();

        return View( $this->folder.'.booking.view', $this->params );
    }

    public function update( $id )
    {
      
    }

    public function destroy($id)
    {

        $booking = Booking::find($id);
        if( !$booking ) {
            $this->params['msg'] = 'Booking not found or already been deleted.';
            $this->params['error'] = true; 
            return response()->json( $this->params );
        }
        $booking->delete();

        $this->params['msg'] = 'Booking has been successfully deleted.';
        $this->params['error'] = false; 
        
        return response()->json( $this->params );
    }

}
