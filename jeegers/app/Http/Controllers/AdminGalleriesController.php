<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Http\Requests;
use Response;
use Redirect;
use Input;
use Crypt;
use DB;
use Image;
use App\Package;
use App\Gallery;
use App\Galleryterm;

class AdminGalleriesController extends Controller
{
    //
    public function __construct()
    {
        
        $this->params = [
                'error'         => false, 
                'msg'           => '',
                'form_errors'   => null,
                'results'       => [],
                'is_logged'     => true,
                'forced_login'  => false,
                'module_name'   =>'gallery'
            ];

        if( !Auth::check() ) return Redirect::to('/login1994')->send();
        $this->user = Auth::user();
        $this->folder = $this->user->user_type;
        
    }

	public function index()
    {

        $this->params['galleries'] = Gallery::all();
        $this->params['galleriesname'] = Galleryterm::all();

		return View( $this->folder.'.gallery.index', $this->params );

	}

    public function create()
    {

        return View( $this->folder.'.gallery.create', $this->params );
    }

    public function store()
    {

        $gallery_name = Input::get('gallery_name');
        $gallery_id = Input::get('gallery_id');

        if( $gallery_name ) {
            $rules = [
                'gallery_name'   => 'required|min:2|max:100',
            ];
        

            $validator = Validator::make(Input::all(), $rules);

            if( $validator->fails() ) {

                $messages = $validator->messages()->getMessages();
                $this->params['error'] = true;
                $this->params['msg'] = 'Form validation error. Please fix.';
                $this->params['form_errors'] = $messages;
                return Redirect::to('/admin/gallery/create')->withInput()->withErrors($messages);
            }
        } else {
            $gallery = Galleryterm::findOrFail( $gallery_id );
            $gallery_name = $gallery->name;
        }
        

        $files = Input::file('filesToUpload');
        $files_count = count($files);
        $filename = [];
        $x = 0;

        // uploaded image

        foreach ( $files as $file ) {

            if( $file ) {                
                
                $ext = strtolower( pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION) );

                if( !in_array( $ext, array('jpg', 'jpeg', 'png') )  ) {

                  $this->params['error'] = true;
                  $this->params['msg'] = 'File extension is not valid.';
                  $this->params['status'] = 'error';

                  return Redirect::to('/admin/gallery/create')->with( $this->params );
                }

                $img = Image::make($file->getRealPath());
                $imageid = time().'_'.md5( $this->user->id.rand(0,100) ).'_'.rand(0,100);
                $filename[] = $imageid.'.'.$ext;

                $path = public_path('uploads/'.$gallery_name.'/');
                if( !File::exists($path) ){
                     $result = File::makeDirectory($path, 0775, true);
                }

                $target = $path.$filename[$x];

                // Check if file extension is valid
                

                 $img_path = implode(',', $filename);

                // Now save the iamge file
                $success = $img->save( $target );
                $x++;
            }else{

                 $img_path = "";

            }

        }

        
        if( $gallery_id ) {
        
           $term_id = $gallery_id; 
        
        } else{

            $term = new Galleryterm();
            $term->name = Input::get('gallery_name');
            $term->save();
            $term_id = $term->id; 
        }


        foreach ($filename as $img_name) {

            $gallery = new Gallery();
            $gallery->galleryterm_id = $term_id; 
            $gallery->img_path = $img_name; 
            $gallery->user_id = $this->user->id; 
            $gallery->save(); 

        }
        

        $this->params['msg'] = 'gallery has been successfully created.';
        $this->params['status'] = 'success';
        
        return Redirect::to('/admin/gallery/')->with( $this->params );   

    }

    public function destroy( $id )
    {
        $gallery = Gallery::find($id);
        if( !$gallery ) {
            $this->params['msg'] = 'Package not found or already been deleted.';
            $this->params['error'] = true; 
            return response()->json( $this->params );
        }
        File::delete(public_path('uploads/'.$gallery->galleryterm->name.'/'.$gallery->img_path));
        $gallery->delete();

        $this->params['msg'] = 'Package has been successfully deleted.';
        $this->params['error'] = false; 
        
        return response()->json( $this->params );
    }

}
