<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Http\Requests;
use Response;
use Redirect;
use Session;
use Input;
use Crypt;
use DB;
use Image;
use App\Package;
use App\Category;

class AdminPackagesController extends Controller
{
    //
    public function __construct()
    {
        
        $this->params = [
                'error'         => false, 
                'msg'           => '',
                'form_errors'   => null,
                'results'       => [],
                'is_logged'     => true,
                'forced_login'  => false,
                'module_name'   =>'manage_ads'
            ];

        if( !Auth::check() ) return Redirect::to('/login1994')->send();

        $this->folder = 'admin';
        $this->user = Auth::user();
    }

	public function index()
    {

        $this->params['packages'] = Package::all();
		return View( $this->folder.'.packages.index', $this->params );
	}

    public function create()
    {
        $this->params['categories'] = Category::all();
        return View( $this->folder.'.packages.create', $this->params );
    }

    public function store( )
    {     
        $rules = [
            'destination'   => 'required|min:5|max:100',
            'no_of_days'    => 'required|min:2|max:20',
            'price'         => 'required|numeric',
            'itinerary'     => '',
            'promo_price'   => 'numeric',
            'inclusion.*'     => 'required'
        ];

        //check if category is exist
        $cat = Category::where('id',Input::get('category'))->first();
        $validator = Validator::make( Input::all(), $rules);
        
        if( $validator->fails() ) {

            $messages = $validator->messages()->getMessages();
            $this->params['error'] = true;
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            return Redirect::to('/admin/packages/create')->withInput()->withErrors($messages);
        }
        // uploaded image
            $files = Input::file('filesToUpload');
            $files_count = count($files);
            $filename = [];
            $x = 0;

            foreach ($files as $file) {
                if( $file ){
                    $path = public_path('uploads/'.$cat->name.'/');
                    if( !File::exists($path) ){
                         $result = File::makeDirectory($path, 0775, true);
                    }
                    $destination_path = $path;
                    $img = Image::make($file->getRealPath());
                    $imageid = time().'_'.md5( $this->user->id.rand(0,100) ).'_'.rand(0,100);
                    $ext = strtolower( pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION) );
                    $filename[] = $imageid.'.'.$ext;

                    $target = $destination_path.$filename[$x];

                    // Check if file extension is valid
                    if( !in_array( $ext, array('jpg', 'jpeg', 'png') )  ) {
                      $this->params['error'] = true;
                      $this->params['msg'] = 'File extension is not valid.';
                      return Redirect::to('/admin/packages/create')->with( $this->params );
                    }

                     $img_path = implode(',', $filename);

                    // Now save the iamge file
                    $success = $img->save( $target );
                    $x++;
                }else{
                     $img_path = "";
                }
            }
 
            $packages = new Package();
            $packages->destination = Input::get('destination');   
            $packages->no_of_days = Input::get('no_of_days');   
            $packages->description = Input::get('description');   
            $packages->price = Input::get('price');   
            $packages->promo_price = Input::get('promo_price');   
            $packages->itinerary = Input::get('itinerary');   
            $packages->inclusions = serialize(Input::get('inclusion'));  
            $packages->img_path =  $img_path; 
            $packages->category_id = Input::get('category');  
            $packages->user_id = $this->user->id;  
            $packages->save(); 

            $this->params['msg'] = 'destination has been successfully udpated.';
            $this->params['status'] = 'success';
            
            return Redirect::to('/admin/packages/')->with( $this->params );   
                
    }

    public function show( $id )
    {
        $this->params['package'] = Package::findOrFail($id);
        return View( $this->folder.'.packages.edit',$this->params );
    }

    public function edit( $id )
    {
        $this->params['categories'] = Category::all();
        $this->params['package'] = Package::findOrFail($id);
        return View( $this->folder.'.packages.edit',$this->params );
    }
    public function update( $id )
    {
       $package = Package::findOrFail( $id );
       $cat = Category::where('id',$package->category_id)->first();

        $rules = [
            'destination'   => 'required|min:5|max:100',
            'no_of_days'    => 'required|min:2|max:20',
            'price'         => 'required|numeric',
            'promo_price'   => 'numeric',
            'description'   => 'required|min:2',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails() ) {

            $messages = $validator->messages()->getMessages();
            $this->params['error'] = true;
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            return Redirect::to('/admin/packages/'.$id.'/edit')->withInput()->withErrors($messages);
        }

        $files = Input::file('filesToUpload');
        $files_count = count($files);
        $filename = [];
        $x = 0;

            foreach ($files as $file) {
                if( $file ){
                    $destination_path = public_path('uploads/'.$cat->name.'/');
                    $img = Image::make($file->getRealPath());
                    $imageid = time().'_'.md5( $this->user->id.rand(0,100) ).'_'.rand(0,100);
                    $ext = strtolower( pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION) );
                    $filename[] = $imageid.'.'.$ext;

                    $target = $destination_path.$filename[$x];

                    // Check if file extension is valid
                    if( !in_array( $ext, array('jpg', 'jpeg', 'png') )  ) {
                      $this->params['error'] = true;
                      $this->params['msg'] = 'File extension is not valid.';
                      return Redirect::to('/admin/packages/'.$id.'/edit')->with( $this->params );
                    }

                    // Now save the iamge file
                    $success = $img->save( $target );
                    $x++;
                }
            }
        if( $filename ){

        $img_path = implode(',',$filename );

       }else{

        $img_path = $package->img_path;

       }

        $package->destination = Input::get('destination');   
        $package->no_of_days = Input::get('no_of_days');   
        $package->description = Input::get('description');   
        $package->price = Input::get('price');   
        $package->promo_price = Input::get('promo_price');   
        $package->itinerary = Input::get('itinerary');   
        $package->inclusions = serialize(Input::get('inclusion'));  
        $package->img_path = $img_path;  
        $package->category_id = Input::get('category');  
        $package->user_id = $this->user->id;  
        $package->save(); 

        $this->params['msg'] = 'destination has been successfully updated.';
        $this->params['status'] = 'success';
        
        return Redirect::to('/admin/packages/')->with( $this->params );
        
    }

    public function destroy($id)
    {
        $package = Package::find($id);
        if( !$package ) {
            $this->params['msg'] = 'Package not found or already been deleted.';
            $this->params['error'] = true; 
            return response()->json( $this->params );
        }
        $package->delete();

        $this->params['msg'] = 'Package has been successfully deleted.';
        $this->params['error'] = false; 
        
        return response()->json( $this->params );
    }

}
