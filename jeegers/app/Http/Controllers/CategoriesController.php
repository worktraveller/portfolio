<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Validator;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Response;
use Redirect;
use Session;
use Input;
use Crypt;
use App\Package;
use App\Category;

class CategoriesController extends Controller
{
    //
    public function __construct()
    {
        
        $this->params = [
                'error'         => false, 
                'msg'           => '',
                'form_errors'   => null,
                'results'       => [],
                'is_logged'     => true,
                'forced_login'  => false,
                'module_name'   =>'manage_ads'
            ];

        if( !Auth::check() ) return Redirect::to('/login1994')->send();

        $this->folder = 'categories';
        $this->user = Auth::user();
    }

	public function index()
    {
        
        $this->params['packages'] = Package::all();
		return View( $this->folder.'.packages.index', $this->params );

	}

    public function create()
    {
        $this->params['categories'] = Category::all();
        return View( $this->folder.'.packages.create', $this->params );
    }

    public function store()
    {

        foreach (Input::get('itinerary') as $key => $number) {
            $rules['itinerary'] = 'required|min:2|max:10';
        }
        foreach (Input::get('inclusions') as $key => $number) {
            $rules['inclusions'] = 'required|min:2|max:10';
        }

        $rules = [
            'destination'   => 'required|min:5|max:100',
            'no_of_days'    => 'required|min:2|max:20',
            'price'         => 'required|numeric',
            'promo_price'   => 'numeric',
            'description'   => 'required|min:2',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails() ) {
            $messages = $validator->messages()->getMessages();
            //dd( $messages );
            $this->params['error'] = true;
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            
            return Redirect::to('/admin/packages/create')->withInput()->withErrors($messages);
        }
        
        $packages = new Package();
        $packages->destination = Input::get('destination');   
        $packages->no_of_days = Input::get('no_of_days');   
        $packages->description = Input::get('description');   
        $packages->price = Input::get('price');   
        $packages->promo_price = Input::get('promo_price');   
        $packages->itinerary = serialize(Input::get('itinerary'));   
        $packages->inclusions = serialize(Input::get('inclusions'));  
        $packages->user_id = $this->user->id;  
        $packages->save(); 

        $this->params['msg'] = 'destination has been successfully udpated.';
        $this->params['status'] = 'success';
        // Redirect to user lists
        return Redirect::to('/admin/packages/')->with( $this->params );
    }

}
