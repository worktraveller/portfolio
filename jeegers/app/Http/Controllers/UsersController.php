<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Auth;
use Input;
use Crypt;
use App\User;

class UsersController extends Controller
{
    //
    public function __construct(){

        $this->params = array(
            'error' => false,
            'redirect' => false,
            'msg' => ''
        );
    }

	public function index()
    {
		return View('admin.index');

	}

    public function create()
    {

        return View('admin.create');
    }

    public function store()
    {

        $user = new User();
        $user->firstname = 'Peter Jude';
        $user->lastname = 'Gargaritano';
        $user->contactno = '123456';
        $user->email = 'juphet@admin.com';
        $user->password = Crypt::encrypt('admin');
        $user->user_type = 'admin';
        $user->save();
    }
}
