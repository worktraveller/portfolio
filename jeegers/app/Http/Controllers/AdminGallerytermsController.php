<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Http\Requests;
use Response;
use Redirect;
use Input;
use Crypt;
use DB;
use Image;
use App\Package;
use App\Gallery;
use App\Galleryterm;

class AdminGallerytermsController extends Controller
{
    //
    public function __construct()
    {
        
        $this->params = [
                'error'         => false, 
                'msg'           => '',
                'form_errors'   => null,
                'results'       => [],
                'is_logged'     => true,
                'forced_login'  => false,
                'module_name'   =>'gallery'
            ];

        if( !Auth::check() ) return Redirect::to('/login1994')->send();
        $this->user = Auth::user();
        $this->folder = $this->user->user_type;
        
    }

	public function index()
    {


	}

    public function create()
    {

        return View( $this->folder.'.gallery.create', $this->params );
    }

    public function store()
    {  

    }

    public function destroy( $id )
    {
        $galleryterm = Galleryterm::find($id);

        if( !$galleryterm ) {
            $this->params['msg'] = 'Package not found or already been deleted.';
            $this->params['error'] = true; 
            return response()->json( $this->params );
        }

        $galleryterm->delete();

        File::deleteDirectory(public_path('uploads/'.$galleryterm->name));
        Gallery::where('galleryterm_id', $id)->delete();
        
        $this->params['msg'] = 'Package has been successfully deleted.';
        $this->params['error'] = false; 
        
        return response()->json( $this->params );
    }

}
