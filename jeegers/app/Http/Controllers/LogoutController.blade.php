<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Input;
use App\User;
use Crypt;
use Redirect;

class LogoutController extends Controller
{
    public function __construct()
    {
        $this->params = [
            'error'                 => false, 
            'status_code'           => 200,
            'msg'                   => '', 
            'is_logged'             => false,
       ];
    }
    
    public function index()
    {
    	Auth::logout();
        return Redirect::to('/login1194');

    }
}
?>