<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Validator;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Redirect;
use Session;
use Input;
use Crypt;
use App\User;
use App\Booking;
use App\Classes\Tracker;

class AdminController extends Controller
{
    //
    public function __construct()
    {
        //Tracker::hit();
        $this->params = [
                'error'         => false, 
                'msg'           => '',
                'form_errors'   => null,
                'results'       => [],
                'is_logged'     => true,
                'forced_login'  => false,
                'module_name'   =>'manage_ads'
            ];

        if( !Auth::check() ) return Redirect::to('/login1994')->send();

        $this->folder = 'admin';
    }

	public function index()
    {
        $this->params['count'] = Booking::where('status','1')->count();
		return View( $this->folder.'.index', $this->params );

	}

    public function create()
    {

        return View('admin.create');
    }

    public function store()
    {

        $user = new User();
        $user->username = 'admin';
        $user->firstname = 'John';
        $user->lastname = 'Doe';
        $user->phone = '123456';
        $user->email = 'admin@gmail.com';
        $user->password = Crypt::encrypt('admin');
        $user->save();
    }
}
