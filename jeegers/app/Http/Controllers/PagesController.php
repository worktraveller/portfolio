<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use URL;
use Mail;
use Crypt;
use Input;
use Response;
use Redirect;
use Request;
use Eloquent;
use Validator;
use App\Package;
use App\Category;
use App\Gallery;
use App\Galleryterm;
use App\Booking;


class PagesController extends Controller
{
	public function __construct() 
    {
		$this->params = array(
			'error'         => false,
            'redirect'      => false,
            'redirect_to'   => '/',
            'form_errors'   => null,
            'page_title'    => 'Jeeger\'s Travel' 
		);

        $this->folder = 'front';
	}
/*
|-----------------------------
| Homepage 
|-----------------------------
*/
    public function index() 
    {


        $this->params['page_title'] = 'home';
        $this->params['packages'] = Package::all(); 
        $this->params['categories'] = Category::all(); 
      	return view( $this->folder.'.index',$this->params );
   	}

    public function services()
    {
        $this->params['page_title'] = 'services';
        return view( $this->folder.'.services', $this->params );
    } 

    public function gallery()
    {
        $this->params['page_title'] = 'gallery';
        $this->params['galleries'] = Gallery::all();
        $this->params['terms'] = Galleryterm::all();
        return view( $this->folder.'.gallery', $this->params );
    }

    public function destination()
    {

        $this->params['page_title'] = 'destinations';
        $this->params['packages'] = Package::all(); 
        $this->params['categories'] = Category::all(); 
        return view( $this->folder.'.destination', $this->params );
    }

    public function single( $id )
    {  
        $this->params['page_title'] = 'destinations';
        $this->params['package'] = Package::findOrFail($id);
        //dd($this->params['package']);

        return view( $this->folder.'.single', $this->params );
    }

    public function package() {

        return view( $this->folder.'.package', $this->params );
    }

    public function contact() {

        $this->params['page_title'] = 'contact-us';
        return view( $this->folder.'.contact', $this->params );
    }

    public function book( $id ){
        $this->params['page_title'] = 'Book';
        $this->params['package'] = Package::findOrFail( $id );

        return view( $this->folder.'.book', $this->params );
    }

    public function add_booking(){
        
        $id = Input::get('package_id');
        $package = Package::findOrFail($id);
        $rules = [
            'name'                  => 'required|min:5|max:100',
            'no_of_participants'    => 'required|min:1',
            'email'                 => 'required|min:5|max:50',
            'phone'                 => 'required|digits_between:5,15|integer',
            'date_of_travel'        => 'required|date|date_format:Y-m-d|after:yesterday',
            'help'                  => 'required',
        ];

        $validator = Validator::make( Input::all(), $rules);
        
        if( $validator->fails() ) {

            $messages = $validator->messages()->getMessages();
            $this->params['error'] = true;
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            //dd($messages);
            return Redirect::to('/destinations/'.$id.'/package/book')->withInput()->withErrors($messages);
        }
        
        $book = new Booking;
        $book->name = Input::get('name');
        $book->email = Input::get('email');
        $book->no_of_participants = Input::get('no_of_participants');
        $book->phone = Input::get('phone');
        $book->date_of_travel = Input::get('date_of_travel');
        $book->inquiry = Input::get('help');
        $book->package_id = Input::get('package_id');
        $book->save();

        $this->params['msg'] = 'Booked successfully.';
        $this->params['status'] = 'success';
        return Redirect::to('destinations/'.$id.'/package/book')->with( $this->params );
    }

    //search function

    public function search( $query, $keyword )
    {
        if( $keyword ) {

            $query->where(function ($query) use ( $keyword ){
                 $query->where("name", "LIKE","%$keyword%");
            });
        }
    }

/*
|-------------------------------
|Front-End Email Inquiry
|-------------------------------
 */
    public function e_inquery() 
    {

        $rules = array(
                'firstname' => 'required|min:5|max:80', 
                'lastname'  => 'required|min:5|max:80', 
                'email'     => 'required|min:5|max:80', 
                'phone'     => 'required|digits_between:5,15|integer', 
                'subject'   => 'required|min:5|max: 255', 
                'help'      => 'required|min:5', 
            );

        $validator = Validator::make( Input::all(), $rules );

        if( $validator->fails() ) {

            $messages = $validator->messages()->getMessages();

            $this->params['error'] = true;
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            dd($messages);
            return Redirect::to('/')->withInput()->withErrors($messages);

        } else {
            $firstname  = Input::get('firstname');
            $lastname   = Input::get('lastname');
            $email      = Input::get('email');
            $phone      = Input::get('phone');
            $subject    = Input::get('subject');
            $help       = Input::get('help');

            Mail::send('front.test',['content' => $help, 'phone' => $phone], function( $message ) use ( $firstname, $lastname, $email, $subject ) {
                $message->from($email, $firstname.' '.$lastname);
                $message->to('jiggerstraveltours@gmail.com');
                $message->subject($subject);
            });

           if( count( Mail::failures() ) > 0 ) {
                $errors = 'Failed to send password reset email, please try again.';
            
            } else {
              
              return Redirect::to('/')->with('msg','Your inquery was sent')->send();
            }
        }

    }
}
