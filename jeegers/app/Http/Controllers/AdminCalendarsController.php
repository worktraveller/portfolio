<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Http\Requests;
use Response;
use Redirect;
use Session;
use Input;
use Crypt;
use Image;
use App\Calendar;

class AdminCalendarsController extends Controller
{
    //
    public function __construct()
    {
        
        $this->params = [
                'error'         => false, 
                'msg'           => '',
                'form_errors'   => null,
                'results'       => [],
                'is_logged'     => true,
                'forced_login'  => false,
                'module_name'   =>'calendar'
            ];

        if( !Auth::check() ) return Redirect::to('/login1994')->send();

        $this->folder = 'admin';
        $this->user = Auth::user();
    }

	public function index()
    {

        $this->params['events'] = Calendar::all();
        
		return View( $this->folder.'.calendar.index', $this->params );
	}

    public function create()
    {

        return View( $this->folder.'.calendar.create' );
    }

    public function store()
    {
        $rules = array(
            'destination'   => 'required|min:2|max:50',
            'name'          => 'required|min:2|max:50',
            'date_start'    => 'required|date|date_format:Y-m-d|after:yesterday',
            'date_end'      => 'required|date|date_format:Y-m-d|after:date_start'
        );

        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails() ) {
            $messages = $validator->messages()->getMessages();

            $this->params['error'] = true;
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            return Redirect::to('/admin/event-calendar/create')->withInput()->withErrors($messages);
        }

        $event_cal = new Calendar;
        $event_cal->destination = Input::get('destination');
        $event_cal->name        = Input::get('name');
        $event_cal->date_start  = Input::get('date_start');
        $event_cal->date_end  = Input::get('date_end');
        $event_cal->save();

        $this->params['msg'] = 'Event has been successfully added.';
        $this->params['status'] = 'success'; 

        return Redirect::to('/admin/event-calendar/')->with($this->params);

    }

    public function show( $id )
    {
       
    }

    public function edit( $id )
    {
        
        $this->params['event'] = Calendar::findOrFail( $id );
        return View( $this->folder.'.calendar.edit', $this->params );       
    }

    public function update( $id )
    {

    }

    // JSON for Calendarfull.js

    public function calendar_json()
    {
        $json_event = Calendar::all();

        $array_event = [];
        foreach ($json_event as $event ) {
            $e = array(
                'title' => "Destination: ".$event->destination."\n MR: ".$event->name, 
                'start' => $event->date_start, 
                'end' => date('Y-m-d',strtotime($event->date_end . '+1 days')),
                'allDay' => true,
                'url' => url('admin/event-calendar/'.$event->id.'/edit'),
            );  
            array_push($array_event,$e);
        }

        return response()->json( $array_event );
    }

    public function destroy($id)
    {

        $booking = Calendar::findOrFail($id);
        if( !$booking ) {
            $this->params['msg'] = 'Event not found or already been deleted.';
            $this->params['status'] = 'error'; 
            return Redirect::to('/admin/event-calendar/')->with($this->params);
        }
        $booking->delete();

        $this->params['msg'] = 'Event has been successfully deleted.';
        $this->params['status'] = 'success'; 

        return Redirect::to('/admin/event-calendar/')->with($this->params);
    }

}
