<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galleryterm extends Model
{
    //
    public function gallery()
    {
        return $this->belongsTo('App\Gallery');
    }
}
