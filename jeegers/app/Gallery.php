<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    
    public function galleryterm()
    {
        return $this->hasOne('App\Galleryterm', 'id', 'galleryterm_id' );
    }
}
