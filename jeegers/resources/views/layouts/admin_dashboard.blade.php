<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Jeeger's Travel | {{ isset($page_title) ? $page_title:' Jigger\'s Travel' }}</title>

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

    <!-- Toastr style -->
    <link rel="stylesheet" href="{{ asset('fonts/fontawesome/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('css/plugins/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/plugins/switchery/switchery.css') }}">
    <link rel="stylesheet" href="{{ asset('css/plugins/icheck/custom.css') }}">

    <!-- Gritter -->
    <link rel="stylesheet" href="{{ asset('js/plugins/gritter/jquery.gritter.css') }}">
    
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin-style.css') }}">
    @yield('styles')
</head>

<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> 
                            <span class="text-center upload" data-url="{{ url('s_admin/profpic') }}">   
                                    <img alt="image" class="img-circle center-block" src="" width="50"/>
    
                            </span>

                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear text-center"> <span class="block m-t-xs"> 
                                <strong class="font-bold">aa</strong>
                            </span> 
                            <span class="text-muted text-xs block text-center">Propietor</span> </span> </a>
                        </div>
                        <div class="logo-element">
                            IN+
                        </div>
                    </li>

                    <li class="{{ (isset($module_name) && $module_name == 'dashboard') ? 'active':'' }}">
                        <a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> <span class="nav-label">Dashboards</span></a>
                    </li>

                    <li class="{{ (isset($module_name) && $module_name == 'super_admin') ? 'active':'' }}">
                        <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Users</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{ url('/admin/users') }}">Users Lists</a></li>
                            <li><a href="{{ url('/admin/users/create') }}">Register New User</a></li>
                        </ul>
                    </li>

                    <li class="">
                        <a href="mailbox.html"><i class="fa fa-paper-plane"></i> <span class="nav-label">Packages</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{ url('/admin/packages') }}">Destinations Lists</a></li>
                            <li><a href="{{ url('/admin/packages/create') }}">Add Destinations</a></li>
                        </ul>
                    </li>

                    <li class="{{ (isset($module_name) && $module_name == 'gallery') ? 'active':'' }}">
                        <a href="#"><i class="fa fa-picture-o"></i> <span class="nav-label">Gallery</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{ url('/admin/gallery') }}">Gallery Lists</a></li>
                            <li><a href="{{ url('/admin/gallery/create') }}">Add Gallery</a></li>
                        </ul>
                    </li>
                    <li class="{{ (isset($module_name) && $module_name == 'booking') ? 'active':'' }}">
                        <a href="{{ url('admin/booking') }}"><i class="fa fa-inbox "></i> <span class="nav-label">Bookings</span></a>
                    </li>
                    
                    <li class="{{ (isset($module_name) && $module_name == 'calendar') ? 'active':'' }}">
                        <a href="{{ url('admin/event-calendar') }}"><i class="fa fa-calendar"></i> <span class="nav-label">Events Calendar</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{ url('admin/event-calendar') }}">Calendar</a></li>
                            <li><a href="{{ url('admin/event-calendar/create') }}">Add Calendar</a></li>
                        </ul>
                    </li>

                    <li class="{{ (isset($module_name) && $module_name == 'logout') ? 'active':'' }}">
                        <a href="{{ url('/logout') }}"><i class="fa fa-power-off"></i> <span class="nav-label">Logout</span></a>
                    </li>
                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="http://webapplayers.com/inspinia_admin-v2.0/search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li class="">
                    <a class="dropdown-toggle count-info" href="#">
                        <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
                    </a>
                </li>


                <li>
                    <a href="/logout">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>

		@yield('content')
                    
    </div>

    <!-- Mainly scripts -->
    <script src="{{ asset('js/jquery-2.1.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('js/my_jquery.js') }}"></script>

    <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('js/inspinia.js') }}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>

    <!-- Flot -->
    <script src="{{ asset('js/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.pie.js') }}"></script>

    <!-- Peity -->
    <script src="{{ asset('js/plugins/peity/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('js/demo/peity-demo.js') }}"></script>

    <!-- jQuery UI >
    <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js') }}"></script-->

    <!-- GITTER -->
    <script src="{{ asset('js/plugins/gritter/jquery.gritter.min.js') }}"></script>
    <script src="{{ asset('js/plugins/switchery/switchery.js') }}"></script>

    <!-- Sparkline -->
    <script src="{{ asset('js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>

    <!-- Sparkline demo data  -->
    <script src="{{ asset('js/demo/sparkline-demo.js') }}"></script>

    <!-- ChartJS-->
    <script src="{{ asset('js/plugins/chartJs/Chart.min.js') }}"></script>
    <script src="{{ asset('js/plugins/summernote/summernote.min.js') }}"></script>

    <!-- Toastr -->
    <script src="{{ asset('js/plugins/toastr/toastr.min.js') }}"></script>
    <!-- add yield-->
    @yield('scripts')
    <script type="text/javascript">

        $(document).ready(function() {

            $('.summernote').summernote({
                height:300,
            });

            $('.gallery').mouseover(function(){
                $(this).find('.gallery-delete').show();
            });
            $('.gallery').mouseout(function(){
                $(this).find('.gallery-delete').hide();
            });


            $('.add-inclusion').click(function(){
                $('.package-inclusion').append('<div class="hr-line-dashed"></div><input type="input" name="inclusion[]" class="form-control"/>');
            });

            //get the input and UL list
            var input = document.getElementById('filesToUpload');
            var list = document.getElementById('fileList');           

            //var counter = 0;
            $('#filesToUpload').on('change', function(e){

                if( $('#fileList li').length > 0 ){
                    $('#fileList li').remove();
                }
                              
                for (var x = 0; x < input.files.length; x++) {

                    var reader = new FileReader();
                    var image_holder = $('#fileList');

                    reader.onload = function (e) {
                        var li = "<li><img src='"+ e.target.result +"' class='img-responsive' /></li>";
                         image_holder.append(li);
                     }

                     image_holder.show();
                     reader.readAsDataURL($(this)[0].files[x]);

                  }  
            });
        });


    </script>

    @if ( !session('error') && session('msg') )
        <script type="text/javascript">
        $(document).ready(function() {
           setTimeout(function() {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        timeOut: 4000
                    };
                    toastr.{{ session("status") }}('{{ session("msg") }}');

                }, 500);
        });
        </script>
    @endif

    @if ( $errors->count() > 0 )
        @foreach( $errors->all() as $message )
            <script type="text/javascript">
            $(document).ready(function() {
               setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            progressBar: true,
                            showMethod: 'slideDown',
                            timeOut: 4000
                        };
                        toastr.error('{{ $message }}');

                    }, 500);
            });
            </script>
         @endforeach
    @endif    
</body>
</html>
