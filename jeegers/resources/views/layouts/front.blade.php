@include('includes.header')
@include('includes.menu')
<div class="wrapper">
    @include('includes.slider')
    @yield('content')
    @include('includes.contact')
    @include('includes.footer')
</div>
    