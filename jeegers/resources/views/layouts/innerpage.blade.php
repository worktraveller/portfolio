    @include('includes.header')
    @include('includes.menu')
    <div class="wrapper">
    @yield('content')
    @include('includes.footer')
    </div>