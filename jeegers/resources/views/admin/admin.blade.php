@extends('layouts.admin_dashboard')
@section('content')
<div class="row  border-bottom dashboard-header">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>New Entry</h5>

                </div>
                <div class="ibox-content">
                    {!! Form::open(array('url' => 'signup')) !!}
                       <!--
                        |================================
                        |=========Personal Details=======
                        |================================
                        -->
                        <div class="form-group">
                            <span class="label label-primary">Hotel Owner Details</span>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="control-label">Owner Name</label>
                                {!! Form::text('fullname', null, array('class'=>'form-control', 'placeholder'=>'Full name')) !!}
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label">Email ID</label>
                                {!! Form::email('email', null, array('class'=>'form-control', 'placeholder'=>'Email Address')) !!}
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label">Phone / Mobile no</label>
                                {!! Form::text('mobile', null, array('class'=>'form-control', 'placeholder'=>'Phone / Mobile')) !!}
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label">Username:</label>
                                {!! Form::text('username', null, array('class'=>'form-control', 'placeholder'=>'Username')) !!}
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <!--
                        |================================
                        |=========Package Details========
                        |================================
                        -->
                        <div class="form-group">
                            <span class="label label-primary">Hotel Details</span>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row">
                            <div class="col-sm-6">
                                    <img src="http://placehold.it/250x250?text=Hotel+Logo" class="center-block">
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Hotel Name</label>
                                    {!! Form::text('hotelname', null, array('class'=>'form-control', 'placeholder'=>'Hotel Name')) !!}
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Currency</label>
                                    {!! Form::select('currency', array('usd' => 'USD', 'rupee' => 'RUPEE','gbp' => 'GBP', 'jyen'=> 'JAPANIES YEN', 'candollar' => 'CANADIAN DOLLAR', 'php' => 'PHILIPPINE PESO' ), null, array('class' => 'form-control m-b')) !!}
                                </div> 
                                <div class="form-group">
                                    <label class="control-label">Max Room no</label>
                                    {!! Form::text('max_hotel_room', null, array('class'=>'form-control', 'placeholder'=>'Max Hotel Room')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <!--
                        |================================
                        |=========Address Details========
                        |================================
                        -->
                        <div class="form-group">
                            <span class="label label-primary">Address Details</span>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="control-label">Street</label>
                                {!! Form::text('street', null, array('class'=>'form-control', 'placeholder'=>'Street')) !!}
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label">City</label>
                                {!! Form::text('city', null, array('class'=>'form-control', 'placeholder'=>'City')) !!}
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label">State</label>
                                {!! Form::text('state', null, array('class'=>'form-control', 'placeholder'=>'state')) !!}
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-sm-4">
                                <label class="control-label">Country</label>
                                {!! Form::text('country', null, array('class'=>'form-control', 'placeholder'=>'Country')) !!}
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label">Postal / Zip Code</label>
                                {!! Form::text('postal', null, array('class'=>'form-control', 'placeholder'=>'Postal / Zip')) !!}
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <button class="btn btn-primary pull-right" type="submit">Submit</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection('content')
