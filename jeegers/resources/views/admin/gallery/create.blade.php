@extends('layouts.admin_dashboard')
@section('content')
    <div class="row  border-bottom dashboard-header">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Upload Gallery</h5>

                </div>
                <div class="ibox-content">
 
                    {!! Form::open(array('url' => '/admin/gallery/','class'=>'dropzone','id'=>'', 'files'=>true )) !!}

                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                   <input type="text" name="gallery_name" class="form-control" placeholder="Gallery Name" value="{{ Input::old('gallery_name') }}" />
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                   <input type="file" name="filesToUpload[]" id="filesToUpload" multiple="" class="form-control"/>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12" id="gallery">
                                <ul id="fileList">
                                </ul>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                   <input type="submit" class="btn" value="Save" />
                                </div>
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection('content')
