@extends('layouts.admin_dashboard')
@section('content')
<div class="row  border-bottom dashboard-header">
    @foreach( $galleriesname as $galleryname )
        <div class="lists-item galleries">
            <div class="ibox-title">
                <h2 class="no-margins text-uppercase"> {{ $galleryname->name }}
                <a href="#" class="btn btn-info text-center add-image" data-toggle="modal" data-target="#modal"
                    data-id="{{ $galleryname->id }}"
                    >
                    <i class="fa fa-plus"></i>
                </a>
                <a href="#" class="delete-this-item btn btn-danger text-center"
                            data-action="{{ url('admin/gallery/name/destroy/'.$galleryname->id) }}"
                            data-id="{{ $galleryname->id }}" 
                            data-confirmation-msg="Are you sure you want to delete {{ $galleryname->id }}?"
                    >
                <i class="fa fa-trash"></i>
                </a>
                </h2>
            </div>
            <div class="ibox-content">
                <div class="row">

                    @foreach( $galleries as $gallery )
                        @if($gallery->galleryterm_id == $galleryname->id )  
                            <div class="col-sm-4 col-xs-12 text-center gallery lists-item">

                                @if( !$gallery->img_path )
                                    <img src="{{ asset('uploads/palawan/1470997912_3c7781a36bcd6cf08c11a970fbe0e2a6_37.jpg') }}" class="m-b-md" alt="profile" width="125" height="125">                     
                                @else
                                    <img src="{{ asset('uploads/'.$gallery->galleryterm->name.'/'.$gallery->img_path) }}" class="img-responsive" alt="profile">                     
                                @endif
                                <a href="#" class="delete-this-item btn btn-danger btn-circle btn-lg vertical-center gallery-delete text-center"
                                        data-action="{{ url('admin/gallery/destroy/'.$gallery->id) }}"
                                        data-id="{{ $gallery->id }}" 
                                        data-confirmation-msg="Are you sure you want to delete {{ $gallery->img_path }}?"
                                >
                                <i class="fa fa-trash fa-6"></i>
                                </a>
                            </div>  
                        @endif
                    @endforeach

                </div>
            </div>
        </div>
     @endforeach
</div>
@endsection('content')

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" id="modal">
  <div class="modal-dialog" role="document">
    {!! Form::open(array('url' => '/admin/gallery/','class'=>'dropzone','id'=>'', 'files'=>true )) !!}
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><i class="fa fa-plus"></i> Add Image</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            
            <input type="hidden" name="gallery_id" value="" id="gallery_id" class="form-control"/>

            <div class="col-md-12 col-sm-12">
                <div class="form-group">
                   <input type="file" name="filesToUpload[]" id="filesToUpload" multiple="" class="form-control"/>
                </div>
            </div>
            <div class="col-md-12 col-sm-12" id="gallery">
                <ul id="fileList">
                </ul>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
    {!! Form::close() !!}
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->