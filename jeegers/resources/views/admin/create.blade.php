<!DOCTYPE html>
<html>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.0/login_two_columns.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Apr 2015 10:53:24 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>D2GAPP | Login </title>

    <link rel="stylesheet" href="{{ asset('admin/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts/font-awesome/css/font-awesome.css') }}">

    <link rel="stylesheet" href="{{ asset('admin/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">

</head>

<body class="gray-bg">

    <div class="loginColumns animated fadeInDown">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                @if ( $errors->count() > 0 )
                    <div class="form_errors alert alert-danger">
                            @foreach( $errors->all() as $message )
                              {{ $message }}
                            @endforeach
                    </div>
                @endif
                <div class="ibox-content">
                    {!! Form::open(['url' => '/admin/user', 'method' => 'post', 'class' => 'aaa']) !!}
                        <div class="form-group">
                            <input type="email" name = "email" class="form-control" placeholder="Username" required="">
                        </div>
                        <div class="form-group">
                            <input type="password" name = "password" class="form-control" placeholder="Password" required="">
                        </div>
                        <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.0/login_two_columns.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Apr 2015 10:53:24 GMT -->
</html>
