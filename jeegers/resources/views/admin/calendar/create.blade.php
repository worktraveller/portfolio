@extends('layouts.admin_dashboard')
@section('content')
<div class="row  border-bottom dashboard-header">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Add Destination</h5>
            </div>
            <div class="ibox-content">
                {!! Form::open(['url' => '/admin/event-calendar', 'id'=>'create_event_calendar', 'method' => 'post', 'class' => 'form-horizontal']) !!}
                    <div class="form-group"><label class="col-sm-2 control-label">Destination</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="destination">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <div class='input-group date' id='time_live_from'>
                                <input type="text" class="form-control" name="date_start" value="{{ Input::old('time_live_start') }}" placeholder="date from">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class='input-group date' id='time_live_from'>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                <input type="text" class="form-control" name="date_end" value="{{ Input::old('time_live_start') }}" placeholder="date to">
                            </div>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="reset">Cancel</button>
                            <button class="btn btn-primary" type="submit">Save changes</button>
                        </div>
                    </div>
                {!! Form::close() !!}   
            </div>
        </div>
    </div>
</div>
@endsection('content')
    
@section('styles')

@endsection('styles')

@section('scripts')
<script src="{{ asset('js/moment.js') }}"></script>
<script src="{{ asset('js/plugins/datapicker/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.date').datetimepicker({
            format: "YYYY-MM-DD",
        });
    });      
</script>
    
@endsection('scripts')
