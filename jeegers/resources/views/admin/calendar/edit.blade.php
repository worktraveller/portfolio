@extends('layouts.admin_dashboard')
@section('content')
<div class="row  border-bottom dashboard-header">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Edit {{ $event->destination }}</h5>
            </div>
            <div class="ibox-content">
                <form method="get" class="form-horizontal">
                    <div class="form-group"><label class="col-sm-2 control-label">Destination</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="destination" value="{{ $event->destination }}">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" value="{{ $event->name }}">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <div class='input-group date' id='date_start'>
                                <input type="text" class="form-control" name="date_start" value="{{ $event->date_start }}" placeholder="date start">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class='input-group date' id='date_end'>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                <input type="text" class="form-control" name="date_end" value="{{ $event->date_end }}" placeholder="date end">
                            </div>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <a href="{{ url('admin/event-calendar/'.$event->id.'/destroy') }}" class="delete-this-item btn btn-danger">
                                Delete <i class="fa fa-trash"></i>
                            </a>
                            <button class="btn btn-primary" type="submit">Save changes <i class="fa fa-floppy-o"></i></button>
                        </div>
                    </div>
                </form>   
            </div>
        </div>
    </div>
</div>
@endsection('content')
    
@section('styles')

@endsection('styles')

@section('scripts')
<script src="{{ asset('js/moment.js') }}"></script>
<script src="{{ asset('js/plugins/datapicker/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.date').datetimepicker({
            format: "YYYY-MM-DD",
        });
    });      
</script>
    
@endsection('scripts')
