@extends('layouts.admin_dashboard')
@section('content')
<div class="row  border-bottom dashboard-header">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Striped Table </h5>
            </div>
            <div class="ibox-content">
                <div id="calendar"></div>
            </div>
        </div>
    </div>
</div>
@endsection('content')
    
@section('styles')

    <link href="{{ url('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ url('css/plugins/fullcalendar/fullcalendar.css') }}" rel="stylesheet">
@endsection('styles')

@section('scripts')
    <script src="{{ url('js/plugins/fullcalendar/moment.min.js') }}"></script>
    <script src="{{ url('js/jquery-ui.custom.min.js') }}"></script>
    <script src="{{ url('js/plugins/fullcalendar/fullcalendar.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {

        /* initialize the external events
         -----------------------------------------------------------------*/


        $('#external-events div.external-event').each(function() {

            // store data so the calendar knows to render an event upon drop
            $(this).data('event', {
                title: $.trim($(this).text()), // use the element's text as the event title
                stick: true // maintain when user navigates (see docs on the renderEvent method)
            });

            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 1111999,
                revert: true,      // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });

        });



        /* initialize the calendar
         -----------------------------------------------------------------*/
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            //external url include
            eventSources: 'event-calendar/json',
        })
    });
    </script>
@endsection('scripts')
