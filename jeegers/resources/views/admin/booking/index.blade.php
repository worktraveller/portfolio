@extends('layouts.admin_dashboard')
@section('content')
<div class="row  border-bottom dashboard-header">
    <div class="col-md-3 col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content mailbox-content">
                <div class="file-manager">
                    <a class="btn btn-block btn-primary compose-mail" href="mail_compose.html">Compose Mail</a>
                    <div class="space-25"></div>
                    <h5>Folders</h5>
                    <ul class="folder-list m-b-md" style="padding: 0">
                        <li><a href="{{ url('admin/booking') }}"> <i class="fa fa-inbox "></i> Inbox <span class="label label-warning pull-right">{{ $count }}</span> </a></li>
                        <li><a href="mailbox.html"> <i class="fa fa-trash-o"></i> Trash</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-9 col-xs-12">
        <div class="mail-box-header">
            <h2>
                Inbox ( {{ $count }} )
            </h2>
            <div class="mail-tools tooltip-demo m-t-md">
                <div class="btn-group pull-right">
                    <button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i></button>
                    <button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i></button>

                </div>
                <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" title="Refresh inbox"><i class="fa fa-refresh"></i> Refresh</button>
                <button class="btn btn-white btn-sm delete" data-toggle="tooltip" data-placement="top" title="" data-original-title="Move to trash" data-url="booking/destroy/">
                    <i class="fa fa-trash-o"></i>
                </button>

            </div>
        </div>
        <div class="mail-box">
            <table class="table table-hover table-mail">
                <tbody>
                    @foreach($bookings->sortByDesc('status') as $booking)
                        <tr class="{{$booking->status == 1 ? 'unread' : 'read' }} lists-item-{{ $booking->id }}">
                            <td class="check-mail">
                                <input type="checkbox" class="i-checks" data-id='{{ $booking->id }}'>
                            </td>
                            <td class="mail-ontact"><a href="{{ url('admin/booking/'.$booking->id) }}">{{ $booking->name }}</a></td>
                            <td class="mail-subject"><a href="{{ url('admin/booking/'.$booking->id) }}">{{ $booking->inquiry }}</a></td>
                            <td class=""><i class="fa fa-paperclip"></i></td>

                            @if( Carbon::today()->format('d M Y') === $booking->created_at->format('d M Y'))
                                <td class="text-right mail-date" >{{ $booking->created_at->format('H:i M d Y') }}</td>
                            @else
                                <td class="text-right mail-date">{{ $booking->created_at->format('M d,Y') }}</td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection('content')

@section('scripts')
    <script src="{{ url('/js/plugins/iCheck/icheck.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.table-mail').is('checked',function(){
                
            });
            $('.delete').click(function(){
                $this = $(this);
                url = $(this).data('url');
                $('.table-mail input:checkbox:checked').each(function(){

                    id = $(this).data('id');

                    $.get( url+''+id , function(data) {
                        if( data.error === false ){
                            $('.lists-item-'+id).hide('fade');
                        } else {
                            alert( data.msg );
                        }

                    },"json");
                    
                });
            });
        });
    </script>
@endsection('scripts')
