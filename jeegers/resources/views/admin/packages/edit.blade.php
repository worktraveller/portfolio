@extends('layouts.admin_dashboard')
@section('content')
    <div class="row  border-bottom dashboard-header">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>New Package</h5>

                </div>
                <div class="ibox-content">
 
                    {!! Form::model($package, array('route' => array( 'admin.packages.update', $package->id ), 'method' => 'PATCH', 'files'=> true)) !!}
                       <!--
                        |================================
                        |=========Personal Details=======
                        |================================
                        -->
                        <div class="form-group">
                            <span class="label label-primary">Destination Details</span>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <label class="control-label">Package Destination</label>
                                {!! Form::text('destination', null, array('class'=>'form-control', 'placeholder'=>'Destination')) !!}
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <label class="control-label">No of Days</label>
                                {!! Form::text('no_of_days', null, array('class'=>'form-control', 'placeholder'=>'Nights and Days')) !!}
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <label class="control-label">Category</label>
                                <select name="category" id="" class="form-control m-b" required="required">
                                    <option value="">-- Select --</option>
                                    @foreach( $categories as $category)
                                        <option value="{{ $category->id }}" {{ $package->category_id == $category->id ? 'selected="selected"':'' }}>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">    
                            <div class="col-md-6 col-sm-6">
                                <label class="control-label">Package Cost</label>
                                {!! Form::text('price', null, array('class'=>'form-control', 'placeholder'=>'Package Cost')) !!}
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <label class="control-label">Package Promo Cost</label>
                                {!! Form::text('promo_price', null, array('class'=>'form-control', 'placeholder'=>'Package Promo Cost')) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label">Package Description</label>
                                {!! Form::textarea('description', null, array('class'=>'form-control', 'placeholder'=>'Package Description', 'rows'=> '5')) !!}
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group">
                                   <input type="file" name="filesToUpload[]" id="filesToUpload" multiple="" />
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                                <?php $images = explode(',', $package->img_path); ?>
                                <ul id="fileList" class="images">
                                    @for($i = 0; count($images) > $i; $i++ )
                                    <li><img src="{{ asset('uploads/'.$package->category->name.'/'.$images[$i]) }}" /></li>
                                    @endfor
                                </ul>
                            </div>
                        </div>
                        
                        <div class="hr-line-dashed"></div>

                        <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <span class="label label-primary">Destination Itinerary / Inclusions</span>
                            </div>
                        <div class="hr-line-dashed"></div>

                        <div class="row">
                            <div class="col-md-7 col-sm-12 edit-itinerary">

                                {!! Form::textarea('itinerary', null, array('class'=>'form-control summernote', 'placeholder'=>'Itinerary', 'rows'=>'4')) !!}
                                
                            </div>
                            <div class="col-md-5 col-sm-12 edit-inclusion package-inclusion">
                                <div class="form-group">
                                   <span class="label label-primary add-inclusion"> Add Inclusions <i class="fa fa-plus-square"></i></span>
                                </div>
                                <label class="control-label">Package Inclusions</label>
                                <?php 
                                    $inclusion = unserialize( $package->inclusions ); 
                                ?>
                                @if($inclusion)
                                    @for($i = 0; count($inclusion) > $i; $i++ )
                                        @if($inclusion[$i])
                                            <div class="hr-line-dashed"></div>
                                                <input name="inclusion[]" value="{{ $inclusion[$i] }}" class="form-control" placeholder="Package" />
                                            <div class="clearfix"></div>
                                        @endif
                                    @endfor
                                @else
                                    <div class="hr-line-dashed"></div>
                                        <input name="inclusion[]" value="{{ $inclusion[$i] }}" class="form-control" placeholder="Package" />
                                    <div class="clearfix"></div>
                                @endif

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-primary pull-right" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection('content')
