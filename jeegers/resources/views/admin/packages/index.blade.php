@extends('layouts.admin_dashboard')
@section('content')
<div class="row  border-bottom dashboard-header">
    <div class="row">
            @foreach( $packages as $package )
            <div class="col-lg-4 lists-item">
                    <div class="widget-head-color-box navy-bg p-lg text-center">
                        <div class="m-b-md">
                        <h2 class="font-bold no-margins">
                            {{ $package->destination }}
                        </h2>
                            <h4>{{ $package->no_of_days }}</h4>
                        </div>
                        
                        <?php $images = explode(',',$package->img_path) ?>
                        @if( !$images[0] )
                            <img src="{{ asset('images/default-single-image.jpg') }}" class="m-b-md" alt="profile" width="125" height="125">                     
                        @else
                            <img src="{{ asset('uploads/'.$package->category->name.'/'.$images[0]) }}" class="m-b-md" alt="profile" width="125" height="125">                     
                        @endif
                    </div>
                    <div class="widget-text-box">
                        <p>{{ $package->description }}</p>
                        <div class="text-right">
                            <a href="{{ url('/admin/packages/'.$package->id.'/edit') }}" class="btn btn-xs btn-white"><i class="fa fa-pencil"></i></a>
                            <a href="#" class="label label-danger delete-this-item"
                                    data-action="{{ url('admin/packages/destroy/'.$package->id) }}"
                                    data-id="{{ $package->id }}" 
                                    data-confirmation-msg="Are you sure you want to delete {{ $package->destination }}?"
                            >
                            <i class="fa fa-trash"></i>
                            </a>
                        </div>
                    </div>
            </div>
            @endforeach
    </div>
</div>
@endsection('content')