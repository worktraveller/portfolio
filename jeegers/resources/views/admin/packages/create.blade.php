@extends('layouts.admin_dashboard')
@section('content')
    <div class="row  border-bottom dashboard-header">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>New Package</h5>

                </div>
                <div class="ibox-content">
 
                    {!! Form::open(array('url' => '/admin/packages/','class'=>'dropzone','id'=>'my-awesome-dropzone', 'files'=>true )) !!}
                       <!--
                        |================================
                        |=========Personal Details=======
                        |================================
                        -->
                        <div class="form-group">
                            <span class="label label-primary">Destination Details</span>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <label class="control-label">Package Destination</label>
                                {!! Form::text('destination', null, array('class'=>'form-control', 'placeholder'=>'Destination')) !!}
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <label class="control-label">No of Days</label>
                                <select name="no_of_days" id="" class="form-control m-b" required="required">
                                    <option value="">-- Select --</option>
                                    <option value="2D / 1N" {{ Input::old( 'no_of_days' ) == '2D / 1N' ? 'selected="selected"':'' }}>2D / 1N</option>
                                    <option value="3D / 2N" {{ Input::old( 'no_of_days' ) == '3D / 2N' ? 'selected="selected"':'' }}>3D / 2N</option>
                                    <option value="4D / 3N" {{ Input::old( 'no_of_days' ) == '4D / 3N' ? 'selected="selected"':'' }}>4D / 3N</option>
                                    <option value="5D / 3N" {{ Input::old( 'no_of_days' ) == '5D / 3N' ? 'selected="selected"':'' }}>5D / 4N</option>
                                </select>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <label class="control-label">Category</label>
                                <select name="category" id="" class="form-control m-b" required="required">
                                    <option value="">-- Select --</option>
                                    @foreach( $categories as $category)
                                        <option value="{{ $category->id }}" {{ Input::old( 'category' ) == $category->id ? 'selected="selected"':'' }}>{{ ucwords( $category->name ) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">    
                            <div class="col-md-6 col-sm-6">
                                <label class="control-label">Package Cost</label>
                                {!! Form::text('price', null, array('class'=>'form-control', 'placeholder'=>'Package Cost')) !!}
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <label class="control-label">Package Promo Cost</label>
                                {!! Form::text('promo_price', null, array('class'=>'form-control', 'placeholder'=>'Package Promo Cost')) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label">Package Description</label>
                                {!! Form::textarea('description', null, array('class'=>'form-control', 'placeholder'=>'Package Description', 'rows'=> '5')) !!}
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group">
                                   <input type="file" name="filesToUpload" id="filesToUpload" multiple="" />
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                                <ul id="fileList">
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="hr-line-dashed"></div>

                        <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <span class="label label-primary">Destination Itinerary / Inclusions</span>
                            </div>
                        <div class="hr-line-dashed"></div>

                        <div class="row">
                            <div class="col-md-7 col-sm-12 itinerary">

                                <div class="col-sm-12">
                                    <label class="control-label">Day Itinerary</label>
                                    {!! Form::textarea('itinerary', null, array('class'=>'form-control summernote', 'placeholder'=>'Itinerary', 'rows'=>'4')) !!}
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-5 col-sm-12 package-inclusion">

                                <div class="form-group">
                                   <span class="label label-primary add-inclusion"> Add Inclusions <i class="fa fa-plus-square"></i></span>
                                </div>

                                <label class="control-label">Package Inclusions</label>
                                <input type="input" name="inclusion[]" class="form-control"/>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-primary pull-right" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection('content')
