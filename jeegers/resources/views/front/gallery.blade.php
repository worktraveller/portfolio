@extends('layouts.innerpage')

<div class="container-fluid">
        <div class="slider-banner">
            <div class="jumbotron container-max-width text-center">
                <h1 class="title">{{  isset($page_title) ? $page_title:' Jigger\'s Travel' }}</h1>
                <h3 class="text-center subtitle">A COLLECTION OF MEMORIES WITH OUR BELOVED CLIENTS</h3>
            </div>
        </div>
</div>

@section('content')
<div class="container">
        <!--portfolio start-->
	        <div class="gallery-container">
	        	<ul id="filters" class="list-unstyled">
	        			<li><a href="#" data-filter="*"> All</a></li>
	        		@foreach( $terms as $term )
		                <li><a href="#" data-filter=".{{ $term->name }}">{{ $term->name }}</a></li>
	                @endforeach
	            </ul>
	            <div id="gallery" class="col-4 isotope">
	            	@foreach($galleries as $gallery)
		            	<div class="element {{ $gallery->galleryterm->name }} item isotope-item">
		            		<a data-zl-popup="link2" class="fancybox" rel="group" href="{{ asset('uploads/'.$gallery->galleryterm->name.'/'.$gallery->img_path) }}">
		                    	<img src="{{ asset('uploads/'.$gallery->galleryterm->name.'/'.$gallery->img_path) }}" alt="" >
		                   	</a>
		                </div>
	            	@endforeach
	            </div>
	        </div>
        <!--portfolio end-->
</div>
@endsection('content')