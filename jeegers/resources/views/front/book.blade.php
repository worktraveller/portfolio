@extends('layouts.innerpage')
@section('content')

<div class="container-fluid">
        <div class="slider-banner">
            <div class="jumbotron container-max-width text-center">
                <h1 class="title">
	                @if ( !session('error') && session('msg') )
	                    {{ session("msg") }}
	                @else
	                	{{  isset($page_title) ? $page_title :' Book' }}
	                @endif
                </h1>
                <h3 class="text-center subtitle">{{ $package->destination.' - '.$package->no_of_days }}</h3>
            </div>
        </div>
</div>

<div class="container">
	<div class="row book">
		<div class="col-xs-12">
			{!! Form::open(['url' => 'destinations/add/booking', 'id'=>'e_inquery', 'method' => 'post', 'class' => '']) !!}
				{{ Form::hidden('package_id', $package->id, array('id' => 'id')) }}
				  <div class="form-group col-sm-6">
				    <label for="firstname">Name*</label>  {{ $errors->first('name') }}
				    <input type="text" class="form-control" id="name" name="name" value="{{ Input::old('name') }}">
				  </div>
				  <div class="form-group col-sm-6">
	                <label class="control-label">No of Participants</label>
                    <select name="no_of_participants" id="" class="form-control m-b" required="required">
                        @for( $x = 1; 10 > $x; $x++)
                        	<option value="{{ $x }}" {{ Input::old( 'no_of_participants' ) == $x ? 'selected="selected"':'' }}>{{ $x }}</option>
                        @endfor
                        	<option value="10up" {{ Input::old( 'no_of_participants' ) == '10up+' ? 'selected="selected"':'' }}>10 and up</option>
                    </select>
				  </div>
				  <div class="form-group col-sm-6">
				    <label for="email">Email Address*</label>  {{ $errors->first('email') }}
				    <input type="email" class="form-control" id="email" name="email" value="{{ Input::old('email') }}">
				  </div>
				  <div class="form-group col-sm-6">
				    <label for="phone">Phone*</label> {{ $errors->first('phone') }}
				    <input type="phone" class="form-control" id="phone" name="phone" value="{{ Input::old('phone') }}">
				  </div>
				  <div class="clearfix"></div>
				  <div class="form-group col-sm-12">
					  <label for="subject">Date of Travel* </label> {{ $errors->first('date_of_travel') }}
					  <div class='input-group form-group date'>
					  	<span class="input-group-addon">
	                        <i class="glyphicon glyphicon-calendar"></i>
	                    </span>
						  <input type="text" class="form-control" id="date" name="date_of_travel" value="{{ Input::old('date_of_travel') }}">
						  	
					  </div>
				  </div>
				  <div class="form-group col-sm-12">
				  	<label for="help">How can we help you?</label> {{ $errors->first('help') }}
				  	  <textarea class="form-control" rows="8" name="help">
				  	  	{{ Input::old('help') }}
				  	  </textarea>
				  </div>
				  <div class="form-group col-sm-6">
				  <button type="submit" class="btn btn-danger btn-lg">Submit</button>
				  </div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection('content')