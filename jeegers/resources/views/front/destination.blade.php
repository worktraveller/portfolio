@extends('layouts.innerpage')
@include('includes.slider')
@section('content')
<div class="container-fluid destination">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<h1 class="text-center"><span>TRAVEL TIPS</span></h1>
			<h4 class="text-center">GUIDES FOR A SUCCESSFUL TRAVEL</h4>
		</div>
		<div class="row border-bottom">
			<div class="col-md-3 col-xs-12 col-sm-6 sm-margin-eleven text-center border-right border-top">
		        <div class="travel-guide center-col">
		            <div class="features-box-style1-sub">
		                <i class="icon-streetsign icon-medium medium-gray-text"></i>
		                <h4 class="text-large text-uppercase letter-spacing-2 light-gray-text alt-font">Explore your<br> destination</h4>
		            </div>
		        </div>
		    </div>
		    <div class="col-md-3 col-xs-12 col-sm-6 sm-margin-eleven text-center border-right border-top">
		        <div class="travel-guide center-col">
		            <div class="features-box-style1-sub">
		                <i class="icon-search icon-medium medium-gray-text"></i>
		                <h4 class="text-large text-uppercase letter-spacing-2 light-gray-text alt-font">CHECK SHEET<br> AVAILABILITY</h4>
		            </div>
		        </div>
		    </div>
		    <div class="col-md-3 col-xs-12 col-sm-6 sm-margin-eleven text-center border-right border-top">
		        <div class="travel-guide center-col">
		            <div class="features-box-style1-sub">
		                <i class="icon-briefcase icon-medium medium-gray-text"></i>
		                <h4 class="text-large text-uppercase letter-spacing-2 light-gray-text alt-font">GET READY TO <br> ENJOY TOUR</h4>
		            </div>
		        </div>
		    </div>
		    <div class="col-md-3 col-xs-12 col-sm-6 sm-margin-eleven text-center border-top">
		        <div class="travel-guide center-col">
		            <div class="features-box-style1-sub">
		                <i class="icon-ribbon icon-medium medium-gray-text"></i>
		                <h4 class="text-large text-uppercase letter-spacing-2 light-gray-text alt-font">BOOK YOUR <br> SELECTED SHEET</h4>
		            </div>
		        </div>
		    </div>
		</div>
</div>
<div class="container tour-packages">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<h1 class="text-center"><span>DESTINATIONS</span> </h1>
			<h4 class="text-center">A GREAT COLLECTION OF OUR TOUR DESTINATIONS</h4>
		</div>
        <!--portfolio start-->
	        <div class="gallery-container">
	            <ul id="filters" class="list-unstyled">
	                <li><a href="#" data-filter="*"> All</a></li>
	                @foreach( $packages as $package)
	                <li><a href="#" data-filter=".{{ $package->category->name }}">{{ $package->category->name }}</a></li>
						
					@endforeach
	            </ul>
	            <div id="gallery" class="col-4 isotope">
	            	@foreach( $packages as $package)
		                <div class="element {{ $package->category->name }} item isotope-item">
		                	<?php $img_path = explode(',',$package->img_path)?>
		                    <img src="{{ asset('uploads/'.$package->category->name.'/'.$img_path[0]) }}" alt="" >
		                    <div class="text-center package-wrap">
		                        <h3>{{ $package->destination }}</h3>
		                        <h5><small>{{ $package->no_of_days }} - PHP {{ ( $package->promo_price != 0  ? $package->promo_price : $package->price ) }} </small></h5>
			                    <div class="explore text-center">
			                    	<a href="{{ url('/destinations/'.$package->id.'/package') }}" class="btn">Explore Now</a>
			                    </div>
		                    </div>
		                </div>
	                @endforeach
	            </div>
	        </div>
        <!--portfolio end-->
</div>
@endsection('content')