@extends('layouts.innerpage')
@section('content')
<div class="container-fluid">
        <div class="slider-banner">
            <div class="jumbotron container-max-width text-center">
                <h1 class="title">{{  isset($page_title) ? $page_title:' Jigger\'s Travel' }}</h1>
                <h3 class="text-center subtitle">WE CATER FROM THE HEART</h3>
            </div>
        </div>
</div>
<div class="container-fluid">
	<div class="container-fluid text-center icon">
		<div class="col-sm-6">
			<h1><span class="fa fa-hotel"></span></h1>
			<h2>Hotel Reservation</h2>
			<p>tag line</p>
			
		</div>
		<div class="col-sm-6">
			<h1><span class="fa fa-cutlery"></span></h1>
			<h2>Restaurant Reservation</h2>
			<p>tag line</p>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="container-fluid text-center icon border-top">
		<div class="row">
			<div class="col-sm-4">
				<h1><span class="fa fa-plane"></span></h1>
				<h2>Airline Tickets</h2>
				<p>tag line</p>
			</div>
			<div class="col-sm-4">
				<h1><span class="fa fa-bus"></span></h1>
				<h2>Bus / Van Rental</h2>
				<p>tag line</p>
			</div>
			<div class="col-sm-4">
				<h1><span class="fa fa-ship"></span></h1>
				<h2>Boat Tickets</h2>
				<p>tag line</p>
			</div>
		</div>
	</div>

</div>
@endsection('content')