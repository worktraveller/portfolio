@extends('layouts.innerpage')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 col-sm-12 col-xs-12 itinerary-cover-background package-slider">
            <!-- Carousel items -->
            <?php $images = explode(',',$package->img_path) ?>

            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
			  	@if($images[0])
			  		<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				  	@for($i = 1; count($images) > $i; $i++)
				    <li data-target="#carousel-example-generic" data-slide-to="{{ $i }}"></li>
				    @endfor
			    @endif
			  </ol>

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner">
			  	@if($images)
			  	@if(!$images[0])
				  	<div class="item active">
				      <img src="{{ asset('images/default-single-image.jpg') }}" alt="...">
				    </div>
				@else
					<div class="item active">
				      <img src="{{ asset('uploads/'.$package->category->name.'/'.$images[0]) }}" alt="...">
				    </div> 
				@endif 

				  	@for($i = 1; count($images) > $i; $i++)
				    <div class="item">
				      <img src="{{ asset('uploads/'.$package->category->name.'/'.$images[$i]) }}" alt="...">
				    </div>
				    @endfor
			    @endif
			  </div>
			</div>
			
			<div class="vertical-center text-uppercase col-xs-12 front-text text-center">
				<h1 class="text-center alt-font">{{ $package->destination}}</h1>
				<h3 class="text-center">{{ $package->no_of_days }} - PHP {{ ( $package->promo_price != 0  ? $package->promo_price : $package->price ) }}</h3>
				<a href="{{ url('/destinations/'.$package->id.'/package/book') }}" class="btn btn-danger btn-lg">Book Now</a>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 col-xs-12" style="padding: 60px 60px 0px 60px;">
			<div class="row">
				@if($package->itinerary)
				<div class="col-xs-12">
					<hr>
						<h4>ITINERARIES</h4>
					<hr>
					<div class=" padding-ten no-padding-lr pull-left itinerary ajax-popup-content">
                        	{!! $package->itinerary !!}
	                </div>
				</div>
				@endif
			</div>
			<div class="row">	
				<div class="col-xs-12">
					<hr>
						<h4>INCLUSIONS</h4>
					<hr>
					<div class=" padding-ten no-padding-lr pull-left ajax-popup-content">
                        	<?php 
                                $inclusion = unserialize( $package->inclusions ); 
                            ?>
                            <ul class="inclusions">
		                        @for($i = 0; count($inclusion) > $i; $i++ )
		                        	@if( $inclusion[$i])
		                            	<li> <i class="fa fa-check-circle"></i> {{ $inclusion[$i]}} </li>
		                       		@endif
		                        @endfor
                            </ul>
   	                </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection('content')