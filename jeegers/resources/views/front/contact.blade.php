@extends('layouts.innerpage')
@section('content')
<div class="container-fluid">
	<div class="row">

				<div id="map"></div>

		<div class="contact-front">
			<div class="container">
				<div class="col-xs-12 fr-contact">
					<h1 class="text-center contact-us"><span>CONTACT US</span></h1>
					<h4 class="text-center"><span>GET IN TOUCH WITH US</span></h4>
				</div>
				<div class="row">
					<div class="col-md-4 col-sm-4 text-center xs-margin-twenty-three xs-no-margin-lr xs-no-margin-top">
			            <i class="icon-phone icon-medium medium-gray-text no-margin-bottom"></i>
			            <span class="text-uppercase display-block alt-font margin-one no-margin-lr medium-gray-text no-margin-bottom">Call us at</span>
			            <h6 class="text-medium alt-font medium-gray-text letter-spacing-1 no-margin-top">431-59-11 / (+63)918 -626-7070 <br /> (+63)905 -877-3024</h6>
			        </div>
			        <div class="col-md-4 col-sm-4 text-center xs-margin-twenty-three xs-no-margin-lr xs-no-margin-top">
		                <i class="icon-map-pin icon-medium medium-gray-text no-margin-bottom"></i>
		                <span class="display-block alt-font margin-one no-margin-lr medium-gray-text no-margin-bottom">Corner Sofia -Gonzaga St.,</span>
		                <h6 class="text-medium alt-font letter-spacing-1 text-transform-none medium-gray-text">Brgy. Estefania, Bacolod City</h6>
		            </div>
		            <div class="col-md-4 col-sm-4 text-center">
		                <i class="icon-envelope icon-medium medium-gray-text no-margin-bottom"></i>
		                <span class="text-uppercase display-block medium-gray-text alt-font margin-one no-margin-lr no-margin-bottom">Email us at</span>
		                <h6 class="text-medium alt-font letter-spacing-1 no-margin-top medium-gray-text text-transform-none"><a href="mailto:jiggerstraveltours@gmail.com" class="medium-gray-text">jiggerstraveltours@gmail.com</a></h6>
		            </div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<hr class="divider"/>
					</div>
					<div class="col-xs-12">
						{!! Form::open(['url' => '/request/email', 'id'=>'e_inquery', 'method' => 'post', 'class' => '']) !!}
				    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
							  <div class="form-group col-sm-6">
							    <label for="firstname">First Name* </label> {{ $errors->first('firstname') }}
							    <input type="text" class="form-control" id="firstname" name="firstname" value="{{ Input::old('firstname') }}">
							  </div>
							  <div class="form-group col-sm-6">
							    <label for="lastname">Last Name*</label> {{ $errors->first('lastname') }}
							    <input type="text" class="form-control" id="lastname" name="lastname" value="{{ Input::old('lastname') }}">
							  </div>
							  <div class="form-group col-sm-6">
							    <label for="email">Email Address*</label> {{ $errors->first('email') }}
							    <input type="email" class="form-control" id="email" name="email" value="{{ Input::old('email') }}">
							  </div>
							  <div class="form-group col-sm-6">
							    <label for="phone">Phone</label> {{ $errors->first('phone') }}
							    <input type="phone" class="form-control" id="phone" name="phone" value="{{ Input::old('phone') }}">
							  </div>
							  <div class="clearfix"></div>
							  <div class="form-group col-sm-12">
								  <label for="subject">Subject* </label> {{ $errors->first('subject') }} 
								  <input type="text" class="form-control" id="subject" name="subject" value="{{ Input::old('subject') }}">
							  </div>
							  <div class="form-group col-sm-12">
							  	<label for="help">How can we help you?</label> {{ $errors->first('help') }} 
							  	  <textarea class="form-control" rows="8" name="help">
							  	  	{{ Input::old('help') }}
							  	  </textarea>
							  </div>
							  <div class="form-group col-sm-6">
							  <button type="submit" class="btn btn-danger btn-lg">Submit</button>
							  </div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection('content')
