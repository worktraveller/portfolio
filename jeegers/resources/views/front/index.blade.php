@extends('layouts.front')
@section('content')
	<div class="container tour-packages">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h1 class="text-center"><span>TOUR PACKAGES</span> </h1>
				<h4 class="text-center">A GREAT DEAL FOR YOUR TOUR</h4>
			</div>
			
	        <!--portfolio start-->
		        <div class="gallery-container">
		            <ul id="filters" class="list-unstyled">
		                <li><a href="#" data-filter="*"> All</a></li>
			                @foreach( $packages as $package)
			                	<li><a href="#" data-filter=".{{ $package->category->name }}">{{ $package->category->name }}</a></li>
							@endforeach
		            </ul>
		            <div id="gallery" class="col-4 isotope">
		            	@foreach( $packages as $package )

		            		<?php $img_path = explode(',',$package->img_path)?>
			            
			                <div class="element {{ $package->category->name }} item isotope-item">
			                    <img src="{{ asset('uploads/'.$package->category->name.'/'.$img_path[0]) }}" alt="" >
			                    <div class="text-center package-wrap package-wrap-front">
			                        <h3>{{ $package->destination }}</h3>
			                        <h5><small>{{ $package->no_of_days }} - PHP {{ ( $package->promo_price != 0  ? $package->promo_price : $package->price ) }} </small></h5>
				                    <div class="explore text-center">
				                    	<a href="{{ url('/destinations/'.$package->id.'/package') }}" class="btn">Explore Now</a>
				                    </div>
			                    </div>
			                </div>
		                @endforeach
		            </div>
		        </div>
	        <!--portfolio end-->
	</div>
	<div class="why-choose-us jumbotron">
		<div class="container">
			<h1 class="text-center"><span>WHY CHOOSE US?</span> </h1>
			<h3 class="text-center class="subtitle"">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h3>
			<div class="col-md-3 col-xs-12 col-sm-6 sm-margin-eleven text-center">
		        <div class="features-box-style1 center-col">
		            <div class="features-box-style1-sub">
		                <i class="icon-ribbon icon-medium medium-gray-text"></i>
		                <h5 class="text-large text-uppercase letter-spacing-2 light-gray-text alt-font">Best Price<br> Guarantee</h5>
		            </div>
		        </div>
		    </div>
		    <div class="col-md-3 col-xs-12 col-sm-6 sm-margin-eleven text-center">
	            <div class="features-box-style1 center-col">
	                <div class="features-box-style1-sub">
	                    <i class="icon-lock icon-medium medium-gray-text"></i>
	                    <h5 class="text-large text-uppercase letter-spacing-2 light-gray-text alt-font">Trust and<br> Safety</h5>
	                </div>
	            </div>
	        </div>
		    <div class="col-md-3 col-xs-12 col-sm-6 sm-margin-eleven text-center">
	            <div class="features-box-style1 center-col">
	                <div class="features-box-style1-sub">
	                    <i class="icon-heart icon-medium medium-gray-text"></i>
	                    <h5 class="text-large text-uppercase letter-spacing-2 light-gray-text alt-font">Best Travel<br> Agent</h5>
	                </div>
	            </div>
	        </div>
		    <div class="col-md-3 col-xs-12 col-sm-6 sm-margin-eleven text-center">
		        <div class="features-box-style1 center-col">
	                <div class="features-box-style1-sub">
	                    <i class="icon-compass icon-medium medium-gray-text"></i>
	                    <h5 class="text-large text-uppercase letter-spacing-2 light-gray-text alt-font">Travel<br> Insurance</h5>
	                </div>
	            </div>
		    </div>
		</div>
	</div>
	<div class="container-fluid text-center icon">
		<div class="col-sm-6">
			<h1><span class="fa fa-hotel"></span></h1>
			<h2>Hotel</h2>
			<p>Where you can will sleep like a baby</p>
			
		</div>
		<div class="col-sm-6">
			<h1><span class="fa fa-cutlery"></span></h1>
			<h2>Restaurant</h2>
			<p>Travel like a prince and eat like a King</p>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="container-fluid text-center icon border-top">
		<div class="row">
			<div class="col-sm-4">
				<h1><span class="fa fa-plane"></span></h1>
				<h2>Airplane</h2>
				<p>An airplane can fly you to the moon.</p>
			</div>
			<div class="col-sm-4">
				<h1><span class="fa fa-bus"></span></h1>
				<h2>Bus / Van</h2>
				<p>A Bus that will secure your safety and bring you where you belong.</p>
			</div>
			<div class="col-sm-4">
				<h1><span class="fa fa-ship"></span></h1>
				<h2>Boat</h2>
				<p>We will float you from one island to another</p>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="about jumbotron" style="background: #3c7cc7; ">
		<div class="container">
			<h1 class="text-center"><span>JEEGER'S</span> </h1>
			<h3 class="text-center class="subtitle"">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</h3>
		</div>
	</div>
@endsection('content')