<footer class="footer-bg">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center social" >
          <h4>CAN'T WAIT TO GET INVOLVED,</h4>
          <h4>CONNECT WITH US NOW</h4>
          <a href="https://www.facebook.com/jeegerstravelandtours" class="btn social-icon">
            <i class="fa fa-facebook"></i>
          </a>
          <a href="" class="btn social-icon">
            <i class="fa fa-google-plus"></i>
          </a>
          <a href="" class="btn social-icon">
            <i class="fa fa-instagram"></i>
          </a>
      </div>
    </div>
     <div class="copyright">
          <h4 class="text-center"><small>&copy; 2016 Jeeger's Travel. All Rights Reserved / Privacy Policy</small></h4>
     </div>
  </div>
</footer>
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/jquery-1.8.3.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/hover-dropdown.js') }}"></script>
    <script src="{{ asset('js/jquery.flexslider.js') }}"></script>
    <script src="{{ asset('js/bxslider/jquery.bxslider.js') }}"></script>
    <script src="{{ asset('js/superfish.js') }}"></script>
    <script src="{{ asset('js/jquery.isotope.js') }}"></script>
    <script src="{{ asset('js/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/link-hover.js') }}"></script>
    <script src="{{ asset('js/fancybox/source/jquery.fancybox.pack.js') }}"></script>
    <script src="{{ asset('js/moment.js') }}"></script>
    <script src="{{ asset('js/plugins/datapicker/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('js/common-scripts.js') }}"></script>
    @if($page_title == 'contact-us')
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8nr4RaRDoOmr5uNXJ7O7y_UuZ-14lncc&callback=initMap"
      async defer></script>
    @endif
    <script>
    <?php ?>
    @if($page_title == 'contact-us')
      //google map
      function initMap() {
        var LatLong = new google.maps.LatLng(10.669693, 122.987335);
        var mapOptions = {
          zoom: 17,
          scrollwheel: true,
          center: LatLong,
          mapTypeId: google.maps.MapTypeId.TERRAIN
        }
        map = new google.maps.Map(document.getElementById('map'), mapOptions);
        var contentString = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h2>JEEGER\'S TRAVEL AND TOURS</h2>'+
        '<div id="bodyContent">'+
        '<p><b>Address : </b>Corner Sofia - Gonzaga St. Brgy. Estefania, Bacolod City, Philippines</p>' +
        '<p><b>Contact : </b>(034) 431-59-11 / (+63)918 -626-7070 / (+63)905 -877-3024</p>'+
        '</div>'+
        '</div>';
        var infowindow = new google.maps.InfoWindow({
            content: contentString
          });

        var marker = new google.maps.Marker({
            position: LatLong,
            map: map,
            title: 'Jeeger\'s Travel And Tours'
        });

        infowindow.open(map, marker);
        marker.addListener('click', function(){
          infowindow.open(map, marker);
        });
        
      }
      @endif



    
      $(window).load(function() {
          $('[data-zlname = reverse-effect]').mateHover({
              position: 'y-reverse',
              overlayStyle: 'rolling',
              overlayBg: '#fff',
              overlayOpacity: 0.7,
              overlayEasing: 'easeOutCirc',
              rollingPosition: 'top',
              popupEasing: 'easeOutBack',
              popup2Easing: 'easeOutBack'
          });
      });

      $(window).load(function() {
          $('.flexslider').flexslider({
              animation: "slide",
              start: function(slider) {
                  $('body').removeClass('loading');
              }
          });
      });

      //    fancybox
      $(".fancybox").fancybox();

      $(function() {
          var $container = $('#gallery');
          $container.isotope({
              itemSelector: '.item',
              animationOptions: {
                  duration: 750,
                  easing: 'linear',
                  queue: false
              }
          });

          // filter items when filter link is clicked
          $('#filters a').click(function() {
              var selector = $(this).attr('data-filter');
              $container.isotope({filter: selector});
              return false;
          });
      });

      $(document).ready(function(){
        $('.package-wrap').mouseover(function(){
            $(this).closest('.element').find('.explore').show();
        });
        $('.package-wrap').mouseout(function(){
            $(this).closest('.element').find('.explore').hide();
        });

        $('#date').datetimepicker({
            format: "YYYY-MM-DD",
        });

      });
  </script>
  </body>
</html>