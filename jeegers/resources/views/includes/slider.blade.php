<div class="container-fluid">
        <div class="slider-banner slider-banner-home">
            <div class="jumbotron container-max-width text-center">
                <h1 class="title">THE BEST TRAVEL PARTNER OF YOUR CHOICE</h1>
                <h3 class="subtitle">The web hosting industry's most reliable, intuitive control panel since 1997. With our first-class support and rich feature set, it's easy to see why our customers and partners make cPanel & WHM their hosting platform of choice.</h3>
                    @if(Request::is('/') )
                    <div class="col-sm-4 col-sm-offset-4">
                        <a class="btn btn-default btn-lg btn-block btn-white">Request A Quote</a>
                    </div>
                    @endif
            </div>
        </div>
</div>
