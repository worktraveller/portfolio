<nav class="site-header navbar navbar-default navbar-fixed-top" role="navigation">

    <div class="container-fluid container-max-width">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">
            <img src="{{ asset('images/jeegers.png') }}" alt="jeegers logo" class="img-responsive logo" style="position: absolute;" >
        <p>Jeegers Travel & tours</p>
        </a>
    </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        
        <ul class="nav navbar-nav navbar-right">
            <li class="{{ (isset($page_title) && $page_title == 'home' ) ? 'active' :'' }}">
                  <a href="/">Home</a>
            </li>    
            <li class="{{ (isset($page_title) && $page_title == 'destinations' ) ? 'active' :'' }}" >
                  <a href="{{ url('/destinations') }}">Destinations</a>
            </li>
            <li class="{{ (isset($page_title) && $page_title == 'services' ) ? 'active' :'' }}">
                  <a href="{{ url('/services') }}">Services</a>
            </li>
            <li class="{{ (isset($page_title) && $page_title == 'gallery' ) ? 'active' :'' }}">
                  <a href="/gallery/">Gallery</a>
            </li>
            <li >
                  <a href="/support/">Blog</a>
            </li>
            <li class="{{ (isset($page_title) && $page_title == 'contact-us' ) ? 'active' :'' }}">
                  <a href="{{ url('/contact') }}">Contact Us</a>
            </li>        
        </ul>
    </div><!-- /.navbar-collapse -->
    </div>
</nav>