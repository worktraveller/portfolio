var host = 'http://'+top.location.host+"";
var xhr = null;

var error_msgs = new Array("Naaa!","Oops!","Baaam!");
$(document).ready(function(){

	// category
	$('#category').change(function(){
		var cat = $(this).val(); 
		if( cat == 'breakfast' ){
			$('.breakfast').show();
			$('.dining').hide();
			$('.beverage').hide();
		}else if( cat == 'dining' ){
			$('.breakfast').hide();
			$('.dining').show();
			$('.beverage').hide();
		}else if( cat == 'beverage' ){
			$('.breakfast').hide();
			$('.dining').hide();
			$('.beverage').show();	
		}else{

		}
	});


	$('.lists-item').on('click', '.delete-this-item', function() {
		var $this = $(this);
		if( !confirm( $this.attr('data-confirmation-msg') ) ) return false;

		var id = $this.attr('data-id');
		var url = $(this).attr('data-action'); 

		$.get( url , function(data) {
			if( data.error === false ){
				$this.closest('.lists-item').hide('fade');
			} else {
				alert( data.msg );
			}
		},"json");

		return false;
	});

	$('.add-image').click(function(){
		id = $(this).data('id');
		$('#gallery_id').val(id);
	});


}); // end document ready	

	
