<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Quest extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'answers' => 'array',
    ];
    public function getAnswersAttribute($answers)
    {
        return unserialize($answers);
    }

   /* public function setAnswersAttribute($answers)
    {
        $this->attributes['answers'] = serialize($answers);
    }*/

    public function user()
    {
    	return $this->hasOne('App\Quest', 'user_id', '_id');
    }
}
