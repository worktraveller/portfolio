<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class SkillResult extends Authenticatable
{
	protected $table = 'skill_results';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'value' => 'array',
    ];
}
