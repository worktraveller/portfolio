<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Student extends Authenticatable
{
     //protected $table = 'students';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /*protected $fillable = [
        'id','username', 'password',
    ];*/

    public function quest()
    {
        return $this->hasMany('App\Quest', 'student_id', '_id');
    }

    public function result()
    {
        return $this->hasOne('App\Result', 'user_id', 'user_id');
    }

    public function student()
    {
        return $this->belongTo('App\User');
    }

}
