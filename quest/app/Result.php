<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Result extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'answers' => 'array',
        'sub_score' => 'array',
    ];
    
    public function getAnswersAttribute($answers)
    {
        return unserialize($answers);
    }

    public function setAnswersAttribute($answers)
    {
        $this->attributes['answers'] = serialize($answers);
    }

    public function student()
    {
        return $this->hasOne('App\Student', 'user_id', 'user_id');
    }
    
    public function teacher()
    {
        return $this->hasOne('App\Teacher', 'user_id', 'user_id');
    }

    /*public function quest()
    {
        $implode = array(1,2);
        Quest::whereIn('id',$implode)->whereIn('correct_answer', array(a,b) )->count();
    }*/
}
