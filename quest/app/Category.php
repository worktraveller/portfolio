<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Category extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function quest()
    {
        return $this->hasMany('App\Quest', 'category_id', 'id');
    }

}
