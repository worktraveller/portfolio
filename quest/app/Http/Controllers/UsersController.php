<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Student;
use Input;
use Image;
use DB;
use Crypt;
use Excel;
use Auth;
use Redirect;

class AdminStudentsController extends Controller
{
    public function __construct()
    {

        $this->folder = 'admin';
        $this->user = Auth::user();
    }

     /**
     * View all users with default range
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $this->params['users'] = User::where('user_type','student')->get();
        return view( $this->folder.'.students.manage', $this->params );
    }

     /**
     * View all users with default range
     */

    public function import()
     {
        
        // Import a Student provided file
        $student = new Student;
        $file = Input::hasFile('import_file');
        $path = Input::file('import_file')->getRealPath();
        $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){

                foreach ($data as $key => $value) {

                    $insert[] = [
                    'username'           => $value->username, 
                    'password'        => Crypt::encrypt($value->password),
                    ];
                }
            }

            if( !empty($insert) ){
                    $student->insert( $insert );

                $this->params['msg'] = 'Students has been added.';
                 $this->params['status'] = 'success';
                return Redirect::to('/admin/student/create')->with( $this->params );
            }

        $this->params['msg'] = 'Students has been added 1.';
        $this->params['status'] = 'success';
        return Redirect::to('/admin/student/create')->with( $this->params );
     }

    /**
     * Display the specified user profile.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view($this->folder.'.students.create');
    }

    public function store()
    {
        $rules = [
                'username' => 'required|min:2|max:50|unique:students',
                'password' => 'required|min:5|max:12',
            ];

        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails() ) {
            $messages = $validator->messages()->getMessages();

            $this->params['error'] = true;
            $this->params['status'] = 'error'; 
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            return Redirect::to('admin/students/create')->withInput()->withErrors($messages);
        }

        $students = new Student;
        $students->username = Input::get('username');
        $students->password = Crypt::encrypt(Input::get('password'));
        $students->save();

        $this->params['msg'] = 'Student has been added.';
        $this->params['status'] = 'success'; 

        return Redirect::to('admin/students/create')->with( $this->params );

    } 

    public function show( $id )
    {

    }
    public function edit( $id )
    {
        $this->params['user'] = User::findOrFail( $id );
        return view( $this->folder.'.students.edit', $this->params );
    }

    /**
     * Update user.
     *
     * @param  int  $id
     */
    public function update( $id )
    {

        $user = User::findOrFail( $id );

        $rules = [
                'fullname' => 'required|min:2|max:50',
                'email' => 'required|min:2|max:50|unique:users,email,'.$user->id.',id',
            ];

        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails() ) {
            $messages = $validator->messages()->getMessages();

            $this->params['error'] = true;
            $this->params['status'] = 'error'; 
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            return Redirect::to('admin/students/'.$id.'/edit')->withInput()->withErrors($messages);
        }

        $user->fullname = Input::get('fullname');
        $user->email = Input::get('email');
        $user->save();

        $this->params['msg'] = 'User has been successfully updated.';
        $this->params['status'] = 'success'; 
        // Redirect to user lists
        return Redirect::to('/admin/students/manage')->with( $this->params );

    }

    public function destroy( $id )
    {
        $user = User::find($id);
        if( !$user ) {
            $this->params['msg'] = 'User not found or already been deleted.';
            $this->params['error'] = true; 
            return response()->json( $this->params );
        }
        $user->delete();

        $this->params['msg'] = 'User has been successfully deleted.';
        $this->params['error'] = false; 
        
        return response()->json( $this->params );
    }

}
