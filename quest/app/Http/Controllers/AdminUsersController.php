<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Input;
use App\User;
use App\Student;
use App\Teacher;
use App\Result;
use App\Quest;
use App\Category;
use Crypt;
use Redirect;
use Session;
use Validator;

class AdminUsersController extends Controller
{
    public function __construct()
    {
    	$this->params = [
                'title' => 'Dashboard',
                'module_name' => 'user',
            ];

        $this->folder = 'Admin';
        $this->user = Auth::user();

        if( !Auth::check() ) return Redirect::to('/')->send();
        if( Auth::user()->user_type != 'admin' ) return Redirect::to('/')->send();

    }

     /**
     * View all users with default range
     * @return \Illuminate\Http\Response
     */

     public function index()
     {
     	$this->params['module_name'] = 'dashboard';
        $this->params['users'] = Result::where('status','1')->get();
        $this->params['results'] = Result::all();

        $this->params['categories'] = Category::all();

        return view( $this->folder .'.index', $this->params );

     }

    /**
     * Display the specified user profile.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view( $this->folder .'.create', $this->params );
    }

    public function manage()
    {
        $this->params['users'] = User::all();
        return view( $this->folder .'.manage', $this->params );
    }

    public function manage_teacher()
    {
        $this->params['users'] = User::all();
        return view( $this->folder .'.manage', $this->params );
    }

    public function store()
    {

        $rules = [
            'username' => 'required|min:5|max:15|unique:users',
            'password' => 'required|min:5|max:15',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails() ) {
            $messages = $validator->messages()->getMessages();

            $this->params['error'] = true;
            $this->params['status'] = 'error'; 
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            return Redirect::to('admin/user/create')->withInput()->withErrors($messages);
        }

        $type_check = array('student','encoder','teacher');

        if(in_array(Input::get('user_type'), $type_check)){
        
            $user = new User;
            $user->username = Input::get('username');
            $user->password = Crypt::encrypt( Input::get('password') );
            $user->user_type = Input::get('user_type');
            $user->save();

            //add student
            if( Input::get('user_type') == 'student' ){
                $student = new Student;
                $student->user_id = $user->id;   
                $student->save();   
            }
            // add teacher

            if( Input::get('user_type') == 'teacher' ){
                $teacher = new Teacher;
                $teacher->user_id = $user->id;   
                $teacher->save();   
            }

            $this->params['msg'] = 'You successfully add '.Input::get('user_type');
            $this->params['status'] = 'success'; 

            return Redirect::to('admin/user/create')->with( $this->params );

        } else {

        $this->params['msg'] = 'User type is undefine';
        $this->params['status'] = 'error'; 

        return Redirect::to('admin/user/create')->with( $this->params );
        }
    } 
    

    /**
     * Update user.
     *
     * @param  int  $id
     */
    public function update( $id )
    {

    }
}
