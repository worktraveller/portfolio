<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Skill;
use App\SkillResult;
use App\Result;
use Input;
use Image;
use DB;
use Crypt;
use Excel;
use Auth;
use Redirect;

class SkillsController extends Controller
{
    public function __construct()
    {
        $this->params = [
            'title' => ''
            ];
        $this->user = Auth::user();
        if(!$this->user) return Redirect::to('/')->send();

    }

     /**
     * View all users with default range
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        
        $this->params['title'] = 'ICT SKILLS';
        $this->params['skills'] = Skill::all();

        $SkillResult = Result::where('user_id', $this->user->id)->where('status','2')->first();
        if( !$SkillResult ) return Redirect::to($this->user->user_type.'/exam/post');

        return view( 'ict',$this->params );
    }

    /**
     * Display the specified user profile.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }


    public function store()
    {

        $rules = [
                'skill' => 'required',
            ];

        $validator = Validator::make(Input::all(), $rules);
       
        if( $validator->fails() ) {
            $messages = $validator->messages()->getMessages();

            $this->params['error'] = true;
            $this->params['status'] = 'error'; 
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            return Redirect::to('admin/teacher/create')->withInput()->withErrors($messages);
        }
        
        $skill = new SkillResult();
        $skill->user_id = $this->user->id;
        $skill->value = Input::get('skill');
        $skill->save();

        $this->params['msg'] = 'ICT Skill Added';
        $this->params['status'] = 'success'; 

        return Redirect::to( $this->user->user_type.'/exam/result' )->with( $this->params );

    } 

    public function show( $id )
    {

    }
    public function edit( $id )
    {
      
    }

    /**
     * Update user.
     *
     * @param  int  $id
     */
    public function update( $id )
    {

      
    }

    public function destroy( $id )
    {
        
    }

}
