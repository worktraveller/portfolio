<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Teacher;
use App\Result;
use App\Quest;
use App\Category;
use App\Skill;
use App\SkillResult;
use Auth;
use Input;
use Redirect;
use Response;


class TeachersResultsController extends Controller
{
    public function __construct()
    {   
        $this->params = [
               'title' => 'Result'
            ];

        $this->folder = 'teacher';
        $this->user = Auth::user();
        if( !Auth::check() ) return Redirect::to('/')->send();
        if( Auth::user()->user_type != 'teacher' ) return Redirect::to('/check')->send();

    }

     //exam result for teacher only
    public function index()
    {
        $this->params['title'] = 'MY EXAM RESULT';
        $this->params['teacher'] = Teacher::where( 'user_id',$this->user->id )->first();
        $this->params['preResult'] = Result::where('user_id',$this->user->id )->where('status','1')->first();
        $this->params['postResult'] = Result::where('user_id', $this->user->id )->where('status','2')->first();

        $this->params['categories'] = Category::all();

        //count items
        $counts = Result::where('user_id', $this->user->id )->first();

        $this->params['total_items'] = ( $counts ) ? array_sum(array_map("count", $counts->answers)) : 0;

        $this->params['skill_result'] = SkillResult::where('user_id', $this->user->id )->first();
        
        $this->params['skills'] = Skill::all();
        //dd();
        //ict skill not yet answer
        if( !$this->params['skill_result'] ) return Redirect::to('/ict/skill');

        //$this->params['corrects'] = Quest::all();
        //$this->params['results'] = Result::where( 'user_id', $this->user->id )->get();
        return view( $this->folder.'.result.index', $this->params );
    }

    public function store()
    {

        $resultcheck = Result::where('user_id', $this->user->id)->orderBy('created_at', 'asc')->first();

        $rules = [
            'answer' => 'required|max:255',
        ];


        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails() ) {

            $messages = $validator->messages()->getMessages();

            $this->params['errors'] = true;
            $this->params['status'] = 'error';
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            return Redirect::to('/teacher/exam/pre')->withErrors( $messages )->withInput();
        }

        $quests = Quest::get( array('id','category_id','correct_answer') );
        $results = Input::get('answer');

        
        //count of correct answer
        $total = 0;
        $cat = [];
        foreach ( $quests as $quest ) {
            foreach ($results as $cat_id => $answers) {

                if( $quest->category_id ==  $cat_id ) {
                    //dd($results);
                    foreach ( $answers as $id => $answer ) {
                        if( !isset($cat[$cat_id]) ){
                            $sub_total = 0;
                        }   
                        //$name = strtoupper($quest->name);
                        if( $quest->id == $id && $quest->correct_answer ==  $answer) { 

                                $t = 1 + $sub_total++;
                                $cat[$cat_id] = $t;
                                $total++;       
                        } 
                    }
                }
            }
        }

        if( !$resultcheck ) {

            $answer = Input::get('answer');
            $results = new Result;
            $results->user_id = $this->user->id;
            $results->answers = $answer;
            $results->sub_score = $cat;
            $results->score = $total;
            $results->status = 1;
            $results->save();

            $this->params['msg'] = 'You Successfully take the exam';
            $this->params['status'] = 'success'; 

            return redirect('/teacher/exam/pre')->with( $this->params );

        } else {
            // check if status is equal to 1
            if( $resultcheck->status == 1 ){

                $results = new Result;
                $results->user_id = $this->user->id;
                $results->answers = Input::get('answer');
                $results->sub_score = $cat;
                $results->score = $total;
                $results->status = 2;
                $results->save();

                $this->params['msg'] = 'You Successfully take the exam';
                $this->params['status'] = 'success'; 

                return redirect('/teacher/exam/result')->with( $this->params );
            }
            
        }

        $this->params['msg'] = 'You\'re already take the Exam';
        $this->params['status'] = 'error'; 

        return redirect('/teacher/exam/pre')->with( $this->params );
    } 

}
