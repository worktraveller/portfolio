<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Input;
use Image;
use DB;
use Crypt;

class UsersController extends Controller
{
    public function __construct()
    {

    }

     /**
     * View all users with default range
     * @return \Illuminate\Http\Response
     */

     public function index()
     {

        return view('login');
     }

    /**
     * Display the specified user profile.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User;
        $user->fullname = 'Admin Exam Quest';
        $user->email = 'admin@gmail.com';
        $user->password = Crypt::encrypt('123456');
        $user->user_type = 'admin';
        $user->save();

    }

    public function store()
    {


    } 

    public function show( $id )
    {

    }

    /**
     * Update user.
     *
     * @param  int  $id
     */
    public function update( $id )
    {

    }

}
