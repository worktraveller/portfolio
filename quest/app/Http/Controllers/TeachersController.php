<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Teacher;
use App\Result;
use App\Quest;
use Input;
use DB;
use Crypt;
use Auth;
use Redirect;

class TeachersController extends Controller
{
    public function __construct()
    {

        $this->params = [
                'title' => ''
            ];
        $this->folder = 'teacher';
        $this->user = Auth::user();

        if( !Auth::check() ) return Redirect::to('/')->send();
        if( Auth::user()->user_type != 'teacher' ) return Redirect::to('/check')->send();
    }

    // Teacher Profile
    public function index()
    {
        $this->params['title'] = 'TEACHER PROFILE';
        $teacher = Teacher::where('user_id', $this->user->id )->firstOrFail();
        $this->params['teacher'] = $teacher;
        if( $teacher->first_name 
                &&  $teacher->last_name 
                &&  $teacher->age 
                &&  $teacher->gender 
                &&  $teacher->highest_attainment 
                &&  $teacher->present_school 
                &&  $teacher->subject_taught 
                &&  $teacher->specialization 
                &&  $teacher->years_in_service 
            ) {

           return Redirect::to('teacher/exam/pre');
        }

        return view( $this->folder.'.index', $this->params );        
    }

    public function profile()
    {

        $this->params['title'] = 'TEACHER PROFILE';
        $this->params['teacher'] = Teacher::where('user_id', $this->user->id )->firstOrFail();
        return view( $this->folder.'.index', $this->params );
    }

    //Learning Intervention
    public function intervention()
    {
        
        $this->params['title'] = 'LEARNING INTERVENTION';

        $result = Result::where( 'user_id', $this->user->id )->orderBy('created_at','desc')->first();

        if( !$result ) return Redirect::to('teacher');

        if( $result->status == 2 )return Redirect::to('/teacher/exam/result');

            $int_status = Result::find($result->id);

            if( $int_status->status == 0 ) {

                $int_status->status = 1;
                $int_status->save();
            }
        
        $this->params['quests'] = Quest::all();
        $this->params['results'] = $result;
        return View( $this->folder.'.intervention', $this->params );
    }

    //proceed to intervention or Post test
    public function proceed(){

        $result = Result::where( 'user_id', $this->user->id )->orderBy('created_at','desc')->first();

        if( !$result ) return Redirect::to('/teacher/exam/result');

        if( $result->status == 2 ) return Redirect::to('teacher/exam/result');
        
        return View( $this->folder.'.proceed', $this->params );
    }

    //confirm to take Post test
    public function confirmation()
    {

        $result = Result::where( 'user_id', $this->user->id )->orderBy('created_at','desc')->first();

        if( !$result )return Redirect::to('teacher/exam/result');
        if( $result->status == 2 )return Redirect::to('teacher/exam/result');

        return View( $this->folder.'.confirm', $this->params );
    }
    
    //update teacher profile

    public function update( $id )
    {

        $rules = [
                'first_name' => 'required|min:2|max:100|regex:/(^[A-Za-z0-9 ]+$)+/',
                'last_name' => 'required|min:2|max:100|regex:/(^[A-Za-z0-9 ]+$)+/',
                'age' => 'required|min:18|integer',
                'gender' => 'required|integer',
                'highest_attainment' => 'required|min:1',
                'present_school' => 'required|min:2|max:150|regex:/(^[A-Za-z0-9 ]+$)+/',
                'subject_taught' => 'required|min:2|max:100|regex:/(^[A-Za-z0-9 ]+$)+/',
                'specialization' => 'required|min:2|max:100|regex:/(^[A-Za-z0-9 ]+$)+/',
                'years_in_service' => 'required|integer',
            ];

        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails() ) {
            $messages = $validator->messages()->getMessages();

            $this->params['error'] = true;
            $this->params['status'] = 'error'; 
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            return Redirect::to('teacher/profile')->withInput()->withErrors($messages);
        }

        $teacher = Teacher::where('user_id', $this->user->id )->firstOrFail();
        $teacher->first_name = strtoupper(Input::get('first_name'));
        $teacher->last_name = strtoupper(Input::get('last_name'));
        $teacher->age = Input::get('age');
        $teacher->gender = Input::get('gender');
        $teacher->highest_attainment = strtoupper(Input::get('highest_attainment'));
        $teacher->present_school = strtoupper(Input::get('present_school'));
        $teacher->subject_taught = strtoupper(Input::get('subject_taught'));
        $teacher->specialization = strtoupper(Input::get('specialization'));
        $teacher->years_in_service = Input::get('years_in_service');
        $teacher->save();

        $this->params['msg'] = 'Profile save.';
        $this->params['status'] = 'success'; 

        return Redirect::to('teacher/profile')->with( $this->params );
    }

}
