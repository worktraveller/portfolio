<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Student;
use App\Category;
use App\Result;
use Input;
use Image;
use DB;
use Crypt;
use Excel;
use Auth;
use Redirect;

class AdminStudentsController extends Controller
{
    public function __construct()
    {
        $this->params = [
                'title' => '',
                'module_name' => 'user'
            ];

        $this->folder = 'admin';
        $this->user = Auth::user();
    }

     /**
     * View all users with default range
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $this->params['users'] = User::all();
        return view( $this->folder.'.students.manage', $this->params );
    }

     /**
     * View all users with default range
     */

    public function import()
     {
        
        // Import a Student provided file
        $student = new Student;
        $file = Input::hasFile('import_file');
        $path = Input::file('import_file')->getRealPath();
        $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){

                foreach ($data as $key => $value) {

                    $insert[] = [
                    'username'           => $value->username, 
                    'password'        => Crypt::encrypt($value->password),
                    ];
                }
            }

            if( !empty($insert) ){
                    $student->insert( $insert );

                $this->params['msg'] = 'Students has been added.';
                 $this->params['status'] = 'success';
                return Redirect::to('/admin/student/create')->with( $this->params );
            }

        $this->params['msg'] = 'Students has been added 1.';
        $this->params['status'] = 'success';
        return Redirect::to('/admin/student/create')->with( $this->params );
     }

    /**
     * Display the specified user profile.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view($this->folder.'.students.create');
    }

    public function store()
    {
        $rules = [
                'username' => 'required|min:2|max:50|unique:students',
                'password' => 'required|min:5|max:12',
            ];

        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails() ) {
            $messages = $validator->messages()->getMessages();

            $this->params['error'] = true;
            $this->params['status'] = 'error'; 
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            return Redirect::to('admin/students/create')->withInput()->withErrors($messages);
        }

        $students = new Student;
        $students->username = Input::get('username');
        $students->password = Crypt::encrypt(Input::get('password'));
        $students->save();

        $this->params['msg'] = 'Student has been added.';
        $this->params['status'] = 'success'; 

        return Redirect::to('admin/students/create')->with( $this->params );

    } 

    public function profile( $id )
    {
        $this->params['student'] = Student::where( 'user_id',$id )->first();
        $this->params['preResult'] = Result::where('user_id',$id )->where('status','1')->first();
        $this->params['postResult'] = Result::where('user_id',$id )->where('status','2')->first();
        $this->params['categories'] = Category::all();

        //count items
        $counts = Result::where('user_id', $id)->first();
        
        $this->params['total_items'] = ( $counts ) ? array_sum(array_map("count", $counts->answers)) : 0;

        return view( $this->folder.'.students.profile', $this->params );  
    }
    
    public function edit( $id )
    {

        $this->params['student'] = Student::where('user_id',$id )->firstOrFail();
        return view( $this->folder.'.students.edit', $this->params );
    }

    /**
     * Update user.
     *
     * @param  int  $id
     */
    public function update( $id )
    {

        $rules = [
                'first_name'    => 'required|min:2|max:100|regex:/(^[A-Za-z0-9 ]+$)+/',
                'last_name'     => 'required|min:2|max:100|regex:/(^[A-Za-z0-9 ]+$)+/',
                'age'           => 'required|min:13|integer',
                'gender'        => 'required|min:1',
                'school'        => 'required|min:2|max:100|regex:/(^[A-Za-z0-9 ]+$)+/',
                'teacher_1'     => 'required|min:2|max:100|regex:/(^[A-Za-z0-9 ]+$)+/',
                'teacher_2'     => 'required|min:2|max:100|regex:/(^[A-Za-z0-9 ]+$)+/',
                'teacher_3'     => 'required|min:2|max:100|regex:/(^[A-Za-z0-9 ]+$)+/',
                'teacher_4'     => 'required|min:2|max:100|regex:/(^[A-Za-z0-9 ]+$)+/',
            ];

        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails() ) {
            $messages = $validator->messages()->getMessages();

            $this->params['error'] = true;
            $this->params['status'] = 'error'; 
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            return Redirect::to('admin/student/'.$id.'/edit')->withInput()->withErrors($messages);
        }

        $students = Student::where('user_id',$id )->firstOrFail();
        $students->first_name = strtoupper(Input::get('first_name'));
        $students->last_name = strtoupper(Input::get('last_name'));
        $students->age = Input::get('age');
        $students->gender = Input::get('gender');
        $students->school = strtoupper(Input::get('school'));
        $students->teacher_1 = strtoupper(Input::get('teacher_1'));
        $students->teacher_2 = strtoupper(Input::get('teacher_2'));
        $students->teacher_3 = strtoupper(Input::get('teacher_3'));
        $students->teacher_4 = strtoupper(Input::get('teacher_4'));
        $students->save();

        $this->params['msg'] = 'Profile save.';
        $this->params['status'] = 'success'; 

        return Redirect::to('admin/student/'.$id.'/edit')->with( $this->params );

    }

    public function destroy( $id )
    {
        $user = User::find($id);
        $student = Student::where('user_id',$id);
        if( !$user && !$student ) {
            $this->params['msg'] = 'User not found or already been deleted.';
            $this->params['error'] = true; 
            return response()->json( $this->params );
        }
        $student->delete();
        $user->delete();

        $this->params['msg'] = 'User has been successfully deleted.';
        $this->params['error'] = false; 
        
        return response()->json( $this->params );
    }

}
