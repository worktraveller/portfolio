<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Input;
use App\User;
use App\Student;
use App\Teacher;
use App\Result;
use Crypt;
use Redirect;
use Excel;
use Session;
use Validator;

class AdminImportController extends Controller
{
    public function __construct()
    {
    	        $this->params = [
               
            ];

        $this->folder = 'Admin';
        $this->user = Auth::user();

        if( !Auth::check() ) return Redirect::to('/')->send();
        if( Auth::user()->user_type != 'admin' ) return Redirect::to('/')->send();

    }

    public function import()
     {
        
        // Import a Student provided file
        $user = new User;
        $student = new Student;
        $file = Input::hasFile('import_file');
        $path = Input::file('import_file')->getRealPath();
        $data = Excel::load($path, function($reader) {
            })->get();

            if(!empty($data) && $data->count()){

                foreach ($data as $key => $value) {

                    $insert[] = [
                    'username'        => $value->username, 
                    'password'        => Crypt::encrypt($value->password),
                    ];
                }
            }

            if( !empty($insert) ){
                $user->insert( $insert );
                $id = User::insertGetId($user);
                dd( $insert );
                $student->save();

                $this->params['msg'] = 'Students has been added.';
                 $this->params['status'] = 'success';
                return Redirect::to('/admin/student/create')->with( $this->params );
            }

        $this->params['msg'] = 'Students has been added 1.';
        $this->params['status'] = 'success';
        return Redirect::to('/admin/student/create')->with( $this->params );
     }
}
