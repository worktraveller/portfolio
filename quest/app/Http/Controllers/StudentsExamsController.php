<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Student;
use App\Quest;
use App\Category;
use App\Result;
use Auth;
use Input;
use Redirect;
use Response;


class StudentsExamsController extends Controller
{
    public function __construct()
    {   
        $this->params = [
               'title' => ''
            ];

        $this->folder = 'student';
        $this->user = Auth::user();

        if( !Auth::check() ) return Redirect::to('/')->send();
        if( Auth::user()->user_type != 'student' ) return Redirect::to('/check')->send();

    }
    
    // Pre Examination
    public function preExam()
    {

        //check if done taking Pre Exam
        $result = Result::where( 'user_id', $this->user->id )->orderBy('created_at','asc')->first();


        if( $result ) {

            if( $result->status == 1 ) {

               return Redirect::to('student/proceed');
            }

            if ( $result->status == 2 ) {

                $this->params['msg'] = 'You are done all the exam';
                $this->params['status'] = 'success'; 

                return Redirect::to('student/exam/result');
            }

            return Redirect::to('student/intervention');
        }

        $this->params['title'] = 'PRE EXAM';

        $cats = Category::all();

        //dd( $cats );
        //Science

        $this->params['exam_categories'] = $cats;

        //query for all exam per category
        foreach ( $cats as $cat) {
            $quests = Quest::where('category_id',$cat->id)->get();
            $exam_count = count( $quests );

                foreach ( $quests as $quest ) {

                    if( $quest->category_id == $cat->id ) {   
                        if( $exam_count >= $cat->no_to_show) {
                             
                            if( 2 >= $cat->no_to_show ){
                                $this->params['categories'][$cat->id] = Category::find($cat->id)->quest()->get()->random(2) ; 
                            } else {
                              $this->params['categories'][$cat->id] = Category::find($cat->id)->quest()->get()->random($cat->no_to_show) ;  
                            }                    
                            
                        } else {

                            $this->params['categories'][$cat->id] = Category::find($cat->id)->quest()->get()->random($exam_count);
                        }
                    }
                }
        }    

        return View( $this->folder.'.quest.preexam', $this->params );
    }

    // Post Examination
    public function postExam()
    {

        //check if done taking Pre Exam
        $result = Result::where( 'user_id', $this->user->id )->orderBy('created_at','asc')->first();

        //array_keys serve as question id
        if( !$result ) return Redirect::to('student/exam/pre');
        $imp  = array_keys( $result->answers );
        //dd($result->answers);
        $cats = Category::whereIn( 'id', $imp )->get();

        $quest_id = array();
        foreach ( $result->answers as $value ) {
            foreach ( $value as $x => $k) {
                  $quest_id[] = $x;
            }
        }

        if( $result->status == 1 ) {

           $this->params['title'] = 'POST EXAM';
           $this->params['categories'] = Category::all();
           $this->params['questions'] = Quest::whereIn('id', $quest_id )->get();
            return View( $this->folder.'.quest.postexam', $this->params );

        } else {

           return Redirect::to('student/exam/result'); 
        }
    }     
}
