<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Student;
use App\Teacher;
use App\Quest;
use App\Category;
use App\Result;
use Auth;
use Input;
use Redirect;
use Response;


class StudentsController extends Controller
{
    public function __construct()
    {   
        $this->params = [
               'title' => ''
            ];

        $this->folder = 'student';
        $this->user = Auth::user();

        if( !Auth::check() ) return Redirect::to('/')->send();

    }

     /**
     * View all users with default range
     * @return \Illuminate\Http\Response
     */

     public function index()
     {    
        $this->params['title'] = 'STUDENT PROFILE';
        $student = Student::where('user_id',$this->user->id )->first();
        $this->params['teachers'] = Teacher::all();
        if( !$student ){
            $student = new Student;
            $student->user_id = $this->user->id;
            $student->save();
        }

        $this->params['student'] = $student;
        if( $student->first_name 
                &&  $student->last_name 
                &&  $student->age 
                &&  $student->gender 
                &&  $student->school 
                &&  $student->teacher_1 
                &&  $student->teacher_2 
                &&  $student->teacher_3 
                &&  $student->teacher_4
            ) {

           return Redirect::to('student/exam/pre');
        }

        return view( $this->folder.'.index', $this->params );
     }

    /**
     * Display the specified user profile.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $this->params['title'] = 'STUDENT PROFILE';
        $this->params['teachers'] = Teacher::all();
        $this->params['student'] = Student::where('user_id',$this->user->id )->firstOrFail();
        return view( $this->folder.'.index', $this->params );
    }

    public function proceed()
    {
        $result = Result::where( 'user_id', $this->user->id )->orderBy('created_at','desc')->first();
        
        //check if student was already take the exam
        if( !$result ) return Redirect::to('student/exam/pre');
        if( $result->status == 2 ) return Redirect::to('student/exam/result');
        
        return View( $this->folder.'.quest.proceed', $this->params );
    }

    public function confirm()
    {
        $result = Result::where( 'user_id', $this->user->id )->orderBy('created_at','desc')->first();
        if( $result->status == 2 )return Redirect::to('student/exam/result');

        return View( $this->folder.'.quest.confirm', $this->params );
    }

    public function intervention()
    {
        $this->params['title'] = 'LEARNING INTERVENTION';

        $result = Result::where( 'user_id', $this->user->id)->orderBy('created_at','desc')->first();

        if( !$result ) return Redirect::to('student');
        if( $result->status == 2 )return Redirect::to('student/exam/result');

            $int_status = Result::find($result->id);

            if( $int_status->status == 0 ) {

                $int_status->status = 1;
                $int_status->save();
            }
        
        $this->params['quests'] = Quest::all();
        $this->params['results'] = $result;
        return View( $this->folder.'.quest.intervention', $this->params );
    }

    /**
     * Update user.
     *
     * @param  int  $id
     */
    public function update( $id )
    {       

        $rules = [
                'first_name'    => 'required|min:2|max:100|regex:/(^[A-Za-z0-9 ]+$)+/',
                'last_name'     => 'required|min:2|max:100|regex:/(^[A-Za-z0-9 ]+$)+/',
                'age'           => 'required|min:13|integer',
                'gender'        => 'required|min:1',
                'school'        => 'required|min:2|max:100|regex:/(^[A-Za-z0-9 ]+$)+/',
                'teacher_1'     => 'required|min:2|max:100|regex:/(^[A-Za-z0-9 ]+$)+/',
                'teacher_2'     => 'required|min:2|max:100|regex:/(^[A-Za-z0-9 ]+$)+/',
                'teacher_3'     => 'required|min:2|max:100|regex:/(^[A-Za-z0-9 ]+$)+/',
                'teacher_4'     => 'required|min:2|max:100|regex:/(^[A-Za-z0-9 ]+$)+/',
            ];

        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails() ) {
            $messages = $validator->messages()->getMessages();

            $this->params['error'] = true;
            $this->params['status'] = 'error'; 
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            return Redirect::to('/student/profile')->withInput()->withErrors($messages);
        }

        $students = Student::where('user_id',$this->user->id )->firstOrFail();
        $students->first_name = strtoupper(Input::get('first_name'));
        $students->last_name = strtoupper(Input::get('last_name'));
        $students->age = Input::get('age');
        $students->gender = Input::get('gender');
        $students->school = strtoupper(Input::get('school'));
        $students->teacher_1 = strtoupper(Input::get('teacher_1'));
        $students->teacher_2 = strtoupper(Input::get('teacher_2'));
        $students->teacher_3 = strtoupper(Input::get('teacher_3'));
        $students->teacher_4 = strtoupper(Input::get('teacher_4'));
        $students->save();

        $this->params['msg'] = 'Profile save.';
        $this->params['status'] = 'success'; 

        return Redirect::to('/student/profile')->with( $this->params );
    }

}
