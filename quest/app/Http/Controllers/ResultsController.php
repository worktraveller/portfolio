<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Student;
use App\Result;
use Auth;
use Input;
use Redirect;
use Response;


class ResultsController extends Controller
{
    public function __construct()
    {   
        $this->params = [
               'title' => 'Result'
            ];

        $this->folder = 'student';
        $this->user = Auth::user();
        if( !Auth::check() ) return Redirect::to('/')->send();

    }

     /**
     * View all users with default range
     * @return \Illuminate\Http\Response
     */

     //exam result for student only
     public function index()
     {
        $this->params['title'] = 'MY EXAM RESULT';
        $this->params['student'] = Student::findOrFail( $this->user->id );
        //$this->params['corrects'] = Quest::all();
        //$this->params['results'] = Result::where( 'user_id', $this->user->id )->get();
        return view( $this->folder.'.results.index', $this->params );
     }

    /**
     * Display the specified user profile.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view($this->folder.'.create');
    }

    public function store()
    {

        $resultcheck = Result::where('user_id', $this->user->id)->orderBy('created_at', 'asc')->first();

        $rules = [
            'answer' => 'required|max:255',
        ];


        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails() ) {

            $messages = $validator->messages()->getMessages();

            $this->params['errors'] = true;
            $this->params['status'] = 'error';
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            return Redirect::to('quests')->withErrors( $messages )->withInput();
        }

        if( !$resultcheck ) {

            $answer = Input::get('answer');
            $results = new Result;
            $results->user_id = $this->user->id;
            $results->answers = $answer;
            $results->status = 0;
            $results->save();

            $this->params['msg'] = 'You Successfully take the exam';
            $this->params['status'] = 'success'; 
            $this->params['errors'] = false; 

            return redirect('/student/pre-exam')->with( $this->params );

        } else {
            // check if status is equal to 1
            if( $resultcheck->status == 1 ){

                $results = new Result;
                $results->user_id = $this->user->id;
                $results->answers = Input::get('answer');
                $results->status = 2;
                $results->save();

                $this->params['msg'] = 'You Successfully take the exam';
                $this->params['status'] = 'success'; 
                $this->params['errors'] = false; 

                return redirect('/result')->with( $this->params );
            }
            
        }

        $this->params['msg'] = 'You\'re already take the Exam';
        $this->params['status'] = 'error'; 

        return redirect('/student/pre-exam')->with( $this->params );
    } 

    public function show( $id )
    {

    }

    /**
     * Update user.
     *
     * @param  int  $id
     */
    public function update( $id )
    {

    }

}
