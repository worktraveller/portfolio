<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Quest;
use Input;
use Image;
use DB;
use Crypt;
use Excel;
use Auth;
use Redirect;

class StudentQuestsController extends Controller
{
    public function __construct()
    {

        $this->folder = 'quests';
        $this->user = Auth::user();
        if( !Auth::check() ) return Redirect::to('student/login')->send();
    }

     /**
     * View all users with default range
     * @return \Illuminate\Http\Response
     */

     public function index()
     {
        $this->params['questions'] = Quest::all();
        return view( $this->folder.'.index', $this->params );
     }

    /**
     * Display the specified user profile.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view($this->folder.'.create');
    }

    public function store()
    {

        // Import a menu provided file
        $quest = new Quest;
        $file = Input::hasFile('import_file');
        $path = Input::file('import_file')->getRealPath();
        $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){

                foreach ($data as $key => $value) {
                    $answers = array(
                        'A' => $value->a, 
                        'B' => $value->b, 
                        'C' => $value->c, 
                        'D' => $value->d, 
                        );

                    $insert[] = [
                    'user_id'           => $this->user->id, 
                    'questions'          => $value->questions,
                    'image'             => '',
                    'answers'           => serialize($answers),
                    'correct_answer'    => $value->correct,
                    ];
                }
            }

            if( !empty($insert) ){
                    $quest->insert( $insert );
            }

        $this->params['msg'] = 'Batch menu has been added.';
        $this->params['error'] = false; 

        return Redirect::to('quests/create')->with( $this->params );

 /*       $user = new User;
        $user->fullname = 'Peter Jude';
        $user->email = 'student@gmail.com';
        $user->password = Crypt::encrypt('123456');
        $user->user_type = 'student';
        $user->save();*/
    } 

    public function show( $id )
    {

    }

    /**
     * Update user.
     *
     * @param  int  $id
     */
    public function update( $id )
    {

    }

}
