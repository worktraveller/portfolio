<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Quest;
use App\Category;
use Input;
use Image;
use DB;
use Crypt;
use Excel;
use Auth;
use Redirect;
use Serialize;

class AdminQuestsController extends Controller
{
    public function __construct()
    {   

        $this->params = [
                'title' => '',
                'module_name' => 'exam'
            ];

        $this->folder = 'admin';
        $this->user = Auth::user();
    }

     /**
     * View all users with default range
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        
        $this->params['questions'] = Quest::all();
        return view( $this->folder.'.quests.index', $this->params );
    }

     /**
     * View all users with default range
     */

    public function import()
     {
        $quest = new Quest;
        $file = Input::hasFile('import_file');
        $path = Input::file('import_file')->getRealPath();

        $data = Excel::load($path, function($reader) {
            })->get();
        //dd( $data );
            if(!empty($data) && $data->count()){

                foreach ($data as $key => $value) {
                    
                    $category = new Category;
                    $category_id = Category::where('name',$value->category)->first();

                    if( !$category_id ){
                        $category = new Category;
                        $category->name = $value->category;
                        $category->no_to_show = 2;
                        $category->color = '#cccccc';
                        $category->save();
                    }

                    $answers = array(
                        'A' => $value->a, 
                        'B' => $value->b, 
                        'C' => $value->c, 
                        'D' => $value->d, 
                        );

                    $insert[] = [
                    'user_id'           => $this->user->id,
                    'category_id'       => ( $category_id ) ? $category_id->id : $category->id,
                    'questions'         => $value->questions,
                    'image'             => '',
                    'answers'           => serialize($answers),
                    'correct_answer'    => $value->correct,
                    'link_1'            => $value->link_1,
                    'link_2'            => $value->link_2,
                    'link_3'            => $value->link_3,
                    ];
                }
            }

            if( !empty($insert) ){
                    $quest->insert( $insert );

                $this->params['msg'] = 'Batch menu has been added.';
                $this->params['status'] = 'success'; 

                return Redirect::to('admin/quests/create')->with( $this->params );
            }

        $this->params['msg'] = 'Batch menu has been added.';
        $this->params['status'] = 'error'; 

        return Redirect::to('admin/quests/create')->with( $this->params );
     }

    /**
     * Display the specified user profile.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $this->params['categories'] = Category::all();

        return view($this->folder.'.quests.create', $this->params );
    }

    public function store()
    {

        $quest = new Quest;

        $rules = [
                'question' => 'required|min:2|max:255',
                'A' => 'required|min:1',
                'B' => 'required|min:1',
                'C' => 'required|min:1',
                'D' => 'required|min:1',
                'category' => 'required|integer',
                'correct_answer' => 'required',
            ];

        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails() ) {
            $messages = $validator->messages()->getMessages();

            $this->params['error'] = true;
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            return Redirect::to('admin/quests/create')->withInput()->withErrors($messages);
        }

        $answers = array(
                'A' => Input::get('A'), 
                'B' => Input::get('B'), 
                'C' => Input::get('C'), 
                'D' => Input::get('D')
            );

            $quest->user_id = $this->user->id;
            $quest->category_id = Input::get('category');
            $quest->questions = Input::get('question');
            $quest->answers =  $answers;
            $quest->correct_answer = Input::get('correct_answer');

        if(!$_FILES['image']['name']) {

            $quest->image = '';
            $quest->save();
        }

        try{
            // check if an image is added.
            $destination_path = public_path('uploads/exams/');
            $img = Image::make( $_FILES['image']['tmp_name'] );
            $imageid = time().'_'.md5( $this->user->id.rand(0,100) ).'_'.rand(0,100);
            $ext = strtolower( pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION) );
            $filename = $imageid.".".$ext;
            $target = $destination_path.$filename;

            // Check if file extension is valid
            if( !in_array( $ext, array('jpg', 'jpeg', 'png') )  ) {
              $this->params['error'] = true;
              $this->params['msg'] = 'File extension is not valid.';
            }

            // Now save the iamge file
            $success = $img->save( $target );

            if( $success ) {

                $quest->image = $filename;
                $quest->save();
            } 

        } catch( \Exception $e ) {

            $this->params['msg'] = 'Error uploading image, please try again.';
            $this->params['status'] = 'error';

            return Redirect::to('admin/quests/create')->with( $this->params );
        }

        $this->params['msg'] = 'Quest has been added.';
        $this->params['status'] = 'success'; 

        return Redirect::to('admin/quests/create')->with( $this->params );
    } 
    public function edit( $id )
    {
        $this->params['quest'] = Quest::findOrFail($id);
        $this->params['categories'] = Category::all();
        return view( $this->folder.'.quests.edit', $this->params );

    }
    public function show( $id )
    {

    }

    /**
     * Update user.
     *
     * @param  int  $id
     */
    public function update( $id )
    {
        $quest = Quest::findOrFail( $id );

        $rules = [
                'question' => 'required|min:2|max:255',
                'A' => 'required|min:1',
                'B' => 'required|min:1',
                'C' => 'required|min:1',
                'D' => 'required|min:1',
                'category' => 'required|integer',
                'correct_answer' => 'required',
            ];

        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails() ) {
            $messages = $validator->messages()->getMessages();

            $this->params['error'] = true;
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            return Redirect::to('admin/quests/'.$id.'/edit')->withInput()->withErrors($messages);
        }

        $answers = array(
                'A' => Input::get('A'), 
                'B' => Input::get('B'), 
                'C' => Input::get('C'), 
                'D' => Input::get('D')
            );

            $quest->category_id = Input::get('category');
            $quest->questions = Input::get('question');
            $quest->image = $quest->image;
            $quest->answers =  $answers;
            $quest->correct_answer = Input::get('correct_answer');

            if(!$_FILES['image']['name']) {

                $quest->save();
            }

            try{
                // check if an image is added.
                $destination_path = public_path('uploads/exams/');
                $img = Image::make( $_FILES['image']['tmp_name'] );
                $imageid = time().'_'.md5( $this->user->id.rand(0,100) ).'_'.rand(0,100);
                $ext = strtolower( pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION) );
                $filename = $imageid.".".$ext;
                $target = $destination_path.$filename;

                // Check if file extension is valid
                if( !in_array( $ext, array('jpg', 'jpeg', 'png') )  ) {
                  $this->params['error'] = true;
                  $this->params['msg'] = 'File extension is not valid.';
                }

                // Now save the iamge file
                $success = $img->save( $target );

                    if( $success ) {

                        $quest->image = $filename;
                        $quest->save();

                    } 

            } catch( \Exception $e ) {
                
                $this->params['msg'] = 'Error uploading image, please try again.';
                $this->params['status'] = 'error';

                return Redirect::to('admin/quests/create')->with( $this->params );
            }

        $this->params['msg'] = 'Quest has been updated.';
        $this->params['status'] = 'success'; 

        return Redirect::to('admin/manage/quests')->with( $this->params );
    }

    public function destroy( $id )
    {
        $quest = Quest::find($id);
        if( !$quest ) {
            $this->params['msg'] = 'Admin not found or already been deleted.';
            $this->params['error'] = true; 
            return response()->json( $this->params );
        }
        $quest->delete();

        $this->params['msg'] = 'Admin has been successfully deleted.';
        $this->params['error'] = false; 
        
        return response()->json( $this->params );
    }

}
