<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Teacher;
use App\Result;
use App\Category;
use Input;
use Image;
use DB;
use Crypt;
use Excel;
use Auth;
use Redirect;

class AdminTeachersController extends Controller
{
    public function __construct()
    {
        $this->params =[
                'title' => 'Teacher'
            ];
        $this->folder = 'admin';
        $this->user = Auth::user();
    }

     /**
     * View all users with default range
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $this->params['users'] = User::all();
        return view( $this->folder.'.teacher.manage', $this->params );
    }

    /**
     * Display the specified user profile.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view( $this->folder.'.teacher.create' );
    }

    public function profile( $id )
    {
        $this->params['teacher'] = Teacher::where( 'user_id',$id )->first();
        $this->params['preResult'] = Result::where('user_id',$id )->where('status','1')->first();
        $this->params['postResult'] = Result::where('user_id',$id )->where('status','2')->first();
        $this->params['categories'] = Category::all();

        //count items
        $counts = Result::where('user_id', $id)->first();
        
        $this->params['total_items'] = ( $counts ) ? array_sum(array_map("count", $counts->answers)) : 0;

        return view( $this->folder.'.teacher.profile', $this->params );  
    }

    public function store()
    {

        $rules = [
                'username' => 'required|min:2|max:50|unique:users',
                'password' => 'required|min:5|max:12',
            ];

        $validator = Validator::make(Input::all(), $rules);
       
        if( $validator->fails() ) {
            $messages = $validator->messages()->getMessages();

            $this->params['error'] = true;
            $this->params['status'] = 'error'; 
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            return Redirect::to('admin/teacher/create')->withInput()->withErrors($messages);
        }

        $teacher = new User;
        $teacher->username = Input::get('username');
        $teacher->password = Crypt::encrypt(Input::get('password'));
        $teacher->user_type = "teacher";
        $teacher->save();

        dd();
        $profile = new Teacher;
        $profile->user_id = $teacher->id;
        $profile->save();

        $this->params['msg'] = 'Student has been added.';
        $this->params['status'] = 'success'; 

        return Redirect::to('admin/teacher/create')->with( $this->params );

    } 

    public function show( $id )
    {

    }
    public function edit( $id )
    {
        /*$this->params['user'] = User::findOrFail( $id );
        return view( $this->folder.'.students.edit', $this->params );*/
    }

    /**
     * Update user.
     *
     * @param  int  $id
     */
    public function update( $id )
    {

       /* $user = User::findOrFail( $id );

        $rules = [
                'fullname' => 'required|min:2|max:50',
                'email' => 'required|min:2|max:50|unique:users,email,'.$user->id.',id',
            ];

        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails() ) {
            $messages = $validator->messages()->getMessages();

            $this->params['error'] = true;
            $this->params['status'] = 'error'; 
            $this->params['msg'] = 'Form validation error. Please fix.';
            $this->params['form_errors'] = $messages;
            return Redirect::to('admin/students/'.$id.'/edit')->withInput()->withErrors($messages);
        }

        $user->fullname = Input::get('fullname');
        $user->email = Input::get('email');
        $user->save();

        $this->params['msg'] = 'User has been successfully updated.';
        $this->params['status'] = 'success'; 
        // Redirect to user lists
        return Redirect::to('/admin/students/manage')->with( $this->params );*/

    }

    public function destroy( $id )
    {
        /*$user = User::find($id);
        if( !$user ) {
            $this->params['msg'] = 'User not found or already been deleted.';
            $this->params['error'] = true; 
            return response()->json( $this->params );
        }
        $user->delete();

        $this->params['msg'] = 'User has been successfully deleted.';
        $this->params['error'] = false; 
        
        return response()->json( $this->params );*/
    }

}
