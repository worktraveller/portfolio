<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use URL;
use Input;
use Response;
use Redirect;
use Request;
use Validator;


class PagesController extends Controller
{
	public function __construct() 
    {
		$this->params = array(
			'error'         => false,
            'redirect'      => false,
            'redirect_to'   => '/',
            'form_errors'   => null,
            'page_title'    => 'Jeeger\'s Travel' 
		);

        $this->folder = 'front';
	}
/*
|-----------------------------
| Homepage 
|-----------------------------
*/
    public function index() 
    {
        return view('login');
   	}

    public function studentLogin()
    {
        return view('student.login');
    } 

}
