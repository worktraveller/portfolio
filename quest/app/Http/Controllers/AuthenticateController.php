<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Input;
use App\User;
use App\Student;
use Crypt;
use Redirect;
use Response;
use Session;

class AuthenticateController extends Controller
{
    public function __construct()
    {
        $this->params = [
            'error'                 => false, 
            'status_code'           => 200,
            'msg'                   => '', 
            'is_logged'             => false,
       ];
    }

    public function usercheck( ){
        
        $user = Auth::user();
        if( $user->user_type == "admin" ) {
                return Redirect::to('admin');
            } 

            //login as student
            if( $user->user_type == "student" ) {

                return Redirect::to('/student');
            }

            //login as teacher
            if( $user->user_type == "teacher" ) {

                return Redirect::to('/teacher');
            } 
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('username', 'password');

        $password = Input::get('password');
        $username = Input::get('username');

        $user = User::where('username', strtolower($username))->first();

        if( !$user || $user->password == null || $user->password == '' ) {
            //dd('a');
            $this->params['msg'] = 'Username and password not match.';
            $this->params['error'] = true;
            $this->params['forced_login'] = false;
            return Redirect::to('/')->withErrors( $this->params['msg'] );

        }
         
        $decrypted_password = Crypt::decrypt( $user->password );

        if( $password !== $decrypted_password ) {
            
            $this->params['msg'] = 'Username and password not match.';
            $this->params['error'] = true;
            $this->params['forced_login'] = false;
            return Redirect::to('/')->withErrors( $this->params['msg'] );

            //return response()->json( $this->params );
        }

        Auth::login($user);
       
        if ( !Auth::check() ) {
             return Redirect::to('/')->send();
        } else {
            $this->params['msg'] = 'Successfully logged in.';
            $this->params['is_logged'] = true;
            $this->params['error'] = false;
            $this->params['forced_login'] = false;
            $this->params['user'] = $user;
            
            //login as admin
            if( $user->user_type == "admin" ) {
                return Redirect::to('/admin/user');
            } 

            //login as student
            if( $user->user_type == "student" ) {

                return Redirect::to('/student');
            }

            //login as teacher
            if( $user->user_type == "teacher" ) {

                return Redirect::to('/teacher');
            } 
        }

        return Redirect::to('/')->withErrors( $this->params );
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('/');
    }
}