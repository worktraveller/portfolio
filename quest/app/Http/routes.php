<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
    /*
    |Admin Student Route 
    */
    
    Route::get('/admin/dashboard','AdminUsersController@index');
    Route::resource('/admin/user','AdminUsersController');

    Route::post('/admin/student/import','AdminStudentsController@import');
    Route::get('/admin/student/destroy/{id}','AdminStudentsController@destroy');

    Route::get('/admin/student/{id}/profile','AdminStudentsController@profile');
    Route::get('/admin/student/manage','AdminStudentsController@index');
    Route::resource('/admin/student','AdminStudentsController');

    /*
    |Admin Route Teacher
    */
    Route::get('/admin/teacher/{id}/profile','AdminTeachersController@profile');
    Route::get('/admin/teacher/manage','AdminTeachersController@index');
    Route::resource('/admin/teacher','AdminTeachersController');

    /*
    |Quest Route
    */
    Route::post('/admin/import','AdminImportController@import');

    Route::get('/admin/manage/quests','AdminQuestsController@index');
    Route::get('/admin/quests/destroy/{id}','AdminQuestsController@destroy');
    Route::post('/admin/quests/import','AdminQuestsController@import');
    Route::resource('/admin/quests','AdminQuestsController');
    
    /*
    |Student Route
    */
    Route::post('/student/exam/pre','StudentsResultsController@store');
    Route::get('/student/exam/result','StudentsResultsController@index');

    Route::get('/student/exam/pre','StudentsExamsController@preExam');
    Route::get('/student/exam/post','StudentsExamsController@postExam');

    Route::get('/student/proceed','StudentsController@proceed');
    Route::get('/student/confirmation','StudentsController@confirm');
    Route::get('/student/intervention','StudentsController@intervention');
    Route::get('/student/profile','StudentsController@profile');
    Route::resource('/student','StudentsController');

    /*
    |Teacher Route
    */
    //submit exam save answer pre and post
    Route::post('/teacher/exam/pre','TeachersResultsController@store');
    Route::get('/teacher/exam/result','TeachersResultsController@index');
    
    Route::get('/teacher/exam/pre','TeachersExamsController@preexam');
    Route::get('/teacher/exam/post','TeachersExamsController@postexam');

    Route::get('/teacher/profile','TeachersController@profile');
    Route::get('/teacher/proceed','TeachersController@proceed');
    Route::get('/teacher/intervention','TeachersController@intervention');
    Route::get('/teacher/confirmation','TeachersController@confirmation');
    Route::resource('/teacher','TeachersController');

    /*
    | ICT DIFFICULTY
    */
    Route::resource('/ict/skill','SkillsController');

    /*
    |Login / Logout Route
    */
    
    Route::get('/','PagesController@index');
    Route::get('/students/login','PagesController@studentLogin');

    Route::get('/check', 'AuthenticateController@usercheck');
    Route::post('/authenticate', 'AuthenticateController@authenticate');
    Route::post('/studentauthenticate', 'AuthenticateController@studentauthenticate');
    Route::get('/logout', 'AuthenticateController@Logout');



