<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'username', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /*public function quest()
    {
        return $this->hasMany('App\Quest', 'user_id', '_id');
    }

    public function result()
    {
        return $this->hasOne('App\Result', 'user_id', 'id');
    }*/

    public function student()
    {
        return $this->HasMany('App\Student');
    }
    public function teacher()
    {
        return $this->HasMany('App\Teacher');
    }
}
