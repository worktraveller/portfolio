<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Teacher extends Authenticatable
{
     protected $table = 'teachers';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /*protected $fillable = [
        'id','username', 'password',
    ];*/

    public function result()
    {
        return $this->hasMany('App\Result', 'user_id', 'id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'teacher_id');
    }
}
