var host = 'http://'+top.location.host+"";
var xhr = null;

var error_msgs = new Array("Naaa!","Oops!","Baaam!");
$(document).ready(function(){

	$('.lists-item').on('click', '.delete-this-item', function() {
		var $this = $(this);
		if( !confirm( $this.attr('data-confirmation-msg') ) ) return false;

		var id = $this.attr('data-id');
		var url = $(this).attr('data-action'); 

		$.get( url , function(data) {
			if( data.error === false ){
				$this.parents('.lists-item').hide('fade');
			} else {
				alert( data.msg );
			}
		},"json");

		return false;
	});
}); // end document ready	

	
