$( document ).ready(function() {
        
    var ctx3 = document.getElementById("chart3").getContext("2d");
    var data3 = [
        {
            value: 43,
            color:"#F25656",
            highlight: "#FD7A7A",
            label: "Grade 7"
        },
        {
            value: 17,
            color: "#22BAA0",
            highlight: "#36E7C8",
            label: "Grade 8"
        },
        {
            value: 20,
            color: "#F2CA4C",
            highlight: "#FBDB6E",
            label: "Grade 9"
        },
        {
            value: 30,
            color: "#20599c",
            highlight: "#3c7cc7",
            label: "Grade 10"
        }
    ];
    
    var myPieChart = new Chart(ctx3).Pie(data3,{
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 2,
        animationSteps : 100,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });    
});