<?php

use Illuminate\Database\Seeder;
use Crypt;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'fullname' => 'Peter Jude',
            'email' => 'admin@gmail.com',
            'password' => Crypt::encrypt('123456'),
            'user_type' => 'admin',
        ]);
    }
}
