@extends('layouts.student')
@section('content')
<div class="container text-center proceed">
	<p><a href="{{ url('student/intervention') }}" class="btn btn-success btn-rounded btn-large">Continue Intervention</a></p>
	<h2><strong>or</strong></h2> 
	<p><a href="{{ url('student/confirmation') }}" class="btn btn-info btn-rounded btn-large">Are you Ready to take a Post Test</a></p>
</div>
@endsection('content')