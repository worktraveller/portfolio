@extends('layouts.student')
@section('content')
<div class="container text-center proceed">
	<p><a href="{{ url('student/exam/post') }}" class="btn btn-success btn-rounded btn-large">Are you sure you want to Proceed</a></p>
</div>
@endsection('content')