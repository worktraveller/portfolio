@extends('layouts.student')
@section('content')
<div class="container">
	{!! Form::open(['url' => '/student/exam/pre', 'id'=>'question_answer', 'method' => 'post', 'class' => '']) !!}

	<?php  $count = 1; ?>

	@foreach( $exam_categories  as $exam_category )
		@if( $categories )

			<?php  $cat_id = array_keys($categories); ?>
			@foreach( $cat_id as $id )
				@if( $id == $exam_category->id )
					<div class="panel panel-white">
					    <div class="panel-heading label-success">
					        <div class="panel-title">{{ strtoupper($exam_category->name) }}</div>
					    </div>
					</div>
				@endif
			@endforeach

			@foreach( $categories as $category )
				@foreach( $category as $cat )
					@if( $cat->category_id == $exam_category->id )
						<div class="panel panel-white">
						    <div class="panel-heading">
						        <div class="panel-title">{{ $count++ }}. {{ $cat->questions }}</div>
						    </div>
						    <div class="panel-body">
						    	<div class="col-sm-6"> 
						    	    <label class="radio-inline">
								      <input type="radio" name="answer[{{ $exam_category->id }}][{{ $cat->id }}]" value="A" class="form-input" required />A. {{ $cat->answers['A'] }}
								    </label>
								</div>
								<div class="col-sm-6">
								    <label class="radio-inline">
								      <input type="radio" name="answer[{{ $exam_category->id }}][{{ $cat->id }}]" value="B" class="form-input" />B. {{ $cat->answers['B'] }}
								    </label>
								</div>
								<div class="col-sm-6">
								    <label class="radio-inline">
								      <input type="radio" name="answer[{{ $exam_category->id }}][{{ $cat->id }}]" value="C" class="form-input" />C. {{ $cat->answers['C'] }}
								    </label>
								</div>
								<div class="col-sm-6">
						          	<label class="radio-inline">
								      <input type="radio" name="answer[{{ $exam_category->id }}][{{ $cat->id }}]" value="D" class="form-input" />D. {{ $cat->answers['D'] }}
								    </label>
								</div>
							</div>
						</div>
					@endif
				@endforeach
			@endforeach
		@endif
	@endforeach

			<button type="submit" class="btn btn-default btn-md">Submit</button>
	{!! form::close() !!}
</div>
@endsection
