@extends('layouts.student')
@section('content')
<div class="container intervention">
	<div class="panel panel-white">
		<div class="panel-body">
			<table class="table">
			    <thead>
			        <tr>
			            <th class="text-center">#</th>
			            <th>Question</th>
			            <th class="text-center">Your Answer</th>
			            <!--th class="text-center">Correct Answer</th-->
			            <th class="text-center">Links</th>
			        </tr>
			    </thead>
			    <tbody>
			    	<?php $count = 1; ?>
			    	
			        @foreach( $results->answers as $id => $result )
			            @foreach( $quests as $quest )
				            @foreach( $result as $key => $res )
				            	@if( $quest->category_id == $id )
						            @if( $quest->id == $key ) 
						            
						            		<tr class="{{ $res != strtoupper( $quest->correct_answer ) ? 'danger' : 'success' }}">
						    		            <th class="text-center">{{ $count++ }}</th>
						    		            <td class="">{{ $quest->questions  }}</td>
						    		            <td class="text-center">{{ $res }}</td>
						    		            <td class="">
						                            @if( $res != strtoupper( $quest->correct_answer ) )						                         
						                                <a href="{{ $quest->link_1 }}" target="_blank">Intervention</a><br />
						                                <a href="{{ $quest->link_2 }}" target="_blank">Intervention</a><br />
						                                <a href="{{ $quest->link_3 }}" target="_blank">Intervention</a>	
						                            @else
						                            	Correct
						                            @endif
						                        </td>
						    		        </tr>
						            @endif
						        @endif
				            @endforeach
			            @endforeach
			        @endforeach
			    </tbody>
			</table>
			<a href="{{ url('/student/confirmation') }}" class="btn btn-danger btn-rounded btn-large pull-right">Proceed to Post Test</a>
		</div>
	</div>
</div>
@endsection('content')
