@extends('layouts.student')
@section('content')
<div class="container">
	{!! Form::open(['url' => '/student/exam/pre', 'id'=>'question_answer', 'method' => 'post', 'class' => '']) !!}
	<?php  $count = 1; ?>
	@foreach( $categories as $category )

		<div class="panel ">
		    <div class="panel-heading label-info">
		        <div class="panel-title text-uppercase">{{ $category->name }}</div>
		    </div>
		</div>	

		@foreach( $questions  as $question )
			@if( $category->id == $question->category_id )
			<div class="panel panel-white">
			    <div class="panel-heading">
			        <div class="panel-title">{{ $count++ }}. {{ $question->questions }}</div>
			    </div>
			    <div class="panel-body">
			    	<div class="col-sm-6">
			    	    <label class="radio-inline">
					      <input type="radio" name="answer[{{ $category->id }}][{{ $question->id }}]" value="A" class="form-input" required />A. {{ $question->answers['A'] }}
					    </label>
					</div>
					<div class="col-sm-6">
					    <label class="radio-inline">
					      <input type="radio" name="answer[{{ $category->id }}][{{ $question->id }}]" value="B" class="form-input" />B. {{ $question->answers['B'] }}
					    </label>
					</div>
					<div class="col-sm-6">
					    <label class="radio-inline">
					      <input type="radio" name="answer[{{ $category->id }}][{{ $question->id }}]" value="C" class="form-input" />C. {{ $question->answers['C'] }}
					    </label>
					</div>
					<div class="col-sm-6">
			          	<label class="radio-inline">
					      <input type="radio" name="answer[{{ $category->id }}][{{ $question->id }}]" value="D" class="form-input" />D. {{ $question->answers['D'] }}
					    </label>
					</div>
			    </div>
			</div>
			@endif
		@endforeach
		
	@endforeach
			<button type="submit" class="btn btn-default btn-md">Submit</button>
	{!! form::close() !!}
</div>
@endsection
