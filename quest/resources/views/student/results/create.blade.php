@extends('layouts.dashboard')
@section('content')
<div class="row  border-bottom dashboard-header">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="panel-body">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            {!! Form::open(['url' => '/quests', 'id'=>'create_quest_form', 'method' => 'post', 'class' => '','enctype'=>'multipart/form-data']) !!}
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="control-label">Import Guests</label>
                                    <input type="file" class="form-control" name="import_file">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </div>
                            </div>
                            {!! form::close() !!}            
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>
@endsection