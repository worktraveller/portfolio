@extends('layouts.student')
@section('content')

<?php $count = 1; ?>
<?php $x = 1; ?>


<div class="container">
    <div class="row">
        {!! Form::open(['url' => '/ict/skill', 'id'=>'question_answer', 'method' => 'post', 'class' => '']) !!}

            <div class="panel ">
                <div class="panel-heading label-info">
                    <div class="panel-title text-uppercase">PC Operations</div>
                </div>
            </div>  

            @foreach( $skills as $skill )
                @if( $skill->category == "PC Operations" )
                <div class="panel panel-white">
                    <div class="panel-heading">
                        <div class="panel-title">{{ $count++ }}. {{ $skill->question }}</div>
                    </div>
                    <div class="panel-body">

                        <label class="radio-inline">
                            <input type="radio" name="skill[{{ $skill->id }}]" value="1" class="form-input" required /> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="skill[{{ $skill->id }}]" value="0" class="form-input" required /> No
                        </label>

                    </div>   
                </div>
                @endif
            @endforeach

            <div class="panel ">
                <div class="panel-heading label-success">
                    <div class="panel-title text-uppercase">Internet Applications</div>
                </div>
            </div>

            @foreach( $skills as $skill )
                @if( $skill->category == "Internet Applications" )
                <div class="panel panel-white">
                    <div class="panel-heading">
                        <div class="panel-title">{{ $x++ }}. {{ $skill->question }}</div>
                    </div>
                    <div class="panel-body">

                        <label class="radio-inline">
                            <input type="radio" name="skill[{{ $skill->id }}]" value="1" class="form-input" required /> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="skill[{{ $skill->id }}]" value="0" class="form-input" required /> No
                        </label>

                    </div>   
                </div>
                @endif
            @endforeach

            <button type="submit" class="btn btn-default btn-md">Submit</button>
        {!! Form::close() !!}
    </div><!-- Row -->
</div>

@endsection('content')

