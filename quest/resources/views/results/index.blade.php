@extends('layouts.dashboard')
@section('content')
	
	<table class="table">
        <thead>
            <tr>
                <th class="text-center">#</th>
                <th>Question</th>
                <th class="text-center">Your Answer</th>
                <th class="text-center">Correct Answer</th>
                <th class="text-center">Links</th>
            </tr>
        </thead>
        <tbody>
        	<?php $count = 1; ?>
            
            @foreach( $results as $result )
            	@foreach( $corrects as $correct )
                {{ dd( $result->answers ) }}
            	    <tr>
    		            <th class="text-center">{{ $count++ }}</th>
    		            <td>{{ $correct->questions }}</td>
    		            <td class="text-center">{{ $result->answers[$correct->id]  }}</td>
                        <td class="text-center">{{ strtoupper($correct->correct_answer) }}</td>
    		            <td class="text-center">
                            @if( $result->answers[$correct->id] == strtoupper($correct->correct_answer) )
                                correct
                            @else
                                display links
                            @endif
                        </td>
    		        </tr>
    			@endforeach
            @endforeach
        </tbody>
    </table>
@endsection