@extends('layouts.teacher')
@section('content')
<div class="container">
    <div class="row">
       <div class="panel panel-white">
            <div class="panel-body">

                {!! Form::model($teacher, array('route' => array( 'teacher.update', $teacher->id ), 'method' => 'PATCH')) !!}
                
                    <div class="row">
                        <div class="form-group col-md-6 col-sm-12">
                            <label for="exampleInputEmail1">First Name</label>
                            <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Enter First Name" value="{{ ( $teacher->first_name ) ? $teacher->first_name : Input::old('first_name') }}">
                        </div>  
                        <div class="form-group col-md-6 col-sm-12">
                            <label for="exampleInputEmail1">Last Name</label>
                            <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter Last Name" value="{{ ( $teacher->last_name ) ? $teacher->last_name : Input::old('last_name') }}">
                        </div>
                    </div>  

                    <div class="row">
                        <div class="form-group col-md-6 col-sm-12">
                            <label for="exampleInputEmail1">Age</label>
                            <select name="age" class="form-control m-b-sm" required="required">
                                <option value="">--Select--</option>
                                @for( $age = 1; 65 >= $age; $age++ )
                                    <option value="{{ $age }}" {{ Input::old('a') == 1 ? 'selected' : '' }} {{ $teacher->gender == 1 ? 'selected' : '' }}>{{ $age }}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label for="exampleInputEmail1">Gender</label>
                            <select name="gender" class="form-control m-b-sm" required="required">
                                <option value="">--Select--</option>
                                <option value="1" {{ Input::old('gender') == 1 ? 'selected' : '' }} {{ $teacher->gender == 1 ? 'selected' : '' }}>Male</option>
                                <option value="2" {{ Input::old('gender') == 2 ? 'selected' : '' }} {{ $teacher->gender == 2 ? 'selected' : '' }}>Female</option>
                            </select>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="form-group col-md-3 col-sm-12">
                            <label for="exampleInputEmail1">Highest educational attainment</label>
                            <select name="highest_attainment" class="form-control m-b-sm" required="required">
                                <option value="">--Select--</option>
                                <option value="High school degree" {{ Input::old('highest_attainment') == 'High school degree' ? 'selected' : '' }} {{ $teacher->highest_attainment == 'High school degree' ? 'selected' : '' }}>High school degree</option>
                                <option value="Associate degree" {{ Input::old('highest_attainment') == 'Associate degree' ? 'selected' : '' }} {{ $teacher->highest_attainment == 'Associate degree' ? 'selected' : '' }}>Associate degree</option>
                                <option value="Bachelors degree" {{ Input::old('highest_attainment') == 'Bachelors degree' ? 'selected' : '' }} {{ $teacher->highest_attainment == 'Bachelors degree' ? 'selected' : '' }}>Bachelor's degree</option>
                                <option value="Masters degree" {{ Input::old('highest_attainment') == 'Masters degree' ? 'selected' : '' }} {{ $teacher->highest_attainment == 'Masters degree' ? 'selected' : '' }}>Master's degree</option>
                                <option value="Doctorate" {{ Input::old('highest_attainment') == 'Doctorate' ? 'selected' : '' }} {{ $teacher->highest_attainment == 'Doctorate' ? 'selected' : '' }}>Doctorate</option>
                            </select>    
                        </div>
                        <div class="form-group col-md-3 col-sm-12">
                            <label for="exampleInputEmail1">Subject taught</label>
                            <input type="text" class="form-control" name="subject_taught" id="subject_taught" placeholder="Enter Subject Taught" value="{{ ( $teacher->subject_taught ) ? $teacher->subject_taught : Input::old('subject_taught') }}">
                        </div>
                        <div class="form-group col-md-3 col-sm-12">
                            <label for="exampleInputEmail1">Specialization</label>
                            <input type="text" class="form-control" name="specialization" id="specialization" placeholder="Enter Specialization" value="{{ ( $teacher->specialization ) ? $teacher->specialization : Input::old('specialization') }}">
                        </div>
                        <div class="form-group col-md-3 col-sm-12">
                            <label for="exampleInputEmail1">Years in Service</label>
                            <select name="years_in_service" class="form-control m-b-sm" required="required">
                                <option value="">--Select--</option>
                                @for( $i = 1; 50 >= $i; $i++ )
                                   <option value="{{ $i }}" {{ Input::old('years_in_service') == $i ? 'selected' : '' }} {{ $teacher->years_in_service == $i ? 'selected' : '' }}>{{ $i }}</option>
                                @endfor
                            </select> 
                        </div> 
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12 col-sm-12">
                            <label for="exampleInputEmail1">Present School</label>
                            <input type="text" class="form-control" id="school" name="present_school" placeholder="Enter Name of Present School" value="{{  ( $teacher->present_school ) ? $teacher->present_school : Input::old('present_school') }}">
                        </div>  
                    </div>     

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div><!-- Row -->
</div>
@endsection('content')