@extends('layouts.teacher')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title text-center">My Profile</div>
                </div>
                <ul class="list-group">
                    <li class="list-group-item text-uppercase">
                        <table style="width: 100%;">
                            <tr>
                                <td><b>Name:</b> {{ $teacher->first_name.' '.$teacher->last_name }}</td>
                                <td><b>Gender:</b> {{ $teacher->gender == 1 ? 'male' : female }}</td>
                                <td><b>Age:</b> {{ $teacher->age }}</td>
                            </tr>
                        </table>
                    </li>
                    <li class="list-group-item text-uppercase">
                        <b>Highest Educational Attained:</b> {{ $teacher->highest_attainment }}
                    </li>
                    <li class="list-group-item text-uppercase">
                        <b>Subject Taught:</b> {{ $teacher->subject_taught }}
                    </li>
                    <li class="list-group-item text-uppercase">
                        <b>Specialization:</b> {{ $teacher->specialization }}
                    </li>
                    <li class="list-group-item text-uppercase">
                        <b>Years in Service:</b> {{ $teacher->years_in_service }}
                    </li>
                    <li class="list-group-item text-uppercase">
                        <b>School:</b> {{ $teacher->present_school }}
                    </li>
                </ul>

            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title text-center">{{ ($preResult) ? 'Total score: '. $preResult->score.'/'.$total_items : 'Please Take Exam'}}</div>
                </div>
                <div class="panel-body">
                    <div>
                        <div id="preResult" style="height:150px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title text-center">{!! ($postResult) ? 'Total score: '. $postResult->score.'/'.$total_items  : '<a href="'.url('teacher/confirmation').'">Please Take Exam</a>' !!} </div>
                </div>
                <div class="panel-body">
                    <div>
                        <div id="postResult" style="height:150px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-info stats-info">
                <div class="panel-heading">
                    <div class="panel-title text-center">PC OPERATIONS</div>
                </div>
                <div class="panel-body">
                    <ul class="list-unstyled">
                        <?php $count = 1; ?>
                        @foreach( $skills as $skill )                    
                            @foreach( $skill_result->value as $id => $result )                     
                                @if( $skill->id == $id && $skill->category == 'PC Operations' )

                                <li> <b>{{ $count++ }}.</b> {{ $skill->question }}
                                    @if( $result == 1)
                                        <div class="pull-right"><span class="label label-info">Yes</span></div>
                                    @else
                                        <div class="pull-right"><span class="label label-danger">No</span></div>
                                    @endif
                                </li>
                                @endif
                            @endforeach
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-info stats-info">
                <div class="panel-heading">
                    <div class="panel-title text-center">INTERNET APPLICATIONS</div>
                </div>
                <div class="panel-body">
                    <ul class="list-unstyled">
                        <?php $count = 1; ?>
                        @foreach( $skills as $skill )                    
                            @foreach( $skill_result->value as $id => $result )                     
                                @if( $skill->id == $id && $skill->category == 'Internet Applications' )

                                <li> <b>{{ $count++ }}.</b> {{ $skill->question }}
                                    @if( $result == 1)
                                        <div class="pull-right"><span class="label label-info">Yes</span></div>
                                    @else
                                        <div class="pull-right"><span class="label label-danger">No</span></div>
                                    @endif
                                </li>
                                @endif
                            @endforeach
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection('content')

@section('scripts')
<script src="{{ asset('plugins/flot/jquery.flot.min.js') }}"></script>
<script src="{{ asset('plugins/flot/jquery.flot.time.min.js') }}"></script>
<script src="{{ asset('plugins/flot/jquery.flot.symbol.min.js') }}"></script>
<script src="{{ asset('plugins/flot/jquery.flot.resize.min.js') }}"></script>
<script src="{{ asset('plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ asset('plugins/flot/jquery.flot.pie.min.js') }}"></script>

<script type="text/javascript">
$(document).ready(function () {

    @if( $preResult ) 
    var preExam = function () {
        var data = [
            @foreach( $categories as $category )    
                @foreach( $preResult->sub_score as $key => $sub_score ) 
                    @if( $category->id == $key )
                        {
                            label: " {{ $category->name }} ",
                            data: " {{ $sub_score }}",
                            color: "#{{ $category->color }}",
                        },
                        @endif
                @endforeach
            @endforeach
            {
                label: " Mistakes",
                data: "{{ $total_items - $preResult->score  }}",
                color: "#C6C6C6",
            },
        
        ];
        var options = {
            series: {
                pie: {
                    show: true
                }
            },
            legend: {
                labelFormatter: function(label, series){
                    return '<span class="pie-chart-legend"> '+label+'</span>';
                }
            },
            grid: {
                hoverable: true
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s ( %y.0 )",
                shifts: {
                    x: 20,
                    y: 0
                },
                defaultTheme: false
            }
        };
        $.plot($("#preResult"), data, options);
    };
    preExam();
    @endif

    @if( $postResult )
        var postExam = function () {
            var data = [
           
            @foreach( $categories as $category )
                    
                    @foreach( $postResult->sub_score as $key => $sub_score ) 
                         @if( $category->id == $key )
                            {
                                label: " {{ $category->name }}",
                                data: " {{ $sub_score }}",
                                color: "#{{ $category->color }}",
                            },
                            @endif
                    @endforeach
                @endforeach
                {
                    label: " Mistakes",
                    data: "{{ $total_items - $preResult->score  }}",
                    color: "#C6C6C6",
                },
            
            ];
            var options = {
                series: {
                    pie: {
                        show: true
                    }
                },
                legend: {
                    labelFormatter: function(label, series){
                        return '<span class="pie-chart-legend">'+label+'</span>';
                    }
                },
                grid: {
                    hoverable: true
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s ( %y.0 )",
                    shifts: {
                        x: 20,
                        y: 0
                    },
                    defaultTheme: false
                }
            };
            $.plot($("#postResult"), data, options);
        };

        postExam();  
    @endif  
});
</script>
@endsection('scripts')