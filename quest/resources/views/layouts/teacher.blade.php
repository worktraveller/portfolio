<!DOCTYPE html>
<html>
    
<!-- Mirrored from lambdathemes.in/modern/layout-hover-menu.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Apr 2015 11:09:24 GMT -->
<head>
        
        <!-- Title -->
        <title>Modern | Layouts - Hover Menu</title>
        
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcode" />
        
        <!-- Styles -->        
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="{{ asset('plugins/pace-master/themes/blue/pace-theme-flash.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/uniform/css/uniform.default.min.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/fontawesome/css/font-awesome.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/line-icons/simple-line-icons.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/offcanvasmenueffects/css/menu_cornerbox.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/waves/waves.min.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/switchery/switchery.min.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/3d-bold-navigation/css/style.css') }}">
        <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel='stylesheet' type='text/css'>
        
        <!-- Theme Styles -->
        <link rel="stylesheet" href="{{ asset('css/modern.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/themes/white.css') }}">
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

        <script rel="stylesheet" href="{{ asset('plugins/3d-bold-navigation/js/modernizr.js') }}"></script>
        <script rel="stylesheet" href="{{ asset('plugins/offcanvasmenueffects/js/snap.svg-min.js') }}"></script>
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
        .page-horizontal-bar.page-header-fixed .horizontal-bar{
            padding-top:0px;
        }
        </style>
        
    </head>
    <body class="page-header-fixed page-horizontal-bar">
        <div class="overlay"></div>

        <form class="search-form" action="#" method="GET">
            <div class="input-group">
                <input type="text" name="search" class="form-control search-input" placeholder="Search...">
                <span class="input-group-btn">
                    <button class="btn btn-default close-search waves-effect waves-button waves-classic" type="button"><i class="fa fa-times"></i></button>
                </span>
            </div><!-- Input Group -->
        </form><!-- Search Form -->
        <main class="page-content content-wrap">
            <div class="horizontal-bar sidebar">
                <div class="page-sidebar-inner slimscroll">
                    <div class="sidebar-header">
                        <div class="sidebar-profile">
                            <a href="javascript:void(0);" id="profile-menu-link">
                                <div class="sidebar-profile-image">
                                    <img src="assets/images/avatar1.png" class="img-circle img-responsive" alt="">
                                </div>
                                <div class="sidebar-profile-details">
                                    <span>David Green<br><small>Art Director</small></span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <ul class="menu accordion-menu">
                        <li>
                            <a href="{{ url('teacher/exam/result') }}" class="waves-effect waves-button">
                                <span class="menu-icon glyphicon glyphicon-home"></span><p>Results</p><span class="arrow"></span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('teacher/profile') }}" class="waves-effect waves-button">
                                <span class="menu-icon glyphicon glyphicon-user"></span><p>Profile</p>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('teacher/exam/pre') }}" class="waves-effect waves-button">
                                <span class="menu-icon glyphicon glyphicon-pencil"></span><p>Exam</p><span class="arrow"></span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('logout') }}" class="waves-effect waves-button">
                                <span class="menu-icon fa fa-power-off"></span><p>Logout</p><span class="arrow"></span>
                            </a>
                        </li>
                    </ul>
                </div><!-- Page Sidebar Inner -->
            </div><!-- Page Sidebar -->
            <div class="page-inner">
                <div class="page-title">
                    <h3 class="text-center">{{ $title }}</h3>
                </div>
                <div id="main-wrapper">
                    
                    @yield('content')

                </div><!-- Main Wrapper -->
                <div class="page-footer">
                    <p class="no-s">2016 &copy; Pre Assessment Examination by Juphet.</p>
                </div>
            </div><!-- Page Inner -->
        </main><!-- Page Content -->
        <div class="cd-overlay"></div>
    

        <!-- Javascripts -->
        <script src="{{ asset('plugins/jquery/jquery-2.1.3.min.js') }}"></script>
        <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('plugins/pace-master/pace.min.js') }}"></script>
        <script src="{{ asset('plugins/jquery-blockui/jquery.blockui.js') }}"></script>
        <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('plugins/switchery/switchery.min.js') }}"></script>
        <script src="{{ asset('plugins/uniform/jquery.uniform.min.js') }}"></script>
        <script src="{{ asset('plugins/offcanvasmenueffects/js/classie.js') }}"></script>
        <script src="{{ asset('plugins/offcanvasmenueffects/js/main.js') }}"></script>
        <script src="{{ asset('plugins/waves/waves.min.js') }}"></script>
        <script src="{{ asset('plugins/3d-bold-navigation/js/main.js') }}"></script>
        <script src="{{ asset('plugins/chartsjs/Chart.min.js') }}"></script>
        <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
        <script src="{{ asset('js/modern.min.js') }}"></script>
        
        @yield('scripts')

        @if ( !session('error') && session('msg') )
            <script type="text/javascript">
            $(document).ready(function() {

               setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            progressBar: true,
                            showMethod: 'slideDown',
                            timeOut: 4000
                        };
                        toastr.{{session('status')}}('{{ session("msg") }}');

                    }, 500);
            });
            </script>
        @endif

        @if ( $errors->count() > 0 )
            @foreach( $errors->all() as $message )
                <script type="text/javascript">
                    $(document).ready(function() {
                        setTimeout(function() {
                            toastr.options = {
                                closeButton: true,
                                progressBar: true,
                                showMethod: 'fadeIn',
                                hideMethod: 'fadeOut',
                                timeOut: 3000
                            };
                            toastr.error('{{ $message }}');
                        }, 300);
                    });
                </script>
             @endforeach
        @endif 
        
    </body>

<!-- Mirrored from lambdathemes.in/modern/layout-hover-menu.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Apr 2015 11:09:29 GMT -->
</html>