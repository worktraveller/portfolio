<!DOCTYPE html>
<html>
    
<!-- Mirrored from lambdathemes.in/modern/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Apr 2015 11:01:57 GMT -->
<head>
        
        <!-- Title -->
        <title>Assessement | {{ $title }}</title>
        
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcode" />
        
        <!-- Styles -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="{{ asset('plugins/uniform/css/uniform.default.min.css') }}">
        <link href="{{ asset('plugins/pace-master/themes/blue/pace-theme-flash.css') }}" rel='stylesheet' type='text/css'>
        <link href="{{ asset('plugins/uniform/css/uniform.default.min.css') }}" rel='stylesheet' type='text/css'>
        <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel='stylesheet' type='text/css'>
        <link href="{{ asset('plugins/fontawesome/css/font-awesome.css') }}" rel='stylesheet' type='text/css'>
        <link href="{{ asset('plugins/line-icons/simple-line-icons.css') }}" rel='stylesheet' type='text/css'>
        <link href="{{ asset('plugins/offcanvasmenueffects/css/menu_cornerbox.css') }}" rel='stylesheet' type='text/css'>
        <link href="{{ asset('plugins/waves/waves.min.css') }}" rel='stylesheet' type='text/css'>
        <link href="{{ asset('plugins/3d-bold-navigation/css/style.css') }}" rel='stylesheet' type='text/css'>
        <!--link href="{{ asset('plugins/weather-icons-master/css/weather-icons.min.css') }}" rel='stylesheet' type='text/css'>
        <link href="{{ asset('plugins/metrojs/MetroJs.min.css') }}" rel='stylesheet' type='text/css'-->
        <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel='stylesheet' type='text/css'>
        	
        <!-- Theme Styles -->
        <link href="{{ asset('css/modern.min.css') }}" rel='stylesheet' type='text/css'>
        <link href="{{ asset('css/themes/white.css') }}" rel='stylesheet' type='text/css'>
        <link href="{{ asset('css/custom.css') }}" rel='stylesheet' type='text/css'>
        
        <script src="{{ asset('plugins/3d-bold-navigation/js/modernizr.js') }}"></script>
        <script src="{{ asset('plugins/offcanvasmenueffects/js/snap.svg-min.js') }}"></script>
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="page-header-fixed">
        <div class="overlay"></div>

        <main class="page-content content-wrap">
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="logo-box">
                        <a href="{{ url('admin/dashboard') }}" class="logo-text"><span>Assessment</span></a>
                    </div><!-- Logo Box -->
                    <div class="search-button">
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic show-search"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="topmenu-outer">
                        <div class="top-menu">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                                        <span class="user-name">{{ Auth::user()->username }}</span>
                                        <img class="img-circle avatar" src="{{ url('assets/images/avatar1.png') }}" width="40" height="40" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/logout') }}" class="log-out waves-effect waves-button waves-classic">
                                        <span><i class="fa fa-sign-out m-r-xs"></i>Log out</span>
                                    </a>
                                </li>
                            </ul><!-- Nav -->
                        </div><!-- Top Menu -->
                    </div>
                </div>
            </div><!-- Navbar -->
            <div class="page-sidebar sidebar">
                <div class="page-sidebar-inner slimscroll">
                    <div class="sidebar-header">
                        <div class="sidebar-profile">
                            <a href="javascript:void(0);" id="">
                                <div class="sidebar-profile-image">
                                    <img src="{{ url('assets/images/avatar1.png') }}" class="img-circle img-responsive" alt="">
                                </div>
                                <div class="sidebar-profile-details">
                                    <span>{{ Auth::user()->username }}<br><small>{{ Auth::user()->user_type }}</small></span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <ul class="menu accordion-menu ">
                        <li class="{{ (isset($module_name) && $module_name == 'dashboard') ? 'active':'' }}">
                            <a href="{{ url('/admin/dashboard') }}" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-home"></span><p>Dashboard</p></a>
                        </li>
                        <li class="droplink {{ (isset($module_name) && $module_name == 'user') ? 'open':'' }}">
                            <a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-user"></span><p>User</p><span class="arrow"></span></a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('admin/user/create') }}">Add User</a></li>
                                <li><a href="{{ url('admin/student/manage') }}">Manage Student</a></li>
                                <li><a href="{{ url('admin/teacher/manage') }}">Manage Teacher</a></li>
                            </ul>
                        </li>
                        <li class="droplink {{ (isset($module_name) && $module_name == 'exam') ? 'open':'' }}">
                            <a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-list-alt"></span><p>Student Exams</p><span class="arrow"></span></a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('admin/quests/create') }}">Add Exams</a></li>
                                <li><a href="{{ url('admin/manage/quests') }}">Manage Exams</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ url('/logout') }}" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-off"></span><p>Logout</p></a></li>
                    </ul>
                </div><!-- Page Sidebar Inner -->
            </div><!-- Page Sidebar -->
            <div class="page-inner">
                <div class="page-title">
                    <h3>Profenciency Quest</h3>
                    <div class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                </div>
                <div id="main-wrapper">  
                @yield('content')    
                    
                </div><!-- Main Wrapper -->
                <div class="page-footer">
                    <p class="no-s">2015 &copy; Modern by .</p>
                </div>
            </div><!-- Page Inner -->
        </main><!-- Page Content -->

        <div class="cd-overlay"></div>
	

    <!-- Javascripts -->

     <script src="{{ asset('plugins/jquery/jquery-2.1.3.min.js') }}"></script>
     <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
     
     <script src="{{ asset('js/my_jquery.js') }}"></script>

     <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
     <script src="{{ asset('plugins/pace-master/pace.min.js') }}"></script>
     <script src="{{ asset('plugins/jquery-blockui/jquery.blockui.js') }}"></script>
     <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
     <script src="{{ asset('plugins/switchery/switchery.min.js') }}"></script>
     <script src="{{ asset('plugins/uniform/jquery.uniform.min.js') }}"></script>
     <script src="{{ asset('plugins/offcanvasmenueffects/js/classie.js') }}"></script>
     <!--script src="{{ asset('plugins/offcanvasmenueffects/js/main.js') }}"></script-->
     <script src="{{ asset('plugins/waves/waves.min.js') }}"></script>
     <script src="{{ asset('plugins/3d-bold-navigation/js/main.js') }}"></script>
     <script src="{{ asset('plugins/waypoints/jquery.waypoints.min.js') }}"></script>
     <script src="{{ asset('plugins/jquery-counterup/jquery.counterup.min.js') }}"></script>
     <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
     <script src="{{ asset('plugins/jquery-counterup/jquery.counterup.min.js') }}"></script>
     <script src="{{ asset('plugins/flot/jquery.flot.min.js') }}"></script>
     <script src="{{ asset('plugins/flot/jquery.flot.time.min.js') }}"></script>
     <script src="{{ asset('plugins/flot/jquery.flot.symbol.min.js') }}"></script>
     <script src="{{ asset('plugins/flot/jquery.flot.resize.min.js') }}"></script>
     <script src="{{ asset('plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
     <script src="{{ asset('plugins/curvedlines/curvedLines.js') }}"></script>
     <script src="{{ asset('plugins/metrojs/MetroJs.min.js') }}"></script>

    <script src="{{ asset('js/modern.min.js') }}"></script>

    @yield('scripts')

    @if ( !session('error') && session('msg') )
        <script type="text/javascript">
        $(document).ready(function() {

           setTimeout(function() {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        timeOut: 4000
                    };
                    toastr.{{session('status')}}('{{ session("msg") }}');

                }, 500);
        });
        </script>
    @endif

    @if ( $errors->count() > 0 )
        @foreach( $errors->all() as $message )
            <script type="text/javascript">
                $(document).ready(function() {
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            progressBar: true,
                            showMethod: 'fadeIn',
                            hideMethod: 'fadeOut',
                            timeOut: 10000
                        };
                        toastr.error('{{ $message }}');
                    }, 300);
                });
            </script>
         @endforeach
    @endif   
        
    </body>

<!-- Mirrored from lambdathemes.in/modern/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Apr 2015 11:03:35 GMT -->

</html>