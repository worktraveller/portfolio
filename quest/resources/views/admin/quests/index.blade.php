@extends('layouts.admin_dashboard')
@section('content')
{!! Form::open(['url' => '/results', 'id'=>'question_answer', 'method' => 'post', 'class' => '']) !!}
	<?php  $count = 1; ?>
	@foreach( $questions as $question )
		<div class="panel panel-white lists-item">
		    <div class="panel-heading">
		        <div class="panel-title">
		        	{{ $count++ }}. {{ $question->questions }}
		        </div>
		        <div class="pull-right">
	        		<a href="{{ url('/admin/quests/'.$question->id.'/edit') }}" class="label label-info"><i class="fa fa-pencil"></i></a> | 
	        		<a href="#" class="label label-danger delete-this-item"
                        data-action="{{ url('admin/quests/destroy/'.$question->id) }}"
                        data-id="{{ $question->id }}" 
                        data-confirmation-msg="Are you sure you want to delete {{ $question->id }}?"
                    >
                        <i class="fa fa-trash"></i>
                    </a> &nbsp;
	        	</div>
		    </div>
		    <div class="panel-body">
		    	<div class="col-sm-6">
		    	    <label class="radio-inline">
				      <input type="radio" name="answer[{{ $question->id }}]" value="A" class="form-input" required />{{ $question->answers['A'] }}
				    </label>
				</div>
				<div class="col-sm-6">
				    <label class="radio-inline">
				      <input type="radio" name="answer[{{ $question->id }}]" value="B" class="form-input" />{{ $question->answers['B'] }}
				    </label>
				</div>
				<div class="col-sm-6">
				    <label class="radio-inline">
				      <input type="radio" name="answer[{{ $question->id }}]" value="C" class="form-input" />{{ $question->answers['C'] }}
				    </label>
				</div>
				<div class="col-sm-6">
		          	<label class="radio-inline">
				      <input type="radio" name="answer[{{ $question->id }}]" value="D" class="form-input" />{{ $question->answers['D'] }}
				    </label>
				</div>
		    </div>
		</div>
	@endforeach
			<button type="submit" class="btn btn-default btn-md">Submit</button>
{!! form::close() !!}
@endsection