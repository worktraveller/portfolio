@extends('layouts.admin_dashboard')
@section('content')
<div class="row  border-bottom dashboard-header">
    <div class="col-lg-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <div class="panel-title">Upload Students</div>
            </div>
            <div class="panel-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab1" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false" class="active">Add Quest</a></li>
                        <li role="presentation" class=""><a href="#tab2" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false">Import Quests</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="tab1">
                            {!! Form::open(['url' => '/admin/quests', 'id'=>'create_quest_form', 'method' => 'post', 'class' => '', 'files'=> true]) !!}
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label class="control-label">Question:</label>
                                            <input type="text" class="form-control" name="question" value="{{ Input::old('question') }}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label class="control-label">Image</label>
                                            <input type="file" class="form-control" name="image" value="{{ Input::old('image') }}"> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="control-label">Correct Answer</label>
                                            <select name="correct_answer" class="form-control m-b-sm">
                                                <option value="">--Select--</option>
                                                <option value="A" {{ Input::old('correct_answer') == 'A' ? 'selected' : ''}}>A</option>
                                                <option value="B" {{ Input::old('correct_answer') == 'B' ? 'selected' : ''}}>B</option>
                                                <option value="C" {{ Input::old('correct_answer') == 'C' ? 'selected' : ''}}>C</option>
                                                <option value="D" {{ Input::old('correct_answer') == 'D' ? 'selected' : ''}}>D</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="control-label">Exam Category</label>
                                            <select name="category" class="form-control m-b-sm">
                                                <option value="">--Select--</option>
                                                @foreach( $categories as $category )
                                                    <option value="{{ $category->id }}" {{ Input::old('category') == $category->id ? 'selected' : ''}} >{{ strtoupper( $category->name ) }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="control-label">A</label>
                                                <input type="text" class="form-control" name="A" value="{{ Input::old('a') }}" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="control-label">B</label>
                                                <input type="text" class="form-control" name="B" value="{{ Input::old('b')}}" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="control-label">C</label>
                                                <input type="text" class="form-control" name="C" value="{{ Input::old('c')}}" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="control-label">D</label>
                                                <input type="text" class="form-control" name="D" value="{{ Input::old('d')}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <button class="btn btn-primary" type="submit">Submit</button>
                                        </div>
                                    </div>
                            {!! form::close() !!}    
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab2">
                            {!! Form::open(['url' => '/admin/quests/import', 'id'=>'create_quest_form', 'method' => 'post', 'class' => '','enctype'=>'multipart/form-data']) !!}
                                    <div class="form-group">
                                        <label class="control-label">Import Quests</label>
                                        <input type="file" class="form-control" name="import_file">
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <button class="btn btn-primary" type="submit">Submit</button>
                                        </div>
                                    </div>
                            {!! form::close() !!}    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection