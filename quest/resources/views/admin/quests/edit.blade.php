@extends('layouts.admin_dashboard')
@section('content')
<div class="row  border-bottom dashboard-header">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="panel-body">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            {!! Form::model($quest, array('route' => array( 'admin.quests.update', $quest->id ), 'method' => 'PATCH', 'files'=> true )) !!}
                            <div class="hr-line-dashed"></div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label class="control-label">Question:</label>
                                            <input type="text" class="form-control" name="question" value="{{ $quest->questions }}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label class="control-label">Image</label>
                                            <input type="file" class="form-control" name="image" value="{{ $quest->image }}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="control-label">Correct Answer</label>
                                            <select name="correct_answer" class="form-control m-b-sm">
                                                <option value="">--Select--</option>
                                                <option value="A" {{ $quest->correct_answer == 'A' ? 'selected="selected"':'' }}>A</option>
                                                <option value="B" {{ $quest->correct_answer == 'B' ? 'selected="selected"':'' }}>B</option>
                                                <option value="C" {{ $quest->correct_answer == 'C' ? 'selected="selected"':'' }}>C</option>
                                                <option value="D" {{ $quest->correct_answer == 'D' ? 'selected="selected"':'' }}>D</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="control-label">Exam Category</label>
                                            <select name="category" class="form-control m-b-sm">
                                                <option value="">--Select--</option>
                                                @foreach( $categories as $category )
                                                    <option value="{{ $category->id }}" {{ $quest->category_id == $category->id ? 'selected' : ''}} >{{ strtoupper( $category->name ) }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="control-label">A</label>
                                                <input type="text" class="form-control" name="A"  value="{{ $quest->answers['A'] }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="control-label">B</label>
                                                <input type="text" class="form-control" name="B"  value="{{ $quest->answers['B'] }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="control-label">C</label>
                                                <input type="text" class="form-control" name="C"  value="{{ $quest->answers['C'] }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="control-label">D</label>
                                                <input type="text" class="form-control" name="D" value="{{ $quest->answers['D'] }}">
                                            </div>
                                        </div>
                                    </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <button class="btn btn-primary" type="submit">Update</button>
                                    </div>
                                </div>
                            </div>
                            {!! form::close() !!}            
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>
@endsection