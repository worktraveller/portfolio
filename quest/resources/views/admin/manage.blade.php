@extends('layouts.admin_dashboard')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <div class="panel-title">List of student who take the exam</div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="example" class="display table dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                        <thead>
                            <tr role="row">
                                <th class="text-center">Name</th>
                                <th class="text-center">Age</th>
                                <th class="text-center">Gender</th>
                                <th class="text-center">School</th>
                                <th class="text-center">Grade 7 Teacher</th>
                                <th class="text-center">Grade 8 Teacher</th>
                                <th class="text-center">Grade 9 Teacher</th>
                                <th class="text-center">Grade 10 Teacher</th>
                                <th class="text-center">Option</th>
                        </thead>
                        <tbody>
                        @foreach( $users as $user )
                            @foreach( $user->student as $student )

                                @if( $student )

                                <tr role="row" class="odd">
                                    <td class="">{{  $student->first_name.' '.$student->last_name }} </td>
                                    <td class="text-center">{{  $student->age }} </td>
                                    <td class="text-center">{{  $student->gender == 1 ? 'Male' : 'Female' }} </td>
                                    <td class="text-center">{{  $student->school }} </td>
                                    <td class="text-center">{{  $student->teacher_1 }} </td>
                                    <td class="text-center">{{  $student->teacher_2 }} </td>
                                    <td class="text-center">{{  $student->teacher_3 }} </td>
                                    <td class="text-center">{{  $student->teacher_4 }} </td>
                                    <td class="text-center">
                                        <a href="" class="label label-info"><i class="fa fa-edit"></i></a>
                                        <a href="" class="label label-danger"><i class="fa fa-trash"></i></a> &nbsp; &nbsp;
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection