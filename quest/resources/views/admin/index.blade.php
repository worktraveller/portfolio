@extends('layouts.admin_dashboard')
@section('content')
<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">List of student who take the exam</div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="example" class="display table dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                        <thead>
                            <tr role="row">
                                <th class="" width="200">First name</th>
                                <th class="" width="150">Last Name</th>
                                <th class="text-center">Pre Score</th>
                                <th class="text-center">Post Score</th>
                        </thead>

                        <tbody>
                            @foreach( $users as $user )

                                @if( $user->student )
                                <tr role="row" class="odd">
                                    <td class=""><a href="{{ url('admin/student/'.$user->student['user_id'].'/profile') }}">{{ $user->student['first_name'] }}</a></td>
                                    <td class=""><a href="{{ url('admin/student/'.$user->student['user_id'].'/profile') }}">{{ $user->student['last_name'] }}</a></td>
                                    @foreach( $results as $result )
                                        @if( $result->user_id == $user->student['user_id'] )
                                                
                                                @if( $result->status == 1 ) 
                                                    <td class="text-center">{{ $result->score }}</td>
                                                @endif

                                                @if( $result->status == 2 )
                                                    <td class="text-center">{{ $result->score }}</td>
                                                @endif 
                                        @endif 
                                    @endforeach
                                </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>                   

    <div class="col-sm-6">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div class="panel-title">List of student who take the exam</div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="example" class="display table dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                        <thead>
                            <tr role="row">
                                <th class="" width="150">First name</th>
                                <th class="" width="150">Last Name</th>
                                <th class="text-center">Pre Score</th>
                                <th class="text-center">Post Score</th>
                        </thead>

                        <tbody>
                            @foreach( $users as $user )
                                @if( $user->teacher )
                                <tr role="row" class="odd">
                                    <td class=""><a href="{{ url('admin/teacher/'.$user->teacher['user_id'].'/profile') }}">{{ $user->teacher['first_name'] }}</a></td>
                                    <td class=""><a href="{{ url('admin/teacher/'.$user->teacher['user_id'].'/profile') }}">{{ $user->teacher['last_name'] }}</a></td>
                                    @foreach( $results as $result )
                                        @if( $result->user_id == $user->teacher['user_id'] )
                                            @if( $result->status == 1 ) 
                                                <td class="text-center">{{ $result->score }}</td>
                                            @endif
                                            @if( $result->status == 2 )
                                                <td class="text-center">{{ $result->score }}</td>
                                            @endif 
                                        @endif  
                                    @endforeach
                                </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>                   
</div>
@endsection