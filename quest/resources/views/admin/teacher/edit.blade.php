@extends('layouts.admin_dashboard')
@section('content')
<div class="row  border-bottom dashboard-header">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="panel-body">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            {!! Form::model($user, array('route' => array( 'admin.students.update', $user->id ), 'method' => 'PATCH', 'files'=> true )) !!}
                            <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                	<div class="row">
	                                	<div class="col-sm-6">
	                                        <label class="control-label">Email:</label>
	                                        <input type="text" class="form-control" name="email" value="{!! $user->email !!}">
	                                    </div>
	                                    <div class="col-sm-6">
	                                        <label class="control-label">Difficulty Level:</label>
	                                        <select class="form-control m-b-sm">
	                                                <option>--SELECT--</option>
	                                                <option>2</option>
	                                                <option>3</option>
	                                                <option>4</option>
	                                                <option>5</option>
	                                        </select>
	                                    </div>
                                	</div>
                                </div>
                                <div class="form-group">
                                	<div class="row">
	                                    <div class="col-sm-12">
	                                        <label class="control-label">Fullname:</label>
	                                        <input type="text" class="form-control" name="fullname" value="{!! $user->fullname !!}">
	                                    </div>
	                                </div>
	                            </div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
		                                	<button class="btn btn-primary" type="submit">Update</button>
		                                </div>
	                                </div>
                                </div>
                            {!! form::close() !!}            
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>
@endsection