@extends('layouts.admin_dashboard')
@section('content')

<div class="row  border-bottom dashboard-header">
    <div class="col-md-6">	
    	<div class="panel panel-white">
            <ul class="list-group">
                <li class="list-group-item text-uppercase">
                    <table style="width: 100%;">
                        <tr>
                            <td><b>Name:</b> {{ $teacher->first_name.' '.$teacher->last_name }}</td>
                            <td><b>Gender:</b> {{ $teacher->gender == 1 ? 'male' : female }}</td>
                            <td><b>Age:</b> {{ $teacher->age }}</td>
                        </tr>
                    </table>
                </li>
                <li class="list-group-item text-uppercase">
                    <b>Highest Attainment:</b> {{ $teacher->highest_attainment }}
                </li>
                <li class="list-group-item text-uppercase">
                    <b>Subject Taught:</b> {{ $teacher->subject_taught }}
                </li>
                <li class="list-group-item text-uppercase">
                    <b>Specialization:</b> {{ $teacher->specialization }}
                </li>
                <li class="list-group-item text-uppercase">
                    <b>Years in service:</b> {{ $teacher->years_in_service }}
                </li>
                <li class="list-group-item text-uppercase">
                    <b>School:</b> {{ $teacher->present_school }}
                </li>
            </ul>
        </div>
    </div>
    <div class="col-md-3">	
    	<div class="panel panel-info">
    		<div class="panel-heading ">
	            <h3 class="panel-title">{{ ($preResult) ? 'Pre Exam Total score: '.$preResult->score.'/'.$total_items : 'Not yet taken PRE Exam'}}</h3>
	        </div>
	        <div class="panel-body">
            	<div id="preExam" style="height:150px;"></div>
            </div>		
        </div>
    </div>
    <div class="col-md-3">	
    	<div class="panel panel-info">
    		<div class="panel-heading">
	            <h3 class="panel-title">{{ ($postResult) ? 'Post Exam Total score: '.$postResult->score.'/'.$total_items : 'Not yet taken POST Exam'}}</h3>
	        </div>
	        <div class="panel-body">
            	<div id="postExam" style="height:150px;"></div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('styles')

@endsection('styles')

@section('scripts')
<script src="{{ asset('plugins/flot/jquery.flot.pie.min.js') }}"></script>

<script>

$(document).ready(function () {

    @if( $preResult ) 
    var preExam = function () {
        var data = [
            @foreach( $categories as $category )    
                @foreach( $preResult->sub_score as $key => $sub_score ) 
                    @if( $category->id == $key )
                        {
                            label: "{{ $category->name }}",
                            data: "{{ $sub_score }}",
                            color: "#{{ $category->color }}",
                        },
                        @endif
                @endforeach
            @endforeach
            {
                label: "Mistakes",
                data: "{{ $total_items - $preResult->score  }}",
                color: "#C6C6C6",
            },
        
        ];
        var options = {
            series: {
                pie: {
                    show: true
                }
            },
            legend: {
                labelFormatter: function(label, series){
                    return '<span class="pie-chart-legend"> '+label+'</span>';
                }
            },
            grid: {
                hoverable: true
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s ( %y.0 )",
                shifts: {
                    x: 20,
                    y: 0
                },
                defaultTheme: false
            }
        };
        $.plot($("#preExam"), data, options);
    };
    preExam();
    @endif

    @if( $postResult )
        var postExam = function () {
            var data = [
           
            @foreach( $categories as $category )
                    
                    @foreach( $postResult->sub_score as $key => $sub_score ) 
                         @if( $category->id == $key )
                            {
                                label: "{{ $category->name }}",
                                data: "{{ $sub_score }}",
                                color: "#{{ $category->color }}",
                            },
                            @endif
                    @endforeach
                @endforeach
                {
                    label: "Mistakes",
                    data: "{{ $total_items - $postResult->score  }}",
                    color: "#C6C6C6",
                },
            
            ];
            var options = {
                series: {
                    pie: {
                        show: true
                    }
                },
                legend: {
                    labelFormatter: function(label, series){
                        return '<span class="pie-chart-legend">'+label+'</span>';
                    }
                },
                grid: {
                    hoverable: true
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s ( %y.0 )",
                    shifts: {
                        x: 20,
                        y: 0
                    },
                    defaultTheme: false
                }
            };
            $.plot($("#postExam"), data, options);
        };

        postExam();  
    @endif  
});

</script>
@endsection('scripts')