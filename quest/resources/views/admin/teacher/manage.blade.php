@extends('layouts.admin_dashboard')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <div class="panel-title">List of student who take the exam</div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="example" class="display table dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                        <thead>
                            <tr role="row">
                                <th class="">Name</th>
                                <th class="text-center">Age</th>
                                <th class="text-center">Gender</th>
                                <th class="">School</th>
                                <th class="text-center">Highest Education Attained</th>
                                <th class="text-center">Specialization</th>
                                <th class="text-center">Subject Taught</th>
                                <th class="text-center">Years in Service</th>
                                <th class="text-center">Option</th>
                        </thead>
                        <tbody>
                        @foreach( $users as $user )
                            @foreach( $user->teacher as $teacher )

                                @if( $teacher )

                                <tr role="row" class="odd">
                                    <td class=""><a href="{{ url('admin/teacher/'.$teacher->user_id.'/profile') }}">{{  $teacher->first_name.' '.$teacher->last_name }}</a></td>
                                    <td class="text-center">{{  $teacher->age }} </td>
                                    <td class="text-center">{{  $teacher->gender == 1 ? 'Male' : 'Female' }} </td>
                                    <td class="">{{  $teacher->present_school }} </td>
                                    <td class="text-center">{{  $teacher->highest_attainment }} </td>
                                    <td class="text-center">{{  $teacher->specialization }} </td>
                                    <td class="text-center">{{  $teacher->subject_taught }} </td>
                                    <td class="text-center">{{  $teacher->years_in_service }} </td>
                                    <td class="">
                                        <a href="" class="label label-info"><i class="fa fa-edit"></i></a>
                                        <a href="" class="label label-danger"><i class="fa fa-trash"></i></a>
                                    </td>

                                </tr>
                                @endif
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection