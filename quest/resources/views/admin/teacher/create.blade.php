@extends('layouts.admin_dashboard')
@section('content')
<div class="row  border-bottom dashboard-header">
    <div class="col-lg-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <div class="panel-title">Upload Students</div>
            </div>
            <div class="panel-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab1" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false" class="active">Add Teacher</a></li>
                        <li role="presentation" class=""><a href="#tab2" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false">Import Teachers</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="tab1">
                            {!! Form::open(['url' => '/admin/teacher', 'id'=>'create_quest_form', 'method' => 'post', 'class' => '','enctype'=>'multipart/form-data']) !!}
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Username:</label>
                                            <input type="text" class="form-control" name="username" value="{{ Input::old('username') }}" >
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Password</label>
                                            <input type="password" class="form-control" name="password" value="{{ Input::old('password') }}" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <button class="btn btn-primary" type="submit">Submit</button>
                                        </div>
                                    </div>
                            {!! form::close() !!}    
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab2">
                            {!! Form::open(['url' => '/admin/teacher/import', 'id'=>'create_quest_form', 'method' => 'post', 'class' => '','enctype'=>'multipart/form-data']) !!}
                                    <div class="form-group">
                                        <label class="control-label">Import Students</label>
                                        <input type="file" class="form-control" name="import_file">
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <button class="btn btn-primary" type="submit">Submit</button>
                                        </div>
                                    </div>
                            {!! form::close() !!}    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection