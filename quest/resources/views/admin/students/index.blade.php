@extends('layouts.dashboard')
@section('content')
{!! Form::open(['url' => '/results', 'id'=>'question_answer', 'method' => 'post', 'class' => '']) !!}
	<?php  $count = 1; ?>
	@foreach( $questions as $question )
		<div class="panel panel-white">
		    <div class="panel-heading">
		        <div class="panel-title">{{ $count++ }}. {{ $question->questions }}</div>
		    </div>
		    <div class="panel-body">
		    	    <label class="radio-inline">
				      <input type="radio" name="answer[{{ $question->id }}]" value="A" class="form-input" required />{{ $question->answers['A'] }}
				    </label>
				    <label class="radio-inline">
				      <input type="radio" name="answer[{{ $question->id }}]" value="B" class="form-input" />{{ $question->answers['B'] }}
				    </label>
				    <label class="radio-inline">
				      <input type="radio" name="answer[{{ $question->id }}]" value="C" class="form-input" />{{ $question->answers['C'] }}
				    </label>
		          	<label class="radio-inline">
				      <input type="radio" name="answer[{{ $question->id }}]" value="D" class="form-input" />{{ $question->answers['D'] }}
				    </label>
		    </div>
		</div>
	@endforeach
			<button type="submit" class="btn btn-default btn-md">Submit</button>
{!! form::close() !!}
@endsection