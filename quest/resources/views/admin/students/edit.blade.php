@extends('layouts.admin_dashboard')
@section('content')
<div class="row  border-bottom dashboard-header">
    <div class="col-lg-12">
    	<div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">List of student who take the exam</div>
            </div>
            <div class="panel-body">
            	
            	{!! Form::model($student, array('route' => array( 'admin.student.update', $student->user_id ), 'method' => 'PATCH')) !!}
                    <div class="row">
                        <div class="form-group col-md-6 col-sm-12">
                            <label for="exampleInputEmail1">First Name</label>
                            <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Enter First Name" value="{{ ( $student->first_name ) ? $student->first_name : Input::old('first_name') }}">
                        </div>  
                        <div class="form-group col-md-6 col-sm-12">
                            <label for="exampleInputEmail1">Last Name</label>
                            <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter Last Name" value="{{ ( $student->last_name ) ? $student->last_name : Input::old('last_name') }}">
                        </div>
                    </div>  

                    <div class="row">
                        <div class="form-group col-md-6 col-sm-12">
                            <label for="exampleInputEmail1">Age</label>
                            <select name="age" class="form-control m-b-sm" required="required">
                                <option value="">--Select--</option>
                                @for( $age = 16; 65 >= $age; $age++ )
                                    <option value="{{ $age }}" {{ Input::old('age') == $age ? 'selected' : '' }} {{  $student->age == $age ? 'selected' : '' }} >{{ $age }}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label for="exampleInputEmail1">Gender</label>
                            <select name="gender" class="form-control m-b-sm" required="required">
                                <option value="">--Select--</option>
                                <option value="1" {{ Input::old('gender') == 1 ? 'selected' : '' }} {{ $student->gender == 1 ? 'selected' : '' }}>Male</option>
                                <option value="2" {{ Input::old('gender') == 2 ? 'selected' : '' }} {{ $student->gender == 2 ? 'selected' : '' }}>Female</option>
                            </select>
                        </div>  
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12 col-sm-12">
                            <label for="exampleInputEmail1">Name of school attended</label>
                            <input type="text" class="form-control" id="school" name="school" placeholder="Enter Name of School attended" value="{{ ( $student->school ) ? $student->school : Input::old('school') }}">
                        </div>  
                    </div>    

                    <div class="row">
                        <div class="form-group col-md-6 col-sm-12">
                            <label for="exampleInputEmail1">Grade 7 Teacher</label>
                            <input type="text" class="form-control teacher" id="tags" name="teacher_1" placeholder="Enter Grade 7 Teacher" value="{{ ( $student->teacher_1 ) ? $student->teacher_1 : Input::old('teacher_1') }}">
                        </div>  
                        <div class="form-group col-md-6 col-sm-12">
                            <label for="exampleInputEmail1">Grade 8 Teacher</label>
                            <input type="text" class="form-control teacher" id="teacher_2" name="teacher_2" placeholder="Enter Grade 8 Teacher" value="{{ ( $student->teacher_2 ) ? $student->teacher_2 : Input::old('teacher_2') }}">
                        </div>  
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6 col-sm-12">
                            <label for="exampleInputEmail1">Grade 9 Teacher</label>
                            <input type="text" class="form-control teacher" id="teacher_3" name="teacher_3" placeholder="Enter Grade 9 Teacher" value="{{ ( $student->teacher_3 ) ? $student->teacher_3 : Input::old('teacher_3') }}">
                        </div>  
                        <div class="form-group col-md-6 col-sm-12">
                            <label for="exampleInputEmail1">Grade 10 Teacher</label>
                            <input type="text" class="form-control teacher" id="teacher_4" name="teacher_4" placeholder="Enter Grade 10 Teacher" value="{{ ( $student->teacher_4 ) ? $student->teacher_4 : Input::old('teacher_4') }}">
                        </div>  
                    </div>  

                    <button type="submit" class="btn btn-primary">Submit</button>
                {!! form::close() !!}             
        	</div>
        </div>
    </div>
</div>
@endsection