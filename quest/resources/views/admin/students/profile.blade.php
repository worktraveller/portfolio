@extends('layouts.admin_dashboard')
@section('content')

<div class="row  border-bottom dashboard-header">
    <div class="col-md-6">	
    	<div class="panel panel-white">
            <ul class="list-group">
                <li class="list-group-item text-uppercase">
                    <table style="width: 100%;">
                        <tr>
                            <td><b>Name:</b> {{ $student->first_name.' '.$student->last_name }}</td>
                            <td><b>Gender:</b> {{ $student->gender == 1 ? 'male' : female }}</td>
                            <td><b>Age:</b> {{ $student->age }}</td>
                        </tr>
                    </table>
                </li>
                <li class="list-group-item text-uppercase">
                    <b>School:</b> {{ $student->school }}
                </li>
                <li class="list-group-item text-uppercase">
                    <b>Grade 7 Teacher:</b> {{ $student->teacher_1 }}
                </li>
                <li class="list-group-item text-uppercase">
                    <b>Grade 8 Teacher:</b> {{ $student->teacher_2 }}
                </li>
                <li class="list-group-item text-uppercase">
                    <b>Grade 9 Teacher:</b> {{ $student->teacher_3 }}
                </li>
                <li class="list-group-item text-uppercase">
                    <b>Grade 10 Teacher:</b> {{ $student->teacher_4 }}
                </li>
            </ul>
        </div>
    </div>
    <div class="col-md-3">	
    	<div class="panel panel-info">
    		<div class="panel-heading ">
	            <h3 class="panel-title">{{ ($preResult) ? 'Pre Exam Total score: '.$preResult->score.'/'.$total_items : 'Not yet taken PRE Exam'}}</h3>
	        </div>
	        <div class="panel-body">
            	<div id="preExam" style="height:150px;"></div>
            </div>		
        </div>
    </div>
    <div class="col-md-3">	
    	<div class="panel panel-info">
    		<div class="panel-heading">
	            <h3 class="panel-title">{{ ($postResult) ? 'Post Exam Total score: '.$postResult->score.'/'.$total_items : 'Not yet taken POST Exam'}}</h3>
	        </div>
	        <div class="panel-body">
            	<div id="postExam" style="height:150px;"></div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('styles')

@endsection('styles')

@section('scripts')
<script src="{{ asset('plugins/flot/jquery.flot.pie.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {

    @if( $preResult ) 
    var preExam = function () {
        var data = [
            @foreach( $categories as $category )    
                @foreach( $preResult->sub_score as $key => $sub_score ) 
                    @if( $category->id == $key )
                        {
                            label: "{{ $category->name }}",
                            data: "{{ $sub_score }}",
                            color: "#{{ $category->color }}",
                        },
                        @endif
                @endforeach
            @endforeach
            {
                label: "Mistakes",
                data: "{{ $total_items - $preResult->score  }}",
                color: "#C6C6C6",
            },
        
        ];
        var options = {
            series: {
                pie: {
                    show: true
                }
            },
            legend: {
                labelFormatter: function(label, series){
                    return '<span class="pie-chart-legend"> '+label+'</span>';
                }
            },
            grid: {
                hoverable: true
            },
            tooltip: true,
            tooltipOpts: {
                content: "%y.0 (%s)",
                shifts: {
                    x: 20,
                    y: 0
                },
                defaultTheme: false
            }
        };
        $.plot($("#preExam"), data, options);
    };
    preExam();
    @endif

    @if( $postResult )
        var postExam = function () {
            var data = [
           
            @foreach( $categories as $category )
                    
                    @foreach( $postResult->sub_score as $key => $sub_score ) 
                         @if( $category->id == $key )
                            {
                                label: "{{ $category->name }}",
                                data: "{{ $sub_score }}",
                                color: "#{{ $category->color }}",
                            },
                            @endif
                    @endforeach
                @endforeach
                {
                    label: "Mistakes",
                    data: "{{ $total_items - $postResult->score  }}",
                    color: "#C6C6C6",
                },
            
            ];
            var options = {
                series: {
                    pie: {
                        show: true
                    }
                },
                legend: {
                    labelFormatter: function(label, series){
                        return '<span class="pie-chart-legend">'+label+'</span>';
                    }
                },
                grid: {
                    hoverable: true
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%y.0 (%s)",
                    shifts: {
                        x: 20,
                        y: 0
                    },
                    defaultTheme: false
                }
            };
            $.plot($("#postExam"), data, options);
        };

        postExam();  
    @endif  
});
</script>
@endsection('scripts')