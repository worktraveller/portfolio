@extends('layouts.admin_dashboard')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">List of student who take the exam</div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="example" class="display table dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                        <thead>
                            <tr role="row">
                                <th class="">Name</th>
                                <th class="text-center">Age</th>
                                <th class="text-center">Gender</th>
                                <th class="">School</th>
                                <th class="text-center">Option</th>
                        </thead>
                        <tbody>
                        @foreach( $users as $user )
                            @foreach( $user->student as $student )

                                @if( $student )

                                <tr role="row" class="lists-item">
                                    <td class=""><a href="{{ url('admin/student/'.$student->user_id.'/profile') }}">{{  $student->first_name.' '.$student->last_name }}</a></td>
                                    <td class="text-center">{{  $student->age }} </td>
                                    <td class="text-center">{{  $student->gender == 1 ? 'Male' : 'Female' }} </td>
                                    <td class="">{{  $student->school }} </td>
                                    <td class="text-center">
                                        <a href="{{ url('admin/student/'.$student->user_id.'/edit') }}" class="label label-info"><i class="fa fa-edit"></i></a>
                                        <a href="#" class="label label-danger delete-this-item"
                                            data-action="{{ url('admin/student/destroy/'.$student->user_id) }}"
                                            data-id="{{ $student->user_id }}" 
                                            data-confirmation-msg="Are you sure you want to delete {{ $student->user_id }}?"
                                        >
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection