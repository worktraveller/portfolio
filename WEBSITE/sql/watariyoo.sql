-- phpMyAdmin SQL Dump
-- version 4.5.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 07, 2016 at 05:40 AM
-- Server version: 5.7.11
-- PHP Version: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `watariyoo`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cinemas`
--

CREATE TABLE `cinemas` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` decimal(9,6) NOT NULL,
  `longitude` decimal(9,6) NOT NULL,
  `last_crawled` int(11) NOT NULL,
  `crawable` tinyint(1) NOT NULL,
  `crawled` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cinemas`
--

INSERT INTO `cinemas` (`id`, `name`, `address`, `phone`, `link`, `latitude`, `longitude`, `last_crawled`, `crawable`, `crawled`, `created_at`, `updated_at`) VALUES
(1, 'CGV Blitz Pacific Place', 'Pacific Place Mall Lt. 6, Jl. Jend Sudirman Kav. 52-53, Daerah Khusus Ibukota Jakarta 12190, Indonesia', '+62 22 82063630', NULL, '-6.226676', '106.810463', 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Cinema 21', 'Blok M Square Lt. 5, Jl. Melawai V, Kby. Baru, South Jakarta City, Daerah Khusus Ibukota Jakarta 12160, Indonesia', '+62 22 82063630', NULL, '-6.245783', '106.799558', 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Cinema 21, Daan Mogot', 'Mal Daan Mogot, Jl. Raya Daan Mogot KM 16, Special Capital Region of Jakarta 11840, Indonesia', '+62 22 82063630', NULL, '-6.152256', '106.702825', 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Sutos XXI', 'Surabaya Town Square Lt. 2, Jl. Adityawarman, Jawa Timur 60242, Indonesia', '+62 22 82063630', NULL, '-7.295377', '112.733134', 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Cinema XXI', 'Jl. Kejawen Putih Mutiara No. 17, Pakuwon City Lantai 3, Jawa Timur 60112, Indonesia', '+62 22 82063630', NULL, '-7.278462', '112.806402', 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Supermal XXI', 'Jl. Puncak Indah Lontar No. 2, Pakuwon Supermal/Trade Centre Lantai 1,, Jawa Timur 60112, Indonesia', '+62 22 82063630', NULL, '-7.271045', '112.807572', 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Tharmin 21', 'Jl. MH Thamrin No.75R, Sumatera Utara 20212, Indonesia', '+62 22 82063630', NULL, '3.586403', '98.691376', 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Cinema XXI', 'Centre Point Lt. 3A, Jalan Jawa No 8, Sumatera Utara 20231, Indonesia', '+62 22 82063630', NULL, '3.591931', '98.680181', 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Hermes XXI', 'Jl. Wolter Monginsidi No.45, Hermes Place Polonia, Sumatera Utara 20152, Indonesia', '+62 22 82063630', NULL, '3.579305', '98.664184', 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Bioscop Empire XXI', 'Jalan Urip Sumoharjo No. 104, Gondokusuman, Daerah Istimewa Yogyakarta 55221, Indonesia', '+62 22 82063630', NULL, '-7.783130', '110.383267', 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Cinema XXI', 'Jl. Jend. Gatot Subroto, Trans Studio Mall Lantai 3, Jawa Barat 40237, Indonesia', '+62 22 82063630', NULL, '-6.954757', '107.605996', 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'CGV Blitz', 'Paris Van Java, Jl. Sukajadi No. 137-139, Pasteur, Sukajadi, Jawa Barat 40162, Indonesia', '+62 22 82063630', NULL, '-6.891289', '107.599337', 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Summarecon Mal Bekasi Imaxbekasi', 'Summarecon Mal Bekasi Lantai 3 \nJl. Bulevar Ahmad Yani\nBekasi\r\n	PHONE\r\n			:\r\n			(021) 29572421\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp. 40.000\nJumat Rp. 50.000Sabtu/Minggu/Libur Rp. 60.000', '(021) 29572421', 'http://www.21cineplex.com/theater/bioskop-summarecon-mal-bekasi-imax,376,BKSIXSB.htm', '0.000000', '0.000000', 1459131645, 1, 1, '2016-03-28 02:06:10', '2016-03-28 02:20:45'),
(14, 'Gading Imaxjakarta', NULL, '(021) 45853821', 'http://www.21cineplex.com/theater/bioskop-gading-imax,331,JKTIXGD.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(15, 'Gandaria Imaxjakarta', NULL, '(021) 29053218', 'http://www.21cineplex.com/theater/bioskop-gandaria-imax,318,JKTIXGN.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(16, 'Tunjungan 5 Imaxsurabaya', NULL, '(031) 51164521', 'http://www.21cineplex.com/theater/bioskop-tunjungan-5-imax,391,SBYIXT5.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(17, 'Summarecon Mal Serpong Imaxtangerang', NULL, '(021) 542 026 21', 'http://www.21cineplex.com/theater/bioskop-summarecon-mal-serpong-imax,355,TGRIXSS.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(18, 'E-walk Premierebalikpapan', NULL, '(0542) 7213921', 'http://www.21cineplex.com/theater/bioskop-ewalk-premiere,377,BLPPREW.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(19, 'Ciwalk Premiere Bandung', NULL, '(022) 2061121', 'http://www.21cineplex.com/theater/bioskop-ciwalk-premiere,286,BDGPRCI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(20, 'Ciputra Cibubur Premiere Bekasi', NULL, '(021) 29377421', 'http://www.21cineplex.com/theater/bioskop-ciputra-cibubur-premiere,349,BKSPRCC.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(21, 'Grand Metropolitan Premierebekasi', NULL, '(021) 29464660', 'http://www.21cineplex.com/theater/bioskop-grand-metropolitan-premiere,352,BKSPRGM.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(22, 'Summarecon Mal Bekasi Premiere Bekasi', NULL, '(021) 29572421', 'http://www.21cineplex.com/theater/bioskop-summarecon-mal-bekasi-premiere,337,BKSPRSB.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(23, 'Beachwalk Premiere Denpasar', NULL, '(0361) 846 5621', 'http://www.21cineplex.com/theater/bioskop-beachwalk-premiere,324,DPRPRBW.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(24, 'Baywalk Pluit Premiere Jakarta', NULL, '(021) 29629621', 'http://www.21cineplex.com/theater/bioskop-baywalk-pluit-premiere,344,JKTPRBP.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(25, 'Emporium Pluit Premierejakarta', NULL, '(021) 6667 6561', 'http://www.21cineplex.com/theater/bioskop-emporium-pluit-premiere,281,JKTPREP.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(26, 'Gading Premiere Jakarta', NULL, '(021) 458 53 821', 'http://www.21cineplex.com/theater/bioskop-gading-premiere,296,JKTPRGA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(27, 'Gandaria Premiere Jakarta', NULL, '(021) 29053218', 'http://www.21cineplex.com/theater/bioskop-gandaria-premiere,301,JKTPRGN.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(28, 'Kasablanka Premierejakarta', NULL, '(021) 2946 5221', 'http://www.21cineplex.com/theater/bioskop-kasablanka-premiere,321,JKTPRKA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(29, 'Kemang Village Premierejakarta', NULL, '(021) 29056866', 'http://www.21cineplex.com/theater/bioskop-kemang-village-premiere,323,JKTPRKV.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(30, 'Lotte S. Avenue Xxi Premiere Jakarta', NULL, '(021) 29889421', 'http://www.21cineplex.com/theater/bioskop-lotte-s-avenue-xxi-premiere,334,JKTPRLS.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(31, 'Metropole Premierejakarta', NULL, '(021) 31922249', 'http://www.21cineplex.com/theater/bioskop-metropole-premiere,401,JKTPRME.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(32, 'One Bel Park Premierejakarta', NULL, '(021) 27653422', 'http://www.21cineplex.com/theater/bioskop-one-bel-park-premiere,388,JKTPROB.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(33, 'Plaza Indonesia Premiere Jakarta', NULL, '(021) 398 38 779', 'http://www.21cineplex.com/theater/bioskop-plaza-indonesia-premiere,362,JKTPRPN.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(34, 'Plaza Senayan Premiere Jakarta', NULL, '(021) 572 5536', 'http://www.21cineplex.com/theater/bioskop-plaza-senayan-premiere,255,JKTPRPS.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(35, 'Pondok Indah Premiere Jakarta', NULL, '(021) 7592-0784', 'http://www.21cineplex.com/theater/bioskop-pondok-indah-premiere,229,JKTPRPI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(36, 'Puri Premiere Jakarta', NULL, '(021) 582 2521', 'http://www.21cineplex.com/theater/bioskop-puri-premiere,285,JKTPRPU.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(37, 'Senayan City Premierejakarta', NULL, '(021) 7278 1221', 'http://www.21cineplex.com/theater/bioskop-senayan-city-premiere,360,JKTPRSC.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(38, 'St. Moritz Premierejakarta', NULL, '(021) 29111370', 'http://www.21cineplex.com/theater/bioskop-st-moritz-premiere,375,JKTPRSM.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(39, 'Panakkukang Premieremakassar', NULL, '(0411) 466 2023', 'http://www.21cineplex.com/theater/bioskop-panakkukang-premiere,397,UPGPRPA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(40, 'Mantos 3 Premieremanado', NULL, '(0431) 8890521', 'http://www.21cineplex.com/theater/bioskop-mantos-3-premiere,400,MNDPRM3.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(41, 'Lem Premieremataram', NULL, '(0370) 6172121', 'http://www.21cineplex.com/theater/bioskop-lem-premiere,395,MTRPRLE.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(42, 'Centre Point Premiere Medan', NULL, '(061) 80510321', 'http://www.21cineplex.com/theater/bioskop-centre-point-premiere,354,MDNPRCP.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(43, 'Ringroad Citywalks Premieremedan', NULL, '(061) 80026553', 'http://www.21cineplex.com/theater/bioskop-ringroad-citywalks-premiere,386,MDNPRRC.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(44, 'Opi Mall Premierepalembang', NULL, '(0711) 5624052', 'http://www.21cineplex.com/theater/bioskop-opi-mall-premiere,382,PLGPROP.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(45, 'Pim Premierepalembang', NULL, '(0711) 7623064', 'http://www.21cineplex.com/theater/bioskop-pim-premiere,367,PLGPRPI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(46, 'Grand Mall Palu Premierepalu', NULL, '(0451) 4131021', 'http://www.21cineplex.com/theater/bioskop-grand-mall-palu-premiere,384,PLUPRGR.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(47, 'Ciputra Seraya Premierepekanbaru', NULL, '(0761) 868 521', 'http://www.21cineplex.com/theater/bioskop-ciputra-seraya-premiere,380,PBRPRCS.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(48, 'Big Mall Premieresamarinda', NULL, '(0541) 6294777', 'http://www.21cineplex.com/theater/bioskop-big-mall-premiere,371,SMDPRBM.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(49, 'Solo Paragon Premiere Solo', NULL, '(0271) 7890756', 'http://www.21cineplex.com/theater/bioskop-solo-paragon-premiere,346,SKAPRSP.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(50, 'The Park Premiere Solo', NULL, '(0271) 789 1321', 'http://www.21cineplex.com/theater/bioskop-the-park-premiere,359,SKAPRTP.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(51, 'Ciputra World Premiere Surabaya', NULL, '(031) 512 00021', 'http://www.21cineplex.com/theater/bioskop-ciputra-world-premiere,314,SBYPRCW.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(52, 'Grand City Premieresurabaya', NULL, '(031) 524 05821', 'http://www.21cineplex.com/theater/bioskop-grand-city-premiere,303,SBYPRGC.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(53, 'Lenmarc Premieresurabaya', NULL, '(031) 5116 2921', 'http://www.21cineplex.com/theater/bioskop-lenmarc-premiere,305,SBYPRLE.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(54, 'Tunjungan 5 Premieresurabaya', NULL, '(031) 60621', 'http://www.21cineplex.com/theater/bioskop-tunjungan-5-premiere,390,SBYPRT5.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(55, 'Aeon Mall Bsd City Premieretangerang', NULL, '(021) 29168366', 'http://www.21cineplex.com/theater/bioskop-aeon-mall-bsd-city-premiere,379,TGRPRAB.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(56, 'Alam Sutera Premieretangerang', NULL, '(021) 30448331', 'http://www.21cineplex.com/theater/bioskop-alam-sutera-premiere,330,TGRPRAS.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(57, 'Karawaci Premieretangerang', NULL, '(021) 5421 2354', 'http://www.21cineplex.com/theater/bioskop-karawaci-premiere,295,TGRPRKA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(58, 'Living World Premiere Tangerang', NULL, '(021) 5312 5569', 'http://www.21cineplex.com/theater/bioskop-living-world-premiere,310,TGRPRLW.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(59, 'Summarecon Mal Serpong Premiere Tangerang', NULL, '(021) 542 026 21', 'http://www.21cineplex.com/theater/bioskop-summarecon-mal-serpong-premiere,364,TGRPRSS.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(60, 'Ambarrukmo Premiereyogyakarta', NULL, '(0274) 433 1221', 'http://www.21cineplex.com/theater/bioskop-ambarrukmo-premiere,369,YGYPRAM.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(61, 'Empire Premiereyogyakarta', NULL, '(0274) 551 021', 'http://www.21cineplex.com/theater/bioskop-empire-premiere,396,YGYPREM.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(62, 'Jogja City Premiereyogyakarta', NULL, '(0274) 5304221', 'http://www.21cineplex.com/theater/bioskop-jogja-city-premiere,366,YGYPRJC.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(63, 'Ambon City Center Xxiambon', NULL, '(0911) 362092', 'http://www.21cineplex.com/theater/bioskop-ambon-city-center-xxi,398,ABNAMCC.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(64, 'Studioambon', NULL, '(0911) 321 717', 'http://www.21cineplex.com/theater/bioskop-studio,269,ABNSTUO.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(65, 'E-walk Xxibalikpapan', NULL, '(0542) 7213921', 'http://www.21cineplex.com/theater/bioskop-ewalk-xxi,297,BLPEWAL.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(66, 'Studio Xxibalikpapan', NULL, '(0542) 7582621', 'http://www.21cineplex.com/theater/bioskop-studio-xxi,276,BLPSTUD.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(67, 'Braga Xxibandung', NULL, '(022) 844 60121', 'http://www.21cineplex.com/theater/bioskop-braga-xxi,231,BDGBRAG.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(68, 'Btc Xxibandung', NULL, '(022) 424019', 'http://www.21cineplex.com/theater/bioskop-btc-xxi,274,BDGBTC.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(69, 'Ciwalk Xxibandung', NULL, '(022) 2061017', 'http://www.21cineplex.com/theater/bioskop-ciwalk-xxi,249,BDGCIWL.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(70, 'Empire Xxibandung', NULL, '(022) 424 0719', 'http://www.21cineplex.com/theater/bioskop-empire-xxi,23,BDGEMPI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(71, 'Festival Citylink Xxibandung', NULL, '(022) 6128708', 'http://www.21cineplex.com/theater/bioskop-festival-citylink-xxi,326,BDGFECI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(72, 'Jatosbandung', NULL, '(022) 8792 0089', 'http://www.21cineplex.com/theater/bioskop-jatos,259,BDGJATO.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(73, 'Tsm Xxibandung', NULL, '(022) 910 91121', 'http://www.21cineplex.com/theater/bioskop-tsm-xxi,186,BDGBSM.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(74, 'Studio Xxibanjarmasin', NULL, '(0511) 436 5521', 'http://www.21cineplex.com/theater/bioskop-studio-xxi,251,BJMSTUO.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(75, 'Bcsbatam', NULL, '(0778) 7435 721', 'http://www.21cineplex.com/theater/bioskop-bcs,287,BTMBCS.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(76, 'Mega Xxibatam', NULL, '(0778) 466 121', 'http://www.21cineplex.com/theater/bioskop-mega-xxi,266,BTMMEGA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(77, 'Studiobatam', NULL, '(0778) 749 3521', 'http://www.21cineplex.com/theater/bioskop-studio,67,BTMSTUO.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(78, 'Bekasi Square Xxibekasi', NULL, '(021) 824 24 801', 'http://www.21cineplex.com/theater/bioskop-bekasi-square-xxi,263,BKSBESQ.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(79, 'Ciputra Cibubur  Xxibekasi', NULL, '(021) 29377421', 'http://www.21cineplex.com/theater/bioskop-ciputra-cibuburxxi,348,BKSCICI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(80, 'Grand Malbekasi', NULL, '(021) 889 0971', 'http://www.21cineplex.com/theater/bioskop-grand-mal,288,BKSGRMA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(81, 'Grand Metropolitan Xxibekasi', NULL, '(021) 29464660', 'http://www.21cineplex.com/theater/bioskop-grand-metropolitan-xxi,351,BKSGRME.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(82, 'Mega Bekasi Xxibekasi', NULL, '(021) 889 666 21', 'http://www.21cineplex.com/theater/bioskop-mega-bekasi-xxi,258,BKSMEBE.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(83, 'Metropolitan Xxibekasi', NULL, '(021) 884 4985', 'http://www.21cineplex.com/theater/bioskop-metropolitan-xxi,129,BKSMETL.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(84, 'Plasa Cibubur Xxibekasi', NULL, '(021) 84597403', 'http://www.21cineplex.com/theater/bioskop-plasa-cibubur-xxi,361,BKSPLCI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(85, 'Pondok Gedebekasi', NULL, '(021) 849 380 21', 'http://www.21cineplex.com/theater/bioskop-pondok-gede,257,BKSPOGD.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(86, 'Summarecon Mal Bekasi Xxibekasi', NULL, '(021) 29572421', 'http://www.21cineplex.com/theater/bioskop-summarecon-mal-bekasi-xxi,336,BKSSUBE.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(87, 'Megabengkulu', NULL, '(0736) 25333', 'http://www.21cineplex.com/theater/bioskop-mega,289,BKLMEGA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(88, 'Binjaibinjai', NULL, '(061) 882 6221', 'http://www.21cineplex.com/theater/bioskop-binjai,272,BNJBINJ.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(89, 'Bellanovabogor', NULL, '(021) 879 239 25', 'http://www.21cineplex.com/theater/bioskop-bellanova,260,BGRBELL.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(90, 'Botani Xxibogor', NULL, '(0251) 840 0821', 'http://www.21cineplex.com/theater/bioskop-botani-xxi,273,BGRBOTA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(91, 'Btmbogor', NULL, '(0251) 840 1241', 'http://www.21cineplex.com/theater/bioskop-btm,253,BGRBTM.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(92, 'Cibinong City Xxibogor', NULL, '(021) 2986 0091', 'http://www.21cineplex.com/theater/bioskop-cibinong-city-xxi,356,BGRCICI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(93, 'Cinerebogor', NULL, '(021) 754 0592', 'http://www.21cineplex.com/theater/bioskop-cinere,56,BGRCINE.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(94, 'Cinere Bellevue Xxibogor', NULL, '(021) 29403889', 'http://www.21cineplex.com/theater/bioskop-cinere-bellevue-xxi,368,BGRCIBE.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(95, 'Depokbogor', NULL, '(021) 776 0679', 'http://www.21cineplex.com/theater/bioskop-depok,57,BGRDEPK.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(96, 'Detosbogor', NULL, '(021) 788 70 221', 'http://www.21cineplex.com/theater/bioskop-detos,237,BGRDETO.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(97, 'Ekalokasaribogor', NULL, '(0251) 8363 137', 'http://www.21cineplex.com/theater/bioskop-ekalokasari,262,BGREKAL.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(98, 'Margo Platinumbogor', NULL, '(021) 7887 0901', 'http://www.21cineplex.com/theater/bioskop-margo-platinum,252,BGRMAPL.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(99, 'Cilegoncilegon', NULL, '(0254) 387 858', 'http://www.21cineplex.com/theater/bioskop-cilegon,136,CLGCILE.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(100, 'Csb Xxicirebon', NULL, '(0231) 8291921', 'http://www.21cineplex.com/theater/bioskop-csb-xxi,329,CRBCSB.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(101, 'Gragecirebon', NULL, '(0231) 222896', 'http://www.21cineplex.com/theater/bioskop-grage,161,CRBGRAG.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(102, 'Beachwalk Xxidenpasar', NULL, '(0361) 846 5621', 'http://www.21cineplex.com/theater/bioskop-beachwalk-xxi,325,DPRBEWA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(103, 'Galeria Xxidenpasar', NULL, '(0361) 767 021', 'http://www.21cineplex.com/theater/bioskop-galeria-xxi,188,DPRGALE.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(104, 'Park 23 Xxidenpasar', NULL, '(0361) 4727621', 'http://www.21cineplex.com/theater/bioskop-park-23-xxi,393,DPRPA23.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(105, 'Gorontalo Xxigorontalo', NULL, '(0435) 859 2521', 'http://www.21cineplex.com/theater/bioskop-gorontalo-xxi,357,GTOGORO.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(106, 'Anggrek Xxijakarta', NULL, '(021) 563 9403', 'http://www.21cineplex.com/theater/bioskop-anggrek-xxi,15,JKTANGG.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(107, 'Arion Xxijakarta', NULL, '(021) 475 7658', 'http://www.21cineplex.com/theater/bioskop-arion-xxi,34,JKTARIO.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(108, 'Artha Gading Xxijakarta', NULL, '(021) 4586 4123', 'http://www.21cineplex.com/theater/bioskop-artha-gading-xxi,245,JKTARGA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(109, 'Atrium Xxijakarta', NULL, '(021) 386 7830', 'http://www.21cineplex.com/theater/bioskop-atrium-xxi,19,JKTATRI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(110, 'Baywalk Pluit Xxijakarta', NULL, '(021) 29629621', 'http://www.21cineplex.com/theater/bioskop-baywalk-pluit-xxi,343,JKTBAPL.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(111, 'Blok M Plazajakarta', NULL, '(021) 720 9437', 'http://www.21cineplex.com/theater/bioskop-blok-m-plaza,35,JKTBLOM.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(112, 'Blok M Squarejakarta', NULL, '(021) 7280 2021', 'http://www.21cineplex.com/theater/bioskop-blok-m-square,282,JKTBMSQ.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(113, 'Cibuburjakarta', NULL, '(021) 877 56 588', 'http://www.21cineplex.com/theater/bioskop-cibubur,232,JKTCIBU.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(114, 'Cijantungjakarta', NULL, '(021) 877 93 446', 'http://www.21cineplex.com/theater/bioskop-cijantung,134,JKTCIJA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(115, 'Cipinang Xxijakarta', NULL, '(021) 29486938', 'http://www.21cineplex.com/theater/bioskop-cipinang-xxi,347,JKTCIPI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(116, 'Citra Xxijakarta', NULL, '(021) 560 0404', 'http://www.21cineplex.com/theater/bioskop-citra-xxi,37,JKTCITR.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(117, 'Daan Mogotjakarta', NULL, '(021) 544 6733', 'http://www.21cineplex.com/theater/bioskop-daan-mogot,194,JKTDAMG.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(118, 'Djakarta Xxijakarta', NULL, '(021) 315 6725', 'http://www.21cineplex.com/theater/bioskop-djakarta-xxi,250,JKTDJAR.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(119, 'Emporium Pluit Xxijakarta', NULL, '(021) 6667 6421', 'http://www.21cineplex.com/theater/bioskop-emporium-pluit-xxi,277,JKTEMPL.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(120, 'Epicentrum Xxijakarta', NULL, '021 2994 1300', 'http://www.21cineplex.com/theater/bioskop-epicentrum-xxi,299,JKTEPIC.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(121, 'Gadingjakarta', NULL, '(021) 453 0274', 'http://www.21cineplex.com/theater/bioskop-gading,39,JKTGADI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(122, 'Gading Xxijakarta', NULL, '(021) 458 53 821', 'http://www.21cineplex.com/theater/bioskop-gading-xxi,218,JKTGADN.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(123, 'Gajah Mada Xxijakarta', NULL, '(021) 634 0349', 'http://www.21cineplex.com/theater/bioskop-gajah-mada-xxi,38,JKTCENT.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(124, 'Gandaria Xxijakarta', NULL, '(021) 29053218', 'http://www.21cineplex.com/theater/bioskop-gandaria-xxi,300,JKTGAND.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(125, 'Hollywood Xxijakarta', NULL, '(021) 525 6351', 'http://www.21cineplex.com/theater/bioskop-hollywood-xxi,36,JKTHOKC.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(126, 'Kalibata Xxijakarta', NULL, '(021) 799 1870', 'http://www.21cineplex.com/theater/bioskop-kalibata-xxi,40,JKTKALI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(127, 'Kasablanka Xxijakarta', NULL, '(021) 2946 5221', 'http://www.21cineplex.com/theater/bioskop-kasablanka-xxi,320,JKTKASA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(128, 'Kemang Village Xxijakarta', NULL, '(021) 290 56866', 'http://www.21cineplex.com/theater/bioskop-kemang-village-xxi,322,JKTKEVI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(129, 'Kramat Jati Xxijakarta', NULL, '(021) 80877457', 'http://www.21cineplex.com/theater/bioskop-kramat-jati-xxi,332,JKTKRJT.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(130, 'Kuningan City Xxijakarta', NULL, '(021) 30480621', 'http://www.21cineplex.com/theater/bioskop-kuningan-city-xxi,319,JKTKUCI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(131, 'La Piazza Xxijakarta', NULL, '(021) 4586 5021', 'http://www.21cineplex.com/theater/bioskop-la-piazza-xxi,241,JKTLAPI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(132, 'Lotte Shopping Avenue Xxijakarta', NULL, '(021) 29889421', 'http://www.21cineplex.com/theater/bioskop-lotte-shopping-avenue-xxi,333,JKTLOSA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(133, 'Metropole Xxijakarta', NULL, '(021) 319 22 249', 'http://www.21cineplex.com/theater/bioskop-metropole-xxi,45,JKTMETR.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(134, 'One Bel Park Xxijakarta', NULL, '(021) 27653422', 'http://www.21cineplex.com/theater/bioskop-one-bel-park-xxi,387,JKTONBE.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(135, 'Pejaten Village Xxijakarta', NULL, '(021) 7823 112', 'http://www.21cineplex.com/theater/bioskop-pejaten-village-xxi,280,JKTPEVI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(136, 'Plaza Indonesia Xxijakarta', NULL, '(021) 398 38 779', 'http://www.21cineplex.com/theater/bioskop-plaza-indonesia-xxi,279,JKTSTUX.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(137, 'Plaza Senayan Xxijakarta', NULL, '(021) 572 5535', 'http://www.21cineplex.com/theater/bioskop-plaza-senayan-xxi,254,JKTPLSE.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(138, 'Pluit Junction Xxijakarta', NULL, '(021) 6660 7321', 'http://www.21cineplex.com/theater/bioskop-pluit-junction-xxi,264,JKTPLJU.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(139, 'Pluit Village Xxijakarta', NULL, '(021) 668 3621', 'http://www.21cineplex.com/theater/bioskop-pluit-village-xxi,168,JKTPLVI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(140, 'Pondok Indah 1 Xxijakarta', NULL, '(021) 750 6921', 'http://www.21cineplex.com/theater/bioskop-pondok-indah-1-xxi,32,JKTPOIN.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(141, 'Pondok Indah 2 Xxijakarta', NULL, '(021) 759 20 781', 'http://www.21cineplex.com/theater/bioskop-pondok-indah-2-xxi,221,JKTPOID.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(142, 'Puri Xxijakarta', NULL, '(021) 582 2285', 'http://www.21cineplex.com/theater/bioskop-puri-xxi,153,JKTPURI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(143, 'Seasons City Xxijakarta', NULL, '(021) 6385 3901', 'http://www.21cineplex.com/theater/bioskop-seasons-city-xxi,292,JKTSACI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(144, 'Senayan City Xxijakarta', NULL, '(021) 7278 1221', 'http://www.21cineplex.com/theater/bioskop-senayan-city-xxi,244,JKTSECI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(145, 'Setiabudi Xxijakarta', NULL, '(021) 521 0721', 'http://www.21cineplex.com/theater/bioskop-setiabudi-xxi,212,JKTSETI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(146, 'St. Moritz Xxijakarta', NULL, '(021) 29111370', 'http://www.21cineplex.com/theater/bioskop-st-moritz-xxi,374,JKTSTMO.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(147, 'Sunterjakarta', NULL, '(021) 658 32 898', 'http://www.21cineplex.com/theater/bioskop-sunter,156,JKTSUNT.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(148, 'Taminijakarta', NULL, '(021) 877 85 921', 'http://www.21cineplex.com/theater/bioskop-tamini,247,JKTTAMI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(149, 'Tim Xxijakarta', NULL, '(021) 319 25 130', 'http://www.21cineplex.com/theater/bioskop-tim-xxi,52,JKTTIM.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(150, 'Wtcjambi', NULL, '(0741) 783 7321', 'http://www.21cineplex.com/theater/bioskop-wtc,271,JMBWTC.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(151, 'Jayapura Xxijayapura', NULL, '(0967) 5150100', 'http://www.21cineplex.com/theater/bioskop-jayapura-xxi,342,JYPJAYA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(152, 'Boemi Kedaton Xxilampung', NULL, '(0721) 8015582', 'http://www.21cineplex.com/theater/bioskop-boemi-kedaton-xxi,372,LMPBOKE.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(153, 'Centrallampung', NULL, '(0721) 262 446', 'http://www.21cineplex.com/theater/bioskop-central,278,LMPCENT.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(154, 'M`tosmakassar', NULL, '(0411) 583 321', 'http://www.21cineplex.com/theater/bioskop-mtos,267,UPGMTOS.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(155, 'Panakkukangmakassar', NULL, '(0411) 424 158', 'http://www.21cineplex.com/theater/bioskop-panakkukang,208,UPGPANA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(156, 'Panakkukang Xximakassar', NULL, '(0411) 466 2023', 'http://www.21cineplex.com/theater/bioskop-panakkukang-xxi,312,UPGPANK.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(157, 'Studio Xximakassar', NULL, '(0411) 851-721', 'http://www.21cineplex.com/theater/bioskop-studio-xxi,226,UPGSTUR.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(158, 'Tsm Xximakassar', NULL, '(0411) 8117221', 'http://www.21cineplex.com/theater/bioskop-tsm-xxi,335,UPGTSM.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(159, 'Diengmalang', NULL, '(0341) 575421', 'http://www.21cineplex.com/theater/bioskop-dieng,284,MLGDIEG.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(160, 'Mandalamalang', NULL, '(0341) 366 560', 'http://www.21cineplex.com/theater/bioskop-mandala,31,MLGMAND.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(161, 'Mantosmanado', NULL, '(0431) 888 1682', 'http://www.21cineplex.com/theater/bioskop-mantos,196,MNDSTUO.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(162, 'Mantos 3 Xximanado', NULL, '(0431) 8890521', 'http://www.21cineplex.com/theater/bioskop-mantos-3-xxi,399,MNDMAN3.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(163, 'Mega Mall Xximanado', NULL, '(0431) 8890112', 'http://www.21cineplex.com/theater/bioskop-mega-mall-xxi,373,MNDMEMA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(164, 'Lem Xximataram', NULL, '(0370) 6172121', 'http://www.21cineplex.com/theater/bioskop-lem-xxi,394,MTRLEM.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(165, 'Centre Point Xximedan', NULL, '(061) 80510321', 'http://www.21cineplex.com/theater/bioskop-centre-point-xxi,353,MDNCEPO.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(166, 'Hermes Xximedan', NULL, '(061) 8050 1121', 'http://www.21cineplex.com/theater/bioskop-hermes-xxi,317,MDNHERM.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(167, 'Palladiummedan', NULL, '(061) 451 4321', 'http://www.21cineplex.com/theater/bioskop-palladium,239,MDNPALL.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(168, 'Ringroad Citywalks Xximedan', NULL, '(061) 80026553', 'http://www.21cineplex.com/theater/bioskop-ringroad-citywalks-xxi,385,MDNRICI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(169, 'Thamrinmedan', NULL, '(061) 736 6855', 'http://www.21cineplex.com/theater/bioskop-thamrin,81,MDNTHAM.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(170, 'Palmapalangkaraya', NULL, '(0563) 322 7221', 'http://www.21cineplex.com/theater/bioskop-palma,290,PLKPALM.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(171, 'Internasionalpalembang', NULL, '(0711) 357766', 'http://www.21cineplex.com/theater/bioskop-internasional,197,PLGINTE.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(172, 'Opi Mall Xxipalembang', NULL, '(0711) 5624052', 'http://www.21cineplex.com/theater/bioskop-opi-mall-xxi,381,PLGOPI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(173, 'Palembang Square Xxipalembang', NULL, '(0711) 380721', 'http://www.21cineplex.com/theater/bioskop-palembang-square-xxi,338,PLGPASQ.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(174, 'Pim Xxipalembang', NULL, '(0711) 7623064', 'http://www.21cineplex.com/theater/bioskop-pim-xxi,242,PLGPIM.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(175, 'Grand Mall Palu Xxipalu', NULL, '(0451) 4131021', 'http://www.21cineplex.com/theater/bioskop-grand-mall-palu-xxi,383,PLUGRAN.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(176, 'Ciputra Seraya Xxipekanbaru', NULL, '(0761) 868 521', 'http://www.21cineplex.com/theater/bioskop-ciputra-seraya-xxi,224,PBRRIAU.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(177, 'Ayani Xxipontianak', NULL, '(0561) 763 666', 'http://www.21cineplex.com/theater/bioskop-ayani-xxi,217,PTKAYAN.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(178, 'Big Mall Xxisamarinda', NULL, '(0541) 6294777', 'http://www.21cineplex.com/theater/bioskop-big-mall-xxi,370,SMDBIMA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(179, 'Scpsamarinda', NULL, '(0541) 749632', 'http://www.21cineplex.com/theater/bioskop-scp,192,SMDSTUD.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(180, 'Scp Xxisamarinda', NULL, '(0541) 742221', 'http://www.21cineplex.com/theater/bioskop-scp-xxi,316,SMDSTUO.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(181, 'Citra Xxisemarang', NULL, '(024) 841 5971', 'http://www.21cineplex.com/theater/bioskop-citra-xxi,101,SMGCITR.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(182, 'Paragon Xxisemarang', NULL, '(024) 865 791 21', 'http://www.21cineplex.com/theater/bioskop-paragon-xxi,298,SMGPARA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(183, 'Sgm Xxi Singkawangsingkawang', NULL, '(0562) 6300062', 'http://www.21cineplex.com/theater/bioskop-sgm-xxi-singkawang,392,SKWSGM.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(184, 'Grandsolo', NULL, '(0271) 733 721', 'http://www.21cineplex.com/theater/bioskop-grand,219,SKAGRAN.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(185, 'Solo Paragon Xxisolo', NULL, '(0271) 7881921', 'http://www.21cineplex.com/theater/bioskop-solo-paragon-xxi,345,SKASOPA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(186, 'Solo Square Xxisolo', NULL, '(0271) 7654 221', 'http://www.21cineplex.com/theater/bioskop-solo-square-xxi,315,SKASOSQ.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(187, 'The Park Xxisolo', NULL, '(0271) 789 1321', 'http://www.21cineplex.com/theater/bioskop-the-park-xxi,358,SKATHPA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(188, 'Ciputra World Xxisurabaya', NULL, '(031) 512 00021', 'http://www.21cineplex.com/theater/bioskop-ciputra-world-xxi,313,SBYCIWO.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(189, 'Citosurabaya', NULL, '(031) 582 51 221', 'http://www.21cineplex.com/theater/bioskop-cito,268,SBYCITO.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(190, 'Deltasurabaya', NULL, '(031) 531 1668', 'http://www.21cineplex.com/theater/bioskop-delta,83,SBYDELT.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(191, 'Galaxy Xxisurabaya', NULL, '(031) 593 7121', 'http://www.21cineplex.com/theater/bioskop-galaxy-xxi,85,SBYGALA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(192, 'Grand City Xxisurabaya', NULL, '(031) 524 05821', 'http://www.21cineplex.com/theater/bioskop-grand-city-xxi,302,SBYGRCI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(193, 'Lenmarc Xxisurabaya', NULL, '(031) 5116 2921', 'http://www.21cineplex.com/theater/bioskop-lenmarc-xxi,304,SBYLENM.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(194, 'Pakuwon City Xxisurabaya', NULL, '(031) 5820 8821', 'http://www.21cineplex.com/theater/bioskop-pakuwon-city-xxi,311,SBYPACI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(195, 'Royalsurabaya', NULL, '(031) 827-1521', 'http://www.21cineplex.com/theater/bioskop-royal,248,SBYROYA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(196, 'Supermal Xxisurabaya', NULL, '(031) 739 0221', 'http://www.21cineplex.com/theater/bioskop-supermal-xxi,203,SBYSTUO.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(197, 'Sutos Xxisurabaya', NULL, '(031) 563-3021', 'http://www.21cineplex.com/theater/bioskop-sutos-xxi,261,SBYSUTO.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(198, 'Tunjungansurabaya', NULL, '(031) 547 2041', 'http://www.21cineplex.com/theater/bioskop-tunjungan,91,SBYTUNJ.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(199, 'Tunjungan 5 Xxisurabaya', NULL, '(031) 51164521', 'http://www.21cineplex.com/theater/bioskop-tunjungan-5-xxi,389,SBYTUN5.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(200, 'Tunjungan Xxisurabaya', NULL, '(031) 532 6621', 'http://www.21cineplex.com/theater/bioskop-tunjungan-xxi,265,SBYTUNU.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(201, 'Aeon Mall Bsd City Xxitangerang', NULL, '(021) 29168366', 'http://www.21cineplex.com/theater/bioskop-aeon-mall-bsd-city-xxi,378,TGRAEBS.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(202, 'Alam Sutera Xxitangerang', NULL, '(021) 30448331', 'http://www.21cineplex.com/theater/bioskop-alam-sutera-xxi,327,TGRALSU.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(203, 'Bale Kota Xxitangerang', NULL, '(021) 2966 8521', 'http://www.21cineplex.com/theater/bioskop-bale-kota-xxi,341,TGRBAKO.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(204, 'Bintaro Xchange Xxitangerang', NULL, '(021) 29864824', 'http://www.21cineplex.com/theater/bioskop-bintaro-xchange-xxi,350,TGRBIXC.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(205, 'Bintaro Xxitangerang', NULL, '(021) 735 5371', 'http://www.21cineplex.com/theater/bioskop-bintaro-xxi,54,TGRBINT.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(206, 'Bsd Xxitangerang', NULL, '(021) 537 2704', 'http://www.21cineplex.com/theater/bioskop-bsd-xxi,126,TGRSERP.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(207, 'Cbd Ciledug Xxitangerang', NULL, '(021) 7345 2221', 'http://www.21cineplex.com/theater/bioskop-cbd-ciledug-xxi,291,TGRCBCI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(208, 'Karawacitangerang', NULL, '(021) 5421 2354', 'http://www.21cineplex.com/theater/bioskop-karawaci,294,TGRLIKA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(209, 'Karawaci Xxitangerang', NULL, '(021) 5421 2354', 'http://www.21cineplex.com/theater/bioskop-karawaci-xxi,122,TGRKARA.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(210, 'Living World Xxitangerang', NULL, '(021) 5312 5569', 'http://www.21cineplex.com/theater/bioskop-living-world-xxi,309,TGRLIWO.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(211, 'Lotte Bintaro Xxitangerang', NULL, '(021) 29310921', 'http://www.21cineplex.com/theater/bioskop-lotte-bintaro-xxi,328,TGRLOBI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(212, 'Summarecon Mal Serpong Xxitangerang', NULL, '(021) 542 026 21', 'http://www.21cineplex.com/theater/bioskop-summarecon-mal-serpong-xxi,256,TGRSERO.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(213, 'Tang City Xxitangerang', NULL, '(021) 29309581', 'http://www.21cineplex.com/theater/bioskop-tang-city-xxi,363,TGRTACI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(214, 'Tasiktasikmalaya', NULL, '(0265) 235 0021', 'http://www.21cineplex.com/theater/bioskop-tasik,293,TSMTASI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(215, 'Ambarukkmo Xxiyogyakarta', NULL, '(0274) 433 1221', 'http://www.21cineplex.com/theater/bioskop-ambarukkmo-xxi,238,YGYSTUD.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(216, 'Empire Xxiyogyakarta', NULL, '(0274) 551 021', 'http://www.21cineplex.com/theater/bioskop-empire-xxi,283,YGYEMPR.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(217, 'Jogja City Xxiyogyakarta', NULL, '(0274) 5304221', 'http://www.21cineplex.com/theater/bioskop-jogja-city-xxi,365,YGYJOCI.htm', '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_12_25_040013_usersTable', 1),
('2015_12_25_040108_passwordResetsTable', 1),
('2015_12_25_054831_moviesTable', 1),
('2015_12_25_064317_categoriesTable', 1),
('2015_12_25_064624_schedulesTable', 1),
('2015_12_31_050542_cinemasTable', 1);

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `id` int(10) UNSIGNED NOT NULL,
  `cinema_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `showtimes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `booking_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `genre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `showing` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`id`, `cinema_id`, `title`, `description`, `showtimes`, `image`, `image_url`, `youtube_url`, `booking_url`, `genre`, `rate`, `showing`, `created_at`, `updated_at`) VALUES
(1, 10, 'this is sample title', 'fasdfas', '12:30,11:00', '', '', 'dfasdf', '', 'Action, Horror', 'rmr,r,r,r', 0, '2016-03-25 06:50:26', '2016-03-28 01:56:41'),
(2, 13, 'BATMAN V SUPERMAN: DAWN OF JUSTICE (IMAX 3D)', 'Pasca Metropolis hancur karena pertarungan Superman dan General Zod, dunia terbelah menjadi dua. Satu yang mendukung Superman sebagai harapan dan satu lagi yang menyebutnya sebagai sebuah ancaman. Dunia berdebat jenis pahlawan apa yang dibutuhkan oleh masyarakat. Sementara Batman dan Superman bertarung, teror diciptakan oleh Lex Luthor (Jesse Eisenberg). Kini Superman dan Batman harus mengesampingkan perselisihan mereka. Bersama Wonder Woman (Gal Gadot) keduanya berusaha menghentikan rencana jahat Lex Luthor dan Doomsday menghancurkan Metropolis.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Ini Dia Penampilan Terbaru Wonder Woman - 25 Mar \'16Istri Johnny Depp Akan Menjadi Kekasih Aquaman - 21 Mar \'16Clark Kent Lebih Diunggulkan di Batman v Superman: Dawn of Justice - 04 Mar \'16', '12:45 |15:45 |18:45 |21:45 ', '', 'http://www.21cineplex.com/data/gallery/pictures/145745173274732_300x430.jpg', '', '', 'Action,  Adventure,  Fantasy', 'RT13', 0, '2016-03-28 02:20:45', '2016-03-28 02:20:45'),
(3, 13, 'BATMAN V SUPERMAN: DAWN OF JUSTICE (IMAX 3D)', 'Pasca Metropolis hancur karena pertarungan Superman dan General Zod, dunia terbelah menjadi dua. Satu yang mendukung Superman sebagai harapan dan satu lagi yang menyebutnya sebagai sebuah ancaman. Dunia berdebat jenis pahlawan apa yang dibutuhkan oleh masyarakat. Sementara Batman dan Superman bertarung, teror diciptakan oleh Lex Luthor (Jesse Eisenberg). Kini Superman dan Batman harus mengesampingkan perselisihan mereka. Bersama Wonder Woman (Gal Gadot) keduanya berusaha menghentikan rencana jahat Lex Luthor dan Doomsday menghancurkan Metropolis.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Ini Dia Penampilan Terbaru Wonder Woman - 25 Mar \'16Istri Johnny Depp Akan Menjadi Kekasih Aquaman - 21 Mar \'16Clark Kent Lebih Diunggulkan di Batman v Superman: Dawn of Justice - 04 Mar \'16', '12:45 |15:45 |18:45 |21:45 ', '', 'http://www.21cineplex.com/data/gallery/pictures/145745173274732_300x430.jpg', '', '', 'Action,  Adventure,  Fantasy', 'RT13', 0, '2016-03-28 02:20:45', '2016-03-28 02:20:45');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(10) UNSIGNED NOT NULL,
  `movie_id` int(11) NOT NULL,
  `cinema_id` int(11) NOT NULL,
  `booking_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `schedule` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$K2V8hQNVF6lZ0hLgZTIFH.5W/cNdcqE2gV5ZgUyAwoNZ3D8FtcyWS', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cinemas`
--
ALTER TABLE `cinemas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cinemas`
--
ALTER TABLE `cinemas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=218;
--
-- AUTO_INCREMENT for table `movies`
--
ALTER TABLE `movies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
