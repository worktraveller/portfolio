<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cinema_id');
            $table->string('title', 100);
            $table->text('description');
            $table->string('showtimes', 100);
            $table->string('image');
            $table->string('image_url', 100);
            $table->string('youtube_url', 100);
            $table->string('booking_url', 255);
            $table->string('genre', 100);
            $table->string('rate', 50);
            $table->boolean('showing', 1);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movies');
    }
}
