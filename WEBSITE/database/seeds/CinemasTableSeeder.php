<?php

use Illuminate\Database\Seeder;

class CinemasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$datas = array(
	        array(
	        	'name' => 'CGV Blitz Pacific Place',
				'address' => 'Pacific Place Mall Lt. 6, Jl. Jend Sudirman Kav. 52-53, Daerah Khusus Ibukota Jakarta 12190, Indonesia',
				'phone' => '+62 22 82063630',
				'latitude' => '-6.226676',
				'longitude' => '106.810463'
	    	),
	    	array(
	        	'name' => 'Cinema 21',
				'address' => 'Blok M Square Lt. 5, Jl. Melawai V, Kby. Baru, South Jakarta City, Daerah Khusus Ibukota Jakarta 12160, Indonesia',
				'phone' => '+62 22 82063630',
				'latitude' => '-6.245783',
				'longitude' => '106.799558'
	    	),
	    	array(
	        	'name' => 'Cinema 21, Daan Mogot',
				'address' => 'Mal Daan Mogot, Jl. Raya Daan Mogot KM 16, Special Capital Region of Jakarta 11840, Indonesia',
				'phone' => '+62 22 82063630',
				'latitude' => '-6.152256',
				'longitude' => '106.702825'
	    	),
	    	array(
	        	'name' => 'Sutos XXI',
				'address' => 'Surabaya Town Square Lt. 2, Jl. Adityawarman, Jawa Timur 60242, Indonesia',
				'phone' => '+62 22 82063630',
				'latitude' => '-7.295377',
				'longitude' => '112.733134'
	    	),
	    	array(
	        	'name' => 'Cinema XXI',
				'address' => 'Jl. Kejawen Putih Mutiara No. 17, Pakuwon City Lantai 3, Jawa Timur 60112, Indonesia',
				'phone' => '+62 22 82063630',
				'latitude' => '-7.278462',
				'longitude' => '112.806402'
	    	),
	    	array(
	    		'name' => 'Supermal XXI',
				'address' => 'Jl. Puncak Indah Lontar No. 2, Pakuwon Supermal/Trade Centre Lantai 1,, Jawa Timur 60112, Indonesia',
				'phone' => '+62 22 82063630',
				'latitude' => '-7.271045',
				'longitude' => '112.807572'
	    	),
	    	array(
	    		'name' => 'Tharmin 21',
				'address' => 'Jl. MH Thamrin No.75R, Sumatera Utara 20212, Indonesia',
				'phone' => '+62 22 82063630',
				'latitude' => '3.586403',
				'longitude' => '98.691376'
	    	),
	    	array(
	    		'name' => 'Cinema XXI',
				'address' => 'Centre Point Lt. 3A, Jalan Jawa No 8, Sumatera Utara 20231, Indonesia',
				'phone' => '+62 22 82063630',
				'latitude' => '3.591931',
				'longitude' => '98.680181'
	    	),
	    	array(
	    		'name' => 'Hermes XXI',
				'address' => 'Jl. Wolter Monginsidi No.45, Hermes Place Polonia, Sumatera Utara 20152, Indonesia',
				'phone' => '+62 22 82063630',
				'latitude' => '3.579305',
				'longitude' => '98.664184'
	    	),
	    	array(
	    		'name' => 'Bioscop Empire XXI',
				'address' => 'Jalan Urip Sumoharjo No. 104, Gondokusuman, Daerah Istimewa Yogyakarta 55221, Indonesia',
				'phone' => '+62 22 82063630',
				'latitude' => '-7.783130',
				'longitude' => '110.383267'
	    	),
	    	array(
	    		'name' => 'Cinema XXI',
				'address' => 'Jl. Jend. Gatot Subroto, Trans Studio Mall Lantai 3, Jawa Barat 40237, Indonesia',
				'phone' => '+62 22 82063630',
				'latitude' => '-6.954757',
				'longitude' => '107.605996'
	    	),
	    	array(
	    		'name' => 'CGV Blitz',
				'address' => 'Paris Van Java, Jl. Sukajadi No. 137-139, Pasteur, Sukajadi, Jawa Barat 40162, Indonesia',
				'phone' => '+62 22 82063630',
				'latitude' => '-6.891289',
				'longitude' => '107.599337'
	    	)
	    );

		foreach ($datas as $data) {
			DB::table('cinemas')->insert($data);
		}
    }
}
