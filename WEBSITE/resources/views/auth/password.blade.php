@extends('public')

@section('title')
ICEBOX | Forgot password
@endsection

@section('content')
<div class="passwordBox animated fadeInDown">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox-content">
        <h2 class="font-bold">Forgot password</h2>
        <p>
          Enter your email address and your password will be reset and emailed to you.
        </p>
        @if (session('status'))
          <div class="alert alert-success">
            {{ session('status') }}
          </div>
        @endif
        @if (count($errors) > 0)
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        <div class="row">
          <div class="col-lg-12">
            <form class="m-t" role="form" method="POST" action="{{ url('/password/email') }}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group">
                <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email address" required="">
              </div>
              <button type="submit" class="btn btn-primary block full-width m-b">Send new password</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <hr/>
  <div class="row">
    <div class="col-md-6">
      <small>Developed by Peter Porras</small>
    </div>
    <div class="col-md-6 text-right">
     <small>IceBox &copy; {{ date('Y') }}</small>
   </div>
 </div>
</div>
@endsection
