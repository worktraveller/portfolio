@extends('public')

@section('title')
ICEBOX | Register
@endsection

@section('content')
<div class="middle-box text-center loginscreen   animated fadeInDown">
  <div>
    <!-- <div>
      <h1 class="logo-name">IceBox</h1>
    </div> -->
    <h3>Register to IceBox</h3>
    <p>Create account to see it in action.</p>
    @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Whoops!</strong> <br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <form class="m-t" role="form" method="POST" action="{{ url('/auth/register') }}">
    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Username" name="username" value="{{ old('username') }}" required="">
      </div>
      <div class="form-group">
        <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required="">
      </div>
      <div class="form-group">
        <input type="password" class="form-control" placeholder="Password" name="password" required="">
      </div>
      <div class="form-group">
        <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" required="">
      </div>
      <button type="submit" class="btn btn-primary block full-width m-b">Register</button>
      <p class="text-muted text-center"><small>Already have an account?</small></p>
      <a class="btn btn-sm btn-white btn-block" href="{{ url('/auth/login') }}">Login</a>
    </form>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('/js/plugins/iCheck/icheck.min.js') }}"></script>
@endsection
