<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Mirrored from seantheme.com/color-admin-v1.7/admin/html/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Apr 2015 11:01:45 GMT -->
<head>
  <meta charset="utf-8" />
  <title>POPCORN | Mobile App</title>
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta content="" name="description" />
  <meta content="" name="author" />
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <link href="{{ asset('/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('/css/animate.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('/css/style.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('/css/style-responsive.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('/css/theme/default.css') }}" rel="stylesheet" id="theme" />
  <script src="{{ asset('/plugins/pace/pace.min.js') }}"></script>
  @yield('head')
</head>
<body class="pace-top">
  <!-- begin #page-loader -->
  <div id="page-loader" class="fade in"><span class="spinner"></span></div>
  <!-- end #page-loader -->
  
  <!-- begin #page-container -->
  <div id="page-container" class="fade">
      <!-- begin login -->
        <div class="login bg-black animated fadeInDown">
            <!-- begin brand -->
            <div class="login-header">
                <div class="brand">
                    <span class="logo"></span> PopCorn
                    <small>Movie Listing and Booking App</small>
                </div>
                <div class="icon">
                    <i class="fa fa-sign-in"></i>
                </div>
            </div>
            <!-- end brand -->
            <div class="login-content">
              @if (count($errors) > 0)
              <div class="alert alert-danger">
                <strong>Whoops!</strong>
                <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div><br>
              @endif
                <form action="{{ url('/auth/login') }}" method="POST" class="margin-bottom-0">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group m-b-20">
                        <input type="text" class="form-control input-lg" name="email" value="{{ old('email') }}" placeholder="Email Address" />
                    </div>
                    <div class="form-group m-b-20">
                        <input type="password" class="form-control input-lg" name="password" placeholder="Password" />
                    </div>
                    <!-- <div class="checkbox m-b-20">
                        <label>
                            <input type="checkbox" /> Remember Me
                        </label>
                    </div> -->
                    <div class="login-buttons">
                        <button type="submit" class="btn btn-success btn-block btn-lg">Sign me in</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- end login -->
  </div>
  <!-- end page container -->
  
  <!-- ================== BEGIN BASE JS ================== -->
  <script src="{{asset('/plugins/jquery/jquery-1.9.1.min.js')}}"></script>
  <script src="{{asset('/plugins/jquery/jquery-migrate-1.1.0.min.js')}}"></script>
  <script src="{{asset('/plugins/jquery-ui/ui/minified/jquery-ui.min.js')}}"></script>
  <script src="{{asset('/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
  <!--[if lt IE 9]>
    <script src="{{asset('/crossbrowserjs/html5shiv.js')}}"></script>
    <script src="{{asset('/crossbrowserjs/respond.min.js')}}"></script>
    <script src="{{asset('/crossbrowserjs/excanvas.min.js')}}"></script>
  <![endif]-->
  <script src="{{asset('/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
  <script src="{{asset('/plugins/jquery-cookie/jquery.cookie.js')}}"></script>
  <!-- ================== END BASE JS ================== -->
  
  <!-- ================== BEGIN PAGE LEVEL JS ================== -->
  <script src="{{asset('/js/apps.min.js')}}"></script>
  <!-- ================== END PAGE LEVEL JS ================== -->
  
  <script>
    $(document).ready(function() {
      App.init();
    });
  </script>
</body>

<!-- Mirrored from seantheme.com/color-admin-v1.7/admin/html/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Apr 2015 11:01:45 GMT -->
</html>
