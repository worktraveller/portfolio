@extends('admin')

@section('title')
Settings
@endsection

@section('content')
<div class="row ">
	<div class="col-md-12">
		@if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

	</div>
	<div class="col-md-6 ui-sortable">
		<div class="panel panel-inverse">
			<div class="panel-heading">Reset Password</div>
			<div class="panel-body">
				<form class="form-horizontal form-bordered" role="form" method="POST" action="{{ url('settings/password') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="form-group">
						<label class="col-md-4 control-label">E-Mail Address</label>
						<div class="col-md-6">
							<input type="email" class="form-control" name="email" value="{{old('email')}}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Password</label>
						<div class="col-md-6">
							<input type="password" class="form-control" name="password">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Confirm Password</label>
						<div class="col-md-6">
							<input type="password" class="form-control" name="password_confirmation">
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="btn btn-primary">
								Reset Password
							</button>
						</div>
					</div>
				</form><!-- form -->
			</div><!-- panel body -->
		</div><!-- panel Default -->

	</div>
	<div class="col-md-6 ui-sortable">

		<div class="panel panel-inverse" data-sortable-id="ui-typography-3">
            <div class="panel-heading">
                <h4 class="panel-title">Change Name & Email</h4>
            </div>
            <div class="panel-body">
            	<form class="form-horizontal form-bordered" role="form" method="POST" action="{{ url('settings/email') }}">
            		<input type="hidden" name="_token" value="{{ csrf_token() }}">
            		<div class="form-group">
						<label class="col-md-4 control-label">Name</label>
						<div class="col-md-6">
							<input type="text" class="form-control" name="name" value="{{ $user->name }}">
						</div>
					</div>
            		<div class="form-group">
						<label class="col-md-4 control-label">E-Mail Address</label>
						<div class="col-md-6">
							<input type="email" class="form-control" name="email" value="{{ $user->email }}">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="btn btn-primary">
								Update
							</button>
						</div>
					</div>
            	</form>
            </div>
        </div>
	</div><!-- col -->
</div>
@endsection