@extends('admin')

@section('title')
Dashboard
@endsection

@section('content')
<div class="row ">
	<!-- begin col-3 -->
	<div class="col-md-3 col-sm-6">
		<div class="widget widget-stats bg-green">
			<div class="stats-icon"><i class="fa fa-film"></i></div>
			<div class="stats-info">
				<h4>TOTAL MOVIES</h4>
				<p>{{number_format($movie_count)}}</p>	
			</div>
			<div class="stats-link">
				<a href="{{url('admin/movies')}}">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-md-3 col-sm-6">
		<div class="widget widget-stats bg-blue">
			<div class="stats-icon"><i class="fa fa-video-camera"></i></div>
			<div class="stats-info">
				<h4>TOTAL CINEMA'S</h4>
				<p>{{number_format($cinema_count)}}</p>	
			</div>
			<div class="stats-link">
				<a href="{{url('admin/cinemas')}}">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-md-3 col-sm-6">
		<div class="widget widget-stats bg-purple">
			<div class="stats-icon"><i class="fa fa-clock-o"></i></div>
			<div class="stats-info">
				<h4>MOVIE SCHEDULES</h4>
				<p>{{number_format($schedule_count)}}</p>	
			</div>
			<div class="stats-link">
				<a href="javascript:;">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<!-- <div class="col-md-3 col-sm-6">
		<div class="widget widget-stats bg-red">
			<div class="stats-icon"><i class="fa fa-clock-o"></i></div>
			<div class="stats-info">
				<h4>AVG TIME ON SITE</h4>
				<p>00:12:23</p>	
			</div>
			<div class="stats-link">
				<a href="javascript:;">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
			</div>
		</div>
	</div> -->
	<!-- end col-3 -->
</div>
@endsection