@extends('admin')

@section('title')
Cinema's
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
	    <!-- begin panel -->
	    @if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
		@endif
		@if (session('error'))
		    <div class="alert alert-danger">
		        {{ session('error') }}
		    </div>
		@endif
		<a href="{{ url('admin/cinemas/create') }}" class="btn btn-success m-r-5 m-b-5">
        	<i class="fa fa-plus"></i> Add Cinema
        </a>
	    <div class="panel panel-inverse" data-sortable-id="form-stuff-3">
	        <div class="panel-heading">
	            <div class="panel-heading-btn">
	                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
	                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
	                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
	                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
	            </div>
	            <h4 class="panel-title">Cinema's</h4>
	        </div>
	        <div class="panel-body">
	            <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@if( $cinemas->count() )
	                    	@foreach ($cinemas as $cinema)
		                    	<tr>
		                            <td>{{ $cinema->name }}</td>
		                            <td>{{ $cinema->address }}</td>
		                            <td><a href="{{url('admin/cinemas/'.$cinema->id.'/edit')}}" class="btn btn-xs btn-success">Edit <i class="fa fa-edit"></i></a></td>
		                        </tr>
						    @endforeach
					    @else
							<tr>
								<td>You may now start adding cinema's.</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
					    @endif
                    </tbody>
                </table>

                <p>{!! $cinemas->render() !!}</p>
	        </div>
	    </div>
	    <!-- end panel -->
	</div>
</div>
@endsection