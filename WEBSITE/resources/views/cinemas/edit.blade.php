@extends('admin')

@section('title')
Edit Cinema
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
	    <!-- begin panel -->
	    @if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
		@endif
	    <div class="panel panel-inverse" data-sortable-id="form-stuff-3">
	        <div class="panel-heading">
	            <div class="panel-heading-btn">
	                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
	                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
	                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
	                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
	            </div>
	            <a href="{{url('admin/cinemas')}}" class="btn btn-sm btn-success"><i class="fa fa-angle-double-left"></i> go back</a>
	            <h4 class="panel-title"></h4>

	        </div>
	        <div class="panel-body">
	        	<div class="col-md-6">
		            <form id="crate-cinema" class="form-horizontal form-bordered" action="{{url('admin/cinemas/'.$cinema->id.'/update')}}" method="POST">
		            	{{ csrf_field() }}
	                    <fieldset>
	                        @include('errors.errors')
	                    
	                		<legend>Details</legend>
	                		<div class="form-group">
	                            <label class="col-md-4 control-label">Cinema Name</label>
	                            <div class="col-md-8">
	                                <input type="text" name="name" value="{{$cinema->name}}" class="form-control" id="cinema-name" placeholder="Cinema Name">
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-md-4 control-label">Full Address</label>
	                            <div class="col-md-8">
	                                <input type="text" name="address" value="{{$cinema->address}}" class="form-control" id="address" placeholder="Address">
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-md-4 control-label">Phone</label>
	                            <div class="col-md-8">
	                                <input type="text" name="phone" value="{{$cinema->phone}}" class="form-control" id="masked-input-phone" placeholder="(999) 999-9999" />
	                            </div>
	                        </div>
							<div class="form-group">
	                            <label class="col-md-4 control-label">Latitude</label>
	                            <div class="col-md-8">
	                                <input type="text" name="latitude" value="{{$cinema->latitude}}" class="form-control" id="latitude" placeholder="10.5718489">
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-md-4 control-label">Longitude</label>
	                            <div class="col-md-8">
	                                <input type="text" name="longitude" value="{{$cinema->longitude}}" class="form-control" id="longitude" placeholder="122.608031">
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <div class="col-md-8 col-md-offset-4">
	                                <button id="submit-cinema" type="submit" class="btn btn-primary m-r-5">
	                                	<i class="fa fa-save"></i> Save
	                            	</button>
	                            	<a href="#modal-alert" id="submit-cinema" data-toggle="modal" class="btn btn-danger m-r-5">
	                                	<i class="fa fa-close"></i> Delete
	                            	</a>
	                            </div>
	                        </div>
	                    </fieldset>
	                </form>
            	</div>
                <div class="col-md-6">
					<p><i>Select a Point in the map to select map coordinates.</i></p>
					<div id="map" style="height: 400px; width: 100%;"></div>
				</div>
	        </div>
	    </div>
	    <!-- end panel -->
	</div>
</div>

<div class="modal fade" id="modal-alert">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<div class="alert alert-danger m-b-0">
					<h4><i class="fa fa-info-circle"></i> Are you sure?</h4>
					<p>Removing this cinema will also remove all schedules affilated with this cinema.</p>
				</div>
			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Cancel</a>
				<a href="{{url('admin/cinemas/'.$cinema->id.'/destroy')}}" class="btn btn-sm btn-danger">Delete</a>
			</div>
		</div>
	</div>
</div>
@endsection

@section('head')
	<link href="{{asset('/plugins/bootstrap-datepicker/css/datepicker.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/ionRangeSlider/css/ion.rangeSlider.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/ionRangeSlider/css/ion.rangeSlider.skinNice.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/password-indicator/css/password-indicator.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/bootstrap-combobox/css/bootstrap-combobox.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/jquery-tag-it/css/jquery.tagit.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/select2/dist/css/select2.min.css')}}" rel="stylesheet" />
	<style>
	#genre-list{margin-top: 10px;}
	#add-genre{cursor: pointer;}
	</style>
@endsection

@section('scripts')
	<script src="{{asset('/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
	<script src="{{asset('/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}"></script>
	<script src="{{asset('/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
	<script src="{{asset('/plugins/masked-input/masked-input.min.js')}}"></script>
	<script src="{{asset('/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
	<script src="{{asset('/plugins/password-indicator/js/password-indicator.js')}}"></script>
	<script src="{{asset('/plugins/bootstrap-combobox/js/bootstrap-combobox.js')}}"></script>
	<script src="{{asset('/plugins/bootstrap-select/bootstrap-select.min.js')}}"></script>
	<script src="{{asset('/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
	<script src="{{asset('/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js')}}"></script>
	<script src="{{asset('/plugins/jquery-tag-it/js/tag-it.min.js')}}"></script>
    <script src="{{asset('/plugins/bootstrap-daterangepicker/moment.js')}}"></script>
    <script src="{{asset('/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('/plugins/select2/dist/js/select2.min.js')}}"></script>
	<script src="{{asset('/js/form-plugins.demo.min.js')}}"></script>
@endsection

@section('docready')
<script>
$(document).ready(function() {
	FormPlugins.init();
});
</script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&sensor=false&key="></script>
<script type="text/javascript">
//<![CDATA[\
var marker;
	var mapOptions = {
		center: new google.maps.LatLng({{($cinema->latitude)? $cinema->latitude:10.5718489}},{{ ($cinema->longitude)? $cinema->longitude:122.608031}}),
		zoom: 11,
		mapTypeControl: true,
		zoomControl: true,
		panControl: true,
		streetViewControl: false,
		styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#C0CADC"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#e2ebf0"},{"visibility":"on"}]}]
	};
	var map = new google.maps.Map(document.getElementById("map"), mapOptions);
	
	var marker = new google.maps.Marker({
		position: new google.maps.LatLng({{($cinema->latitude)? $cinema->latitude:10.5718489}},{{ ($cinema->longitude)? $cinema->longitude:122.608031}}),
		map: map,
	});
	google.maps.event.addListener(map, 'click', function(event) {
		var addr = []; var p=0;
		jQuery.each(event.latLng, function(index, val) {
			addr[p] = val; p++;
		});
		var lataddr = addr[0];
		var longaddr = addr[1];
		jQuery('input[name="latitude"]').val(lataddr);
		jQuery('input[name="longitude"]').val(longaddr);
		placeMarker(event.latLng);
	});
	function placeMarker(location) {
	  if ( marker ) {
	    marker.setPosition(location);
	  } else {
	    marker = new google.maps.Marker({
	      position: location,
	      map: map
	    });
	  }
	}
//]]>
</script>
@endsection