@extends('admin')

@section('title')
Edit Movie
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
	    <!-- begin panel -->
	    @if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
		@endif
	    <div class="panel panel-inverse" data-sortable-id="form-stuff-3">
	        <div class="panel-heading">
	            <div class="panel-heading-btn">
	                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
	                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
	                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
	                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
	            </div>
	            <h4 class="panel-title">&nbsp;</h4>
	        </div>
	        <div class="panel-body">
	            <form enctype="multipart/form-data" id="crate-movie" class="form-horizontal form-bordered" action="{{url('admin/movies/'.$movie->id.'/update')}}" method="POST">
	            	{{ csrf_field() }}
                    <fieldset>
                        @include('errors.errors')
                        <div class="row">
                        	<div class="col-md-6 col-sm-12">
                        		<legend>Details</legend>
                        		<div class="form-group">
		                            <label class="col-md-4 control-label">Movie Title</label>
		                            <div class="col-md-8">
		                                <input type="text" name="title" value="{{$movie->title}}" class="form-control" id="movieTitle" placeholder="Movie Title">
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-md-4 control-label">Description</label>
		                            <div class="col-md-8">
		                                <textarea name="description" rows="5" class="form-control" id="description" placeholder="Description">{{$movie->description}}</textarea>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-md-4 control-label">Youtube URL</label>
		                            <div class="col-md-8">
		                                <input type="text" name="youtube_url" value="{{$movie->youtube_url}}" class="form-control" id="youtube" placeholder="http://">
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-md-4 control-label">Rate</label>
		                            <div class="col-md-8">
		                                <input type="text" name="rate" value="{{$movie->rate}}" class="form-control" id="rate" placeholder="ex. G, PG, PG13, R">
		                            </div>
		                        </div>
		                        <div class="form-group">
									<label class="control-label col-md-4">Genre</label>
									<div class="col-md-8">
										<div class="input-group">
											{{ Form::text('genre', @$movie->genre, ['class'=>'form-control', 'placeholder' => 'ex. Horror, Action'] ) }}
										</div>
									</div>
								</div>
								<div class="form-group">
		                            <label class="col-md-4 control-label">Cover Image</label>
		                            <div class="col-md-8">
		                                <input type="file" name="image" class="form-control" id="cover" placeholder="cover"><br>
		                                @if( $movie->image )
		                            	<img src="{{url('uploads/'.$movie->image)}}" class="thumbnail img-responsive">
		                            	@endif
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <div class="col-md-8 col-md-offset-4">
		                                <button id="submit-movie" type="submit" class="btn btn-success m-r-5">
		                                	<i class="fa fa-save"></i> Save
		                            	</button>
		                            </div>
		                        </div>
                        	</div><!-- col -->
                        	<div class="col-md-6 col-sm-12">
                        		<legend>Schedules</legend>
                        		<div class="form-group">
		                            <label class="col-md-4 control-label">Cinema</label>
		                            <div class="col-md-8">
		                            	<select class="form-control selectpicker" name="cinema_id" data-size="10" data-live-search="true" data-style="btn-white">
		                                	<option value="">Select Cinema</option>
		                                	@if($cinemas)
		                                		@foreach( $cinemas as $cinema )
		                                		<option value="{{$cinema->id}}" {{$cinema->id == $movie->cinema_id ? 'selected="selected"':''}}>{{$cinema->name}}</option>
		                                		@endforeach
		                                	@endif
		                                </select>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-md-4 control-label">Showing</label>
		                            <div class="col-md-8">
		                                <select class="form-control selectpicker" name="showing" data-size="10" data-live-search="true" data-style="btn-white">
		                                		<option value="1" {{1 == $movie->showing ? 'selected="selected"':''}}>Now Showing</option>
		                                		<option value="0" {{0 == $movie->showing ? 'selected="selected"':''}}>Not Showing</option>
		                                </select>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-md-4 control-label">Booking URL</label>
		                            <div class="col-md-8">
		                                <input type="text" class="form-control" id="booking" placeholder="http://">
		                            </div>
		                        </div>
                        		<div class="form-group">
		                            <label class="col-md-4 control-label">Showtimes</label>
		                            <div class="col-md-8">
										{{ Form::text('showtimes', @$movie->showtimes, ['class'=>'form-control', 'placeholder' => '12:30, 21:0, 13:00, 7:30'] ) }}
		                            </div>
		                        </div>
                        	</div>
                        </div>
                        
                    </fieldset>
                </form>
	        </div>
	    </div>
	    <!-- end panel -->
	</div>
</div>
@endsection

@section('head')
	<link href="{{asset('/plugins/bootstrap-datepicker/css/datepicker.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/ionRangeSlider/css/ion.rangeSlider.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/ionRangeSlider/css/ion.rangeSlider.skinNice.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/password-indicator/css/password-indicator.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/bootstrap-combobox/css/bootstrap-combobox.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/jquery-tag-it/css/jquery.tagit.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/select2/dist/css/select2.min.css')}}" rel="stylesheet" />
	<link href="{{asset('/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />	
	<style>
	#genre-list{margin-top: 10px;}
	#add-genre{cursor: pointer;}
	</style>
@endsection

@section('scripts')
	<script src="{{asset('/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
	<script src="{{asset('/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}"></script>
	<script src="{{asset('/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
	<script src="{{asset('/plugins/masked-input/masked-input.min.js')}}"></script>
	<script src="{{asset('/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
	<script src="{{asset('/plugins/password-indicator/js/password-indicator.js')}}"></script>
	<script src="{{asset('/plugins/bootstrap-combobox/js/bootstrap-combobox.js')}}"></script>
	<script src="{{asset('/plugins/bootstrap-select/bootstrap-select.min.js')}}"></script>
	<script src="{{asset('/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
	<script src="{{asset('/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js')}}"></script>
	<script src="{{asset('/plugins/jquery-tag-it/js/tag-it.min.js')}}"></script>
    <script src="{{asset('/plugins/bootstrap-daterangepicker/moment.js')}}"></script>
    <script src="{{asset('/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('/plugins/select2/dist/js/select2.min.js')}}"></script>
	<script src="{{asset('/plugins/gritter/js/jquery.gritter.js')}}"></script>
@endsection