@extends('admin')

@section('title')
Movies
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
	    <!-- begin panel -->
	    @if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
		@endif
        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
	    <a href="{{ url('admin/movies/create') }}" class="btn btn-info m-r-5 m-b-5">
        	<i class="fa fa-plus"></i> Add Movie
        </a>
	    <!-- end panel -->
	    
	    	
	    
	    @if( $movies->count() )
	    

        	<div id="gallery" class="gallery">
            	@foreach ($movies as $movie)
            	<div class="image gallery-group-1" style="float:left;">
                    <div class="image-inner">
                        <a href="{{url('admin/movies/'.$movie->id.'/edit')}}" data-lightbox="gallery-group-1">
                        	<?php $thumb = ($movie->image)? url('uploads/'.$movie->image):asset('img/cinema.png'); ?>
                        	<div style="background:url('{{$thumb}}') no-repeat center center;background-size:contain;height:200px;">
                    		</div>
                        </a>
                        <p class="image-caption">
                        	{{ $movie->rate }}
                        </p>
                    </div>
                    <div class="image-info">
                        <h5 class="title">{{ $movie->title }}</h5>
                        <!--  -->
                        <div class="desc">
                            {{ $movie->description }}
                        </div><br>
                        <a href="{{url('admin/movies/'.$movie->id.'/edit')}}" class="btn btn-block btn-success">Edit <i class="fa fa-edit"></i></a>
                    </div>
                </div>
			    @endforeach
			    <div class="clearfix"></div>
			</div>

		
	    @else
		    <div class="alert alert-info">
				<p>You may now start adding movies.</p>
			</div>
	    @endif
        <p>{!! $movies->render() !!}</p>
	</div>
</div>
@endsection