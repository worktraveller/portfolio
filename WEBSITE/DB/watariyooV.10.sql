-- phpMyAdmin SQL Dump
-- version 4.5.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 29, 2016 at 06:09 AM
-- Server version: 5.7.12
-- PHP Version: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `watariyoo`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cinemas`
--

CREATE TABLE `cinemas` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `booking_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` decimal(9,6) NOT NULL,
  `longitude` decimal(9,6) NOT NULL,
  `last_crawled` int(11) NOT NULL,
  `crawable` tinyint(1) NOT NULL,
  `crawled` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cinemas`
--

INSERT INTO `cinemas` (`id`, `name`, `address`, `phone`, `link`, `booking_url`, `latitude`, `longitude`, `last_crawled`, `crawable`, `crawled`, `created_at`, `updated_at`) VALUES
(13, 'Summarecon Mal Bekasi Imaxbekasi', 'Summarecon Mal Bekasi Lantai 3 \nJl. Bulevar Ahmad Yani\nBekasi\r\n	PHONE\r\n			:\r\n			(021) 29572421\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp. 40.000\nJumat Rp. 50.000Sabtu/Minggu/Libur Rp. 60.000', '(021) 29572421', 'http://www.21cineplex.com/theater/bioskop-summarecon-mal-bekasi-imax,376,BKSIXSB.htm', NULL, '0.000000', '0.000000', 1459131645, 1, 1, '2016-03-28 02:06:10', '2016-03-28 02:20:45'),
(14, 'Gading Imaxjakarta', 'Mal Kelapa Gading 3 Lantai 3\nJl. Bulevar Kelapa Gading\nJakarta Utara\r\n	PHONE\r\n			:\r\n			(021) 45853821\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp 50.000\nJumat Rp 60.000 Sabtu/Minggu/Libur Rp 75.000', '(021) 45853821', 'http://www.21cineplex.com/theater/bioskop-gading-imax,331,JKTIXGD.htm', NULL, '0.000000', '0.000000', 1463632599, 1, 1, '2016-03-28 02:06:10', '2016-05-19 04:36:39'),
(15, 'Gandaria Imaxjakarta', 'Gandaria City Level 2\nJl Sultan Iskandar Muda, Kebayoran Lama\nJakarta Selatan\r\n	PHONE\r\n			:\r\n			(021) 29053218\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp 50.000\nJumat Rp. 60.000 Sabtu/Minggu/Libur Rp 75.000', '(021) 29053218', 'http://www.21cineplex.com/theater/bioskop-gandaria-imax,318,JKTIXGN.htm', NULL, '0.000000', '0.000000', 1463632725, 1, 1, '2016-03-28 02:06:10', '2016-05-19 04:38:45'),
(16, 'Tunjungan 5 Imaxsurabaya', 'Tunjungan Plaza 5 Lt.10\nJln. Basuki Rahmat No.08-12\nSurabaya 60621\r\n	PHONE\r\n			:\r\n			(031) 51164521\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp 50.000 \nJumat Rp 60.000\nSabtu/Minggu/Libur Rp 75.000', '(031) 51164521', 'http://www.21cineplex.com/theater/bioskop-tunjungan-5-imax,391,SBYIXT5.htm', NULL, '0.000000', '0.000000', 1463632742, 1, 1, '2016-03-28 02:06:10', '2016-05-19 04:39:02'),
(17, 'Summarecon Mal Serpong Imaxtangerang', 'SUMMARECON MAL Lantai 3\nJl. Bulevar Gading Serpong\nTangerang\r\n	PHONE\r\n			:\r\n			(021) 542 026 21\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp 50.000\nJumat Rp 60.000Sabtu/Minggu/Libur Rp 75.000', '(021) 542 026 21', 'http://www.21cineplex.com/theater/bioskop-summarecon-mal-serpong-imax,355,TGRIXSS.htm', NULL, '0.000000', '0.000000', 1464062511, 1, 1, '2016-03-28 02:06:10', '2016-05-24 04:01:51'),
(18, 'E-walk Premierebalikpapan', 'e-Walk Lantai 1 Balikpapan Superblok (BSB)\nJl. Jenderal Sudirman\nBalikpapan\r\n	PHONE\r\n			:\r\n			(0542) 7213921\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp 60.000\nJumat Rp 80.000  Sabtu/Minggu/Libur Rp 100.000', '(0542) 7213921', 'http://www.21cineplex.com/theater/bioskop-ewalk-premiere,377,BLPPREW.htm', NULL, '0.000000', '0.000000', 1464062680, 1, 1, '2016-03-28 02:06:10', '2016-05-24 04:04:40'),
(19, 'Ciwalk Premiere Bandung', 'CIWALK Lantai 2\nJl. Cihampelas 160\nBandung \r\n	PHONE\r\n			:\r\n			(022) 2061121\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp. 60.000\nJumat Rp. 80.000Sabtu/Minggu/Libur Rp. 100.000', '(022) 2061121', 'http://www.21cineplex.com/theater/bioskop-ciwalk-premiere,286,BDGPRCI.htm', NULL, '0.000000', '0.000000', 1466412857, 1, 1, '2016-03-28 02:06:10', '2016-06-20 08:54:17'),
(20, 'Ciputra Cibubur Premiere Bekasi', 'Mal Ciputra Cibubur Lt.3\nJl. Alternatif Cibubur\nCileungsi KM.4\nBekasi 17435\r\n	PHONE\r\n			:\r\n			(021) 29377421\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp. 60.000\nJumat Rp. 80.000Sabtu/Minggu/Libur Rp. 100.000', '(021) 29377421', 'http://www.21cineplex.com/theater/bioskop-ciputra-cibubur-premiere,349,BKSPRCC.htm', NULL, '0.000000', '0.000000', 1466420296, 1, 1, '2016-03-28 02:06:10', '2016-06-20 10:58:16'),
(21, 'Grand Metropolitan Premierebekasi', 'Grand Metropolitan Mall Lt.3\nJl.KH Noer Alie\n(Samping Pintu Tol Bekasi Barat)\nBekasi Selatan 17148\r\n	PHONE\r\n			:\r\n			(021) 29464660\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp 60.000 Jumat Rp 80.000\nSabtu/Minggu/Libur Rp 100.000', '(021) 29464660', 'http://www.21cineplex.com/theater/bioskop-grand-metropolitan-premiere,352,BKSPRGM.htm', NULL, '0.000000', '0.000000', 1466435664, 1, 1, '2016-03-28 02:06:10', '2016-06-20 15:14:24'),
(22, 'Summarecon Mal Bekasi Premiere Bekasi', 'Summarecon Mal Bekasi Lantai 3 \nJl. Bulevar Ahmad Yani\nBekasi\r\n	PHONE\r\n			:\r\n			(021) 29572421\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp. 60.000\nJumat Rp. 80.000Sabtu/Minggu/Libur Rp. 100.000', '(021) 29572421', 'http://www.21cineplex.com/theater/bioskop-summarecon-mal-bekasi-premiere,337,BKSPRSB.htm', NULL, '0.000000', '0.000000', 1466436438, 1, 1, '2016-03-28 02:06:10', '2016-06-20 15:27:18'),
(23, 'Beachwalk Premiere Denpasar', 'Beach Walk Lantai 2 \nJl. Pantai Kuta\nDenpasar\r\n	PHONE\r\n			:\r\n			(0361) 846 5621\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp 100.000 Jumat Rp 150.000\nSabtu/Minggu/Libur Rp 200.000', '(0361) 846 5621', 'http://www.21cineplex.com/theater/bioskop-beachwalk-premiere,324,DPRPRBW.htm', NULL, '0.000000', '0.000000', 1466437431, 1, 1, '2016-03-28 02:06:10', '2016-06-20 15:43:51'),
(24, 'Baywalk Pluit Premiere Jakarta', 'MAL BAYWALK PLUIT Lantai.6\nJln. Pluit Karang Ayu Blok B1 Utara\nJakarta Utara\r\n	PHONE\r\n			:\r\n			(021) 29629621\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp. 75.000\nJumat Rp. 100.000Sabtu/Minggu/Libur Rp. 150.000', '(021) 29629621', 'http://www.21cineplex.com/theater/bioskop-baywalk-pluit-premiere,344,JKTPRBP.htm', NULL, '0.000000', '0.000000', 1466442970, 1, 1, '2016-03-28 02:06:10', '2016-06-20 17:16:10'),
(25, 'Emporium Pluit Premierejakarta', 'MAL EMPORIUM PLUIT UNIT 5-01 \nJl. Pluit Selatan Raya \nJakarta Utara \r\n	PHONE\r\n			:\r\n			(021) 6667 6561\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp 100.000\nJumat Rp 150.000\nSabtu/Minggu/Libur Rp. 200.000', '(021) 6667 6561', 'http://www.21cineplex.com/theater/bioskop-emporium-pluit-premiere,281,JKTPREP.htm', NULL, '0.000000', '0.000000', 1466442976, 1, 1, '2016-03-28 02:06:10', '2016-06-20 17:16:16'),
(26, 'Gading Premiere Jakarta', 'MALL KELAPA GADING 3, Lantai 3 \nJl. Bulevar Kelapa GadingJakarta Utara\n\r\n	PHONE\r\n			:\r\n			(021) 458 53 821\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp 100.000\nJumat Rp 150.000\nSabtu/Minggu/Libur Rp. 200.000', '(021) 458 53 821', 'http://www.21cineplex.com/theater/bioskop-gading-premiere,296,JKTPRGA.htm', NULL, '0.000000', '0.000000', 1466442984, 1, 1, '2016-03-28 02:06:10', '2016-06-20 17:16:24'),
(27, 'Gandaria Premiere Jakarta', 'Gandaria City Level 2\nJl Sultan Iskandar Muda, Kebayoran Lama\nJakarta Selatan\r\n	PHONE\r\n			:\r\n			(021) 29053218\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp 75.000 \nJumat Rp 100.000\nSabtu/Minggu/Libur Rp. 150.000', '(021) 29053218', 'http://www.21cineplex.com/theater/bioskop-gandaria-premiere,301,JKTPRGN.htm', NULL, '0.000000', '0.000000', 1466442992, 1, 1, '2016-03-28 02:06:10', '2016-06-20 17:16:32'),
(28, 'Kasablanka Premierejakarta', 'Kota Kasablanka Mall Lantai 2 \nJl. Casablanca Kav 88 \nJakarta Pusat\r\n	PHONE\r\n			:\r\n			(021) 2946 5221\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp. 75.000\nJumat Rp. 100.000Sabtu/Minggu/Libur Rp. 150.000', '(021) 2946 5221', 'http://www.21cineplex.com/theater/bioskop-kasablanka-premiere,321,JKTPRKA.htm', NULL, '0.000000', '0.000000', 1466443000, 1, 1, '2016-03-28 02:06:10', '2016-06-20 17:16:41'),
(29, 'Kemang Village Premierejakarta', 'Mal Kemang Village Lantai 3 \nJl. P. Antasari No. 36\nJakarta Selatan\r\n	PHONE\r\n			:\r\n			(021) 29056866\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp 75.000Jumat Rp 100.000\nSabtu/Minggu/Libur Rp 150.000', '(021) 29056866', 'http://www.21cineplex.com/theater/bioskop-kemang-village-premiere,323,JKTPRKV.htm', NULL, '0.000000', '0.000000', 1466443007, 1, 1, '2016-03-28 02:06:10', '2016-06-20 17:16:47'),
(30, 'Lotte S. Avenue Xxi Premiere Jakarta', 'Ciputra World Jakarta\nJl. PROF. DR. Satrio KAV 3-5, Kuningan\nJakarta Selatan\r\n	PHONE\r\n			:\r\n			(021) 29889421\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp. 75.000\nJumat Rp. 100.000\nSabtu/Minggu/Libur Rp. 150.000', '(021) 29889421', 'http://www.21cineplex.com/theater/bioskop-lotte-s-avenue-xxi-premiere,334,JKTPRLS.htm', NULL, '0.000000', '0.000000', 1466443013, 1, 1, '2016-03-28 02:06:10', '2016-06-20 17:16:53'),
(31, 'Metropole Premierejakarta', 'Komplek Megaria\nJl. Pegangsaan 21\nJakarta Pusat\r\n	PHONE\r\n			:\r\n			(021) 31922249\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp. 75.000\nJumat, Sabtu/Minggu/Libur Rp. 100.000', '(021) 31922249', 'http://www.21cineplex.com/theater/bioskop-metropole-premiere,401,JKTPRME.htm', NULL, '0.000000', '0.000000', 1466443017, 1, 1, '2016-03-28 02:06:10', '2016-06-20 17:16:57'),
(32, 'One Bel Park Premierejakarta', 'One Bel Park Lt.2\nJln. Rumah Sakit Fatmawati No.1 \nKel. Pondok Labu, Kec. Cilandak \nJakarta Selatan.\r\n	PHONE\r\n			:\r\n			(021) 27653422\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp 60.000\nJumat Rp 80.000 Sabtu/Minggu/Libur Rp 100.000', '(021) 27653422', 'http://www.21cineplex.com/theater/bioskop-one-bel-park-premiere,388,JKTPROB.htm', NULL, '0.000000', '0.000000', 1466443020, 1, 1, '2016-03-28 02:06:10', '2016-06-20 17:17:00'),
(33, 'Plaza Indonesia Premiere Jakarta', 'PLAZA INDONESIA Lantai 6\nJl. MH Thamrin Kav 28-30\nJakarta Pusat \r\n	PHONE\r\n			:\r\n			(021) 398 38 779\r\n		HTM\r\n			:\r\n			Senin s/d Kamis Rp 100.000\nJumat Rp 150.000\nSabtu/Minggu/Libur Rp. 250.000', '(021) 398 38 779', 'http://www.21cineplex.com/theater/bioskop-plaza-indonesia-premiere,362,JKTPRPN.htm', NULL, '0.000000', '0.000000', 1466443032, 1, 1, '2016-03-28 02:06:10', '2016-06-20 17:17:12'),
(34, 'Plaza Senayan Premiere Jakarta', NULL, '(021) 572 5536', 'http://www.21cineplex.com/theater/bioskop-plaza-senayan-premiere,255,JKTPRPS.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(35, 'Pondok Indah Premiere Jakarta', NULL, '(021) 7592-0784', 'http://www.21cineplex.com/theater/bioskop-pondok-indah-premiere,229,JKTPRPI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(36, 'Puri Premiere Jakarta', NULL, '(021) 582 2521', 'http://www.21cineplex.com/theater/bioskop-puri-premiere,285,JKTPRPU.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(37, 'Senayan City Premierejakarta', NULL, '(021) 7278 1221', 'http://www.21cineplex.com/theater/bioskop-senayan-city-premiere,360,JKTPRSC.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(38, 'St. Moritz Premierejakarta', NULL, '(021) 29111370', 'http://www.21cineplex.com/theater/bioskop-st-moritz-premiere,375,JKTPRSM.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(39, 'Panakkukang Premieremakassar', NULL, '(0411) 466 2023', 'http://www.21cineplex.com/theater/bioskop-panakkukang-premiere,397,UPGPRPA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(40, 'Mantos 3 Premieremanado', NULL, '(0431) 8890521', 'http://www.21cineplex.com/theater/bioskop-mantos-3-premiere,400,MNDPRM3.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(41, 'Lem Premieremataram', NULL, '(0370) 6172121', 'http://www.21cineplex.com/theater/bioskop-lem-premiere,395,MTRPRLE.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(42, 'Centre Point Premiere Medan', NULL, '(061) 80510321', 'http://www.21cineplex.com/theater/bioskop-centre-point-premiere,354,MDNPRCP.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(43, 'Ringroad Citywalks Premieremedan', NULL, '(061) 80026553', 'http://www.21cineplex.com/theater/bioskop-ringroad-citywalks-premiere,386,MDNPRRC.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(44, 'Opi Mall Premierepalembang', NULL, '(0711) 5624052', 'http://www.21cineplex.com/theater/bioskop-opi-mall-premiere,382,PLGPROP.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(45, 'Pim Premierepalembang', NULL, '(0711) 7623064', 'http://www.21cineplex.com/theater/bioskop-pim-premiere,367,PLGPRPI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(46, 'Grand Mall Palu Premierepalu', NULL, '(0451) 4131021', 'http://www.21cineplex.com/theater/bioskop-grand-mall-palu-premiere,384,PLUPRGR.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(47, 'Ciputra Seraya Premierepekanbaru', NULL, '(0761) 868 521', 'http://www.21cineplex.com/theater/bioskop-ciputra-seraya-premiere,380,PBRPRCS.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(48, 'Big Mall Premieresamarinda', NULL, '(0541) 6294777', 'http://www.21cineplex.com/theater/bioskop-big-mall-premiere,371,SMDPRBM.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(49, 'Solo Paragon Premiere Solo', NULL, '(0271) 7890756', 'http://www.21cineplex.com/theater/bioskop-solo-paragon-premiere,346,SKAPRSP.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(50, 'The Park Premiere Solo', NULL, '(0271) 789 1321', 'http://www.21cineplex.com/theater/bioskop-the-park-premiere,359,SKAPRTP.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(51, 'Ciputra World Premiere Surabaya', NULL, '(031) 512 00021', 'http://www.21cineplex.com/theater/bioskop-ciputra-world-premiere,314,SBYPRCW.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(52, 'Grand City Premieresurabaya', NULL, '(031) 524 05821', 'http://www.21cineplex.com/theater/bioskop-grand-city-premiere,303,SBYPRGC.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(53, 'Lenmarc Premieresurabaya', NULL, '(031) 5116 2921', 'http://www.21cineplex.com/theater/bioskop-lenmarc-premiere,305,SBYPRLE.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(54, 'Tunjungan 5 Premieresurabaya', NULL, '(031) 60621', 'http://www.21cineplex.com/theater/bioskop-tunjungan-5-premiere,390,SBYPRT5.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(55, 'Aeon Mall Bsd City Premieretangerang', NULL, '(021) 29168366', 'http://www.21cineplex.com/theater/bioskop-aeon-mall-bsd-city-premiere,379,TGRPRAB.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(56, 'Alam Sutera Premieretangerang', NULL, '(021) 30448331', 'http://www.21cineplex.com/theater/bioskop-alam-sutera-premiere,330,TGRPRAS.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(57, 'Karawaci Premieretangerang', NULL, '(021) 5421 2354', 'http://www.21cineplex.com/theater/bioskop-karawaci-premiere,295,TGRPRKA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(58, 'Living World Premiere Tangerang', NULL, '(021) 5312 5569', 'http://www.21cineplex.com/theater/bioskop-living-world-premiere,310,TGRPRLW.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(59, 'Summarecon Mal Serpong Premiere Tangerang', NULL, '(021) 542 026 21', 'http://www.21cineplex.com/theater/bioskop-summarecon-mal-serpong-premiere,364,TGRPRSS.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(60, 'Ambarrukmo Premiereyogyakarta', NULL, '(0274) 433 1221', 'http://www.21cineplex.com/theater/bioskop-ambarrukmo-premiere,369,YGYPRAM.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(61, 'Empire Premiereyogyakarta', NULL, '(0274) 551 021', 'http://www.21cineplex.com/theater/bioskop-empire-premiere,396,YGYPREM.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(62, 'Jogja City Premiereyogyakarta', NULL, '(0274) 5304221', 'http://www.21cineplex.com/theater/bioskop-jogja-city-premiere,366,YGYPRJC.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(63, 'Ambon City Center Xxiambon', NULL, '(0911) 362092', 'http://www.21cineplex.com/theater/bioskop-ambon-city-center-xxi,398,ABNAMCC.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(64, 'Studioambon', NULL, '(0911) 321 717', 'http://www.21cineplex.com/theater/bioskop-studio,269,ABNSTUO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(65, 'E-walk Xxibalikpapan', NULL, '(0542) 7213921', 'http://www.21cineplex.com/theater/bioskop-ewalk-xxi,297,BLPEWAL.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(66, 'Studio Xxibalikpapan', NULL, '(0542) 7582621', 'http://www.21cineplex.com/theater/bioskop-studio-xxi,276,BLPSTUD.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(67, 'Braga Xxibandung', NULL, '(022) 844 60121', 'http://www.21cineplex.com/theater/bioskop-braga-xxi,231,BDGBRAG.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(68, 'Btc Xxibandung', NULL, '(022) 424019', 'http://www.21cineplex.com/theater/bioskop-btc-xxi,274,BDGBTC.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(69, 'Ciwalk Xxibandung', NULL, '(022) 2061017', 'http://www.21cineplex.com/theater/bioskop-ciwalk-xxi,249,BDGCIWL.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(70, 'Empire Xxibandung', NULL, '(022) 424 0719', 'http://www.21cineplex.com/theater/bioskop-empire-xxi,23,BDGEMPI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(71, 'Festival Citylink Xxibandung', NULL, '(022) 6128708', 'http://www.21cineplex.com/theater/bioskop-festival-citylink-xxi,326,BDGFECI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(72, 'Jatosbandung', NULL, '(022) 8792 0089', 'http://www.21cineplex.com/theater/bioskop-jatos,259,BDGJATO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(73, 'Tsm Xxibandung', NULL, '(022) 910 91121', 'http://www.21cineplex.com/theater/bioskop-tsm-xxi,186,BDGBSM.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(74, 'Studio Xxibanjarmasin', NULL, '(0511) 436 5521', 'http://www.21cineplex.com/theater/bioskop-studio-xxi,251,BJMSTUO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(75, 'Bcsbatam', NULL, '(0778) 7435 721', 'http://www.21cineplex.com/theater/bioskop-bcs,287,BTMBCS.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(76, 'Mega Xxibatam', NULL, '(0778) 466 121', 'http://www.21cineplex.com/theater/bioskop-mega-xxi,266,BTMMEGA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(77, 'Studiobatam', NULL, '(0778) 749 3521', 'http://www.21cineplex.com/theater/bioskop-studio,67,BTMSTUO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(78, 'Bekasi Square Xxibekasi', NULL, '(021) 824 24 801', 'http://www.21cineplex.com/theater/bioskop-bekasi-square-xxi,263,BKSBESQ.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(79, 'Ciputra Cibubur  Xxibekasi', NULL, '(021) 29377421', 'http://www.21cineplex.com/theater/bioskop-ciputra-cibuburxxi,348,BKSCICI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(80, 'Grand Malbekasi', NULL, '(021) 889 0971', 'http://www.21cineplex.com/theater/bioskop-grand-mal,288,BKSGRMA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(81, 'Grand Metropolitan Xxibekasi', NULL, '(021) 29464660', 'http://www.21cineplex.com/theater/bioskop-grand-metropolitan-xxi,351,BKSGRME.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(82, 'Mega Bekasi Xxibekasi', NULL, '(021) 889 666 21', 'http://www.21cineplex.com/theater/bioskop-mega-bekasi-xxi,258,BKSMEBE.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(83, 'Metropolitan Xxibekasi', NULL, '(021) 884 4985', 'http://www.21cineplex.com/theater/bioskop-metropolitan-xxi,129,BKSMETL.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(84, 'Plasa Cibubur Xxibekasi', NULL, '(021) 84597403', 'http://www.21cineplex.com/theater/bioskop-plasa-cibubur-xxi,361,BKSPLCI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(85, 'Pondok Gedebekasi', NULL, '(021) 849 380 21', 'http://www.21cineplex.com/theater/bioskop-pondok-gede,257,BKSPOGD.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(86, 'Summarecon Mal Bekasi Xxibekasi', NULL, '(021) 29572421', 'http://www.21cineplex.com/theater/bioskop-summarecon-mal-bekasi-xxi,336,BKSSUBE.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(87, 'Megabengkulu', NULL, '(0736) 25333', 'http://www.21cineplex.com/theater/bioskop-mega,289,BKLMEGA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(88, 'Binjaibinjai', NULL, '(061) 882 6221', 'http://www.21cineplex.com/theater/bioskop-binjai,272,BNJBINJ.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(89, 'Bellanovabogor', NULL, '(021) 879 239 25', 'http://www.21cineplex.com/theater/bioskop-bellanova,260,BGRBELL.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(90, 'Botani Xxibogor', NULL, '(0251) 840 0821', 'http://www.21cineplex.com/theater/bioskop-botani-xxi,273,BGRBOTA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(91, 'Btmbogor', NULL, '(0251) 840 1241', 'http://www.21cineplex.com/theater/bioskop-btm,253,BGRBTM.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(92, 'Cibinong City Xxibogor', NULL, '(021) 2986 0091', 'http://www.21cineplex.com/theater/bioskop-cibinong-city-xxi,356,BGRCICI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(93, 'Cinerebogor', NULL, '(021) 754 0592', 'http://www.21cineplex.com/theater/bioskop-cinere,56,BGRCINE.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(94, 'Cinere Bellevue Xxibogor', NULL, '(021) 29403889', 'http://www.21cineplex.com/theater/bioskop-cinere-bellevue-xxi,368,BGRCIBE.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(95, 'Depokbogor', NULL, '(021) 776 0679', 'http://www.21cineplex.com/theater/bioskop-depok,57,BGRDEPK.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(96, 'Detosbogor', NULL, '(021) 788 70 221', 'http://www.21cineplex.com/theater/bioskop-detos,237,BGRDETO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(97, 'Ekalokasaribogor', NULL, '(0251) 8363 137', 'http://www.21cineplex.com/theater/bioskop-ekalokasari,262,BGREKAL.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(98, 'Margo Platinumbogor', NULL, '(021) 7887 0901', 'http://www.21cineplex.com/theater/bioskop-margo-platinum,252,BGRMAPL.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(99, 'Cilegoncilegon', NULL, '(0254) 387 858', 'http://www.21cineplex.com/theater/bioskop-cilegon,136,CLGCILE.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(100, 'Csb Xxicirebon', NULL, '(0231) 8291921', 'http://www.21cineplex.com/theater/bioskop-csb-xxi,329,CRBCSB.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(101, 'Gragecirebon', NULL, '(0231) 222896', 'http://www.21cineplex.com/theater/bioskop-grage,161,CRBGRAG.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(102, 'Beachwalk Xxidenpasar', NULL, '(0361) 846 5621', 'http://www.21cineplex.com/theater/bioskop-beachwalk-xxi,325,DPRBEWA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(103, 'Galeria Xxidenpasar', NULL, '(0361) 767 021', 'http://www.21cineplex.com/theater/bioskop-galeria-xxi,188,DPRGALE.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(104, 'Park 23 Xxidenpasar', NULL, '(0361) 4727621', 'http://www.21cineplex.com/theater/bioskop-park-23-xxi,393,DPRPA23.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(105, 'Gorontalo Xxigorontalo', NULL, '(0435) 859 2521', 'http://www.21cineplex.com/theater/bioskop-gorontalo-xxi,357,GTOGORO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(106, 'Anggrek Xxijakarta', NULL, '(021) 563 9403', 'http://www.21cineplex.com/theater/bioskop-anggrek-xxi,15,JKTANGG.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(107, 'Arion Xxijakarta', NULL, '(021) 475 7658', 'http://www.21cineplex.com/theater/bioskop-arion-xxi,34,JKTARIO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(108, 'Artha Gading Xxijakarta', NULL, '(021) 4586 4123', 'http://www.21cineplex.com/theater/bioskop-artha-gading-xxi,245,JKTARGA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(109, 'Atrium Xxijakarta', NULL, '(021) 386 7830', 'http://www.21cineplex.com/theater/bioskop-atrium-xxi,19,JKTATRI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(110, 'Baywalk Pluit Xxijakarta', NULL, '(021) 29629621', 'http://www.21cineplex.com/theater/bioskop-baywalk-pluit-xxi,343,JKTBAPL.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(111, 'Blok M Plazajakarta', NULL, '(021) 720 9437', 'http://www.21cineplex.com/theater/bioskop-blok-m-plaza,35,JKTBLOM.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(112, 'Blok M Squarejakarta', NULL, '(021) 7280 2021', 'http://www.21cineplex.com/theater/bioskop-blok-m-square,282,JKTBMSQ.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(113, 'Cibuburjakarta', NULL, '(021) 877 56 588', 'http://www.21cineplex.com/theater/bioskop-cibubur,232,JKTCIBU.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(114, 'Cijantungjakarta', NULL, '(021) 877 93 446', 'http://www.21cineplex.com/theater/bioskop-cijantung,134,JKTCIJA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(115, 'Cipinang Xxijakarta', NULL, '(021) 29486938', 'http://www.21cineplex.com/theater/bioskop-cipinang-xxi,347,JKTCIPI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(116, 'Citra Xxijakarta', NULL, '(021) 560 0404', 'http://www.21cineplex.com/theater/bioskop-citra-xxi,37,JKTCITR.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(117, 'Daan Mogotjakarta', NULL, '(021) 544 6733', 'http://www.21cineplex.com/theater/bioskop-daan-mogot,194,JKTDAMG.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(118, 'Djakarta Xxijakarta', NULL, '(021) 315 6725', 'http://www.21cineplex.com/theater/bioskop-djakarta-xxi,250,JKTDJAR.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(119, 'Emporium Pluit Xxijakarta', NULL, '(021) 6667 6421', 'http://www.21cineplex.com/theater/bioskop-emporium-pluit-xxi,277,JKTEMPL.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(120, 'Epicentrum Xxijakarta', NULL, '021 2994 1300', 'http://www.21cineplex.com/theater/bioskop-epicentrum-xxi,299,JKTEPIC.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(121, 'Gadingjakarta', NULL, '(021) 453 0274', 'http://www.21cineplex.com/theater/bioskop-gading,39,JKTGADI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(122, 'Gading Xxijakarta', NULL, '(021) 458 53 821', 'http://www.21cineplex.com/theater/bioskop-gading-xxi,218,JKTGADN.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(123, 'Gajah Mada Xxijakarta', NULL, '(021) 634 0349', 'http://www.21cineplex.com/theater/bioskop-gajah-mada-xxi,38,JKTCENT.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(124, 'Gandaria Xxijakarta', NULL, '(021) 29053218', 'http://www.21cineplex.com/theater/bioskop-gandaria-xxi,300,JKTGAND.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(125, 'Hollywood Xxijakarta', NULL, '(021) 525 6351', 'http://www.21cineplex.com/theater/bioskop-hollywood-xxi,36,JKTHOKC.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(126, 'Kalibata Xxijakarta', NULL, '(021) 799 1870', 'http://www.21cineplex.com/theater/bioskop-kalibata-xxi,40,JKTKALI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(127, 'Kasablanka Xxijakarta', NULL, '(021) 2946 5221', 'http://www.21cineplex.com/theater/bioskop-kasablanka-xxi,320,JKTKASA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(128, 'Kemang Village Xxijakarta', NULL, '(021) 290 56866', 'http://www.21cineplex.com/theater/bioskop-kemang-village-xxi,322,JKTKEVI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(129, 'Kramat Jati Xxijakarta', NULL, '(021) 80877457', 'http://www.21cineplex.com/theater/bioskop-kramat-jati-xxi,332,JKTKRJT.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(130, 'Kuningan City Xxijakarta', NULL, '(021) 30480621', 'http://www.21cineplex.com/theater/bioskop-kuningan-city-xxi,319,JKTKUCI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(131, 'La Piazza Xxijakarta', NULL, '(021) 4586 5021', 'http://www.21cineplex.com/theater/bioskop-la-piazza-xxi,241,JKTLAPI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(132, 'Lotte Shopping Avenue Xxijakarta', NULL, '(021) 29889421', 'http://www.21cineplex.com/theater/bioskop-lotte-shopping-avenue-xxi,333,JKTLOSA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(133, 'Metropole Xxijakarta', NULL, '(021) 319 22 249', 'http://www.21cineplex.com/theater/bioskop-metropole-xxi,45,JKTMETR.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(134, 'One Bel Park Xxijakarta', NULL, '(021) 27653422', 'http://www.21cineplex.com/theater/bioskop-one-bel-park-xxi,387,JKTONBE.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(135, 'Pejaten Village Xxijakarta', NULL, '(021) 7823 112', 'http://www.21cineplex.com/theater/bioskop-pejaten-village-xxi,280,JKTPEVI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(136, 'Plaza Indonesia Xxijakarta', NULL, '(021) 398 38 779', 'http://www.21cineplex.com/theater/bioskop-plaza-indonesia-xxi,279,JKTSTUX.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(137, 'Plaza Senayan Xxijakarta', NULL, '(021) 572 5535', 'http://www.21cineplex.com/theater/bioskop-plaza-senayan-xxi,254,JKTPLSE.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(138, 'Pluit Junction Xxijakarta', NULL, '(021) 6660 7321', 'http://www.21cineplex.com/theater/bioskop-pluit-junction-xxi,264,JKTPLJU.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(139, 'Pluit Village Xxijakarta', NULL, '(021) 668 3621', 'http://www.21cineplex.com/theater/bioskop-pluit-village-xxi,168,JKTPLVI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(140, 'Pondok Indah 1 Xxijakarta', NULL, '(021) 750 6921', 'http://www.21cineplex.com/theater/bioskop-pondok-indah-1-xxi,32,JKTPOIN.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(141, 'Pondok Indah 2 Xxijakarta', NULL, '(021) 759 20 781', 'http://www.21cineplex.com/theater/bioskop-pondok-indah-2-xxi,221,JKTPOID.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(142, 'Puri Xxijakarta', NULL, '(021) 582 2285', 'http://www.21cineplex.com/theater/bioskop-puri-xxi,153,JKTPURI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(143, 'Seasons City Xxijakarta', NULL, '(021) 6385 3901', 'http://www.21cineplex.com/theater/bioskop-seasons-city-xxi,292,JKTSACI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(144, 'Senayan City Xxijakarta', NULL, '(021) 7278 1221', 'http://www.21cineplex.com/theater/bioskop-senayan-city-xxi,244,JKTSECI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(145, 'Setiabudi Xxijakarta', NULL, '(021) 521 0721', 'http://www.21cineplex.com/theater/bioskop-setiabudi-xxi,212,JKTSETI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(146, 'St. Moritz Xxijakarta', NULL, '(021) 29111370', 'http://www.21cineplex.com/theater/bioskop-st-moritz-xxi,374,JKTSTMO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(147, 'Sunterjakarta', NULL, '(021) 658 32 898', 'http://www.21cineplex.com/theater/bioskop-sunter,156,JKTSUNT.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(148, 'Taminijakarta', NULL, '(021) 877 85 921', 'http://www.21cineplex.com/theater/bioskop-tamini,247,JKTTAMI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(149, 'Tim Xxijakarta', NULL, '(021) 319 25 130', 'http://www.21cineplex.com/theater/bioskop-tim-xxi,52,JKTTIM.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(150, 'Wtcjambi', NULL, '(0741) 783 7321', 'http://www.21cineplex.com/theater/bioskop-wtc,271,JMBWTC.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(151, 'Jayapura Xxijayapura', NULL, '(0967) 5150100', 'http://www.21cineplex.com/theater/bioskop-jayapura-xxi,342,JYPJAYA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(152, 'Boemi Kedaton Xxilampung', NULL, '(0721) 8015582', 'http://www.21cineplex.com/theater/bioskop-boemi-kedaton-xxi,372,LMPBOKE.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(153, 'Centrallampung', NULL, '(0721) 262 446', 'http://www.21cineplex.com/theater/bioskop-central,278,LMPCENT.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(154, 'M`tosmakassar', NULL, '(0411) 583 321', 'http://www.21cineplex.com/theater/bioskop-mtos,267,UPGMTOS.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(155, 'Panakkukangmakassar', NULL, '(0411) 424 158', 'http://www.21cineplex.com/theater/bioskop-panakkukang,208,UPGPANA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(156, 'Panakkukang Xximakassar', NULL, '(0411) 466 2023', 'http://www.21cineplex.com/theater/bioskop-panakkukang-xxi,312,UPGPANK.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(157, 'Studio Xximakassar', NULL, '(0411) 851-721', 'http://www.21cineplex.com/theater/bioskop-studio-xxi,226,UPGSTUR.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(158, 'Tsm Xximakassar', NULL, '(0411) 8117221', 'http://www.21cineplex.com/theater/bioskop-tsm-xxi,335,UPGTSM.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(159, 'Diengmalang', NULL, '(0341) 575421', 'http://www.21cineplex.com/theater/bioskop-dieng,284,MLGDIEG.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(160, 'Mandalamalang', NULL, '(0341) 366 560', 'http://www.21cineplex.com/theater/bioskop-mandala,31,MLGMAND.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(161, 'Mantosmanado', NULL, '(0431) 888 1682', 'http://www.21cineplex.com/theater/bioskop-mantos,196,MNDSTUO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(162, 'Mantos 3 Xximanado', NULL, '(0431) 8890521', 'http://www.21cineplex.com/theater/bioskop-mantos-3-xxi,399,MNDMAN3.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(163, 'Mega Mall Xximanado', NULL, '(0431) 8890112', 'http://www.21cineplex.com/theater/bioskop-mega-mall-xxi,373,MNDMEMA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(164, 'Lem Xximataram', NULL, '(0370) 6172121', 'http://www.21cineplex.com/theater/bioskop-lem-xxi,394,MTRLEM.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(165, 'Centre Point Xximedan', NULL, '(061) 80510321', 'http://www.21cineplex.com/theater/bioskop-centre-point-xxi,353,MDNCEPO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(166, 'Hermes Xximedan', NULL, '(061) 8050 1121', 'http://www.21cineplex.com/theater/bioskop-hermes-xxi,317,MDNHERM.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(167, 'Palladiummedan', NULL, '(061) 451 4321', 'http://www.21cineplex.com/theater/bioskop-palladium,239,MDNPALL.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(168, 'Ringroad Citywalks Xximedan', NULL, '(061) 80026553', 'http://www.21cineplex.com/theater/bioskop-ringroad-citywalks-xxi,385,MDNRICI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(169, 'Thamrinmedan', NULL, '(061) 736 6855', 'http://www.21cineplex.com/theater/bioskop-thamrin,81,MDNTHAM.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(170, 'Palmapalangkaraya', NULL, '(0563) 322 7221', 'http://www.21cineplex.com/theater/bioskop-palma,290,PLKPALM.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(171, 'Internasionalpalembang', NULL, '(0711) 357766', 'http://www.21cineplex.com/theater/bioskop-internasional,197,PLGINTE.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(172, 'Opi Mall Xxipalembang', NULL, '(0711) 5624052', 'http://www.21cineplex.com/theater/bioskop-opi-mall-xxi,381,PLGOPI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(173, 'Palembang Square Xxipalembang', NULL, '(0711) 380721', 'http://www.21cineplex.com/theater/bioskop-palembang-square-xxi,338,PLGPASQ.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(174, 'Pim Xxipalembang', NULL, '(0711) 7623064', 'http://www.21cineplex.com/theater/bioskop-pim-xxi,242,PLGPIM.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(175, 'Grand Mall Palu Xxipalu', NULL, '(0451) 4131021', 'http://www.21cineplex.com/theater/bioskop-grand-mall-palu-xxi,383,PLUGRAN.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(176, 'Ciputra Seraya Xxipekanbaru', NULL, '(0761) 868 521', 'http://www.21cineplex.com/theater/bioskop-ciputra-seraya-xxi,224,PBRRIAU.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(177, 'Ayani Xxipontianak', NULL, '(0561) 763 666', 'http://www.21cineplex.com/theater/bioskop-ayani-xxi,217,PTKAYAN.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(178, 'Big Mall Xxisamarinda', NULL, '(0541) 6294777', 'http://www.21cineplex.com/theater/bioskop-big-mall-xxi,370,SMDBIMA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(179, 'Scpsamarinda', NULL, '(0541) 749632', 'http://www.21cineplex.com/theater/bioskop-scp,192,SMDSTUD.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(180, 'Scp Xxisamarinda', NULL, '(0541) 742221', 'http://www.21cineplex.com/theater/bioskop-scp-xxi,316,SMDSTUO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(181, 'Citra Xxisemarang', NULL, '(024) 841 5971', 'http://www.21cineplex.com/theater/bioskop-citra-xxi,101,SMGCITR.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(182, 'Paragon Xxisemarang', NULL, '(024) 865 791 21', 'http://www.21cineplex.com/theater/bioskop-paragon-xxi,298,SMGPARA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(183, 'Sgm Xxi Singkawangsingkawang', NULL, '(0562) 6300062', 'http://www.21cineplex.com/theater/bioskop-sgm-xxi-singkawang,392,SKWSGM.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(184, 'Grandsolo', NULL, '(0271) 733 721', 'http://www.21cineplex.com/theater/bioskop-grand,219,SKAGRAN.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(185, 'Solo Paragon Xxisolo', NULL, '(0271) 7881921', 'http://www.21cineplex.com/theater/bioskop-solo-paragon-xxi,345,SKASOPA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(186, 'Solo Square Xxisolo', NULL, '(0271) 7654 221', 'http://www.21cineplex.com/theater/bioskop-solo-square-xxi,315,SKASOSQ.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(187, 'The Park Xxisolo', NULL, '(0271) 789 1321', 'http://www.21cineplex.com/theater/bioskop-the-park-xxi,358,SKATHPA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(188, 'Ciputra World Xxisurabaya', NULL, '(031) 512 00021', 'http://www.21cineplex.com/theater/bioskop-ciputra-world-xxi,313,SBYCIWO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(189, 'Citosurabaya', NULL, '(031) 582 51 221', 'http://www.21cineplex.com/theater/bioskop-cito,268,SBYCITO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(190, 'Deltasurabaya', NULL, '(031) 531 1668', 'http://www.21cineplex.com/theater/bioskop-delta,83,SBYDELT.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(191, 'Galaxy Xxisurabaya', NULL, '(031) 593 7121', 'http://www.21cineplex.com/theater/bioskop-galaxy-xxi,85,SBYGALA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(192, 'Grand City Xxisurabaya', NULL, '(031) 524 05821', 'http://www.21cineplex.com/theater/bioskop-grand-city-xxi,302,SBYGRCI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(193, 'Lenmarc Xxisurabaya', NULL, '(031) 5116 2921', 'http://www.21cineplex.com/theater/bioskop-lenmarc-xxi,304,SBYLENM.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(194, 'Pakuwon City Xxisurabaya', NULL, '(031) 5820 8821', 'http://www.21cineplex.com/theater/bioskop-pakuwon-city-xxi,311,SBYPACI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(195, 'Royalsurabaya', NULL, '(031) 827-1521', 'http://www.21cineplex.com/theater/bioskop-royal,248,SBYROYA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(196, 'Supermal Xxisurabaya', NULL, '(031) 739 0221', 'http://www.21cineplex.com/theater/bioskop-supermal-xxi,203,SBYSTUO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(197, 'Sutos Xxisurabaya', NULL, '(031) 563-3021', 'http://www.21cineplex.com/theater/bioskop-sutos-xxi,261,SBYSUTO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(198, 'Tunjungansurabaya', NULL, '(031) 547 2041', 'http://www.21cineplex.com/theater/bioskop-tunjungan,91,SBYTUNJ.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(199, 'Tunjungan 5 Xxisurabaya', NULL, '(031) 51164521', 'http://www.21cineplex.com/theater/bioskop-tunjungan-5-xxi,389,SBYTUN5.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(200, 'Tunjungan Xxisurabaya', NULL, '(031) 532 6621', 'http://www.21cineplex.com/theater/bioskop-tunjungan-xxi,265,SBYTUNU.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(201, 'Aeon Mall Bsd City Xxitangerang', NULL, '(021) 29168366', 'http://www.21cineplex.com/theater/bioskop-aeon-mall-bsd-city-xxi,378,TGRAEBS.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(202, 'Alam Sutera Xxitangerang', NULL, '(021) 30448331', 'http://www.21cineplex.com/theater/bioskop-alam-sutera-xxi,327,TGRALSU.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(203, 'Bale Kota Xxitangerang', NULL, '(021) 2966 8521', 'http://www.21cineplex.com/theater/bioskop-bale-kota-xxi,341,TGRBAKO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(204, 'Bintaro Xchange Xxitangerang', NULL, '(021) 29864824', 'http://www.21cineplex.com/theater/bioskop-bintaro-xchange-xxi,350,TGRBIXC.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(205, 'Bintaro Xxitangerang', NULL, '(021) 735 5371', 'http://www.21cineplex.com/theater/bioskop-bintaro-xxi,54,TGRBINT.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(206, 'Bsd Xxitangerang', NULL, '(021) 537 2704', 'http://www.21cineplex.com/theater/bioskop-bsd-xxi,126,TGRSERP.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(207, 'Cbd Ciledug Xxitangerang', NULL, '(021) 7345 2221', 'http://www.21cineplex.com/theater/bioskop-cbd-ciledug-xxi,291,TGRCBCI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(208, 'Karawacitangerang', NULL, '(021) 5421 2354', 'http://www.21cineplex.com/theater/bioskop-karawaci,294,TGRLIKA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(209, 'Karawaci Xxitangerang', NULL, '(021) 5421 2354', 'http://www.21cineplex.com/theater/bioskop-karawaci-xxi,122,TGRKARA.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(210, 'Living World Xxitangerang', NULL, '(021) 5312 5569', 'http://www.21cineplex.com/theater/bioskop-living-world-xxi,309,TGRLIWO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(211, 'Lotte Bintaro Xxitangerang', NULL, '(021) 29310921', 'http://www.21cineplex.com/theater/bioskop-lotte-bintaro-xxi,328,TGRLOBI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(212, 'Summarecon Mal Serpong Xxitangerang', NULL, '(021) 542 026 21', 'http://www.21cineplex.com/theater/bioskop-summarecon-mal-serpong-xxi,256,TGRSERO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(213, 'Tang City Xxitangerang', NULL, '(021) 29309581', 'http://www.21cineplex.com/theater/bioskop-tang-city-xxi,363,TGRTACI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(214, 'Tasiktasikmalaya', NULL, '(0265) 235 0021', 'http://www.21cineplex.com/theater/bioskop-tasik,293,TSMTASI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(215, 'Ambarukkmo Xxiyogyakarta', NULL, '(0274) 433 1221', 'http://www.21cineplex.com/theater/bioskop-ambarukkmo-xxi,238,YGYSTUD.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(216, 'Empire Xxiyogyakarta', NULL, '(0274) 551 021', 'http://www.21cineplex.com/theater/bioskop-empire-xxi,283,YGYEMPR.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(217, 'Jogja City Xxiyogyakarta', NULL, '(0274) 5304221', 'http://www.21cineplex.com/theater/bioskop-jogja-city-xxi,365,YGYJOCI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-03-28 02:06:10', '2016-03-28 02:06:10'),
(218, 'Pondok Gede Xxibekasi', NULL, '(021) 849 380 21', 'http://www.21cineplex.com/theater/bioskop-pondok-gede-xxi,257,BKSPOGD.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-05-24 03:58:05', '2016-05-24 03:58:05'),
(219, 'Depok Xxibogor', NULL, '(021) 776 0679', 'http://www.21cineplex.com/theater/bioskop-depok-xxi,57,BGRDEPK.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-05-24 03:58:05', '2016-05-24 03:58:05'),
(220, 'Metmall Cileungsi Xxibogor', NULL, '(021) 29212061', 'http://www.21cineplex.com/theater/bioskop-metmall-cileungsi-xxi,402,BGRMECI.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-05-24 03:58:05', '2016-05-24 03:58:05'),
(221, 'Bassura Xxijakarta', NULL, '(021) 22807229', 'http://www.21cineplex.com/theater/bioskop-bassura-xxi,403,JKTBASS.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-05-24 03:58:05', '2016-05-24 03:58:05'),
(222, 'Mantos 1 Xximanado', NULL, '(0431) 888 1682', 'http://www.21cineplex.com/theater/bioskop-mantos-1-xxi,196,MNDSTUO.htm', NULL, '0.000000', '0.000000', 0, 1, 0, '2016-05-24 03:58:05', '2016-05-24 03:58:05');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_12_25_040013_usersTable', 1),
('2015_12_25_040108_passwordResetsTable', 1),
('2015_12_25_054831_moviesTable', 1),
('2015_12_25_064317_categoriesTable', 1),
('2015_12_25_064624_schedulesTable', 1),
('2015_12_31_050542_cinemasTable', 1);

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `id` int(10) UNSIGNED NOT NULL,
  `cinema_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `showtimes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `booking_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `genre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `producer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL,
  `director` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `production` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `showing` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`id`, `cinema_id`, `title`, `description`, `showtimes`, `image`, `image_url`, `youtube_url`, `booking_url`, `genre`, `producer`, `rate`, `duration`, `director`, `author`, `production`, `showing`, `created_at`, `updated_at`) VALUES
(1, 24, 'FINDING DORY', 'Usai perjalanannya bersama ayah Nemo (Albert Brooks), kali ini giliran Dory (Ellen DeGeneres) yang akan pergi berpetualang untuk mencari keluarganya. Ikan biru pelupa itu tiba-tiba ingat akan kenangan masa kecilnya, yaitu tentang permata dari Teluk Morro, California.\n \nBersama Marlin, Nemo (Hayden Rolence) dan Hank (Ed O\'Neill) seekor gurita baik hati, Dory menjelajah lautan untuk kembali bertemu dengan keluarganya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Finding Dory Pecahkan Rekor Box Office - 20 Jun \'16Sekuel Finding Nemo Berjudul Finding Dory Siap Dirilis di Tahun 2015 - 03 Apr \'13', '12:30 |14:40 |16:50 |19:00 |21:10 ', '', 'http://www.21cineplex.com/data/gallery/pictures/14639882704563_300x430.jpg', '', '', 'Animation,  Adventure,  Comedy', 'Lindsey Collins', 'SU', 103, 'Andrew Stanton, Angus Maclane', 'Andrew Stanton', 'Walt Disney PicturesCastsEllen DegeneresAlbert BrooksDiane KeatonEugene LevyIdris ElbaAndrew StantonBill Hader	                    \r\n            \r\n            \r\n            \r\n                     SYNOPSIS \r\n                    \r\n                    \r\n    ', 1, '2016-06-20 17:16:08', '2016-06-20 17:16:08'),
(2, 24, 'NOW YOU SEE ME 2', 'Satu tahun setelah pertunjukan besar dalam menipu FBI, the Four Horsemen (Jesse Eisenberg, Dave Franco, Woody Harrelson, Lizzy Caplan) kembali muncul ke publik. Kali ini mereka harus berurusan dengan seorang pengusaha teknologi bernama Walter Mabry (Daniel Radcliffe) Mabry sukses menjebak The four Horsemen, dan dengan sengaja merekrut para pesulap untuk memaksa mereka membantunya menciptakan sebuah trik sulap yang sangat mustahil dan belum pernah dilihat oleh dunia. Tidak ada jalan lain bagi para Horsemen kecuali mengikuti perintah Mabry, sekaligus mencoba menguak siapa dalang dari segala masalah yang mereka alami kepada publik.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Film Hollywood yang Siap Tayang di Tahun 2016 - Part 2 - 05 Jan \'16', '13:00 |15:35 |18:10 |20:45 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146365053455585_300x430.jpg', '', '', 'Action,  Thriller', 'Bobby Cohen, Alex Kurtzman, Roberto Orci', 'RT13', 129, 'Jon M Chu', 'Ed Solomon, Pete Chiarelli', 'Entertainment One\r\n            Homepage', 1, '2016-06-20 17:16:09', '2016-06-20 17:16:09'),
(3, 24, 'THE CONJURING 2', 'Pasangan penyelidik supernatural Ed (Patrick Wilson) dan Lorraine Warren (Vera Famiga) terbang ke Enfield, London untuk membantu Peggy Hodgson (Frances O\'Connor), seorang ibu dengan empat orang anak. Rumah yang mereka tempati ternyata dihuni oleh mahluk halus jahat. Bahkan Salah satu dari roh jahat berhasil merasuki Janet Hodgson (Madison Wolfe), anak perempuan dari Peggy Lorraine sempat tidak percaya dengan cerita keluarga Peggy. Sampai ia mendapatkan sebuah pertanda mengenai eksistensi sang mahluk, yang ternyata ingin mencelakakan mereka. Ed dan Lorraine pun harus berjuang menghentikan sang mahluk untuk mencapai tujuannya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Hantu Biarawati di The Conjuring 2 Siap Difilmkan - 16 Jun \'16The Conjuring 2 Puncaki Box Office - 13 Jun \'16', '11:05 |13:45 |16:25 |19:05 |21:45 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146364988872505_300x430.jpg', '', '', 'Horror,  Thriller', 'Rob Cowan, Peter Safran', 'RT17', 133, 'James Wan', 'Chad Hayes, Carey W. Hayes, James Wan, David Leslie Johnson', 'Warner Bros. PicturesCastsVera FarmigaPatrick WilsonFrances O\'connorMadison WolfeFranka PotenteSimon McburneySterling JerinsFrances O&#x0027;connorFrances O&amp;#x0027;connor	                    \r\n            \r\n            \r\n            \r\n                ', 1, '2016-06-20 17:16:10', '2016-06-20 17:16:10'),
(4, 25, 'FINDING DORY', 'Usai perjalanannya bersama ayah Nemo (Albert Brooks), kali ini giliran Dory (Ellen DeGeneres) yang akan pergi berpetualang untuk mencari keluarganya. Ikan biru pelupa itu tiba-tiba ingat akan kenangan masa kecilnya, yaitu tentang permata dari Teluk Morro, California.\n \nBersama Marlin, Nemo (Hayden Rolence) dan Hank (Ed O\'Neill) seekor gurita baik hati, Dory menjelajah lautan untuk kembali bertemu dengan keluarganya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Finding Dory Pecahkan Rekor Box Office - 20 Jun \'16Sekuel Finding Nemo Berjudul Finding Dory Siap Dirilis di Tahun 2015 - 03 Apr \'13', '12:30 |14:40 |16:50 |19:00 |21:10 ', '', 'http://www.21cineplex.com/data/gallery/pictures/14639882704563_300x430.jpg', '', '', 'Animation,  Adventure,  Comedy', 'Lindsey Collins', 'SU', 103, 'Andrew Stanton, Angus Maclane', 'Andrew Stanton', 'Walt Disney PicturesCastsEllen DegeneresAlbert BrooksDiane KeatonEugene LevyIdris ElbaAndrew StantonBill Hader	                    \r\n            \r\n            \r\n            \r\n                     SYNOPSIS \r\n                    \r\n                    \r\n    ', 1, '2016-06-20 17:16:13', '2016-06-20 17:16:13'),
(5, 25, 'NOW YOU SEE ME 2', 'Satu tahun setelah pertunjukan besar dalam menipu FBI, the Four Horsemen (Jesse Eisenberg, Dave Franco, Woody Harrelson, Lizzy Caplan) kembali muncul ke publik. Kali ini mereka harus berurusan dengan seorang pengusaha teknologi bernama Walter Mabry (Daniel Radcliffe) Mabry sukses menjebak The four Horsemen, dan dengan sengaja merekrut para pesulap untuk memaksa mereka membantunya menciptakan sebuah trik sulap yang sangat mustahil dan belum pernah dilihat oleh dunia. Tidak ada jalan lain bagi para Horsemen kecuali mengikuti perintah Mabry, sekaligus mencoba menguak siapa dalang dari segala masalah yang mereka alami kepada publik.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Film Hollywood yang Siap Tayang di Tahun 2016 - Part 2 - 05 Jan \'16', '13:15 |15:50 |18:25 |21:00 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146365053455585_300x430.jpg', '', '', 'Action,  Thriller', 'Bobby Cohen, Alex Kurtzman, Roberto Orci', 'RT13', 129, 'Jon M Chu', 'Ed Solomon, Pete Chiarelli', 'Entertainment One\r\n            Homepage', 1, '2016-06-20 17:16:14', '2016-06-20 17:16:14'),
(6, 25, 'CENTRAL INTELLIGENCE', 'Dalam sebuah reuni SMA, dua orang sahabat Bob Stone (Dwayne Johnson), dan Calvin (Kevin Hart) kembali bertemu, namun dengan keadaan yang jauh berbeda. Sang bintang sekolah, Calvin, kini menjadi seorang akuntan. di sisi lain, Stone, yang dulunya adalah seorang kutu buku, ternyata bekerja sebagai agen CIA. Dan kali ini sang agen meminta bantuan teman lamanya tersebut untuk membantu dia menyelesaikan sebuah misi rahasia. setelah mengetahui hal tersebut, hidup Calvin pun berubah total. Ia dan Stone harus saling bahu membahu untuk memecahkan masalah ditengah baku tembak dan intrik rahasia, yang mungkin saja akan membuat mereka terbunuh.\r\nCOMPLETE ARCHIVE', '12:30 |14:45 |17:00 |19:15 |21:30 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146494097586077_300x430.jpg', '', '', 'Action,  Comedy', 'Scott Stuber, Paul Young, Michael Fottrell, Peter Principato', 'RT17', 106, 'Rawson Marshall Thurber', 'Ike Barinholtz, David Stassen, Rawson Marshall Thurber', 'Warner Bros. Pictures\r\n            Homepage', 1, '2016-06-20 17:16:15', '2016-06-20 17:16:15'),
(7, 25, 'THE CONJURING 2', 'Pasangan penyelidik supernatural Ed (Patrick Wilson) dan Lorraine Warren (Vera Famiga) terbang ke Enfield, London untuk membantu Peggy Hodgson (Frances O\'Connor), seorang ibu dengan empat orang anak. Rumah yang mereka tempati ternyata dihuni oleh mahluk halus jahat. Bahkan Salah satu dari roh jahat berhasil merasuki Janet Hodgson (Madison Wolfe), anak perempuan dari Peggy Lorraine sempat tidak percaya dengan cerita keluarga Peggy. Sampai ia mendapatkan sebuah pertanda mengenai eksistensi sang mahluk, yang ternyata ingin mencelakakan mereka. Ed dan Lorraine pun harus berjuang menghentikan sang mahluk untuk mencapai tujuannya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Hantu Biarawati di The Conjuring 2 Siap Difilmkan - 16 Jun \'16The Conjuring 2 Puncaki Box Office - 13 Jun \'16', '13:00 |18:20 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146364988872505_300x430.jpg', '', '', 'Horror,  Thriller', 'Rob Cowan, Peter Safran', 'RT17', 133, 'James Wan', 'Chad Hayes, Carey W. Hayes, James Wan, David Leslie Johnson', 'Warner Bros. PicturesCastsVera FarmigaPatrick WilsonFrances O\'connorMadison WolfeFranka PotenteSimon McburneySterling JerinsFrances O&#x0027;connorFrances O&amp;#x0027;connor	                    \r\n            \r\n            \r\n            \r\n                ', 1, '2016-06-20 17:16:16', '2016-06-20 17:16:16'),
(8, 26, 'FINDING DORY', 'Usai perjalanannya bersama ayah Nemo (Albert Brooks), kali ini giliran Dory (Ellen DeGeneres) yang akan pergi berpetualang untuk mencari keluarganya. Ikan biru pelupa itu tiba-tiba ingat akan kenangan masa kecilnya, yaitu tentang permata dari Teluk Morro, California.\n \nBersama Marlin, Nemo (Hayden Rolence) dan Hank (Ed O\'Neill) seekor gurita baik hati, Dory menjelajah lautan untuk kembali bertemu dengan keluarganya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Finding Dory Pecahkan Rekor Box Office - 20 Jun \'16Sekuel Finding Nemo Berjudul Finding Dory Siap Dirilis di Tahun 2015 - 03 Apr \'13', '12:45 |14:55 |17:05 |19:15 |21:25 ', '', 'http://www.21cineplex.com/data/gallery/pictures/14639882704563_300x430.jpg', '', '', 'Animation,  Adventure,  Comedy', 'Lindsey Collins', 'SU', 103, 'Andrew Stanton, Angus Maclane', 'Andrew Stanton', 'Walt Disney PicturesCastsEllen DegeneresAlbert BrooksDiane KeatonEugene LevyIdris ElbaAndrew StantonBill Hader	                    \r\n            \r\n            \r\n            \r\n                     SYNOPSIS \r\n                    \r\n                    \r\n    ', 1, '2016-06-20 17:16:20', '2016-06-20 17:16:20'),
(9, 26, 'NOW YOU SEE ME 2', 'Satu tahun setelah pertunjukan besar dalam menipu FBI, the Four Horsemen (Jesse Eisenberg, Dave Franco, Woody Harrelson, Lizzy Caplan) kembali muncul ke publik. Kali ini mereka harus berurusan dengan seorang pengusaha teknologi bernama Walter Mabry (Daniel Radcliffe) Mabry sukses menjebak The four Horsemen, dan dengan sengaja merekrut para pesulap untuk memaksa mereka membantunya menciptakan sebuah trik sulap yang sangat mustahil dan belum pernah dilihat oleh dunia. Tidak ada jalan lain bagi para Horsemen kecuali mengikuti perintah Mabry, sekaligus mencoba menguak siapa dalang dari segala masalah yang mereka alami kepada publik.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Film Hollywood yang Siap Tayang di Tahun 2016 - Part 2 - 05 Jan \'16', '13:30 |16:05 |18:40 |21:15 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146365053455585_300x430.jpg', '', '', 'Action,  Thriller', 'Bobby Cohen, Alex Kurtzman, Roberto Orci', 'RT13', 129, 'Jon M Chu', 'Ed Solomon, Pete Chiarelli', 'Entertainment One\r\n            Homepage', 1, '2016-06-20 17:16:21', '2016-06-20 17:16:21'),
(10, 26, 'CENTRAL INTELLIGENCE', 'Dalam sebuah reuni SMA, dua orang sahabat Bob Stone (Dwayne Johnson), dan Calvin (Kevin Hart) kembali bertemu, namun dengan keadaan yang jauh berbeda. Sang bintang sekolah, Calvin, kini menjadi seorang akuntan. di sisi lain, Stone, yang dulunya adalah seorang kutu buku, ternyata bekerja sebagai agen CIA. Dan kali ini sang agen meminta bantuan teman lamanya tersebut untuk membantu dia menyelesaikan sebuah misi rahasia. setelah mengetahui hal tersebut, hidup Calvin pun berubah total. Ia dan Stone harus saling bahu membahu untuk memecahkan masalah ditengah baku tembak dan intrik rahasia, yang mungkin saja akan membuat mereka terbunuh.\r\nCOMPLETE ARCHIVE', '12:15 |12:45 |14:30 |15:00 |16:45 |17:15 |19:00 |19:30 |21:15 |21:45 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146494097586077_300x430.jpg', '', '', 'Action,  Comedy', 'Scott Stuber, Paul Young, Michael Fottrell, Peter Principato', 'RT17', 106, 'Rawson Marshall Thurber', 'Ike Barinholtz, David Stassen, Rawson Marshall Thurber', 'Warner Bros. Pictures\r\n            Homepage', 1, '2016-06-20 17:16:22', '2016-06-20 17:16:22'),
(11, 26, 'THE CONJURING 2', 'Pasangan penyelidik supernatural Ed (Patrick Wilson) dan Lorraine Warren (Vera Famiga) terbang ke Enfield, London untuk membantu Peggy Hodgson (Frances O\'Connor), seorang ibu dengan empat orang anak. Rumah yang mereka tempati ternyata dihuni oleh mahluk halus jahat. Bahkan Salah satu dari roh jahat berhasil merasuki Janet Hodgson (Madison Wolfe), anak perempuan dari Peggy Lorraine sempat tidak percaya dengan cerita keluarga Peggy. Sampai ia mendapatkan sebuah pertanda mengenai eksistensi sang mahluk, yang ternyata ingin mencelakakan mereka. Ed dan Lorraine pun harus berjuang menghentikan sang mahluk untuk mencapai tujuannya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Hantu Biarawati di The Conjuring 2 Siap Difilmkan - 16 Jun \'16The Conjuring 2 Puncaki Box Office - 13 Jun \'16', '11:05 |12:15 |12:45 |13:15 |13:45 |14:55 |15:25 |15:55 |16:25 |17:35 |18:05 |18:35 |19:05 |20:15 |20:45 |21:15 |21:45 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146364988872505_300x430.jpg', '', '', 'Horror,  Thriller', 'Rob Cowan, Peter Safran', 'RT17', 133, 'James Wan', 'Chad Hayes, Carey W. Hayes, James Wan, David Leslie Johnson', 'Warner Bros. PicturesCastsVera FarmigaPatrick WilsonFrances O\'connorMadison WolfeFranka PotenteSimon McburneySterling JerinsFrances O&#x0027;connorFrances O&amp;#x0027;connor	                    \r\n            \r\n            \r\n            \r\n                ', 1, '2016-06-20 17:16:23', '2016-06-20 17:16:23'),
(12, 26, 'FINDING DORY (IMAX 3D)', 'Usai perjalanannya bersama ayah Nemo (Albert Brooks), kali ini giliran Dory (Ellen DeGeneres) yang akan pergi berpetualang untuk mencari keluarganya. Ikan biru pelupa itu tiba-tiba ingat akan kenangan masa kecilnya, yaitu tentang permata dari Teluk Morro, California.\n \nBersama Marlin, Nemo (Hayden Rolence) dan Hank (Ed O\'Neill) seekor gurita baik hati, Dory menjelajah lautan untuk kembali bertemu dengan keluarganya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Finding Dory Pecahkan Rekor Box Office - 20 Jun \'16Sekuel Finding Nemo Berjudul Finding Dory Siap Dirilis di Tahun 2015 - 03 Apr \'13', '12:45 |14:55 |17:05 |19:15 |21:25 ', '', 'http://www.21cineplex.com/data/gallery/pictures/14639882704563_300x430.jpg', '', '', 'Animation,  Adventure,  Comedy', 'Lindsey Collins', 'SU', 103, 'Andrew Stanton, Angus Maclane', 'Andrew Stanton', 'Walt Disney PicturesCastsEllen DegeneresAlbert BrooksDiane KeatonEugene LevyIdris ElbaDominic WestTy BurrellHayden RolenceEd O\'neillKaitlin OlsonBob PetersonTorbin BullockAndrew StantonKate MckinnonBill HaderBennett DammannEd O&#	                    \r\n   ', 1, '2016-06-20 17:16:24', '2016-06-20 17:16:24'),
(13, 27, 'FINDING DORY', 'Usai perjalanannya bersama ayah Nemo (Albert Brooks), kali ini giliran Dory (Ellen DeGeneres) yang akan pergi berpetualang untuk mencari keluarganya. Ikan biru pelupa itu tiba-tiba ingat akan kenangan masa kecilnya, yaitu tentang permata dari Teluk Morro, California.\n \nBersama Marlin, Nemo (Hayden Rolence) dan Hank (Ed O\'Neill) seekor gurita baik hati, Dory menjelajah lautan untuk kembali bertemu dengan keluarganya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Finding Dory Pecahkan Rekor Box Office - 20 Jun \'16Sekuel Finding Nemo Berjudul Finding Dory Siap Dirilis di Tahun 2015 - 03 Apr \'13', '12:15 |12:45 |14:25 |14:55 |16:35 |17:05 |18:45 |19:15 |20:55 |21:25 ', '', 'http://www.21cineplex.com/data/gallery/pictures/14639882704563_300x430.jpg', '', '', 'Animation,  Adventure,  Comedy', 'Lindsey Collins', 'SU', 103, 'Andrew Stanton, Angus Maclane', 'Andrew Stanton', 'Walt Disney PicturesCastsEllen DegeneresAlbert BrooksDiane KeatonEugene LevyIdris ElbaAndrew StantonBill Hader	                    \r\n            \r\n            \r\n            \r\n                     SYNOPSIS \r\n                    \r\n                    \r\n    ', 1, '2016-06-20 17:16:28', '2016-06-20 17:16:28'),
(14, 27, 'THE CONJURING 2', 'Pasangan penyelidik supernatural Ed (Patrick Wilson) dan Lorraine Warren (Vera Famiga) terbang ke Enfield, London untuk membantu Peggy Hodgson (Frances O\'Connor), seorang ibu dengan empat orang anak. Rumah yang mereka tempati ternyata dihuni oleh mahluk halus jahat. Bahkan Salah satu dari roh jahat berhasil merasuki Janet Hodgson (Madison Wolfe), anak perempuan dari Peggy Lorraine sempat tidak percaya dengan cerita keluarga Peggy. Sampai ia mendapatkan sebuah pertanda mengenai eksistensi sang mahluk, yang ternyata ingin mencelakakan mereka. Ed dan Lorraine pun harus berjuang menghentikan sang mahluk untuk mencapai tujuannya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Hantu Biarawati di The Conjuring 2 Siap Difilmkan - 16 Jun \'16The Conjuring 2 Puncaki Box Office - 13 Jun \'16', '13:15 |15:55 |18:35 |21:15 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146364988872505_300x430.jpg', '', '', 'Horror,  Thriller', 'Rob Cowan, Peter Safran', 'RT17', 133, 'James Wan', 'Chad Hayes, Carey W. Hayes, James Wan, David Leslie Johnson', 'Warner Bros. PicturesCastsVera FarmigaPatrick WilsonFrances O\'connorMadison WolfeFranka PotenteSimon McburneySterling JerinsFrances O&#x0027;connorFrances O&amp;#x0027;connor	                    \r\n            \r\n            \r\n            \r\n                ', 1, '2016-06-20 17:16:29', '2016-06-20 17:16:29'),
(15, 27, 'NOW YOU SEE ME 2', 'Satu tahun setelah pertunjukan besar dalam menipu FBI, the Four Horsemen (Jesse Eisenberg, Dave Franco, Woody Harrelson, Lizzy Caplan) kembali muncul ke publik. Kali ini mereka harus berurusan dengan seorang pengusaha teknologi bernama Walter Mabry (Daniel Radcliffe) Mabry sukses menjebak The four Horsemen, dan dengan sengaja merekrut para pesulap untuk memaksa mereka membantunya menciptakan sebuah trik sulap yang sangat mustahil dan belum pernah dilihat oleh dunia. Tidak ada jalan lain bagi para Horsemen kecuali mengikuti perintah Mabry, sekaligus mencoba menguak siapa dalang dari segala masalah yang mereka alami kepada publik.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Film Hollywood yang Siap Tayang di Tahun 2016 - Part 2 - 05 Jan \'16', '13:00 |15:35 |18:10 |20:45 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146365053455585_300x430.jpg', '', '', 'Action,  Thriller', 'Bobby Cohen, Alex Kurtzman, Roberto Orci', 'RT13', 129, 'Jon M Chu', 'Ed Solomon, Pete Chiarelli', 'Entertainment One\r\n            Homepage', 1, '2016-06-20 17:16:31', '2016-06-20 17:16:31'),
(16, 27, 'CENTRAL INTELLIGENCE', 'Dalam sebuah reuni SMA, dua orang sahabat Bob Stone (Dwayne Johnson), dan Calvin (Kevin Hart) kembali bertemu, namun dengan keadaan yang jauh berbeda. Sang bintang sekolah, Calvin, kini menjadi seorang akuntan. di sisi lain, Stone, yang dulunya adalah seorang kutu buku, ternyata bekerja sebagai agen CIA. Dan kali ini sang agen meminta bantuan teman lamanya tersebut untuk membantu dia menyelesaikan sebuah misi rahasia. setelah mengetahui hal tersebut, hidup Calvin pun berubah total. Ia dan Stone harus saling bahu membahu untuk memecahkan masalah ditengah baku tembak dan intrik rahasia, yang mungkin saja akan membuat mereka terbunuh.\r\nCOMPLETE ARCHIVE', '12:30 |14:45 |17:00 |19:15 |21:30 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146494097586077_300x430.jpg', '', '', 'Action,  Comedy', 'Scott Stuber, Paul Young, Michael Fottrell, Peter Principato', 'RT17', 106, 'Rawson Marshall Thurber', 'Ike Barinholtz, David Stassen, Rawson Marshall Thurber', 'Warner Bros. Pictures\r\n            Homepage', 1, '2016-06-20 17:16:31', '2016-06-20 17:16:31'),
(17, 27, 'FINDING DORY (IMAX 3D)', 'Usai perjalanannya bersama ayah Nemo (Albert Brooks), kali ini giliran Dory (Ellen DeGeneres) yang akan pergi berpetualang untuk mencari keluarganya. Ikan biru pelupa itu tiba-tiba ingat akan kenangan masa kecilnya, yaitu tentang permata dari Teluk Morro, California.\n \nBersama Marlin, Nemo (Hayden Rolence) dan Hank (Ed O\'Neill) seekor gurita baik hati, Dory menjelajah lautan untuk kembali bertemu dengan keluarganya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Finding Dory Pecahkan Rekor Box Office - 20 Jun \'16Sekuel Finding Nemo Berjudul Finding Dory Siap Dirilis di Tahun 2015 - 03 Apr \'13', '12:45 |14:55 |17:05 |19:15 |21:25 ', '', 'http://www.21cineplex.com/data/gallery/pictures/14639882704563_300x430.jpg', '', '', 'Animation,  Adventure,  Comedy', 'Lindsey Collins', 'SU', 103, 'Andrew Stanton, Angus Maclane', 'Andrew Stanton', 'Walt Disney PicturesCastsEllen DegeneresAlbert BrooksDiane KeatonEugene LevyIdris ElbaDominic WestTy BurrellHayden RolenceEd O\'neillKaitlin OlsonBob PetersonTorbin BullockAndrew StantonKate MckinnonBill HaderBennett DammannEd O&#	                    \r\n   ', 1, '2016-06-20 17:16:32', '2016-06-20 17:16:32'),
(18, 28, 'THE CONJURING 2', 'Pasangan penyelidik supernatural Ed (Patrick Wilson) dan Lorraine Warren (Vera Famiga) terbang ke Enfield, London untuk membantu Peggy Hodgson (Frances O\'Connor), seorang ibu dengan empat orang anak. Rumah yang mereka tempati ternyata dihuni oleh mahluk halus jahat. Bahkan Salah satu dari roh jahat berhasil merasuki Janet Hodgson (Madison Wolfe), anak perempuan dari Peggy Lorraine sempat tidak percaya dengan cerita keluarga Peggy. Sampai ia mendapatkan sebuah pertanda mengenai eksistensi sang mahluk, yang ternyata ingin mencelakakan mereka. Ed dan Lorraine pun harus berjuang menghentikan sang mahluk untuk mencapai tujuannya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Hantu Biarawati di The Conjuring 2 Siap Difilmkan - 16 Jun \'16The Conjuring 2 Puncaki Box Office - 13 Jun \'16', '13:00 |15:40 |18:20 |21:00 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146364988872505_300x430.jpg', '', '', 'Horror,  Thriller', 'Rob Cowan, Peter Safran', 'RT17', 133, 'James Wan', 'Chad Hayes, Carey W. Hayes, James Wan, David Leslie Johnson', 'Warner Bros. PicturesCastsVera FarmigaPatrick WilsonFrances O\'connorMadison WolfeFranka PotenteSimon McburneySterling JerinsFrances O&#x0027;connorFrances O&amp;#x0027;connor	                    \r\n            \r\n            \r\n            \r\n                ', 1, '2016-06-20 17:16:37', '2016-06-20 17:16:37'),
(19, 28, 'FINDING DORY', 'Usai perjalanannya bersama ayah Nemo (Albert Brooks), kali ini giliran Dory (Ellen DeGeneres) yang akan pergi berpetualang untuk mencari keluarganya. Ikan biru pelupa itu tiba-tiba ingat akan kenangan masa kecilnya, yaitu tentang permata dari Teluk Morro, California.\n \nBersama Marlin, Nemo (Hayden Rolence) dan Hank (Ed O\'Neill) seekor gurita baik hati, Dory menjelajah lautan untuk kembali bertemu dengan keluarganya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Finding Dory Pecahkan Rekor Box Office - 20 Jun \'16Sekuel Finding Nemo Berjudul Finding Dory Siap Dirilis di Tahun 2015 - 03 Apr \'13', '12:30 |14:40 |16:50 |19:00 |21:10 ', '', 'http://www.21cineplex.com/data/gallery/pictures/14639882704563_300x430.jpg', '', '', 'Animation,  Adventure,  Comedy', 'Lindsey Collins', 'SU', 103, 'Andrew Stanton, Angus Maclane', 'Andrew Stanton', 'Walt Disney PicturesCastsEllen DegeneresAlbert BrooksDiane KeatonEugene LevyIdris ElbaAndrew StantonBill Hader	                    \r\n            \r\n            \r\n            \r\n                     SYNOPSIS \r\n                    \r\n                    \r\n    ', 1, '2016-06-20 17:16:38', '2016-06-20 17:16:38'),
(20, 28, 'NOW YOU SEE ME 2', 'Satu tahun setelah pertunjukan besar dalam menipu FBI, the Four Horsemen (Jesse Eisenberg, Dave Franco, Woody Harrelson, Lizzy Caplan) kembali muncul ke publik. Kali ini mereka harus berurusan dengan seorang pengusaha teknologi bernama Walter Mabry (Daniel Radcliffe) Mabry sukses menjebak The four Horsemen, dan dengan sengaja merekrut para pesulap untuk memaksa mereka membantunya menciptakan sebuah trik sulap yang sangat mustahil dan belum pernah dilihat oleh dunia. Tidak ada jalan lain bagi para Horsemen kecuali mengikuti perintah Mabry, sekaligus mencoba menguak siapa dalang dari segala masalah yang mereka alami kepada publik.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Film Hollywood yang Siap Tayang di Tahun 2016 - Part 2 - 05 Jan \'16', '13:30 |16:05 |18:40 |21:15 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146365053455585_300x430.jpg', '', '', 'Action,  Thriller', 'Bobby Cohen, Alex Kurtzman, Roberto Orci', 'RT13', 129, 'Jon M Chu', 'Ed Solomon, Pete Chiarelli', 'Entertainment One\r\n            Homepage', 1, '2016-06-20 17:16:39', '2016-06-20 17:16:39'),
(21, 28, 'CENTRAL INTELLIGENCE', 'Dalam sebuah reuni SMA, dua orang sahabat Bob Stone (Dwayne Johnson), dan Calvin (Kevin Hart) kembali bertemu, namun dengan keadaan yang jauh berbeda. Sang bintang sekolah, Calvin, kini menjadi seorang akuntan. di sisi lain, Stone, yang dulunya adalah seorang kutu buku, ternyata bekerja sebagai agen CIA. Dan kali ini sang agen meminta bantuan teman lamanya tersebut untuk membantu dia menyelesaikan sebuah misi rahasia. setelah mengetahui hal tersebut, hidup Calvin pun berubah total. Ia dan Stone harus saling bahu membahu untuk memecahkan masalah ditengah baku tembak dan intrik rahasia, yang mungkin saja akan membuat mereka terbunuh.\r\nCOMPLETE ARCHIVE', '12:30 |14:45 |17:00 |19:15 |21:30 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146494097586077_300x430.jpg', '', '', 'Action,  Comedy', 'Scott Stuber, Paul Young, Michael Fottrell, Peter Principato', 'RT17', 106, 'Rawson Marshall Thurber', 'Ike Barinholtz, David Stassen, Rawson Marshall Thurber', 'Warner Bros. Pictures\r\n            Homepage', 1, '2016-06-20 17:16:40', '2016-06-20 17:16:40'),
(22, 29, 'FINDING DORY', 'Usai perjalanannya bersama ayah Nemo (Albert Brooks), kali ini giliran Dory (Ellen DeGeneres) yang akan pergi berpetualang untuk mencari keluarganya. Ikan biru pelupa itu tiba-tiba ingat akan kenangan masa kecilnya, yaitu tentang permata dari Teluk Morro, California.\n \nBersama Marlin, Nemo (Hayden Rolence) dan Hank (Ed O\'Neill) seekor gurita baik hati, Dory menjelajah lautan untuk kembali bertemu dengan keluarganya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Finding Dory Pecahkan Rekor Box Office - 20 Jun \'16Sekuel Finding Nemo Berjudul Finding Dory Siap Dirilis di Tahun 2015 - 03 Apr \'13', '12:15 |12:45 |14:25 |14:55 |16:35 |17:05 |18:45 |19:15 |20:55 |21:25 ', '', 'http://www.21cineplex.com/data/gallery/pictures/14639882704563_300x430.jpg', '', '', 'Animation,  Adventure,  Comedy', 'Lindsey Collins', 'SU', 103, 'Andrew Stanton, Angus Maclane', 'Andrew Stanton', 'Walt Disney PicturesCastsEllen DegeneresAlbert BrooksDiane KeatonEugene LevyIdris ElbaAndrew StantonBill Hader	                    \r\n            \r\n            \r\n            \r\n                     SYNOPSIS \r\n                    \r\n                    \r\n    ', 1, '2016-06-20 17:16:44', '2016-06-20 17:16:44'),
(23, 29, 'THE CONJURING 2', 'Pasangan penyelidik supernatural Ed (Patrick Wilson) dan Lorraine Warren (Vera Famiga) terbang ke Enfield, London untuk membantu Peggy Hodgson (Frances O\'Connor), seorang ibu dengan empat orang anak. Rumah yang mereka tempati ternyata dihuni oleh mahluk halus jahat. Bahkan Salah satu dari roh jahat berhasil merasuki Janet Hodgson (Madison Wolfe), anak perempuan dari Peggy Lorraine sempat tidak percaya dengan cerita keluarga Peggy. Sampai ia mendapatkan sebuah pertanda mengenai eksistensi sang mahluk, yang ternyata ingin mencelakakan mereka. Ed dan Lorraine pun harus berjuang menghentikan sang mahluk untuk mencapai tujuannya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Hantu Biarawati di The Conjuring 2 Siap Difilmkan - 16 Jun \'16The Conjuring 2 Puncaki Box Office - 13 Jun \'16', '12:45 |15:25 |16:25 |18:05 |19:05 |20:45 |21:45 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146364988872505_300x430.jpg', '', '', 'Horror,  Thriller', 'Rob Cowan, Peter Safran', 'RT17', 133, 'James Wan', 'Chad Hayes, Carey W. Hayes, James Wan, David Leslie Johnson', 'Warner Bros. PicturesCastsVera FarmigaPatrick WilsonFrances O\'connorMadison WolfeFranka PotenteSimon McburneySterling JerinsFrances O&#x0027;connorFrances O&amp;#x0027;connor	                    \r\n            \r\n            \r\n            \r\n                ', 1, '2016-06-20 17:16:45', '2016-06-20 17:16:45'),
(24, 29, 'CENTRAL INTELLIGENCE', 'Dalam sebuah reuni SMA, dua orang sahabat Bob Stone (Dwayne Johnson), dan Calvin (Kevin Hart) kembali bertemu, namun dengan keadaan yang jauh berbeda. Sang bintang sekolah, Calvin, kini menjadi seorang akuntan. di sisi lain, Stone, yang dulunya adalah seorang kutu buku, ternyata bekerja sebagai agen CIA. Dan kali ini sang agen meminta bantuan teman lamanya tersebut untuk membantu dia menyelesaikan sebuah misi rahasia. setelah mengetahui hal tersebut, hidup Calvin pun berubah total. Ia dan Stone harus saling bahu membahu untuk memecahkan masalah ditengah baku tembak dan intrik rahasia, yang mungkin saja akan membuat mereka terbunuh.\r\nCOMPLETE ARCHIVE', '11:55 |14:10 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146494097586077_300x430.jpg', '', '', 'Action,  Comedy', 'Scott Stuber, Paul Young, Michael Fottrell, Peter Principato', 'RT17', 106, 'Rawson Marshall Thurber', 'Ike Barinholtz, David Stassen, Rawson Marshall Thurber', 'Warner Bros. Pictures\r\n            Homepage', 1, '2016-06-20 17:16:46', '2016-06-20 17:16:46'),
(25, 29, 'NOW YOU SEE ME 2', 'Satu tahun setelah pertunjukan besar dalam menipu FBI, the Four Horsemen (Jesse Eisenberg, Dave Franco, Woody Harrelson, Lizzy Caplan) kembali muncul ke publik. Kali ini mereka harus berurusan dengan seorang pengusaha teknologi bernama Walter Mabry (Daniel Radcliffe) Mabry sukses menjebak The four Horsemen, dan dengan sengaja merekrut para pesulap untuk memaksa mereka membantunya menciptakan sebuah trik sulap yang sangat mustahil dan belum pernah dilihat oleh dunia. Tidak ada jalan lain bagi para Horsemen kecuali mengikuti perintah Mabry, sekaligus mencoba menguak siapa dalang dari segala masalah yang mereka alami kepada publik.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Film Hollywood yang Siap Tayang di Tahun 2016 - Part 2 - 05 Jan \'16', '13:30 |16:05 |18:40 |21:15 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146365053455585_300x430.jpg', '', '', 'Action,  Thriller', 'Bobby Cohen, Alex Kurtzman, Roberto Orci', 'RT13', 129, 'Jon M Chu', 'Ed Solomon, Pete Chiarelli', 'Entertainment One\r\n            Homepage', 1, '2016-06-20 17:16:47', '2016-06-20 17:16:47'),
(26, 30, 'FINDING DORY', 'Usai perjalanannya bersama ayah Nemo (Albert Brooks), kali ini giliran Dory (Ellen DeGeneres) yang akan pergi berpetualang untuk mencari keluarganya. Ikan biru pelupa itu tiba-tiba ingat akan kenangan masa kecilnya, yaitu tentang permata dari Teluk Morro, California.\n \nBersama Marlin, Nemo (Hayden Rolence) dan Hank (Ed O\'Neill) seekor gurita baik hati, Dory menjelajah lautan untuk kembali bertemu dengan keluarganya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Finding Dory Pecahkan Rekor Box Office - 20 Jun \'16Sekuel Finding Nemo Berjudul Finding Dory Siap Dirilis di Tahun 2015 - 03 Apr \'13', '12:45 |14:55 |17:05 |19:15 |21:25 ', '', 'http://www.21cineplex.com/data/gallery/pictures/14639882704563_300x430.jpg', '', '', 'Animation,  Adventure,  Comedy', 'Lindsey Collins', 'SU', 103, 'Andrew Stanton, Angus Maclane', 'Andrew Stanton', 'Walt Disney PicturesCastsEllen DegeneresAlbert BrooksDiane KeatonEugene LevyIdris ElbaAndrew StantonBill Hader	                    \r\n            \r\n            \r\n            \r\n                     SYNOPSIS \r\n                    \r\n                    \r\n    ', 1, '2016-06-20 17:16:50', '2016-06-20 17:16:50'),
(27, 30, 'NOW YOU SEE ME 2', 'Satu tahun setelah pertunjukan besar dalam menipu FBI, the Four Horsemen (Jesse Eisenberg, Dave Franco, Woody Harrelson, Lizzy Caplan) kembali muncul ke publik. Kali ini mereka harus berurusan dengan seorang pengusaha teknologi bernama Walter Mabry (Daniel Radcliffe) Mabry sukses menjebak The four Horsemen, dan dengan sengaja merekrut para pesulap untuk memaksa mereka membantunya menciptakan sebuah trik sulap yang sangat mustahil dan belum pernah dilihat oleh dunia. Tidak ada jalan lain bagi para Horsemen kecuali mengikuti perintah Mabry, sekaligus mencoba menguak siapa dalang dari segala masalah yang mereka alami kepada publik.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Film Hollywood yang Siap Tayang di Tahun 2016 - Part 2 - 05 Jan \'16', '13:30 |16:05 |18:40 |21:15 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146365053455585_300x430.jpg', '', '', 'Action,  Thriller', 'Bobby Cohen, Alex Kurtzman, Roberto Orci', 'RT13', 129, 'Jon M Chu', 'Ed Solomon, Pete Chiarelli', 'Entertainment One\r\n            Homepage', 1, '2016-06-20 17:16:51', '2016-06-20 17:16:51'),
(28, 30, 'CENTRAL INTELLIGENCE', 'Dalam sebuah reuni SMA, dua orang sahabat Bob Stone (Dwayne Johnson), dan Calvin (Kevin Hart) kembali bertemu, namun dengan keadaan yang jauh berbeda. Sang bintang sekolah, Calvin, kini menjadi seorang akuntan. di sisi lain, Stone, yang dulunya adalah seorang kutu buku, ternyata bekerja sebagai agen CIA. Dan kali ini sang agen meminta bantuan teman lamanya tersebut untuk membantu dia menyelesaikan sebuah misi rahasia. setelah mengetahui hal tersebut, hidup Calvin pun berubah total. Ia dan Stone harus saling bahu membahu untuk memecahkan masalah ditengah baku tembak dan intrik rahasia, yang mungkin saja akan membuat mereka terbunuh.\r\nCOMPLETE ARCHIVE', '12:30 |14:45 |17:00 |19:15 |21:30 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146494097586077_300x430.jpg', '', '', 'Action,  Comedy', 'Scott Stuber, Paul Young, Michael Fottrell, Peter Principato', 'RT17', 106, 'Rawson Marshall Thurber', 'Ike Barinholtz, David Stassen, Rawson Marshall Thurber', 'Warner Bros. Pictures\r\n            Homepage', 1, '2016-06-20 17:16:52', '2016-06-20 17:16:52'),
(29, 30, 'THE CONJURING 2', 'Pasangan penyelidik supernatural Ed (Patrick Wilson) dan Lorraine Warren (Vera Famiga) terbang ke Enfield, London untuk membantu Peggy Hodgson (Frances O\'Connor), seorang ibu dengan empat orang anak. Rumah yang mereka tempati ternyata dihuni oleh mahluk halus jahat. Bahkan Salah satu dari roh jahat berhasil merasuki Janet Hodgson (Madison Wolfe), anak perempuan dari Peggy Lorraine sempat tidak percaya dengan cerita keluarga Peggy. Sampai ia mendapatkan sebuah pertanda mengenai eksistensi sang mahluk, yang ternyata ingin mencelakakan mereka. Ed dan Lorraine pun harus berjuang menghentikan sang mahluk untuk mencapai tujuannya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Hantu Biarawati di The Conjuring 2 Siap Difilmkan - 16 Jun \'16The Conjuring 2 Puncaki Box Office - 13 Jun \'16', '11:05 |12:45 |13:45 |15:25 |16:25 |18:05 |19:05 |20:45 |21:45 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146364988872505_300x430.jpg', '', '', 'Horror,  Thriller', 'Rob Cowan, Peter Safran', 'RT17', 133, 'James Wan', 'Chad Hayes, Carey W. Hayes, James Wan, David Leslie Johnson', 'Warner Bros. PicturesCastsVera FarmigaPatrick WilsonFrances O\'connorMadison WolfeFranka PotenteSimon McburneySterling JerinsFrances O&#x0027;connorFrances O&amp;#x0027;connor	                    \r\n            \r\n            \r\n            \r\n                ', 1, '2016-06-20 17:16:53', '2016-06-20 17:16:53'),
(30, 31, 'THE CONJURING 2', 'Pasangan penyelidik supernatural Ed (Patrick Wilson) dan Lorraine Warren (Vera Famiga) terbang ke Enfield, London untuk membantu Peggy Hodgson (Frances O\'Connor), seorang ibu dengan empat orang anak. Rumah yang mereka tempati ternyata dihuni oleh mahluk halus jahat. Bahkan Salah satu dari roh jahat berhasil merasuki Janet Hodgson (Madison Wolfe), anak perempuan dari Peggy Lorraine sempat tidak percaya dengan cerita keluarga Peggy. Sampai ia mendapatkan sebuah pertanda mengenai eksistensi sang mahluk, yang ternyata ingin mencelakakan mereka. Ed dan Lorraine pun harus berjuang menghentikan sang mahluk untuk mencapai tujuannya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Hantu Biarawati di The Conjuring 2 Siap Difilmkan - 16 Jun \'16The Conjuring 2 Puncaki Box Office - 13 Jun \'16', '13:00 |15:40 |18:20 |21:00 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146364988872505_300x430.jpg', '', '', 'Horror,  Thriller', 'Rob Cowan, Peter Safran', 'RT17', 133, 'James Wan', 'Chad Hayes, Carey W. Hayes, James Wan, David Leslie Johnson', 'Warner Bros. PicturesCastsVera FarmigaPatrick WilsonFrances O\'connorMadison WolfeFranka PotenteSimon McburneySterling JerinsFrances O&#x0027;connorFrances O&amp;#x0027;connor	                    \r\n            \r\n            \r\n            \r\n                ', 1, '2016-06-20 17:16:56', '2016-06-20 17:16:56'),
(31, 31, 'NOW YOU SEE ME 2', 'Satu tahun setelah pertunjukan besar dalam menipu FBI, the Four Horsemen (Jesse Eisenberg, Dave Franco, Woody Harrelson, Lizzy Caplan) kembali muncul ke publik. Kali ini mereka harus berurusan dengan seorang pengusaha teknologi bernama Walter Mabry (Daniel Radcliffe) Mabry sukses menjebak The four Horsemen, dan dengan sengaja merekrut para pesulap untuk memaksa mereka membantunya menciptakan sebuah trik sulap yang sangat mustahil dan belum pernah dilihat oleh dunia. Tidak ada jalan lain bagi para Horsemen kecuali mengikuti perintah Mabry, sekaligus mencoba menguak siapa dalang dari segala masalah yang mereka alami kepada publik.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Film Hollywood yang Siap Tayang di Tahun 2016 - Part 2 - 05 Jan \'16', '13:30 |16:05 |18:40 |21:15 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146365053455585_300x430.jpg', '', '', 'Action,  Thriller', 'Bobby Cohen, Alex Kurtzman, Roberto Orci', 'RT13', 129, 'Jon M Chu', 'Ed Solomon, Pete Chiarelli', 'Entertainment One\r\n            Homepage', 1, '2016-06-20 17:16:57', '2016-06-20 17:16:57'),
(32, 32, 'FINDING DORY', 'Usai perjalanannya bersama ayah Nemo (Albert Brooks), kali ini giliran Dory (Ellen DeGeneres) yang akan pergi berpetualang untuk mencari keluarganya. Ikan biru pelupa itu tiba-tiba ingat akan kenangan masa kecilnya, yaitu tentang permata dari Teluk Morro, California.\n \nBersama Marlin, Nemo (Hayden Rolence) dan Hank (Ed O\'Neill) seekor gurita baik hati, Dory menjelajah lautan untuk kembali bertemu dengan keluarganya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Finding Dory Pecahkan Rekor Box Office - 20 Jun \'16Sekuel Finding Nemo Berjudul Finding Dory Siap Dirilis di Tahun 2015 - 03 Apr \'13', '12:45 |14:55 |17:05 |19:15 |21:25 ', '', 'http://www.21cineplex.com/data/gallery/pictures/14639882704563_300x430.jpg', '', '', 'Animation,  Adventure,  Comedy', 'Lindsey Collins', 'SU', 103, 'Andrew Stanton, Angus Maclane', 'Andrew Stanton', 'Walt Disney PicturesCastsEllen DegeneresAlbert BrooksDiane KeatonEugene LevyIdris ElbaAndrew StantonBill Hader	                    \r\n            \r\n            \r\n            \r\n                     SYNOPSIS \r\n                    \r\n                    \r\n    ', 1, '2016-06-20 17:17:00', '2016-06-20 17:17:00'),
(33, 33, 'FINDING DORY', 'Usai perjalanannya bersama ayah Nemo (Albert Brooks), kali ini giliran Dory (Ellen DeGeneres) yang akan pergi berpetualang untuk mencari keluarganya. Ikan biru pelupa itu tiba-tiba ingat akan kenangan masa kecilnya, yaitu tentang permata dari Teluk Morro, California.\n \nBersama Marlin, Nemo (Hayden Rolence) dan Hank (Ed O\'Neill) seekor gurita baik hati, Dory menjelajah lautan untuk kembali bertemu dengan keluarganya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Finding Dory Pecahkan Rekor Box Office - 20 Jun \'16Sekuel Finding Nemo Berjudul Finding Dory Siap Dirilis di Tahun 2015 - 03 Apr \'13', '12:45 |14:55 |17:05 |19:15 |21:25 ', '', 'http://www.21cineplex.com/data/gallery/pictures/14639882704563_300x430.jpg', '', '', 'Animation,  Adventure,  Comedy', 'Lindsey Collins', 'SU', 103, 'Andrew Stanton, Angus Maclane', 'Andrew Stanton', 'Walt Disney PicturesCastsEllen DegeneresAlbert BrooksDiane KeatonEugene LevyIdris ElbaAndrew StantonBill Hader	                    \r\n            \r\n            \r\n            \r\n                     SYNOPSIS \r\n                    \r\n                    \r\n    ', 1, '2016-06-20 17:17:08', '2016-06-20 17:17:08'),
(34, 33, 'THE CONJURING 2', 'Pasangan penyelidik supernatural Ed (Patrick Wilson) dan Lorraine Warren (Vera Famiga) terbang ke Enfield, London untuk membantu Peggy Hodgson (Frances O\'Connor), seorang ibu dengan empat orang anak. Rumah yang mereka tempati ternyata dihuni oleh mahluk halus jahat. Bahkan Salah satu dari roh jahat berhasil merasuki Janet Hodgson (Madison Wolfe), anak perempuan dari Peggy Lorraine sempat tidak percaya dengan cerita keluarga Peggy. Sampai ia mendapatkan sebuah pertanda mengenai eksistensi sang mahluk, yang ternyata ingin mencelakakan mereka. Ed dan Lorraine pun harus berjuang menghentikan sang mahluk untuk mencapai tujuannya.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Hantu Biarawati di The Conjuring 2 Siap Difilmkan - 16 Jun \'16The Conjuring 2 Puncaki Box Office - 13 Jun \'16', '13:00 |15:40 |18:20 |21:00 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146364988872505_300x430.jpg', '', '', 'Horror,  Thriller', 'Rob Cowan, Peter Safran', 'RT17', 133, 'James Wan', 'Chad Hayes, Carey W. Hayes, James Wan, David Leslie Johnson', 'Warner Bros. PicturesCastsVera FarmigaPatrick WilsonFrances O\'connorMadison WolfeFranka PotenteSimon McburneySterling JerinsFrances O&#x0027;connorFrances O&amp;#x0027;connor	                    \r\n            \r\n            \r\n            \r\n                ', 1, '2016-06-20 17:17:09', '2016-06-20 17:17:09'),
(35, 33, 'CENTRAL INTELLIGENCE', 'Dalam sebuah reuni SMA, dua orang sahabat Bob Stone (Dwayne Johnson), dan Calvin (Kevin Hart) kembali bertemu, namun dengan keadaan yang jauh berbeda. Sang bintang sekolah, Calvin, kini menjadi seorang akuntan. di sisi lain, Stone, yang dulunya adalah seorang kutu buku, ternyata bekerja sebagai agen CIA. Dan kali ini sang agen meminta bantuan teman lamanya tersebut untuk membantu dia menyelesaikan sebuah misi rahasia. setelah mengetahui hal tersebut, hidup Calvin pun berubah total. Ia dan Stone harus saling bahu membahu untuk memecahkan masalah ditengah baku tembak dan intrik rahasia, yang mungkin saja akan membuat mereka terbunuh.\r\nCOMPLETE ARCHIVE', '12:30 |14:45 |17:00 |19:15 |21:30 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146494097586077_300x430.jpg', '', '', 'Action,  Comedy', 'Scott Stuber, Paul Young, Michael Fottrell, Peter Principato', 'RT17', 106, 'Rawson Marshall Thurber', 'Ike Barinholtz, David Stassen, Rawson Marshall Thurber', 'Warner Bros. Pictures\r\n            Homepage', 1, '2016-06-20 17:17:10', '2016-06-20 17:17:10'),
(36, 33, 'NOW YOU SEE ME 2', 'Satu tahun setelah pertunjukan besar dalam menipu FBI, the Four Horsemen (Jesse Eisenberg, Dave Franco, Woody Harrelson, Lizzy Caplan) kembali muncul ke publik. Kali ini mereka harus berurusan dengan seorang pengusaha teknologi bernama Walter Mabry (Daniel Radcliffe) Mabry sukses menjebak The four Horsemen, dan dengan sengaja merekrut para pesulap untuk memaksa mereka membantunya menciptakan sebuah trik sulap yang sangat mustahil dan belum pernah dilihat oleh dunia. Tidak ada jalan lain bagi para Horsemen kecuali mengikuti perintah Mabry, sekaligus mencoba menguak siapa dalang dari segala masalah yang mereka alami kepada publik.\r\nCOMPLETE ARCHIVE  \r\n                        \r\n                    \r\n							Film Hollywood yang Siap Tayang di Tahun 2016 - Part 2 - 05 Jan \'16', '13:15 |15:50 |18:25 |21:00 ', '', 'http://www.21cineplex.com/data/gallery/pictures/146365053455585_300x430.jpg', '', '', 'Action,  Thriller', 'Bobby Cohen, Alex Kurtzman, Roberto Orci', 'RT13', 129, 'Jon M Chu', 'Ed Solomon, Pete Chiarelli', 'Entertainment One\r\n            Homepage', 1, '2016-06-20 17:17:12', '2016-06-20 17:17:12');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(10) UNSIGNED NOT NULL,
  `movie_id` int(11) NOT NULL,
  `cinema_id` int(11) NOT NULL,
  `booking_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `schedule` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `upcoming_movies`
--

CREATE TABLE `upcoming_movies` (
  `id` int(10) UNSIGNED NOT NULL,
  `cinema_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `showtimes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `booking_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `genre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `producer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL,
  `director` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `production` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `showing` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `upcoming_movies`
--

INSERT INTO `upcoming_movies` (`id`, `cinema_id`, `title`, `description`, `showtimes`, `image`, `image_url`, `youtube_url`, `booking_url`, `genre`, `producer`, `rate`, `duration`, `director`, `author`, `production`, `showing`, `created_at`, `updated_at`) VALUES
(2, 0, 'STAR TREK BEYOND (IMAX 3D)', 'Saat Scotty (Simon Pegg), Bones (Karl Urban) serta kru dari USS Enterprise sudah menempuh setengah perjalanan dari misi, tiba-tiba mereka diserang oleh alien dan terpaksa harus meninggalkan kapal. Terdampar di Starbase Earhart, sebuah planet tak berpenghuni, para kru menemukan diri mereka di dalam pertarungan dengan musuh baru yang kejam.', '', '', 'http://www.21cineplex.com/data/gallery/pictures/146613618940550_452x647.jpg', '', 'http://www.21cineplex.com/imax/star-trek-beyond-(imax-3d)-movie,4233,16STX3.htm', 'Action,  Adventure,  Sci-fi', 'Bryan Burk, J.j. Abrams, Roberto Orci', '', 0, 'Justin Lin', 'Doug Jung, Simon Pegg', 'Paramount Pictures', 0, '2016-06-27 10:53:25', '2016-06-27 10:53:25'),
(3, 0, 'STAR TREK BEYOND (IMAX 3D)', 'Saat Scotty (Simon Pegg), Bones (Karl Urban) serta kru dari USS Enterprise sudah menempuh setengah perjalanan dari misi, tiba-tiba mereka diserang oleh alien dan terpaksa harus meninggalkan kapal. Terdampar di Starbase Earhart, sebuah planet tak berpenghuni, para kru menemukan diri mereka di dalam pertarungan dengan musuh baru yang kejam.', '', '', 'http://www.21cineplex.com/data/gallery/pictures/146613618940550_452x647.jpg', '', 'http://www.21cineplex.com/imax/star-trek-beyond-(imax-3d)-movie,4233,16STX3.htm', 'Action,  Adventure,  Sci-fi', 'Bryan Burk, J.j. Abrams, Roberto Orci', '', 0, 'Justin Lin', 'Doug Jung, Simon Pegg', 'Paramount Pictures', 0, '2016-06-27 10:57:25', '2016-06-27 10:57:25'),
(4, 0, 'STAR TREK BEYOND (IMAX 3D)', 'Saat Scotty (Simon Pegg), Bones (Karl Urban) serta kru dari USS Enterprise sudah menempuh setengah perjalanan dari misi, tiba-tiba mereka diserang oleh alien dan terpaksa harus meninggalkan kapal. Terdampar di Starbase Earhart, sebuah planet tak berpenghuni, para kru menemukan diri mereka di dalam pertarungan dengan musuh baru yang kejam.', '', '', 'http://www.21cineplex.com/data/gallery/pictures/146613618940550_452x647.jpg', '', 'http://www.21cineplex.com/imax/star-trek-beyond-(imax-3d)-movie,4233,16STX3.htm', 'Action,  Adventure,  Sci-fi', 'Bryan Burk, J.j. Abrams, Roberto Orci', '', 0, 'Justin Lin', 'Doug Jung, Simon Pegg', 'Paramount Pictures', 0, '2016-06-27 11:02:55', '2016-06-27 11:02:55');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$K2V8hQNVF6lZ0hLgZTIFH.5W/cNdcqE2gV5ZgUyAwoNZ3D8FtcyWS', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cinemas`
--
ALTER TABLE `cinemas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upcoming_movies`
--
ALTER TABLE `upcoming_movies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cinemas`
--
ALTER TABLE `cinemas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;
--
-- AUTO_INCREMENT for table `movies`
--
ALTER TABLE `movies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `upcoming_movies`
--
ALTER TABLE `upcoming_movies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
