<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/','DashboardController@index');


// API ROUTES

Route::get('api/cinemas','CinemasController@index');
Route::get('api/movies/in_cinemas','MoviesController@in_cinemas');
Route::get('api/movies','MoviesController@index');
Route::get('api/movies/{movie_id}','MoviesController@show');



// ADMINISTRATOR ROUTES

// Begin Routes for movies
Route::get('admin/movies','AdminMoviesConrtoller@index');
Route::get('admin/movies/create','AdminMoviesConrtoller@create');
Route::post('admin/movies/store','AdminMoviesConrtoller@store');
Route::get('admin/movies/{id}/edit','AdminMoviesConrtoller@edit');
Route::post('admin/movies/{id}/update','AdminMoviesConrtoller@update');
Route::get('admin/movies/{id}/destroy','AdminMoviesConrtoller@destroy');

// Begin Routes for movies
Route::get('admin/cinemas','AdminCinemasController@index');
Route::get('admin/cinemas/create','AdminCinemasController@create');
Route::post('admin/cinemas/store','AdminCinemasController@store');
Route::get('admin/cinemas/{id}/edit','AdminCinemasController@edit');
Route::post('admin/cinemas/{id}/update','AdminCinemasController@update');
Route::get('admin/cinemas/{id}/destroy','AdminCinemasController@destroy');

// Begin Routes for Schedules
Route::get('schedules/{id}/destroy','SchedulesController@destroy');

Route::resource('settings','UsersController');
Route::post('settings/password','UsersController@update');
Route::post('settings/email','UsersController@update_email');

/*
Route::get('/', function(){
	$img = Image::make( public_path('laravel.png') )->greyscale();
	return $img->response('png');
});
*/
// Route::get('/', function () {
//     return view('welcome');
// });

/*
|--------------------------------------------------------------------------
| Routs for Authentication
|--------------------------------------------------------------------------
*/
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('scrape_cinemas','CronsController@scrape_cinemas');
Route::get('scrape_upcoming_movies','CronsController@scrape_upcoming_movies');
Route::get('scrape_movies','CronsController@scrape_movies');
Route::get('reset_crawling','CronsController@reset_crawling');

























