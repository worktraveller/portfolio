<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Goutte\Client;
use App\Cinemas;
use App\Movies;
class TestController extends Controller
{
    public function scrape_cinemas()
    {
        set_time_limit(0);
        try {
            $client = new Client();
            $crawler = $client->request('GET', 'http://www.21cineplex.com/theaters/daftar-bioskop-21-di-jakarta,3.htm');
            $crawler->filter('table.table-theater > tr.dark, table.table-theater > tr.light')->each(function ($node) {
                $name = ucwords( strtolower($node->filter('td')->first()->text()) );
                $link = $node->filter('td.title > a')->attr('href');
                if( $name && !Cinemas::where('name', $name)->first() ) {
                    $phone = $node->filter('td')->last()->text();
                    $cinema = new Cinemas;
                    $cinema->name = $name;
                    $cinema->link = $link;
                    $cinema->phone = $phone;
                    $cinema->crawable = 1;
                    $cinema->crawled = 0;
                    $cinema->save();
                }
            });
        } catch( Exception $e) {
            echo "An error occured. Please try again later.";
        }
    }

    public function scrape_movies()
    {
        // Get crawable cinemas
        $cinemas = Cinemas::where('crawled', 0)
            ->where('crawable', 1)
            ->limit(1)
            ->orderBy('last_crawled')
            ->get();

        if( $cinemas->count() ) {
            foreach ($cinemas as $cinema) {
                // Retrieve cinema information
                try {
                    //col-content
                    $client = new Client();
                    $crawler = $client->request('GET',  $cinema->link );
                    // $address = '';

                    // Udate Address
                    $address = $crawler->filter('div.col-m_462')->each(function ($node) {
                        return $address = $node->filter('div.col-content')->text();
                    });
                    
                    if( $address ) {
                        $cinema->address = trim(implode('', $address));
                    }

                    $crawler->filter('table.table-theater-det tr.light, table.table-theater-det tr.dark')->each(function ($node) use($cinema) {
                        if( $node ) {
                            $url = $node->filter('td a')->eq(1)->attr('href');
                            $title = strtoupper( $node->filter('td a')->eq(1)->text() );
                            $showtimes = $node->filter('td')->eq(1)->text();
                            $showtimes = trim($showtimes) ? $showtimes : '';

                            $movie = Movies::where('title', $title)->where('cinema_id', $cinema->id )->first();
                            if( $movie ) {
                                // Do not crawl anymore
                                echo 'existing';
                            } else {
                                // Crawl and get new data.
                                $client = new Client();
                                $movie_crawler = $client->request('GET',  $url );
                                $title = '';
                                $description = '';
                                $title = $movie_crawler->filter('h2.titlebig')->each(function ($node) use($title) {
                                    return $title = $node->text();
                                });

                                $description = $movie_crawler->filter('div.col-m_392 div.col-content')->each(function ($node) {
                                    return $node->text();
                                });

                                $img_src = $movie_crawler->filter('div.detmov-side img')->each(function ($node) {
                                    return $node->attr('src');
                                });

                                // Create and insert new movie
                                if( $title ) {
                                    $title = trim( implode('', $title) );
                                    if( $description ) {
                                        $description = trim( implode('', $description) );
                                    } else {
                                        $description = '';
                                    }

                                    if( $img_src ) {
                                        $img_src = $img_src[0];
                                    } else {
                                        $img_src = '';
                                    }

                                    $movie = new Movies;
                                    $movie->title = $title;
                                    $movie->description = $description;
                                    $movie->image_url = $img_src;
                                    $movie->cinema_id = $cinema->id;
                                    $cinema->last_crawled = time();
                                    $movie->showtimes = $showtimes;
                                    $movie->save();
                                }
                            }
                        }
                    });
                    
                } catch( Exception $e) {
                    echo "An error occured. Please try again later.<br />";
                }

                $cinema->crawled = 1;
                $cinema->save();

            }
        }
    }


    public function reset_crawling() {
        $cinemeas = Cinemas::where('crawable', 1);
        $cinemas->crawled = 0;
        $cinemas->save();
    }

}
