<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Schedules;
use Response;

class SchedulesController extends Controller
{
    public function __construct()
    {
        // check if user is logged in
        $this->middleware('auth');
        $this->params = [
            'error' => false,
            'message' => '',
            'type' => 'info', // danger, success, info, warning
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $schedule = Schedules::find($id);
        if( !$schedule ) {
            $this->params['error'] = true;
            $this->params['message'] = 'Schedule no longer exist.';
            $this->params['type'] = 'warning';
            return Response::json($this->params);
        }

        $schedule->delete();
        
        $this->params['message'] = 'Schedule removed successfully!';
        $this->params['type'] = 'success';
        return Response::json($this->params);
    }
}
