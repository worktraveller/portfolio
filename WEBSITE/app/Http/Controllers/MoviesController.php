<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;

use App\Movies;
use App\Cinemas;

class MoviesController extends Controller
{
    public function __construct()
    {
        $this->params = [
            'error'                 => false, 
            'status_code'           => 200, 
            'msg'                   => '',
            'form_errors'           => null,
            'results'               => [],
            'results_count'         => 0,
            // 'is_logged'             => true,
            // 'forced_login'          => false,
            // 'api_version'           => env('API_VERSION'),
        ];
    }
    

    function index()
    {
        // Get query strings
        $fields = self::explode_query_string('fields');
        $offset = (int)Input::get('offset', 0);
        $take = (int)Input::get('take', 20);
        $movie_title = Input::get('movie_title', null);
        $cinema_id = Input::get('cinema_id', null);
        $distinct_title = Input::get('distinct_title', 0);

        $movies = Movies::select();


        if( $cinema_id ) {  
            $movies->where('cinema_id', $cinema_id);
        } else {
            // Get all distinc movies according with title
            if( $distinct_title ) {
                if( $movie_title ) {
                      $movies = Movies::where('title', $movie_title);
                     $movies->where('title', $movie_title)
                          ->groupBy('title');
                 } else {
                    $movies->groupBy('title');
                }
            } else {
                $movies = Movies::where('title', $movie_title);
            }
        }




        // Selected fields only.
        if( $fields ) {
          $movies->select( $fields );
        }

        // OFFSET
        if( $offset ) {
          $movies->offset($offset);
        }

        // TODO: Allow limit of not more than 100 only.
        if( $take && $take !== -1 ) {
          $movies->take( $take );
        }


        if( $movie_title && $cinema_id ) {
            $this->params['results'] = $movies->first();
            $this->params['results_count'] = $movies->count();
            return response()->json( $this->params );
        }

        $this->params['results'] = $movies->get();
        $this->params['results_count'] = $movies->count();

        return response()->json( $this->params );
    }


    function show( $movie_id )
    {
        // Get query strings
        $fields = self::explode_query_string('fields');
        $offset = (int)Input::get('offset', 0);

        $movie = Movies::with('cinema')->find($movie_id);

        // Selected fields only.
        if( $fields ) {
          $movie->select( $fields );
        }

        $this->params['results'] = $movie;
        $this->params['results_count'] = 1;

        return response()->json( $this->params );
    }


    function in_cinemas()
    {
        // Get query strings
        $fields = self::explode_query_string('fields');
        $offset = (int)Input::get('offset', 0);
        $take = (int)Input::get('take', 50);

        $movies = Movies::select();
        $movies->groupBy('title');

        // Selected fields only.
        if( $fields ) {
          $movies->select( $fields );
        }

        // OFFSET
        if( $offset ) {
          $movies->offset($offset);
        }

        // TODO: Allow limit of not more than 100 only.
        if( $take && $take !== -1 ) {
          $movies->take( $take );
        }

        $this->params['results'] = $movies->get();
        $this->params['results_count'] = $movies->count();
        return response()->json( $this->params );
    }







    function explode_query_string( $query_string = [] )
    {
        if( !$query_string )
            return [];

        $query_string = Input::get($query_string , null);
        if( is_string($query_string) ) {
            $query_string = array_filter( explode(',', $query_string) );
            return empty( $query_string ) ? [] : $query_string;
        }

        return [];
    }

    // Get movies by Title of movies
    //http://watariyoo.dev/api/movies?movie_title=this%20is%20sample%20title&fields=name

    // Get all movies without a limit
    //http://watariyoo.dev/api/movies?take=-1

    

}
