<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function __construct()
    {
        // check if user is logged in
        $this->middleware('auth');
        $this->params = [
            'error' => false,
            'message' => '',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.settings',['user'=>Auth::user()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request)
    {

        $user = Auth::user();

        $args = array(
            //'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ); 

        if( $request->input('name') && $user->name != $request->input('name') ) {
            $args['name'] = 'required|max:255';
            $user->name = $request->input('name');
        }

        $this->validate($request, $args);
        
        $credentials = $request->only(
            'email', 'password', 'password_confirmation'
        );

        //$user->email = $request->input('email');
        $user->password = bcrypt($credentials['password']);
        $user->save();

        return redirect('settings')->with('status', 'Password successfully updated!');
    }

    public function update_email(Request $request)
    {

        $user = Auth::user();

        $args = array(
            'email' => 'required|email|max:255',
        ); 

        if( $request->input('name') && $user->name != $request->input('name') ) {
            $args['name'] = 'required|max:255';
            $user->name = $request->input('name');
        }

        $this->validate($request, $args);
        
        $credentials = $request->only('email');
        
        $user->email = $request->input('email');
        $user->save();

        return redirect('settings')->with('status', 'Details successfully updated!');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
