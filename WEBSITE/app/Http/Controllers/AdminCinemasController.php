<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Cinemas;
use Validator;

class AdminCinemasController extends Controller
{
    public function __construct()
    {
        // check if user is logged in
        $this->middleware('auth');
        $this->params = [
            'error' => false,
            'message' => '',
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $cinemas = DB::table('cinemas')->orderBy('name', 'asc')->paginate(10);
        return view('cinemas.index',['cinemas'=>$cinemas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('cinemas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:4|max:100',
            'address' => 'required|min:5',            
        ]);

        if ( $validator->fails() ) {
            return redirect('cinemas/create')
                ->withErrors($validator)
                ->withInput();
        }

        $cinema = new Cinemas;
        $cinema->name = $request->input('name');
        $cinema->address = $request->input('address');

        if( $request->has('phone') ){
            $cinema->phone = $request->input('phone');
        }

        if( $request->has('latitude') ){
            $cinema->latitude = $request->input('latitude');
        }

        if( $request->has('longitude') ){
            $cinema->longitude = $request->input('longitude');
        }

        $cinema->save();

        return redirect('cinemas')->with('status', 'Cinema successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $cinema = Cinemas::find($id);
        if( !$cinema ) {
            return redirect('cinemas')->with('error', 'Cinema not found!');
        }
        return view('cinemas.edit',['cinema'=>$cinema]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $cinema = Cinemas::find($id);
        if( !$cinema ) {
            return redirect('cinemas')->with('error', 'Cinema not found!');
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|min:4|max:100',
            'address' => 'required|min:5',            
        ]);

        if ( $validator->fails() ) {
            return redirect('cinemas/create')
                ->withErrors($validator)
                ->withInput();
        }

        $cinema->name = $request->input('name');
        $cinema->address = $request->input('address');

        if( $request->has('phone') ){
            $cinema->phone = $request->input('phone');
        }

        if( $request->has('latitude') ){
            $cinema->latitude = $request->input('latitude');
        }

        if( $request->has('longitude') ){
            $cinema->longitude = $request->input('longitude');
        }

        $cinema->save();

        return redirect('cinemas/'.$cinema->id.'/edit')->with('status', 'Cinema successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $cinema = Cinemas::find($id);
        if( !$cinema ) {
            return redirect('cinemas')->with('error', 'Cinema not found!');
        }

        $cinema->delete();
        
        return redirect('cinemas')->with('status', 'Cinema successfully removed!');
    }
}
