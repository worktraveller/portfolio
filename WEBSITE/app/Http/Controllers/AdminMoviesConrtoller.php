<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Movies;
use App\Cinemas;
use App\Schedules;
use Input;
use DB;
use Image;
use URL;
use Storage;
use Redirect;

class AdminMoviesConrtoller extends Controller
{
    public function __construct()
    {
        // check if user is logged in
        $this->middleware('auth');
        $this->params = [
            'cinemas' => Cinemas::all(),
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $movies = DB::table('movies')->paginate(15);
        return view('movies.index',['movies'=>$movies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('movies.create',$this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store()
    {        
        $validator = Validator::make(Input::all(), [
            'title'         => 'required|min:4|max:100',
            'description'   => 'required|min:5',
            'rate'          => 'required|min:2',
            'genre'         => 'required|min:5',
            'cinema_id'     => 'required|integer',
            'showing'       => 'required|integer',
            'showtimes'     => 'required|min:4|max:100',
        ]);

        if ( $validator->fails() ) {
            return Redirect::to('movies/create')->withInput()->withErrors($validator);
        }

        $movie = new Movies;
        $movie->title           = Input::get('title');
        $movie->description     = Input::get('description');
        $movie->youtube_url     = Input::get('youtube_url');
        $movie->rate            = Input::get('rate');
        $movie->genre           = Input::get('genre');
        $movie->cinema_id       = Input::get('cinema_id');
        $movie->showing       = Input::get('showing');
        $movie->showtimes       = Input::get('showtimes');
        

        if( Input::hasFile('image') ) {
            
            $destinationPath = public_path() . '/uploads/';
            $original_filename = Input::file('image')->getClientOriginalName();
            $extension = Input::file('image')->getClientOriginalExtension();
            $filename = md5( uniqid() . $original_filename );

            Input::file('image')->move( $destinationPath, $filename . "." . $extension );

            $filename = $filename.'.'.$extension;

            // THEN RESIZE IT
            $returnData = 'uploads/' . $filename;
            $image = Image::make(file_get_contents(URL::asset($returnData)));
            
            // 590x393
            $image->resize(590, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath .'large_'. $filename);
            
            // 128x128
            $image->resize(128, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath .'medium_'. $filename);
            
            // 237x200
            $image->resize(237, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath .'thumb_'. $filename);
            
            // 48x48
            // $image->resize(90, null, function ($constraint) {
            //     $constraint->aspectRatio();
            // })->crop(48,48)->save($destinationPath .'small_'. $filename);

            $movie->image = $filename;
            $movie->image_url = url('/').'/uploads/medium_'.$filename;
        
        }    
        
        $movie->save();
        
        return redirect('movies')->with('status', 'Movie successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $movie = Movies::find($id);
        
        if( !$movie ) {
            return redirect('movies')->with('error', 'Movie not found!');
        }

        $this->params['movie'] = $movie;
        $this->params['schedules'] = Schedules::where('movie_id','=',$id)->get();
        //dd($this->params['schedules']);
        return view('movies.edit',$this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //dd(Input::get('schedule'));
        $movie = Movies::find($id);
        if( !$movie ) {
            return redirect('movies')->with('error', 'Movie not found!');
        }

        $validator = Validator::make(Input::all(), [
            'title'         => 'required|min:4|max:100',
            'description'   => 'required|min:5',
            'rate'          => 'required|min:2',
            'genre'         => 'required|min:5',
            'cinema_id'     => 'required|integer',
            'showing'       => 'required|integer',
            'showtimes'     => 'required|min:4|max:100',
            'youtube_url'   => 'min:10|max:100|active_url',
            'booking_url'   => 'min:10|max:100|active_url',

            
        ]);

        if ( $validator->fails() ) {
            return redirect('movies/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput();
        }

        $movie->title           = Input::get('title');
        $movie->description     = Input::get('description');
        $movie->youtube_url     = Input::get('youtube_url');
        $movie->booking_url     = Input::get('booking_url');
        $movie->rate            = Input::get('rate');
        $movie->genre           = Input::get('genre');
        $movie->cinema_id       = Input::get('cinema_id');
        $movie->showing       = Input::get('showing');
        $movie->showtimes       = Input::get('showtimes');

        if( Input::hasFile('image') ) {
            
            $destinationPath = public_path() . '/uploads/';

            if( !empty($movie->image) ){
                // Delete multiple files
                File::delete(
                    $destinationPath.$movie->image, 
                    $destinationPath.'large_'.$movie->image,
                    $destinationPath.'medium_'.$movie->image,
                    $destinationPath.'thumb_'.$movie->image
                );
            }

            $original_filename = Input::file('image')->getClientOriginalName();
            $extension = Input::file('image')->getClientOriginalExtension();
            $filename = md5( uniqid() . $original_filename );

            Input::file('image')->move( $destinationPath, $filename . "." . $extension );

            $filename = $filename.'.'.$extension;

            // THEN RESIZE IT
            $returnData = 'uploads/' . $filename;
            $image = Image::make(file_get_contents(URL::asset($returnData)));
            
            // 590x393
            $image->resize(590, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath .'large_'. $filename);
            
            // 128x128
            $image->resize(128, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath .'medium_'. $filename);
            
            // 237x200
            $image->resize(237, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath .'thumb_'. $filename);
            
            // 48x48
            // $image->resize(90, null, function ($constraint) {
            //     $constraint->aspectRatio();
            // })->crop(48,48)->save($destinationPath .'small_'. $filename);

            $movie->image = $filename;
            $movie->image_url = url('/').'/uploads/medium_'.$filename;
        
        }    
        
        $movie->save();

        // if( Input::get('schedule') ) {
        //     foreach (Input::get('schedule') as $key => $value) {
        //         $schedule = new Schedules;
        //         $schedule->movie_id = $movie->id;
        //         $schedule->cinema_id = $value['cinema_id'];
        //         $schedule->schedule = $value['schedule'];
        //         $schedule->booking_url = $value['booking_url'];
        //         $schedule->save();
        //     }
        // }
        
        return redirect('movies/'.$id.'/edit')->with('status', 'Movie successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $movie = Movies::find($id);
        if( !$movie ) {
            return redirect('movies')->with('error', 'Movie not found!');
        }

        $movie->delete();
        
        return redirect('movies')->with('status', 'Movie successfully removed!');
    }
}
