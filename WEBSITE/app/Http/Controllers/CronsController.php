<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Goutte\Client;
use App\Cinemas;
use App\UpcomingMovies;

class CronsController extends Controller
{
    public function scrape_cinemas()
    {
        set_time_limit(0);
        try {
            $client = new Client();
            $crawler = $client->request('GET', 'http://www.21cineplex.com/theaters/daftar-bioskop-21-di-jakarta,3.htm');
            $crawler->filter('table.table-theater > tr.dark, table.table-theater > tr.light')->each(function ($node) {
                $name = ucwords( strtolower($node->filter('td')->first()->text()) );
                $link = $node->filter('td.title > a')->attr('href');
                if( $name && !Cinemas::where('name', $name)->first() ) {
                    $phone = $node->filter('td')->last()->text();
                    $cinema = new Cinemas;
                    $cinema->name = $name;
                    $cinema->link = $link;
                    $cinema->phone = $phone;
                    $cinema->crawable = 1;
                    $cinema->crawled = 0;
                    $cinema->save();
                }
            });
        } catch( Exception $e) {
            echo "An error occured. Please try again later.";
        }
    }

    public function scrape_movies()
    {
        set_time_limit(0);
        // Get crawable cinemas
        $cinemas = Cinemas::where('crawled', 0)
            ->where('crawable', 1)
            ->limit(1)
            ->orderBy('last_crawled')
            ->get();

        if( $cinemas->count() ) {
            foreach ($cinemas as $cinema) {
                // Retrieve cinema information
                try {
                    //col-content
                    $client = new Client();
                    $crawler = $client->request('GET',  $cinema->link );
                    // $address = '';

                    // Udate Address
                    $address = $crawler->filter('div.col-m_462')->each(function ($node) {
                        return $address = $node->filter('div.col-content')->text();
                    });
                    
                    if( $address ) {
                        $cinema->address = trim(implode('', $address));
                    }

                    $crawler->filter('table.table-theater-det tr.light, table.table-theater-det tr.dark')->each(function ($node) use($cinema) {
                        if( $node ) {
                            $url = $node->filter('td a')->eq(1)->attr('href');
                            $title = strtoupper( $node->filter('td a')->eq(1)->text() );
                            $showtimes = $node->filter('td')->eq(1)->text();
                            $showtimes = trim($showtimes) ? str_replace(' ', '|',trim($showtimes)) : '';
                            $showtimes = preg_replace('/\s+/', '', $showtimes);

                            $movie = Movies::where('title', $title)->where('cinema_id', $cinema->id )->first();
                            if( $movie ) {
                                // Do not crawl anymore
                                echo 'existing';
                            } else {
                                try {
                                    // Crawl and get new data.
                                    $client = new Client();
                                    $movie_crawler = $client->request('GET',  $url );
                                    $title = '';
                                    $description = '';
                                    $title = $movie_crawler->filter('h2.titlebig')->each(function ($node) use($title) {
                                        return $title = $node->text();
                                    });

                                    $description = $movie_crawler->filter('div.col-m_392 div.col-content')->each(function ($node) {
                                        return $node->text();
                                    });

                                    $img_src = $movie_crawler->filter('div.detmov-side img')->each(function ($node) {
                                        return $node->attr('src');
                                    });


                                    $rate = $movie_crawler->filter('div.movinfo span.rate')->attr('class');
                                    $rate = str_replace('rate', '', $rate);
                                    $rate = strtoupper(trim($rate));

                                    $duration = $movie_crawler->filter('div.movinfo span.duration h3')->text('class');
                                    //$duration = str_replace('duration', '', $duration);
                                    //$duration = strtoupper(trim($duration));


                                    $other_info = $movie_crawler->filter('div.col-m_392')->text();
                                    $other_info = explode(' : ', $other_info);
                                    //echo $other_info[0].'|';

                                    //var_dump($other_info[3]);

                                    $code_found = strpos($other_info[0], 'Kode M-TIX');

                                    if( $code_found === false ) {
                                        $genre = str_replace('Produser', '', $other_info[1]);
                                        $genre = trim($genre);

                                        $producer = str_replace('Sutradara', '', $other_info[2]);
                                        $producer = trim($producer);

                                        $director = str_replace('Penulis', '', $other_info[3]);
                                        $director = trim($director);

                                        $author = str_replace('Produksi', '', $other_info[4]);
                                        $author = trim($author);

                                        $production = trim($other_info[5]);

                                    } else {
                                        $genre = str_replace('Produser', '', $other_info[2]);
                                        $genre = trim($genre);

                                        $producer = str_replace('Sutradara', '', $other_info[3]);
                                        $producer = trim($producer);


                                        $director = str_replace('Penulis', '', $other_info[4]);
                                        $director = trim($director);

                                        $author = str_replace('Produksi', '', $other_info[5]);
                                        $author = trim($author);

                                        $production = trim($other_info[6]);
                                    }

                                    
                                    // Create and insert new movie
                                    if( $title ) {
                                        $title = trim( implode('', $title) );
                                        if( $description ) {
                                            $description = trim( implode('', $description) );
                                        } else {
                                            $description = '';
                                        }

                                        if( $img_src ) {
                                            $img_src = $img_src[0];
                                        } else {
                                            $img_src = '';
                                        }

                                        $movie = new Movies;
                                        $movie->title = $title;
                                        $movie->description = $description;
                                        $movie->image_url = $img_src;
                                        $movie->cinema_id = $cinema->id;
                                        $cinema->last_crawled = time();
                                        $movie->showtimes = $showtimes;
                                        $movie->rate = $rate;
                                        $movie->genre = $genre;
                                        $movie->producer = $producer;
                                        $movie->duration = $duration;
                                        $movie->director = $director;
                                        $movie->author = $author;
                                        $movie->production = $production;
                                        $movie->showing = 1;
                                        $movie->booking_url = $url;
                                        $movie->save();
                                    }
                                } catch( Exception $e ) {
                                    echo '<br />Error accessing: '. $url;
                                }
                            }
                        }
                    });
                    
                } catch( Exception $e) {
                    echo "An error occured. Please try again later.<br />";
                }

                $cinema->crawled = 1;
                $cinema->save();

            }
        }
    }

    public function scrape_upcoming_movies()
    {
        set_time_limit(0);

        $upcoming_movies_url = 'http://www.21cineplex.com/comingsoon';
        $client = new Client();
        $crawler = $client->request('GET',  $upcoming_movies_url );

        // Udate Address
        $content = $crawler->filter('div.col-m_462')->each(function ($node) {
            return $content = $node->filter('div.col-content')->text();
        });

        $pos_start = strpos($content[0], '[{"', 1);
        $pos_end = strpos($content[0], '"}]', 1);

        $end = ($pos_end - $pos_start) + 3;


        $movies = substr($content[0], $pos_start, $end);
        $movies = json_decode($movies);

        $i = 1;

        foreach ($movies as $mov) {
            // var_dump($movie);
            // echo $movie->mv_url.'<br />';

            // exit();
            
            $url = $mov->mv_url;
            // Crawl and get new data.
            $client = new Client();
            $movie_crawler = $client->request('GET',  $url );

            $title = '';
            $description = '';

            $title = $movie_crawler->filter('h2.titlebig')->each(function ($node) use($title) {
                return $title = $node->text();
            });

            if( empty($title ) || !isset($title[0]) ){ 
                 $title = $movie_crawler->filter('div.box-right div h3')->each(function ($node) use($title) {
                    return $title = $node->text();
                });
            }

            if( empty($title ) || !isset($title[0]) ){ echo "empty title for ".$url; continue; }

            $title = $title[0];

            $other_info = $movie_crawler->filter('div.col-m_392')->each(function ($node) {
                return $node->text();
            });

            if( empty($other_info ) || !isset($other_info[0]) ) { echo "empty other_info for ".$url; continue; }


            if( !isset($other_info[7]) ) { echo 'empty desc'.$url; continue; }

            $description = $other_info[7];

            //dd( $description );

            $image_url = $movie_crawler->filter('div.col-45 div.box-left img')->each(function ($node) {
                return $node->attr('src');
            });

            //if( empty($image_url ) || !isset($image_url[0]) ) continue;


            if( isset($image_url[0]) && $image_url[0] ) {
                $image_url = $image_url[0];
            }


            $other_info = explode(':', $other_info[0]);
            //dd( $a );


            // $rate = $movie_crawler->filter('div.movinfo span.rate')->attr('class');
            // $rate = str_replace('rate', '', $rate);
            // $rate = strtoupper(trim($rate));

            //$duration = $movie_crawler->filter('div.movinfo span.duration h3')->text('class');
            //$duration = str_replace('duration', '', $duration);
            //$duration = strtoupper(trim($duration));


            // $other_info = $movie_crawler->filter('div.col-m_392')->text();
            // $other_info = explode(' : ', $other_info);
            // //echo $other_info[0].'|';

            //var_dump($other_info[3]);

            //$code_found = strpos($other_info[0], 'Kode M-TIX');

            if( empty($other_info ) || count($other_info) <= 4 ) continue;

            $genre = str_replace('Produser', '', $other_info[1]);
            $genre = trim($genre);

            $producer = str_replace('Sutradara', '', $other_info[2]);
            $producer = trim($producer);


            $director = str_replace('Penulis', '', $other_info[3]);
            $director = trim($director);

            $author = str_replace('Produksi', '', $other_info[4]);
            $author = trim($author);

            $production = trim($other_info[5]);

            
            // Create and insert new movie
            if( $title ) {
                $arr = [
                    'title' => $title,
                    'description' => $description,
                    'image_url' => $image_url,
                    'producer' => $producer,
                    'director' => $director,
                    'author' => $author,
                    'production' => $production,
                    'booking_url' => $url,
                ];

                var_dump($arr);

                $movie = new UpcomingMovies;
                $movie->title = $title;
                $movie->description = $description;
                $movie->image_url = $image_url;
                //$cinema->last_crawled = time();
                //$movie->showtimes = $showtimes;
                //$movie->rate = $rate;
                $movie->genre = $genre;
                $movie->producer = $producer;
                //$movie->duration = $duration;
                $movie->director = $director;
                $movie->author = $author;
                $movie->production = $production;
                $movie->showing = 0;

                $movie->booking_url = $url;
                //dd( $movie );
                $movie->save();
            }

            if( $i++ > 3 )
                break;


            
        }

        //var_dump($movies);

    }

    public function reset_crawling() {
        Cinemas::where('crawable', 1)->update( ['crawled' => 0] );
    }

}
