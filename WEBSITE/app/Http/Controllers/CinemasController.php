<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;

use App\Cinemas;
use App\Movies;

class CinemasController extends Controller
{
    public function __construct()
    {
        $this->params = [
            'error'                 => false, 
            'status_code'           => 200, 
            'msg'                   => '',
            'form_errors'           => null,
            'results'               => [],
            'results_count'         => 0,
            // 'is_logged'             => true,
            // 'forced_login'          => false,
            // 'api_version'           => env('API_VERSION'),
        ];
    }
    

    function index()
    {
        // Get query strings
        $fields = self::explode_query_string('fields');
        $offset = (int)Input::get('offset', 0);
        $take = (int)Input::get('take', 20);
        $movie_title = Input::get('movie_title', null);

        $cinemas = Cinemas::select();
        // Get cinemas with movie title specified
        if( $movie_title ) {
            $cinema_ids = [];

            // Get all ids of movies with title specified
            $movies = Movies::where('title', $movie_title)->get(['cinema_id']);
            foreach ( $movies as $movie ) {
                $cinema_ids[] = $movie->cinema_id;
            }

            // Check if there's movies returned. 
            if( $cinema_ids ) {
                $cinemas->whereIn('id', $cinema_ids );
            }
        }

        // Selected fields only.
        if( $fields ) {
          $cinemas->select( $fields );
        }

        // OFFSET
        if( $offset ) {
          $cinemas->offset($offset);
        }

        // TODO: Allow limit of not more than 100 only.
        if( $take && $take !== -1 ) {
          $cinemas->take( $take );
        }

        $cinemas->orderBy('name');

        $this->params['results'] = $cinemas->get();
        $this->params['results_count'] = $cinemas->count();

        return response()->json( $this->params );
    }


    function explode_query_string( $query_string = [] )
    {
        if( !$query_string )
            return [];

        $query_string = Input::get($query_string , null);
        if( is_string($query_string) ) {
            $query_string = array_filter( explode(',', $query_string) );
            return empty( $query_string ) ? [] : $query_string;
        }

        return [];
    }

    // Get cinemas by Title of movies
    //http://watariyoo.dev/api/cinemas?movie_title=this%20is%20sample%20title&fields=name

    // Get all cinemas without a limit
    //http://watariyoo.dev/api/cinemas?take=-1

    

}
