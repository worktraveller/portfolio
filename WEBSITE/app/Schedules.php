<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cinemas;

class Schedules extends Model
{
    protected $primaryKey = 'id';
	protected $table = 'schedules';

	public function cinema(){
		return $this->hasOne('App\Cinemas', 'id');
	}

	public function cinema_name($cinema_id){
		$cinema = Cinemas::find($cinema_id);
		$name = ($cinema)? $cinema->name:'';
		return $name;
	}
}
