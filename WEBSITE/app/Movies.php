<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movies extends Model
{
    protected $primaryKey = 'id';
	protected $table = 'movies';

	public function schedules(){
		return $this->hasMany('App\Schedules', 'movie_id');
	}

	public function cinema(){
		return $this->hasOne('App\Cinemas','id', 'cinema_id');
	}
}
