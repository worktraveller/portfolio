<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cinemas extends Model
{
    protected $primaryKey = 'id';
	protected $table = 'cinemas';

	public function schedules(){
		return $this->hasMany('App\Schedules', 'cinema_id');
	}
}
