<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpcomingMovies extends Model
{
    protected $primaryKey = 'id';
	protected $table = 'upcoming_movies';

	// public function schedules(){
	// 	return $this->hasMany('App\Schedules', 'movie_id');
	// }
}
